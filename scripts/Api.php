<?php

namespace Scripts\EoneoPaySdk;

use EoneoPaySdk\Client;
use EoneoPaySdk\Entity\Customer;
use EoneoPaySdk\Entity\Customer\ReferenceNumber\Bpay;
use EoneoPaySdk\Entity\Customer\Subscription;
use EoneoPaySdk\Entity\Merchant;
use EoneoPaySdk\Entity\Merchant\Balance;
use EoneoPaySdk\Entity\Payment;
use EoneoPaySdk\Entity\PaymentSource;
use EoneoPaySdk\Entity\PaymentSource\CreditCard;
use EoneoPaySdk\Entity\PaymentSource\Info\CreditCard as CreditCardInfo;
use EoneoPaySdk\Entity\Plan;
use EoneoPaySdk\Entity\TokenisedCard;
use EoneoPaySdk\Resource\Customer as CustomerResource;
use EoneoPaySdk\Resource\Plan as PlanResource;
use EoneoPaySdk\Resource\Customer\Subscription as SubscriptionResource;
use EoneoPaySdk\Sdk\Collection;
use EoneoPaySdk\Sdk\Interfaces\Entities\UserInterface;

class Api
{
    /**
     * The api key
     *
     * @var string
     */
    private $apiKey;

    /**
     * The api base url
     *
     * @var string
     */
    private $baseUrl;

    /**
     * The eoneopay client
     *
     * @var \EoneoPaySdk\Client
     */
    private $client;

    /**
     * Create new api instance
     */
    public function __construct()
    {
        echo PHP_EOL . 'Environment (1. Live, 2. Staging, 3. Manual): ';
        switch (trim(fgets(STDIN))) {
            case '1':
                $this->baseUrl = 'https://api.eoneopay.com';
                break;

            case '2':
                $this->baseUrl = 'https://api-staging.eoneopay.com';
                break;

            default:
                $this->baseUrl = '';
                while (!filter_var($this->baseUrl, FILTER_VALIDATE_URL)) {
                    echo 'Enter API base url, e.g. http://api.eoneopay.com: ';
                    $this->baseUrl = trim(fgets(STDIN));
                }
                break;
        }

        echo PHP_EOL;

        while (!$this->apiKey) {
            echo 'Enter merchant api key: ';
            $this->apiKey = trim(fgets(STDIN));

            if ($this->apiKey) {
                $this->client = (new Client($this->apiKey))->setBaseUrl($this->baseUrl);

                // Test api key
                echo 'Authenticating... ';
                $balance = new Balance;
                $response = $this->client->get($balance);

                // Remove api key if it's not valid
                if (!$response->isSuccessful()) {
                    $this->apiKey = '';
                    echo 'failed' . PHP_EOL . PHP_EOL;
                }
            }
        }

        echo 'OK' . PHP_EOL . PHP_EOL;
    }

    /**
     * Create bpay crn
     *
     * @param \EoneoPaySdk\Entity\Customer $customer
     *
     * @return \EoneoPaySdk\Entity\Customer
     */
    public function createBpayCrn(Customer $customer) : Customer
    {
        // Keep looping until success
        while (1) {
            echo 'Allocating bpay CRN... ';

            $bpay = new Bpay;
            $bpay->forCustomer($customer);
            $response = $this->client->create($bpay);

            if (!$response->isSuccessful() || !$response->hasCustomer()) {
                echo 'failed' . PHP_EOL;
                continue;
            }

            echo 'OK' . PHP_EOL;
            return $response->getCustomer();
        }
    }

    /**
     * Create credit card
     *
     * @param \EoneoPaySdk\Sdk\Interfaces\Entities\UserInterface $user
     *
     * @return \EoneoPaySdk\Entity\PaymentSource\CreditCard
     */
    public function createCreditCard(UserInterface $user) : CreditCard
    {
        $tokenisedCard = new TokenisedCard;

        // Keep looping until successful
        while (1) {
            echo 'Credit card name: ';
            $tokenisedCard->setName(trim(fgets(STDIN)));
            echo 'Credit card number: ';
            $tokenisedCard->setNumber(trim(fgets(STDIN)));
            echo 'Credit card expiry month: ';
            $tokenisedCard->setExpiryMonth(trim(fgets(STDIN)));
            echo 'Credit card expiry year: ';
            $tokenisedCard->setExpiryYear(trim(fgets(STDIN)));
            echo 'Credit card CVC: ';
            $tokenisedCard->setCvc(trim(fgets(STDIN)));

            echo 'Validating... ';
            if (!$tokenisedCard->validate('post')) {
                echo 'failed' . PHP_EOL;
                $this->printValidationErrors($tokenisedCard->getValidationErrors());
                continue;
            }

            echo 'OK' . PHP_EOL;
            echo 'Tokenising credit card... ';

            $response = $this->client->create($tokenisedCard);

            if (!$response->isSuccessful() || !$response->hasTokenisedCard()) {
                echo 'failed' . PHP_EOL;
                continue;
            }

            echo 'OK' . PHP_EOL;
            $tokenisedCard = $response->getTokenisedCard();
            break;
        }

        // Loop adding card to customer
        while (1) {
            echo 'Adding credit card to user... ';

            $creditCard = new CreditCardInfo;
            $creditCard->setToken($tokenisedCard->getToken());

            // Attach
            if ($user instanceof Customer) {
                $creditCard->forCustomer($user);
            }

            if ($user instanceof Merchant) {
                $creditCard->forMerchant($user);
            }

            $response = $this->client->create($creditCard);

            if (!$response->isSuccessful() || !$response->hasCreditCard()) {
                echo 'failed' . PHP_EOL;
                continue;
            }

            echo 'OK' . PHP_EOL;
            return $response->getCreditCard();
        }
    }

    /**
     * Create credit card payment
     *
     * @param \EoneoPaySdk\Entity\PaymentSource\CreditCard $creditCard
     * @param \EoneoPaySdk\Sdk\Interfaces\Entities\UserInterface $user
     *
     * @return \EoneoPaySdk\Entity\Payment
     */
    public function createCreditCardPayment(CreditCard $creditCard, UserInterface $user) : Payment
    {
        $payment = new Payment;

        // Keep looping until successful
        while (1) {
            echo 'Payment amount, in cents: ';
            $payment->setAmount(trim(fgets(STDIN)));
            echo 'Payment reference: ';
            $payment->setReference(trim(fgets(STDIN)));
            echo 'Clearing account id: ';
            $payment->setClearingAccount(trim(fgets(STDIN)) ?: null);

            $payment->fromPaymentSource($creditCard);

            if ($user instanceof Customer) {
                $payment->forCustomer($user);
            }

            echo 'Validating... ';
            if (!$payment->validate('post')) {
                echo 'failed' . PHP_EOL;
                $this->printValidationErrors($payment->getValidationErrors());
                continue;
            }

            echo 'OK' . PHP_EOL;
            echo 'Creating payment... ';

            $response = $this->client->create($payment);

            if (!$response->isSuccessful() || !$response->hasPayment()) {
                echo 'failed' . PHP_EOL;
                continue;
            }

            echo 'OK' . PHP_EOL;
            return $response->getPayment();
            break;
        }
    }

    /**
     * Create customer
     *
     * @return \EoneoPaySdk\Entity\Customer
     */
    public function createCustomer() : Customer
    {
        $customer = new Customer;

        // Keep looping until success
        while (1) {
            echo 'Customer email: ';
            $customer->setEmail(trim(fgets(STDIN)));
            echo 'Customer first name: ';
            $customer->setFirstName(trim(fgets(STDIN)));
            echo 'Customer last name: ';
            $customer->setLastName(trim(fgets(STDIN)));

            echo 'Validating... ';
            if (!$customer->validate('post')) {
                echo 'failed' . PHP_EOL;
                $this->printValidationErrors($customer->getValidationErrors());
                continue;
            }

            echo 'OK' . PHP_EOL;
            echo 'Creating customer... ';

            /** @var \EoneoPaySdk\Sdk\Response $response */
            $response = $this->client->create($customer);
            if (!$response->isSuccessful() || !$response->hasCustomer()) {
                echo 'failed' . PHP_EOL;
                continue;
            }

            echo 'OK' . PHP_EOL;
            return $response->getCustomer();
        }
    }

    /**
     * Create plan
     *
     * @return \EoneoPaySdk\Entity\Plan
     */
    public function createPlan() : Plan
    {
        $plan = new Plan;

        // Keep looping until successful;
        while (1) {
            echo 'Plan name: ';
            $plan->setName(trim(fgets(STDIN)));
            echo 'Plan interval (day, week, month, year): ';
            $plan->setInterval(mb_strtolower(trim(fgets(STDIN))));
            echo 'Plan amount, in cents: ';
            $plan->setAmount(trim(fgets(STDIN)));

            echo 'Validating... ';
            if (!$plan->validate('post')) {
                echo 'failed' . PHP_EOL;
                $this->printValidationErrors($plan->getValidationErrors());
                continue;
            }

            echo 'OK' . PHP_EOL;
            echo 'Creating plan... ';

            $response = $this->client->create($plan);

            if (!$response->isSuccessful() || !$response->hasPlan()) {
                echo 'failed' . PHP_EOL;
                continue;
            }

            echo 'OK' . PHP_EOL;
            return $response->getPlan();
        }
    }

    /**
     * Create subscription
     *
     * @param \EoneoPaySdk\Entity\Customer $customer
     * @param \EoneoPaySdk\Entity\Plan $plan
     *
     * @return \EoneoPaySdk\Entity\Customer\Subscription
     */
    public function createSubscription(Customer $customer, Plan $plan) : Subscription
    {
        $subscription = new Subscription;

        // Keep looping until successful
        while (1) {
            echo 'Subscription start date (1 June 2018): ';
            $subscription->setStartDate(trim(fgets(STDIN)));

            $subscription->forCustomer($customer);
            $subscription->forPlan($plan);
            $subscription->fromReferenceNumber($customer->getReferenceNumbers()->first());

            echo 'Validating... ';
            if (!$subscription->validate('post')) {
                echo 'failed' . PHP_EOL;
                $this->printValidationErrors($subscription->getValidationErrors());
                continue;
            }

            echo 'OK' . PHP_EOL;
            echo 'Creating subscription... ';

            $response = $this->client->create($subscription);

            if (!$response->isSuccessful() || !$response->hasSubscription()) {
                echo 'failed' . PHP_EOL;
                continue;
            }

            echo 'OK' . PHP_EOL;
            return $response->getSubscription();
        }
    }

    /**
     * Find a customer
     *
     * @return \EoneoPaySdk\Entity\Customer
     */
    public function findCustomer() : Customer
    {
        $resource = new CustomerResource;

        // Keep looping until success
        while (1) {
            echo 'Enter first name of customer: ';
            $resource->whereFirstName(trim(fgets(STDIN)) ?: null);
            echo 'Enter last name of customer: ';
            $resource->whereLastName(trim(fgets(STDIN)) ?: null);

            echo 'Getting matching customers... ';

            $response = $this->client->list($resource);

            if (!$response->isSuccessful() || !$response->hasCustomers()) {
                echo 'failed' . PHP_EOL;
                continue;
            }

            echo 'OK' . PHP_EOL . PHP_EOL;

            /** @var \EoneoPaySdk\Entity\Customer $customer */
            foreach ($response->getCustomers() as $index => $customer) {
                echo sprintf(
                    '%d. %s %s %s',
                    ++$index,
                    $customer->getFirstName(),
                    $customer->getLastName(),
                    $customer->getEmail() ? '(' . $customer->getEmail() . ')' : ''
                ) . PHP_EOL;
            }

            // Loop until customer is selected
            while (1) {
                echo 'Select a customer: ';
                $selected = intval(trim(fgets(STDIN)));

                if ($customer = $response->getCustomers()->getNth($selected)) {
                    echo 'Getting customer... ';

                    $customer = new Customer(['id' => $customer->getId()]);
                    $response = $this->client->get($customer);

                    if (!$response->isSuccessful() || !$response->hasCustomer()) {
                        echo 'failed' . PHP_EOL;
                        continue;
                    }

                    echo 'OK' . PHP_EOL;
                    return $response->getCustomer();
                }
            }
        }
    }

    /**
     * Get credit card from a user
     *
     * @param \EoneoPaySdk\Sdk\Interfaces\Entities\UserInterface $user
     *
     * @return \EoneoPaySdk\Entity\PaymentSource
     */
    public function getCreditCard(UserInterface $user) : PaymentSource
    {
        /** @var \EoneoPaySdk\Entity\PaymentSource $creditCard */
        foreach ($user->getCreditCards() as $index => $creditCard) {
            echo sprintf(
                    '%d. %s (%s)',
                    ++$index,
                    $creditCard->getInfo()->getPan(),
                    $creditCard->getInfo()->getExpiryDate()
                ) . PHP_EOL;
        }

        // Loop until card is selected
        while (1) {
            echo 'Select a credit card: ';
            $selected = intval(trim(fgets(STDIN)));

            if ($creditCard = $user->getCreditCards()->getNth($selected)) {
                return $creditCard;
            }
        }
    }

    /**
     * Get all customers
     *
     * return \EoneoPay\Sdk\Collection
     */
    public function getCustomers() : Collection
    {
        echo 'Getting customers... ';

        $response = $this->client->list(new CustomerResource);

        if (!$response->isSuccessful() || !$response->hasCustomers()) {
            echo 'no customers found' . PHP_EOL;
            return new Collection([]);
        }

        echo 'OK' . PHP_EOL;
        return $response->getCustomers();
    }

    /**
     * Get merchant profile
     *
     * @return \EoneoPaySdk\Entity\Merchant
     */
    public function getMerchant() : Merchant
    {
        // Keep looping until success
        while (1) {
            echo 'Getting merchant profile... ';

            $merchant = new Merchant;
            $response = $this->client->get($merchant);

            if (!$response->isSuccessful() || !$response->hasMerchant()) {
                echo 'failed' . PHP_EOL;
                continue;
            }

            echo 'OK' . PHP_EOL;
            return $response->getMerchant();
        }
    }

    /**
     * Get all plans
     *
     * return \EoneoPay\Sdk\Collection
     */
    public function getPlans() : Collection
    {
        echo 'Getting plans... ';

        $response = $this->client->list(new PlanResource);

        if (!$response->isSuccessful() || !$response->hasPlans()) {
            echo 'no plans found' . PHP_EOL;
            return new Collection([]);
        }

        echo 'OK' . PHP_EOL;
        return $response->getPlans();
    }

    /**
     * Get all subscriptions
     *
     * return \EoneoPay\Sdk\Collection
     */
    public function getSubscriptions() : Collection
    {
        echo 'Getting subscriptions... ';

        $response = $this->client->list(new SubscriptionResource);

        if (!$response->isSuccessful() || !$response->hasSubscriptions()) {
            echo 'no subscriptions found' . PHP_EOL;
            return new Collection([]);
        }

        echo 'OK' . PHP_EOL;
        return $response->getSubscriptions();
    }

    /**
     * Print out validation errors in a nice way
     *
     * @param array $errors The errors to print
     *
     * @return void
     */
    public function printValidationErrors(array $errors) : void
    {
        foreach ($errors as $error) {
            if (is_array($error)) {
                $this->printValidationErrors($error);
                continue;
            }

            echo ' - ' . $error . PHP_EOL;
        }
    }

    /**
     * Write data to a csv file
     *
     * @param \EoneoPaySdk\Sdk\Collection $collection
     * @param string $filename
     *
     * @return void
     */
    public function writeToCsv(Collection $collection, string $filename) : void
    {
        // Create export directory
        if (!file_exists('export/')) {
            mkdir('export', 0777, true);
        }

        // Create file
        $file = fopen('export/' . basename($filename), 'w');
        fputcsv($file, array_keys($collection->first()->toArray()));

        // Add each line
        foreach ($collection->toArray() as $entity) {
            // Encode arrays
            foreach ($entity as &$item) {
                if (is_array($item)) {
                    $item = json_encode($item);
                }
            }

            fputcsv($file, $entity);
        }

        fclose($file);
    }
}
