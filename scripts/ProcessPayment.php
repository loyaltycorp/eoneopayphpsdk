<?php

require __DIR__ . '/../vendor/autoload.php';

$api = new \Scripts\EoneoPaySdk\Api;

// Determine payment owner
echo 'Process payment from merchant account? (Y/n): ';
switch (mb_strtolower(trim(fgets(STDIN)))) {
    case 'n':
        echo 'Process payment for existing customer? (y/N): ';
        $selection = mb_strtolower(trim(fgets(STDIN)));
        echo PHP_EOL;
        switch ($selection) {
            case 'y':
                $user = $api->findCustomer();
                break;

            default:
                $user = $api->createCustomer();
                break;
        }
        break;

    default:
        echo PHP_EOL;
        $user = $api->getMerchant();
        break;
}

echo PHP_EOL;

// Get credit card
if ($user->hasCreditCards()) {
    echo 'Use existing credit card? (y/N): ';
    $selection = mb_strtolower(trim(fgets(STDIN)));
    echo PHP_EOL;
    switch ($selection) {
        case 'y':
            $creditCard = $api->getCreditCard($user);
            break;
    }
}

if (!isset($creditCard)) {
    $creditCard = $api->createCreditCard($user);
}

echo PHP_EOL;

// Create payment
$payment = $api->createCreditCardPayment($creditCard, $user);

echo PHP_EOL . 'Transaction ID: ' . $payment->getTxnId() . PHP_EOL . PHP_EOL;
