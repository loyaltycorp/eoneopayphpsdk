<?php

require __DIR__ . '/../vendor/autoload.php';

$api = new \Scripts\EoneoPaySdk\Api;

// Create customer and bpay crn
$customer = $api->createCustomer();
$customer = $api->createBpayCrn($customer);
echo PHP_EOL;

// Create plan and subscription
$plan = $api->createPlan();
echo PHP_EOL;
$subscription = $api->createSubscription($customer, $plan);
echo PHP_EOL;

echo 'BPAY CRN IS ' . $customer->getReferenceNumbers()->first()->getReferenceNumber() . PHP_EOL . PHP_EOL;
