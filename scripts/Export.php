<?php

require __DIR__ . "/../vendor/autoload.php";

$api = new \Scripts\EoneoPaySdk\Api;

// Export customers
$customers = $api->getCustomers();
if ($customers->count()) {
    $api->writeToCsv($customers, 'customers.csv');
}

// Export plans
$plans = $api->getPlans();
if ($plans->count()) {
    $api->writeToCsv($plans, 'plans.csv');
}

// Export subscriptions
$subscriptions = $api->getSubscriptions();
if ($subscriptions->count()) {
    $api->writeToCsv($subscriptions, 'subscriptions.csv');
}

echo PHP_EOL . 'Done' . PHP_EOL . PHP_EOL;
