<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk;

use EoneoPaySdk\Client;
use Illuminate\Support\Str;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use LoyaltyCorp\SdkBlueprint\Sdk\Entity;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\UndefinedMethodException;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\SeriesInterface;

/**
 * @coversNothing
 * @SuppressWarnings(PHPMD.NumberOfChildren) All entity test cases extend this class
 */
class EntityTestCase extends TestCase
{
    /**
     * Assert a value is what is expected
     *
     * @param \LoyaltyCorp\SdkBlueprint\Sdk\Entity $entity The entity to test against
     * @param array $expected The expected values
     *
     * @return void
     */
    private function assertValues(Entity $entity, array $expected) : void
    {
        // Process each expected value
        foreach ($expected as $key => $value) {
            $attribute = Str::studly($key);
            $method = sprintf('get%s', $attribute);

            // If value is an array, test as array
            if (is_array($value)) {
                $this->assertInstanceOf(SeriesInterface::class, $entity->$method());
                $this->assertSame($value, $entity->$method()->toArray());
                continue;
            }

            // Run different tests depending on the value type
            if ($value === false) {
                $method = sprintf('is%s', $attribute);
                $this->assertFalse($entity->$method());
                continue;
            }

            if ($value === null) {
                $this->assertNull($entity->$method());
                continue;
            }

            if ($value === true) {
                $method = sprintf('is%s', $attribute);
                $this->assertTrue($entity->$method());
                continue;
            }

            $this->assertSame($value, $entity->$method());
        }
    }

    /**
     * Test mutators on certain attributes
     *
     * @param string $entity The entity to test
     * @param array $original The original data to use
     * @param array $expected The expected data after mutation
     *
     * @return void
     */
    public function runAttributeMutatorTests(string $entity, array $original, array $expected) : void
    {
        // Create entity with data
        $entity = new $entity($original);

        // Test attributes have been set and values are as expected
        $this->assertValues($entity, $expected);
    }

    /**
     * Test a resource
     *
     * @param string $action The action to perform on the client
     * @param \LoyaltyCorp\SdkBlueprint\Sdk\Entity $entity The entity to test against
     * @param string $code The response code
     * @param string $message The message from the api
     * @param array $response The response data
     * @param int $status The status code for the response
     *
     * @return void
     */
    protected function runEntityTests(
        string $action,
        Entity $entity,
        string $code,
        string $message,
        array $response = [],
        int $status = 200
    ) : void {
        // Set up response data
        $responseData = array_merge(compact('code', 'message'), $response);

        // Set up client
        $client = new Client($this->apiKey, $this->createClient([
            $this->createResponse($responseData, $status),
        ]));

        // Test response
        $this->runResponseTests($entity, $client->$action($entity), $responseData, $status, $action);
    }

    /**
     * Test the mutable attributes
     *
     * @param string $entity The entity to test
     * @param array $original The original data to use
     * @param array $updated The updated data to use
     * @param string $immutable The immutable attribute to check
     *
     * @return void
     */
    public function runMutableAttributeTests(string $entity, array $original, array $updated, string $immutable) : void
    {
        // Create entity
        /** @var \LoyaltyCorp\SdkBlueprint\Sdk\Entity $entity */
        $entity = new $entity($original);

        // Check values are set from constructor
        $this->assertValues($entity, $original);

        // Update mutable values
        foreach ($updated as $key => $value) {
            $attribute = Str::studly($key);

            // Call setter
            $method = sprintf('set%s', $attribute);
            $entity->$method($value);
        }

        // Check values have been updated
        $this->assertValues($entity, $updated);

        // Test immutable method
        $attribute = Str::studly($immutable);

        try {
            // Setter will throw exception
            $method = sprintf('set%s', $attribute);
            $entity->$method(1);
        } catch (UndefinedMethodException $exception) {
            $this->assertSame('Undefined method: set' . $attribute, $exception->getMessage());
        }

        // Make sure setter didn't work, this will be checked even if exception isn't thrown
        $method = sprintf('get%s', $attribute);
        $this->assertSame($original[$immutable], $entity->$method());
    }

    /**
     * Test a raw response
     *
     * @param string $action The action to perform on the client
     * @param \LoyaltyCorp\SdkBlueprint\Sdk\Entity $entity The entity to test against
     * @param string $responseData The response data
     * @param int $status The status code
     *
     * @return void
     */
    protected function runRawTests(
        string $action,
        Entity $entity,
        $responseData,
        $status = 200
    ) : void {

        // Set up client
        $client = new Client($this->apiKey, $this->createClient([
            new GuzzleResponse(200, ['Content-Type' => 'image/png'], $responseData),
        ]));

        /** @var \EoneoPaySdk\Sdk\Response $response */
        $response = $client->$action($entity);

        // Test response
        $this->assertTrue($response->isSuccessful());

        // Set up full expected response
        $expected = array_merge(['raw' => $responseData], ['status_code' => $status, 'successful' => $status === 200]);

        // Test response matches exactly
        $this->assertSame($expected, $response->toArray());
    }
}
