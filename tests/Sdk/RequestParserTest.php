<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Sdk;

use Tests\EoneoPaySdk\ClientTest;
use Tests\EoneoPaySdk\Stubs\EntityStub;
use Tests\EoneoPaySdk\Stubs\RequestParserStub;

/**
 * @covers \EoneoPaySdk\Sdk\RequestParser
 *
 * Extends client test because client test sets all the parser values
 */
class RequestParserTest extends ClientTest
{
    /**
     * Test getting parameters using different methods
     *
     * @return void
     */
    public function testGetParameters() : void
    {
        // Create client and endpoint
        $client = new RequestParserStub($this->apiKey);
        $endpoint = (new EntityStub)->setId(123);
        $client->setEndpoint($endpoint);

        // Set expected response
        $responses = [
            // Auth is api key / merchant id
            'nobody' => ['auth' => [$this->apiKey, '']],
            'body' => ['auth' => [$this->apiKey, ''], 'json' => $endpoint->toArray()],
        ];

        // Iterate through methods and check type
        $methods = [
            'delete' => $responses['nobody'],
            'get' => $responses['nobody'],
            'put' => $responses['body'],
            'post' => $responses['body'],
        ];

        // Test each method
        foreach ($methods as $method => $response) {
            $client->setMethodTo($method);
            $this->assertSame($response, $client->getParameters());
        }
    }
}
