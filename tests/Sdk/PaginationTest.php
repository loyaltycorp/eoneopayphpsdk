<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Sdk;

use EoneoPaySdk\Sdk\Resource;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Sdk\Pagination
 */
class PaginationTest extends ResourceTestCase
{
    /**
     * Test setting pagination values on a resource
     *
     * @return void
     */
    public function testPagination() : void
    {
        $resource = new Resource;

        // Perform assertions on defaults
        $this->assertSame(100, $resource->getLimit());
        $this->assertSame('asc', $resource->getOrder());
        $this->assertSame(1, $resource->getPage());

        // Change values and retest
        $resource->limit(50)->page(5)->order('desc');
        $this->assertSame(50, $resource->getLimit());
        $this->assertSame('desc', $resource->getOrder());
        $this->assertSame(5, $resource->getPage());

        // Test values are remapped to default if invalid
        $resource->limit(-1)->page(-1)->order('forward');
        $this->assertSame(100, $resource->getLimit());
        $this->assertSame('asc', $resource->getOrder());
        $this->assertSame(1, $resource->getPage());
    }
}
