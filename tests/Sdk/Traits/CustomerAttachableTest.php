<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Sdk\Traits;

use EoneoPaySdk\Entity\Customer;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use Tests\EoneoPaySdk\Stubs\EntityStub;
use Tests\EoneoPaySdk\TestCase;

/**
 * @covers \EoneoPaySdk\Sdk\Traits\CustomerAttachable
 */
class CustomerAttachableTest extends TestCase
{
    /**
     * Test attaching entities together
     *
     * @return void
     */
    public function testAttachToCustomer() : void
    {
        // Create customer with correct data, entity stub and attach together
        $customer = new Customer($this->customerData);
        $entity = new EntityStub(['id' => 'ent_0000000000000000']);
        $entity->forCustomer($customer);

        // Ensure the resource has been set correctly
        $expected = sprintf('customers/%s/test/%s', $customer->getId(), $entity->getId());
        $this->assertSame($expected, $entity->getEndpointFromMethod('delete'));
        $this->assertSame($customer->getId(), $entity->getCustomerId());
    }

    /**
     * Test invalid attachment
     *
     * @return void
     */
    public function testInvalidAttachTo() : void
    {
        // Create customer with no data and entity stub
        $customer = new Customer;
        $entity = new EntityStub(['id' => 'ent_0000000000000000']);

        // Attachment should throw exception
        $this->expectException(InvalidEntityException::class);
        $entity->forCustomer($customer);
    }
}
