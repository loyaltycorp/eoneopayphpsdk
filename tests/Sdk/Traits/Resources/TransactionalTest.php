<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Sdk\Traits\Resources;

use Tests\EoneoPaySdk\Stubs\ResourceStub;
use Tests\EoneoPaySdk\TestCase;

/**
 * @covers \EoneoPaySdk\Sdk\Traits\Resources\Transactional
 */
class TransactionalTest extends TestCase
{
    /**
     * Test methods made available by the transactional trait
     *
     * @return void
     */
    public function testDates(): void
    {
        // Create repository
        $resource = new ResourceStub;

        // Assert defaults
        $this->assertNull($resource->getFromDate());
        $this->assertNull($resource->getToDate());
        $this->assertFalse($resource->hasFromDate());
        $this->assertFalse($resource->hasToDate());

        // Add where for created and updated at
        $resource->fromDate('1 July 2017');
        $resource->toDate('31 August 2017');

        // Assert changes
        $this->assertSame('2017-07-01', $resource->getFromDate());
        $this->assertSame('2017-08-31', $resource->getToDate());
        $this->assertTrue($resource->hasFromDate());
        $this->assertTrue($resource->hasToDate());
    }
}
