<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Sdk\Traits;

use EoneoPaySdk\Entity\Merchant;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use Tests\EoneoPaySdk\Stubs\EntityStub;
use Tests\EoneoPaySdk\TestCase;

/**
 * @covers \EoneoPaySdk\Sdk\Traits\MerchantAttachable
 */
class MerchantAttachableTest extends TestCase
{
    /**
     * Test attaching entities together
     *
     * @return void
     */
    public function testAttachToMerchant() : void
    {
        // Create merchant with correct data, entity stub and attach together
        $merchant = new Merchant($this->merchantData);
        $entity = new EntityStub(['id' => 'ent_0000000000000000']);
        $entity->forMerchant($merchant);

        // Ensure the resource has been set correctly
        $expected = sprintf('merchants/%s/test/%s', $merchant->getId(), $entity->getId());
        $this->assertSame($expected, $entity->getEndpointFromMethod('delete'));
        $this->assertSame($merchant->getId(), $entity->getMerchantId());
    }

    /**
     * Test invalid attachment
     *
     * @return void
     */
    public function testInvalidAttachTo() : void
    {
        // Create merchant with no data and entity stub
        $merchant = new Merchant;
        $entity = new EntityStub(['id' => 'ent_0000000000000000']);

        // Attachment should throw exception
        $this->expectException(InvalidEntityException::class);
        $entity->forMerchant($merchant);
    }
}
