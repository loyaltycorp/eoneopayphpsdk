<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Sdk\Traits\Entities;

use EoneoPaySdk\Entity\Charge;
use EoneoPaySdk\Entity\Customer\Subscription;
use EoneoPaySdk\Entity\Ewallet;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use Tests\EoneoPaySdk\Stubs\Repositories\InvalidAllocatableStub;
use Tests\EoneoPaySdk\TestCase;

/**
 * @covers \EoneoPaySdk\Sdk\Traits\Entities\Allocatable
 */
class AllocatableTest extends TestCase
{
    /**
     * Test getting the allocation type
     *
     * @return void
     */
    public function testGetType() : void
    {
        // Test each allocatable type
        $types = [
            'charge' => new Charge,
            'ewallet' => new Ewallet,
            'subscription' => new Subscription,
        ];

        foreach ($types as $type => $entity) {
            $this->assertSame($type, $entity->getType());
        }

        // Test invalid allocation type
        $invalid = new InvalidAllocatableStub;

        $this->expectException(InvalidEntityException::class);
        $invalid->getType();
    }
}
