<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Sdk;

use EoneoPaySdk\Sdk\Resource;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Sdk\Resource
 */
class ResourceTest extends ResourceTestCase
{
    /**
     * Test setting includes on a resource
     *
     * @return void
     */
    public function testIncludes() : void
    {
        $resource = new Resource;

        // Perform assertions on defaults
        $this->assertSame([], $resource->toArray());

        // Set some includes
        $resource->includes(['customers', 'merchants']);
        $this->assertSame(['includes' => 'customers,merchants'], $resource->toArray());

        // Test clearing includes
        $resource->includes();
        $this->assertSame([], $resource->getIncludes()->toArray());
    }

    /**
     * Test pagination passthroughs
     *
     * @return void
     */
    public function testPaginationPassthroughs() : void
    {
        $resource = new Resource;

        // Change values to something other than default and test
        $resource->limit(50)->page(5)->order('desc');
        $this->assertSame(50, $resource->getLimit());
        $this->assertSame('desc', $resource->getOrder());
        $this->assertSame(5, $resource->getPage());
    }
}
