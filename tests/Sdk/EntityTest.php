<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Sdk;

use EoneoPaySdk\Sdk\Entity;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Sdk\Entity
 */
class EntityTest extends EntityTestCase
{
    /**
     * Test creating an entity sets id and timestamp attributes
     *
     * @return void
     */
    public function testCreateEntity() : void
    {
        $entity = new Entity;

        // These shouldn't throw an exception because they exists
        $this->assertNull($entity->getCreatedAt());
        $this->assertNull($entity->getId());
        $this->assertNull($entity->getUpdatedAt());
    }
}
