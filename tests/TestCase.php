<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use LoyaltyCorp\SdkBlueprint\Client;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\EndpointInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Response;
use PHPUnit\Framework\TestCase as BaseTestCase;
use Psr\Http\Message\ResponseInterface;

/**
 * @coversNothing
 * @SuppressWarnings(PHPMD.NumberOfChildren) Everything extends this class
 */
class TestCase extends BaseTestCase
{
    /**
     * Test API key
     *
     * @var string
     */
    protected $apiKey = 'sk_test_ff98e683331743a8e4e68e7f';

    /**
     * The EoneoPay API client
     *
     * @var \EoneoPaySdk\Client
     */
    protected $client;

    /**
     * Example customer data
     *
     * @var array
     */
    protected $customerData = [
        'bank_accounts' => [],
        'created_at' => '2017-01-01 00:00:00',
        'credit_cards' => [],
        'email' => 'user@test.com',
        'ewallet_id' => '0XXXX00',
        'external_customer_id' => 'ext_0000000000',
        'first_name' => 'Test',
        'id' => 'cus_000000000000000000000000',
        'last_name' => 'User',
        'merchant_id' => 'mer_0000000000000000',
        'reference_numbers' => [],
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Example merchant data
     *
     * @var array
     */
    protected $merchantData = [
        'abn' => '00000000000',
        'address_city' => 'Melbourne',
        'address_line_1' => 'Apartment 10',
        'address_line_2' => '123 Test Street',
        'address_postal_code' => '3000',
        'address_state' => 'VIC',
        'available_balance' => [
            'AUD' => [
                'amount' => 0,
                'payments' => [],
                'transfers' => [],
            ],
        ],
        'automatic_transfer' => true,
        'balance' => [
            'AUD' => [
                'amount' => 0,
                'payments' => [],
                'transfers' => [],
            ],
        ],
        'bank_accounts' => [],
        'business_name' => 'Merchant Pty Ltd',
        'business_phone' => '0000000000',
        'business_website' => 'www.test.com',
        'clearing_account' => 0,
        'created_at' => '2017-01-01 00:00:00',
        'credit_cards' => [],
        'email' => 'merchant@test.com',
        'first_name' => 'First',
        'id' => 'mer_0000000000000000',
        'last_name' => 'Last',
        'title' => 'Mr',
        'transfer_day' => 1,
        'transfer_frequency' => 'daily',
        'trust_clearing_account' => true,
        'updated_at' => '2017-01-01 00:00:00',
        'version' => 1,
    ];

    /**
     * Set up API client
     */
    public function setUp()
    {
        parent::setUp();

        // Create EoneoPay client
        $this->client = new Client(new Response);
        //)->setBaseUrl('http://eoneopay.dev');
    }

    /**
     * Create client with mock responses
     *
     * @param array $responses An array of responses to send
     *
     * @return \GuzzleHttp\Client The stacked mock client
     */
    protected function createClient(array $responses) : GuzzleClient
    {
        // Set up mock responses
        $mock = new MockHandler($responses);
        $handler = HandlerStack::create($mock);
        return new GuzzleClient(['handler' => $handler]);
    }

    /**
     * Create a mock json response
     *
     * @param array $data The data to add to the response
     * @param int $statusCode The status code to send with the response
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function createResponse(array $data, int $statusCode = 200) : ResponseInterface
    {
        return new GuzzleResponse($statusCode, ['Content-Type' => 'application/json'], json_encode($data));
    }

    /**
     * Recursively key sort an array
     *
     * @param array $array
     *
     * @return bool
     */
    protected function recursiveKeySort(array &$array) : bool
    {
        foreach ($array as &$value) {
            // Recurse into arrays
            if (is_array($value)) {
                $this->recursiveKeySort($value);
            }
        }

        return ksort($array);
    }

    /**
     * Test a response
     *
     * @param \LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\EndpointInterface $requestable The entity or resource
     * @param \LoyaltyCorp\SdkBlueprint\Sdk\Response $response The response recieved
     * @param array $responseData Original response data
     * @param int $status The status code to use
     * @param string $action The action used
     *
     * @return void
     */
    public function runResponseTests(
        EndpointInterface $requestable,
        Response $response,
        array $responseData,
        int $status = 200,
        string $action = null
    ) : void {
        // Test response result
        switch ($status) {
            case 200:
                $this->assertTrue($response->isSuccessful());
                break;

            default:
                $this->assertFalse($response->isSuccessful());
                break;
        }

        // Set up full expected response
        $expected = array_merge($responseData, ['status_code' => $status, 'successful' => $status === 200]);

        // Get method from action
        switch ($action) {
            case 'create':
                $method = 'post';
                break;

            case 'delete':
            case 'get':
                $method = $action;
                break;

            case 'update':
                $method = 'put';
                break;

            default:
                $method = 'get';
                break;
        }

        // Remap fields if required
        $expected = $requestable->remapResponse($expected, $method);

        // Errors should include the request and response
        if (!$response->isSuccessful()) {
            $response = $this->runUnsuccessfulResponseTests($response);
        }

        // Test response matches exactly
        $actual = $response->toArray();
        $this->recursiveKeySort($expected);
        $this->recursiveKeySort($actual);
        $this->assertSame($expected, $actual);
    }

    /**
     * Run tests on an unsuccessful response
     *
     * @param \LoyaltyCorp\SdkBlueprint\Sdk\Response $response The response to test
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Response The updated response to continue testing on
     */
    private function runUnsuccessfulResponseTests(Response $response) : Response
    {
        $responseArray = $response->toArray();

        // Test exception is caught correctly
        $this->assertArrayHasKey('request', $responseArray);
        $this->assertInstanceOf(GuzzleRequest::class, $responseArray['request']);
        $this->assertArrayHasKey('response', $responseArray);
        $this->assertInstanceOf(GuzzleResponse::class, $responseArray['response']);

        // Test values without request/response
        unset($responseArray['request'], $responseArray['response']);

        // Recreate response without request/response
        return new Response($responseArray);
    }
}
