<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Stubs;

use EoneoPaySdk\Client as BaseClient;
use LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\EndpointInterface;

/**
 * @coversNothing
 */
class RequestParserStub extends BaseClient
{
    /**
     * Make get parameters on the request parset public
     *
     * @return array
     */
    public function getParameters() : array
    {
        return $this->getRequestParser()->getParameters();
    }

    /**
     * Set endpoint
     *
     * @param \LoyaltyCorp\SdkBlueprint\Sdk\Interfaces\EndpointInterface $endpoint
     *
     * @return void
     */
    public function setEndpoint(EndpointInterface $endpoint) : void
    {
        $this->getRequestParser()->setEndpoint($endpoint);
    }

    /**
     * Set method to X
     *
     * @param string $method
     *
     * @return void
     */
    public function setMethodTo(string $method) : void
    {
        $this->getRequestParser()->setMethod($method);
    }
}
