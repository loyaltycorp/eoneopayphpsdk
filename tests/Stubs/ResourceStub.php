<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Stubs;

use EoneoPaySdk\Sdk\Traits\Resources\Transactional;
use EoneoPaySdk\Sdk\Resource as BaseResource;

/**
 * @coversNothing
 */
class ResourceStub extends BaseResource
{
    use Transactional;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'endDate',
        'startDate',
        'value',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => ':resource/:resource_id',
    ];

    /**
     * Mappings for returned values
     *
     * @var array
     */
    protected $mappings = [
        'get' => [
            'test' => 'value',
        ],
        'test' => 'value',
    ];

    /**
     * Define the intial resource for this entity, used where the same data is used
     * for multiple endpoints, e.g. the customer/merchant crossover
     *
     * @var string
     */
    protected $resource = 'test';

    /**
     * Define the resource id for this entity, used where an entity is a child of
     * a resource, usually a customer or merchant
     */
    protected $resourceId = 1;
}
