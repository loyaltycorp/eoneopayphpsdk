<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Stubs\Repositories;

use EoneoPaySdk\Sdk\Interfaces\Entities\AllocationInterface;
use EoneoPaySdk\Sdk\Traits\Entities\Allocatable;
use LoyaltyCorp\SdkBlueprint\Sdk\Entity;

/**
 * @coversNothing
 */
class InvalidAllocatableStub extends Entity implements AllocationInterface
{
    use Allocatable;
}
