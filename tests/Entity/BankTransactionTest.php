<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\BankTransaction;
use EoneoPaySdk\Entity\Ewallet;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\BankTransaction
 */
class BankTransactionTest extends EntityTestCase
{
    /**
     * Test allocating a bank transaction
     *
     * @return void
     */
    public function testAllocateBankTransaction() : void
    {
        // Set up ewallet, entity and expected response
        $ewallet = new Ewallet(['id' => 'XXXX0X1']);
        $entity = new BankTransaction(['id' => '0000000000-1']);

        // Allocate
        $entity->allocateTo($ewallet);

        $this->runEntityTests('create', $entity, '0', 'Bank transaction has been allocated.');
    }

    /**
     * Test invalid attachment
     *
     * @return void
     */
    public function testInvalidAllocationAttachment() : void
    {
        // Create ewallet with no data and entity stub
        $ewallet = new Ewallet;
        $entity = new BankTransaction(['id' => '0000000000-1']);

        // Attachment should throw exception
        $this->expectException(InvalidEntityException::class);
        $entity->allocateTo($ewallet);
    }
}
