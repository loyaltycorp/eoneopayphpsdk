<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\Webhook;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Webhook
 */
class WebhookTest extends EntityTestCase
{
    /**
     * Example webhook data
     *
     * @var array
     */
    private $data = [
        'active' => true,
        'created_at' => '2017-01-01 00:00:00',
        'event_name' => 'PaymentCreated',
        'id' => 'webhook_000000000000000000000000',
        'merchant_id' => 'mer_0000000000000000',
        'name' => 'Test',
        'target_url' => 'http://localhost',
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Test mutators on certain attributes
     *
     * @return void
     */
    public function testAttributeMutators() : void
    {
        // Set original and expected values
        $original = ['active' => '1'];
        $expected = ['active' => true];

        $this->runAttributeMutatorTests(Webhook::class, $original, $expected);
    }

    /**
     * Test creating a webhook
     *
     * @return void
     */
    public function testCreateWebhook() : void
    {
        // Set up entity and response
        $entity = new Webhook($this->data);
        $response = ['webhook' => $this->data];

        $this->runEntityTests('create', $entity, '0', 'Webhook created.', $response);
    }

    /**
     * Test deleting a webhook
     *
     * @return void
     */
    public function testDeleteWebhook() : void
    {
        // Create entity and expected response
        $entity = new Webhook($this->data);

        $this->runEntityTests('delete', $entity, '0', 'Webhook deleted.');
    }

    /**
     * Test getting a webhook by id
     *
     * @return void
     */
    public function testGetWebhookById() : void
    {
        // Create entity and expected response
        $entity = new Webhook($this->data);
        $response = ['webhook' => $this->data];

        $this->runEntityTests('get', $entity, '0', 'Webhook found.', $response);
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set updated data
        $updated = [
            'active' => false,
            'event_name' => 'PaymentSucceeded',
            'name' => 'Updated',
            'target_url' => 'https://localhost',
        ];

        $this->runMutableAttributeTests(Webhook::class, $this->data, $updated, 'merchant_id');
    }

    /**
     * Test updating a webhook
     *
     * @return void
     */
    public function testUpdateWebhook() : void
    {
        // Set up entity and response data
        $entity = new Webhook(array_merge($this->data, ['name' => 'Updated']));
        $response = ['webhook' => array_merge($this->data, $entity->toArray())];

        $this->runEntityTests('update', $entity, '0', 'Webhook updated.', $response);
    }
}
