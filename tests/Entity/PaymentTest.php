<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\Customer;
use EoneoPaySdk\Entity\PaymentSource;
use EoneoPaySdk\Entity\PaymentSource\BankAccount;
use EoneoPaySdk\Entity\PaymentSource\CreditCard;
use EoneoPaySdk\Entity\Ewallet;
use EoneoPaySdk\Entity\Payment;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Payment
 * @SuppressWarnings(PHPMD.TooManyPublicMethods) All test methods must be public
 */
class PaymentTest extends EntityTestCase
{
    /**
     * Example bank account payment data
     *
     * @var array
     */
    private $bankAccountData = [
        'amount' => 100,
        'bank_txn_id' => 1,
        'created_at' => '2017-01-01 00:00:00',
        'currency' => 'AUD',
        'customer_id' => 'cus_000000000000000000000000',
        'external_customer_id' => 'cus_000000000000000000000000',
        'external_txn_id' => 'ddr_000000000000000000000000',
        'fee_amount' => 20,
        'fee_schedule' => [
            'merchant_id' => '*',
            'payment_type' => 'direct_debit',
            'transaction_fee' => '20',
        ],
        'merchant_id' => 'mer_0000000000000000',
        'merchant_transfer_status' => 0,
        'meta' => 'Meta',
        'reference' => 'Test',
        'reference_number' => 'XXXX0X0',
        'statement_descriptor' => '',
        'status' => 0,
        'token' => 'src_000000000000000000000000',
        'txn_date' => '2017-01-01 00:00:00',
        'txn_id' => 'txn_000000000000000000000000',
        'type' => 'direct_debit',
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Example credit card payment data
     *
     * @var array
     */
    private $creditCardData = [
        'amount' => 100,
        'approved' =>  true,
        'authID' => '000001',
        'bank_txn_id' => 1,
        'card_type' => 'Visa',
        'card_type_id' => 6,
        'created_at' => '2017-01-01 00:00:00',
        'currency' => 'AUD',
        'customer_id' => 'cus_000000000000000000000000',
        'external_txn_id' => '000009',
        'fee_amount' => 21,
        'fee_schedule' => [
            'card_rates' => [
                'MasterCard' => 1.5,
                'AMEX' => 2.9,
                'Visa' => 1.5,
            ],
            'merchant_id' => '*',
            'payment_type' => 'cc_payment',
            'transaction_fee' => '20',
        ],
        'merchant_id' => 'mer_0000000000000000',
        'merchant_transfer_status' => 0,
        'meta' => 'Meta',
        'reference' => 'Test',
        'responseCode' => '00',
        'responseText' => 'Approved',
        'settlementDate' => '20170101',
        'statement_descriptor' => '',
        'status' => 1,
        'token' => 'src_000000000000000000000000',
        'txnID' => '000009',
        'txnType' => '0',
        'txn_date' => '2017-01-01 00:00:00',
        'txn_id' => 'txn_000000000000000000000000',
        'type' => 'cc_payment',
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Get credit card data which matches the SDK
     *
     * @return array
     */
    private function getFilteredResponse() : array
    {
        $data = $this->creditCardData;

        // Remove stuff which will be removed by the sdk
        unset(
            $data['approved'],
            $data['authID'],
            $data['responseCode'],
            $data['responseText'],
            $data['settlementDate'],
            $data['txnID'],
            $data['txnType']
        );

        return $data;
    }

    /**
     * Test creating a payment from a bank account
     *
     * @return void
     */
    public function testCreateBankAccountPayment() : void
    {
        // Set up bankAccount, entity and expected response
        $bankAccount = new BankAccount(['id' => $this->bankAccountData['token']]);
        $entity = new Payment([
            'amount' => $this->bankAccountData['amount'],
            'reference' => $this->bankAccountData['reference'],
        ]);
        $response = ['payment' => $this->bankAccountData];

        // Mmake payment from credit card
        $entity->fromPaymentSource($bankAccount);

        $this->runEntityTests('create', $entity, '0', 'Payment created.', $response);
    }

    /**
     * Test creating a payment from a credit card
     *
     * @return void
     */
    public function testCreateCreditCardPayment() : void
    {
        $data = [
            'credit_card' => [
                'cardDescription' => 'Visa',
                'cardType' => '6',
                'expiryDate' => '01/00',
                'expiry_month' => '01',
                'expiry_year' => '2000',
                'id' => 'src_000000000000000000000000',
                'pan' => '444433...111',
                'token' => 'tok_000000000000000000000000'
            ],
            'payment' => $this->creditCardData,
        ];

        // Set up credit card, entity and expected response
        $creditCard = new CreditCard(['id' => $this->creditCardData['token']]);
        $entity = new Payment([
            'amount' => $this->creditCardData['amount'],
            'reference' => $this->creditCardData['reference'],
        ]);
        $response = $data;

        // Remove stuff which will be removed by the sdk
        unset(
            $response['credit_card'],
            $response['payment']['approved'],
            $response['payment']['authID'],
            $response['payment']['responseCode'],
            $response['payment']['responseText'],
            $response['payment']['settlementDate'],
            $response['payment']['txnID'],
            $response['payment']['txnType']
        );

        // Make payment from credit card
        $entity->fromPaymentSource($creditCard);

        $this->runEntityTests('create', $entity, '0', 'Payment request completed', $response);
    }

    /**
     * Test creating a payment with an allocation plan
     *
     * @return void
     */
    public function testCreatePaymentWithAllocationPlan() : void
    {
        // Set up wallet, bank account, and entity
        $wallet = new Ewallet(['id' => 'XXXX0X1']);
        $bankAccount = new BankAccount(['id' => $this->bankAccountData['token']]);
        $entity = new Payment([
            'amount' => $this->bankAccountData['amount'],
            'reference' => $this->bankAccountData['reference'],
        ]);

        // Set expected response
        $response = ['payment' => array_merge($this->bankAccountData, ['allocation_plan' => [
            $wallet->getId() => [
                'amount' => 39,
                'type' => $wallet->getType(),
            ],
            'value' => [
                'amount' => 40,
                'type' => 'sundry',
            ]
        ]])];
        unset($response['reference_number']);

        // Allocate 50% of the funds to wallet, the rest to sundry
        $entity
            ->addAllocation($wallet, 39)
            ->addAllocation('value', 40)
            ->fromPaymentSource($bankAccount);

        $this->runEntityTests('create', $entity, '0', 'Payment created.', $response);
    }

    /**
     * Test deleting/refunding a payment
     *
     * @return void
     */
    public function testDeletePayment() : void
    {
        // Set up entity and expected response
        $entity = new Payment(['txn_id' => $this->bankAccountData['txn_id']]);
        $response = ['payment' => array_merge($this->bankAccountData, [
            'original_txn_id' => $this->bankAccountData['txn_id'],
            'txn_id' => 'txn_000000000000000000000001',
        ])];
        unset($response['reference_number']);

        $this->runEntityTests('delete', $entity, '0', 'Payment refunded.', $response);
    }

    /**
     * Test getting a payment by id
     *
     * @return void
     */
    public function testGetPaymentById() : void
    {
        // Set up entity and expected response
        $entity = new Payment(['txn_id' => $this->bankAccountData['txn_id']]);
        $response = ['payment' => $this->bankAccountData];

        $this->runEntityTests('get', $entity, '0', 'Payment found.', $response);
    }

    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters() : void
    {
        $entity = new Payment;

        $this->assertInstanceOf(Repository::class, $entity->getAllocationPlan());
        $this->assertInstanceOf(Repository::class, $entity->getFeeSchedule());
    }

    /**
     * Test attempting to use an invalid payment source
     *
     * @return void
     */
    public function testInvalidPaymentSourceAttachment() : void
    {
        $paymentSource = new PaymentSource;
        $entity = new Payment;

        $this->expectException(InvalidEntityException::class);
        $entity->fromPaymentSource($paymentSource);
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set updated data
        $updated = [
            'amount' => 101,
            'clearing_account' => 5,
            'meta' => 'Updated',
            'reference' => 'Updated',
        ];

        $this->runMutableAttributeTests(Payment::class, $this->bankAccountData, $updated, 'token');
    }

    /**
     * Test refunding a credit card payment
     *
     * @return void
     */
    public function testRefundCreditCardPayment() : void
    {
        // Set up entity and expected response
        $entity = new Payment([
            'amount' => $this->creditCardData['amount'],
            'reference' => $this->creditCardData['reference'],
            'txn_id' => $this->creditCardData['txn_id'],
            'type' => $this->creditCardData['type']
        ]);
        $response = ['payment' => array_merge($this->getFilteredResponse(), [
            'original_txn_id' => $this->creditCardData['txn_id'],
            'txn_id' => 'txn_000000000000000000000001',
        ])];

        $this->runEntityTests('delete', $entity, '0', 'Refund request completed', $response);
    }

    /**
     * Test making a payment on behalf of a customer
     *
     * @return void
     */
    public function testPaymentForCustomer() : void
    {
        $customer = new Customer(['id' => $this->bankAccountData['customer_id']]);
        $entity = new Payment;
        $this->assertNull($entity->getCustomerId());

        // Test valid customer
        $entity->forCustomer($customer);
        $this->assertSame($this->bankAccountData['customer_id'], $entity->getCustomerId());

        // Test an invalid customer
        $this->expectException(InvalidEntityException::class);
        $customer = new Customer;
        $entity->forCustomer($customer);
    }

    /**
     * Test updating a payment
     *
     * @return void
     */
    public function testUpdatePaymentAllocations() : void
    {
        // Set up entity and expected response
        $entity = new Payment(['txn_id' => $this->bankAccountData['txn_id']]);
        $response = ['payment' => array_merge(
            $this->bankAccountData,
            ['allocation_plan' => ['Test' => ['amount' => 100, 'type' => 'sundry']]]
        )];

        // Add allocation plan to entity
        $entity->addAllocation('Test', 100);

        $this->runEntityTests('update', $entity, '0', 'Payment has successfully been allocated', $response);
    }

    /**
     * Test amount validation flag
     *
     * @return void
     */
    public function testTestValidateAmountFlag() : void
    {
        $entity = new Payment;
        $this->assertNull($entity->getTestValidateAmount());

        // Test truthy value
        $entity->setTestValidateAmount(true);
        $this->assertNull($entity->getTestValidateAmount());

        // Test falsey amount
        $entity->setTestValidateAmount(0);
        $this->assertSame('false', $entity->getTestValidateAmount());

        // Reset and test string false
        $entity->setTestValidateAmount(true)->setTestValidateAmount('false');
        $this->assertSame('false', $entity->getTestValidateAmount());
    }
}
