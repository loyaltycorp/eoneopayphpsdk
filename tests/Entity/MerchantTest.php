<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\Merchant;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Merchant
 */
class MerchantTest extends EntityTestCase
{
    /**
     * Test creating a new merchant
     *
     * @return void
     */
    public function testCreateMerchant() : void
    {
        // Set up entity and response data
        $entity = new Merchant([
            'abn' => $this->merchantData['abn'],
            'business_name' => $this->merchantData['business_name'],
            'business_phone' => $this->merchantData['business_phone'],
            'business_website' => $this->merchantData['business_website'],
            'email' => $this->merchantData['email'],
            'first_name' => $this->merchantData['first_name'],
            'last_name' => $this->merchantData['last_name'],
            'title' => 'Mr',
        ]);
        $response = ['merchant' => $this->merchantData];

        $this->runEntityTests('create', $entity, '0', 'Merchant created.', $response);
    }

    /**
     * Test deleting a merchant
     *
     * @return void
     */
    public function testDeleteMerchant() : void
    {
        // Set up entity
        $entity = new Merchant(['id' => $this->merchantData['id']]);

        $this->runEntityTests('delete', $entity, '0', 'Merchant deleted.');
    }

    /**
     * Test getting a the current merchant
     *
     * @return void
     */
    public function testGetCurrentMerchant() : void
    {
        // Set up entity and response data
        $entity = new Merchant;
        $response = ['merchant' => $this->merchantData];

        $this->runEntityTests('get', $entity, '0', 'Merchant found.', $response);
    }

    /**
     * Test getting a merchant which doesn't exist
     *
     * @return void
     */
    public function testGetDeletedMerchant() : void
    {
        // Set up entity
        $entity = new Merchant(['id' => $this->merchantData['id']]);

        $this->runEntityTests('get', $entity, '6100', 'Merchant not found', [], 404);
    }

    /**
     * Test getting a merchant by id
     *
     * @return void
     */
    public function testGetMerchantById() : void
    {
        // Set up entity and response data
        $entity = new Merchant(['id' => $this->merchantData['id']]);
        $response = ['merchant' => $this->merchantData];

        $this->runEntityTests('get', $entity, '0', 'Merchant found.', $response);
    }

    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters() : void
    {
        $entity = new Merchant;

        $this->assertInstanceOf(Repository::class, $entity->getAvailableBalance());
        $this->assertInstanceOf(Repository::class, $entity->getBalance());
        $this->assertInstanceOf(Collection::class, $entity->getBankAccounts());
        $this->assertInstanceOf(Collection::class, $entity->getCreditCards());
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set original and updated data
        $updated = [
            'abn' => '00000000001',
            'address_city' => 'Sydney',
            'address_line_1' => 'Apartment 17',
            'address_line_2' => '124 Test Street',
            'address_postal_code' => '2000',
            'address_state' => 'NSW',
            'business_name' => 'Merchant Ltd Pty',
            'business_phone' => '0000000000',
            'business_website' => 'www.test.com.au',
            'email' => 'merchant@test.com.au',
            'first_name' => 'Test',
            'last_name' => 'Mutable',
            'statement_descriptor' => 'Test',
            'title' => 'Mrs',
        ];

        $this->runMutableAttributeTests(Merchant::class, $this->merchantData, $updated, 'clearing_account');
    }

    /**
     * Test updating a customer
     *
     * @return void
     */
    public function testUpdateMerchant() : void
    {
        // Set up entity and response data
        $entity = new Merchant(array_merge($this->merchantData, ['first_name' => 'Updated']));
        $response = ['merchant' => array_merge($this->merchantData, $entity->toArray())];

        $this->runEntityTests('update', $entity, '0', 'Merchant updated.', $response);
    }
}
