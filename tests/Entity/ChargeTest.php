<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\Charge;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Charge
 */
class ChargeTest extends EntityTestCase
{
    /**
     * Bank account test data
     *
     * @var array
     */
    private $data = [
        'amount' => '100',
        'created_at' => '2017-01-01 00:00:00',
        'id' => 'chg_000000000000000000000000',
        'merchant_id' => 'mer_0000000000000000',
        'name' => 'Test',
        'outstanding_amount' => '100',
        'precendence' => 'first',
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Test creating a charge
     *
     * @return void
     */
    public function testCreateCharge() : void
    {
        // Create entity and expected response
        $entity = new Charge($this->data);
        $response = [
            'charge' => $this->data,
        ];

        $this->runEntityTests('create', $entity, '0', 'Charge created.', $response);
    }

    /**
     * Test creating a charge
     *
     * @return void
     */
    public function testDeleteCharge() : void
    {
        // Create entity
        $entity = new Charge($this->data);

        $this->runEntityTests('delete', $entity, '0', 'Charge deleted.');
    }

    /**
     * Test getting a charge by id
     *
     * @return void
     */
    public function testGetChargeById() : void
    {
        // Create entity and expected response
        $entity = new Charge($this->data);
        $response = [
            'charge' => array_merge($this->data, ['statement' => ['payments' => []]]),
        ];

        $this->runEntityTests('get', $entity, '0', 'Charge found.', $response);
    }

    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters() : void
    {
        $entity = new Charge;

        $this->assertInstanceOf(Repository::class, $entity->getStatement());
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set original and updated data
        $updated = [
            'amount' => 101,
            'name' => 'Updated',
        ];

        $this->runMutableAttributeTests(Charge::class, $this->data, $updated, 'outstanding_amount');
    }

    /**
     * Test updating a charge
     *
     * @return void
     */
    public function testUpdateCharge() : void
    {
        // Set up entity and response data
        $entity = new Charge(array_merge($this->data, ['name' => 'Updated']));
        $response = ['charge' => array_merge($this->data, $entity->toArray())];

        $this->runEntityTests('update', $entity, '0', 'Charge updated.', $response);
    }
}
