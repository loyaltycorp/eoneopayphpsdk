<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\ClearingAccount;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\ClearingAccount
 */
class ClearingAccountTest extends EntityTestCase
{
    /**
     * Test getting a clearing account by id
     *
     * @return void
     */
    public function testGetClearingAccountById() : void
    {
        // Clearing account data
        $data = [
            'account' => '000000000',
            'bsb' => '000-000',
            'direct_entry_credit_description' => 'TEST',
            'direct_entry_credit_id' => '000000',
            'direct_entry_credit_name' => 'TEST',
            'direct_entry_debit_description' => 'TEST',
            'direct_entry_debit_id' => '000001',
            'direct_entry_debit_name' => 'TEST',
            'id' => 1,
            'merchant_id' => 'XXX0000',
            'merchant_password' => 'XXx0xxX0',
            'name' => 'TEST',
            'trust' => '0',
        ];

        // Create entity and expected response
        $entity = new ClearingAccount($data);
        $response = ['clearing_account' => $data];

        $this->runEntityTests('get', $entity, '0', 'Clearing account found.', $response);
    }
}
