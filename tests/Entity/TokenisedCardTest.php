<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\TokenisedCard;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\TokenisedCard
 */
class TokenisedCardTest extends EntityTestCase
{
    /**
     * Test mutators on certain attributes
     *
     * @return void
     */
    public function testAttributeMutators() : void
    {
        // Set original and expected values
        $original = ['expiry_month' => '1', 'expiry_year' => '15', 'number' => '4444 3333 2222 1111'];
        $expected = [
            'expiry_month' => str_pad($original['expiry_month'], 2, '0', STR_PAD_LEFT),
            'expiry_year' => (string)(2000 + (int)$original['expiry_year']),
            'number' => preg_replace('/[\D]+/', '', $original['number']),
        ];

        $this->runAttributeMutatorTests(TokenisedCard::class, $original, $expected);
    }

    /**
     * Test creating a credit card token
     *
     * @return void
     */
    public function testCreateCreditCardToken() : void
    {
        // Set up entity and response data
        $entity = new TokenisedCard([
            'cvc' => '012',
            'expiry_month' => '01',
            'expiry_year' => '2050',
            'name' => 'Test',
            'number' => '4444333322221111',
        ]);
        $response = [
            'credit_card' => [
                'cardDescription' => 'Visa',
                'cardType' => '6',
                'expiryDate' => '01/50',
                'expiry_month' => $entity->getExpiryMonth(),
                'expiry_year' => $entity->getExpiryYear(),
                'id' => 'src_000000000000000000000000',
                'name' => $entity->getName(),
                'pan' => sprintf(
                    '%s...%s',
                    mb_substr($entity->getNumber(), 0, 6),
                    mb_substr($entity->getNumber(), -3)
                ),
                'token' => 'tok_00000000000000000000000000000000',
            ]
        ];

        $this->runEntityTests('create', $entity, '2000', 'Token generation completed', $response);
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set original and updated data
        $original = [
            'cvc' => '012',
            'expiry_month' => '01',
            'expiry_year' => '2050',
            'name' => 'Test',
            'number' => '4444333322221111',
            'token' => 'tok_00000000000000000000000000000000',
        ];
        $updated = [
            'cvc' => '013',
            'expiry_month' => '02',
            'expiry_year' => '2051',
            'name' => 'Tester',
            'number' => '4444333322221112',
        ];

        $this->runMutableAttributeTests(TokenisedCard::class, $original, $updated, 'token');
    }
}
