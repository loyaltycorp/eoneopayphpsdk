<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\PaymentSource;
use EoneoPaySdk\Entity\PaymentSource\BankAccount;
use EoneoPaySdk\Entity\PaymentSource\CreditCard;
use EoneoPaySdk\Entity\PaymentSource\Info\BankAccount as BankAccountInfo;
use EoneoPaySdk\Entity\PaymentSource\Info\CreditCard as CreditCardInfo;
use EoneoPaySdk\Entity\PaymentSource\Info\Info;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\PaymentSource
 */
class PaymentSourceTest extends EntityTestCase
{
    /**
     * A bank account payment source
     *
     * @var array
     */
    private $bankAccount = [
        'created_at' => '2017-01-01 00:00:00',
        'holder_id' => 'cus_000000000000000000000000',
        'id' => 'src_000000000000000000000000',
        'info' => [
            'address_city' => 'Melbourne',
            'address_country' => 'Australia',
            'address_line1' => 'Apartment 10',
            'address_line2' => '123 Test Street',
            'address_postcode' => '3000',
            'address_state' => 'VIC',
            'bsb' => '000-000',
            'currency' => 'AUD',
            'default' => true,
            'id' => 'src_000000000000000000000001',
            'name' => 'Test',
            'trust' => false,
            'number' => '00000000',
        ],
        'internal_token' => 'tok_000000000000000000000001',
        'token' => 'X0XX0xx00xxx',
        'type' => 'bank_account',
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Credit card payment source
     *
     * @var array
     */
    private $creditCard = [
        'created_at' => '2017-01-01 00:00:00',
        'holder_id' => 'cus_000000000000000000000000',
        'id' => 'src_000000000000000000000002',
        'info' => [
            'address_city' => 'Melbourne',
            'address_country' => 'Australia',
            'address_line1' => 'Apartment 10',
            'address_line2' => '123 Test Street',
            'address_postcode' => '3000',
            'address_state' => 'VIC',
            'cardDescription' => 'Visa',
            'cardType' => 6,
            'currency' => 'AUD',
            'expiry_month' => '12',
            'expiry_year' => '2025',
            'name' => 'Test',
            'number' => '444433...111',
        ],
        'internal_token' => 'tok_000000000000000000000002',
        'token' => 'X0XX0xx00xxx',
        'type' => 'credit_card',
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Test the payment source constructor
     *
     * @return void
     */
    public function testConstructor() : void
    {
        // Test payment source init's correctly when passed just info
        $bankAccount = new BankAccount($this->bankAccount['info']);
        $this->assertInstanceOf(BankAccount::class, $bankAccount);
        $this->recursiveKeySort($this->bankAccount['info']);
        $this->assertSame($this->bankAccount['info'], $bankAccount->getInfo()->toArray());

        $creditCard = new CreditCard($this->creditCard['info']);
        $this->assertInstanceOf(CreditCard::class, $creditCard);
        $cardInfo = $this->creditCard['info'];
        $cardInfo['expiryDate'] = sprintf('%d/%d', $cardInfo['expiry_month'], substr($cardInfo['expiry_year'], -2));
        $cardInfo['pan'] = $cardInfo['number'];
        unset($cardInfo['number']);
        $this->recursiveKeySort($cardInfo);
        $this->assertSame($cardInfo, $creditCard->getInfo()->toArray());
    }

    /**
     * Test deleting a payment source
     *
     * @return void
     */
    public function testDeletePaymentSource() : void
    {
        // Set up entity data and response
        $entity = new BankAccount($this->bankAccount);
        $response = ['customer' => $this->customerData];

        $this->runEntityTests('get', $entity, '0', 'Bank account deleted.', $response);
    }

    /**
     * Test getting a bank account by id
     *
     * @return void
     */
    public function testGetBankAccountPaymentSource() : void
    {
        // Set up entity data and response
        $entity = new PaymentSource(['id' => 'src_000000000000000000000001']);
        $response = ['bank_account' => $this->bankAccount];

        $this->runEntityTests('get', $entity, '0', 'Payment source found.', $response);
    }

    /**
     * Test getting a credit card by id
     *
     * @return void
     */
    public function testGetCreditCardPaymentSource() : void
    {
        $cardInfo = $this->creditCard['info'];

        // Set up entity data and response
        $entity = new PaymentSource(['id' => 'src_000000000000000000000001']);
        $response = ['credit_card' => array_merge($this->creditCard, ['info' => [
            'expiryDate' => sprintf('%d/%d', $cardInfo['expiry_month'], substr($cardInfo['expiry_year'], -2)),
            'pan' => $cardInfo['number'],
        ]])];

        // Remove number from response as this should be mapped to pan
        unset($response['credit_card']['number']);

        $this->runEntityTests('get', $entity, '0', 'Payment source found.', $response);
    }

    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters() : void
    {
        $entity = new PaymentSource(['type' => 'bank_account']);
        $this->assertInstanceOf(BankAccountInfo::class, $entity->getInfo());

        $entity = new PaymentSource(['type' => 'credit_card']);
        $this->assertInstanceOf(CreditCardInfo::class, $entity->getInfo());

        $entity = new PaymentSource;
        $this->assertInstanceOf(Info::class, $entity->getInfo());
    }

    /**
     * Test getting a payment source with mapped return types
     *
     * @return void
     */
    public function testPaymentSourceInit() : void
    {
        // Test payment sources instantiate the correct class based on type
        $paymentSource = new PaymentSource;
        $this->assertInstanceOf(BankAccount::class, $paymentSource->init($this->bankAccount));
        $this->assertInstanceOf(CreditCard::class, $paymentSource->init($this->creditCard));
    }

    /**
     * Test getting a payment source with an invalid type
     *
     * @return void
     */
    public function testPaymentSourceInitWithInvalidType() : void
    {
        // Set up unknown payment type
        $data = ['id' => 'src_000000000000000000000000', 'type' => 'unknown'];

        // If the payment type is invalid an exception should be thrown
        $this->expectException(InvalidEntityException::class);
        $this->assertInstanceOf(PaymentSource::class, (new PaymentSource)->init($data));
    }

    /**
     * Test updating a payment source
     *
     * @return void
     */
    public function testUpdatePaymentSource() : void
    {
        // Set up entity data and response
        $entity = new BankAccount(array_merge($this->bankAccount, ['name' => 'Updated']));
        $response = ['bank_account' => $this->bankAccount];

        $this->runEntityTests('update', $entity, '0', 'Bank account updated.', $response);
    }
}
