<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity\PaymentSource;

use EoneoPaySdk\Entity\Customer;
use EoneoPaySdk\Entity\Merchant;
use EoneoPaySdk\Entity\PaymentSource\Info\CreditCard;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\PaymentSource\Info\CreditCard
 * @covers \EoneoPaySdk\Entity\PaymentSource\Info\Info
 */
class CreditCardTest extends EntityTestCase
{
    /**
     * Credit card test data
     *
     * @var array
     */
    private $data = [
        'address_city' => 'Melbourne',
        'address_country' => 'Australia',
        'address_line1' => 'Apartment 11',
        'address_line2' => '123 Test Street',
        'address_postcode' => '3000',
        'address_state' => 'VIC',
        'cardDescription' => 'Visa',
        'cardType' => 6,
        'currency' => 'AUD',
        'expiry_month' => '12',
        'expiry_year' => '2025',
        'id' => 'src_000000000000000000000000',
        'name' => 'Test',
        'number' => '444433...111',
    ];

    /**
     * Create a credit card response
     *
     * @param string $userId The id The user id this card is being attached to
     * @param string $token The card token
     *
     * @return array
     */
    private function createCreditCardResponse(string $userId, string $token) : array
    {
        // Response card will remap number and created expiryDate
        $creditCard = array_merge($this->data, [
            'expiryDate' => sprintf('%d/%d', $this->data['expiry_month'], substr($this->data['expiry_year'], -2)),
            'pan' => $this->data['number'],
        ]);
        unset($creditCard['number']);

        // Create response
        return [
            'created_at' => '2017-01-01 00:00:00',
            'holder_id' => $userId,
            'id' => 'src_000000000000000000000000',
            'info' => $creditCard,
            'internal_token' => $token,
            'token' => 'X0XX0xx00xxx',
            'type' => 'credit_card',
            'updated_at' => '2017-01-01 00:00:00',
        ];
    }

    /**
     * Test attempting to attach a credit card to an invalid user
     *
     * @return void
     */
    public function testCreateCreditCardWithInvalidUser() : void
    {
        // Create credit card and customer with an invalid id
        $creditCard = new CreditCard($this->data);
        $customer = new Customer(['id' => 'invalid']);

        $this->expectException(InvalidEntityException::class);
        $creditCard->forCustomer($customer);
    }

    /**
     * Test attaching a credit card to a customer
     *
     * @return void
     */
    public function testCreateCustomerCreditCard() : void
    {
        // Create customer, entity, card response and expected response
        $customer = new Customer($this->customerData);
        $entity = new CreditCard(['token' => 'tok_000000000000000000000000']);
        $cardResponse = $this->createCreditCardResponse((string)$customer->getId(), (string)$entity->getToken());
        $response = [
            'credit_card' => $cardResponse,
            'customer' => array_merge($this->customerData, ['credit_cards' => ['info' => $cardResponse['info']]]),
        ];

        // Attach card to customer
        $entity->forCustomer($customer);

        $this->runEntityTests('create', $entity, '0', 'Credit card created.', $response);
    }

    /**
     * Test attaching a credit card to a merchant
     *
     * @return void
     */
    public function testCreateMerchantCreditCard() : void
    {
        // Create customer, entity, card response and expected response
        $merchant = new Merchant($this->merchantData);
        $entity = new CreditCard(['token' => 'tok_000000000000000000000000']);
        $cardResponse = $this->createCreditCardResponse((string)$merchant->getId(), (string)$entity->getToken());
        $response = [
            'credit_card' => $cardResponse,
            'merchant' => array_merge($this->merchantData, ['credit_cards' => ['info' => $cardResponse['info']]]),
        ];

        // Attach card to merchant
        $entity->forMerchant($merchant);

        $this->runEntityTests('create', $entity, '0', 'Credit card created.', $response);
    }

    /**
     * Test credit card constructor converts number to pan and sets expiry
     *
     * @return void
     */
    public function testConstructor() : void
    {
        $creditCard = new CreditCard($this->data);

        // Test pan remapping
        $this->assertArrayNotHasKey('number', $creditCard->toArray());
        $this->assertArrayHasKey('pan', $creditCard->toArray());
        $this->assertSame($this->data['number'], $creditCard->getPan());

        // Test expiry date
        $expiryDate = sprintf('%d/%d', $this->data['expiry_month'], substr($this->data['expiry_year'], -2));
        $this->assertArrayHasKey('expiryDate', $creditCard->toArray());
        $this->assertSame($expiryDate, $creditCard->getExpiryDate());
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set original and updated data
        $original = array_merge($this->data, [
            'pan' => $this->data['number'],
            'token' => 'tok_000000000000000000000000',
        ]);
        unset($original['number']);
        $updated = [
            'address_city' => 'Sydney',
            'address_country' => 'AU',
            'address_line1' => 'Apartment 17',
            'address_line2' => '124 Test Street',
            'address_postcode' => '2000',
            'address_state' => 'NSW',
            'token' => 'tok_0000000000000000000000001'
        ];

        $this->runMutableAttributeTests(CreditCard::class, $original, $updated, 'pan');
    }
}
