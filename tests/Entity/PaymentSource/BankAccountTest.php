<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity\PaymentSource;

use EoneoPaySdk\Entity\Customer;
use EoneoPaySdk\Entity\Merchant;
use EoneoPaySdk\Entity\PaymentSource\Info\BankAccount;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\PaymentSource\Info\BankAccount
 * @covers \EoneoPaySdk\Entity\PaymentSource\Info\Info
 */
class BankAccountTest extends EntityTestCase
{
    /**
     * Bank account test data
     *
     * @var array
     */
    private $data = [
        'address_city' => 'Melbourne',
        'address_country' => 'Australia',
        'address_line1' => 'Apartment 10',
        'address_line2' => '123 Test Street',
        'address_postcode' => '3000',
        'address_state' => 'VIC',
        'bsb' => '000-000',
        'currency' => 'AUD',
        'default' => true,
        'name' => 'Test',
        'trust' => false,
        'number' => '00000000',
    ];

    /**
     * Create a bank account response
     *
     * @param string $userId The id The user id this account is being attached to
     *
     * @return array
     */
    private function createBankAccountResponse(string $userId) : array
    {
        // Create response
        return [
            'created_at' => '2017-01-01 00:00:00',
            'holder_id' => $userId,
            'id' => 'src_000000000000000000000000',
            'info' => $this->data,
            'internal_token' => 'tok_000000000000000000000000',
            'token' => 'X0XX0xx00xxx',
            'type' => 'bank_account',
            'updated_at' => '2017-01-01 00:00:00',
        ];
    }

    /**
     * Test mutators on certain attributes
     *
     * @return void
     */
    public function testAttributeMutators() : void
    {
        // Set original and expected values
        $original = ['default' => 'true', 'trust' => 0];
        $expected = ['default' => true, 'trust' => false];

        $this->runAttributeMutatorTests(BankAccount::class, $original, $expected);
    }

    /**
     * Test attempting to attach a bank account to an invalid user
     *
     * @return void
     */
    public function testCreateBankAccountWithInvalidUser() : void
    {
        // Create bank account and customer with an invalid id
        $bankAccount = new BankAccount($this->data);
        $customer = new Customer(['id' => 'invalid']);

        $this->expectException(InvalidEntityException::class);
        $bankAccount->forCustomer($customer);
    }

    /**
     * Test attaching a bank account to a customer
     *
     * @return void
     */
    public function testCreateCustomerBankAccount() : void
    {
        // Create customer, entity and expected response
        $customer = new Customer($this->customerData);
        $entity = new BankAccount($this->data);
        $response = [
            'bank_account' => $this->createBankAccountResponse((string)$customer->getId()),
            'customer' => array_merge($this->customerData, ['bank_accounts' => ['info' => $this->data]]),
        ];

        // Attach account to customer
        $entity->forCustomer($customer);

        $this->runEntityTests('create', $entity, '0', 'Bank account created.', $response);
    }

    /**
     * Test attaching a bank account to a merchant
     *
     * @return void
     */
    public function testCreateMerchantBankAccount() : void
    {
        // Create merchant, entity and expected response
        $merchant = new Merchant($this->merchantData);
        $entity = new BankAccount($this->data);
        $response = [
            'bank_account' => $this->createBankAccountResponse((string)$merchant->getId()),
            'merchant' => array_merge($this->merchantData, ['bank_accounts' => ['info' => $this->data]]),
        ];

        // Attach account to merchant
        $entity->forMerchant($merchant);

        $this->runEntityTests('create', $entity, '0', 'Bank account created.', $response);
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set original and updated data
        $updated = [
            'address_city' => 'Sydney',
            'address_country' => 'AU',
            'address_line1' => 'Apartment 17',
            'address_line2' => '124 Test Street',
            'address_postcode' => '2000',
            'address_state' => 'NSW',
            'bsb' => '000-001',
            'default' => false,
            'name' => 'Tester',
            'number' => '00000001',
        ];

        $this->runMutableAttributeTests(BankAccount::class, $this->data, $updated, 'currency');
    }
}
