<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity\Ewallet;

use EoneoPaySdk\Entity\Ewallet;
use EoneoPaySdk\Entity\Ewallet\Statement;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Ewallet\Statement
 */
class StatementTest extends EntityTestCase
{
    /**
     * Test getting a statement for an ewallet
     *
     * @return void
     */
    public function testGetStatement() : void
    {
        $data = [
            'balance' => 0,
            'deposits' => [],
            'endDate' => '2017-01-01 00:00:00',
            'withdrawals' => [],
            'startDate' => '2017-01-31 23:59:59',
        ];

        // Set up ewallet, entity and attach them
        $wallet = new Ewallet(['id' => '0XXXX00', 'merchant_id' => $this->merchantData['id']]);
        $entity = (new Statement)->forEwallet($wallet);
        $response = ['ewallet_statement' => $data];

        // Run assertions
        $this->runEntityTests('get', $entity, '0', 'eWallet statement found.', $response);
    }

    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters() : void
    {
        $entity = new Statement;
        $this->assertInstanceOf(Collection::class, $entity->getDeposits());
        $this->assertInstanceOf(Collection::class, $entity->getWithdrawals());
    }

    /**
     * Test attempting to attach an invalid ewallet to a statement
     *
     * @return void
     */
    public function testInvalidEwalletAttachment() : void
    {
        $this->expectException(InvalidEntityException::class);

        // Set up entity and ewallet without an owner
        $wallet = new Ewallet(['id' => '0XXXX00']);
        (new Statement)->forEwallet($wallet);
    }

    /**
     * Test specific billing period can be set
     *
     * @return void
     */
    public function testStatementSetMonth() : void
    {
        // Instantiate statement and set date then test result
        $entity = (new Statement)->forMonth('July 2017');

        // Test conversion worked
        $this->assertSame('2017-07-01', $entity->getDate());
    }
}
