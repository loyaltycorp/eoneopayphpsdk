<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\Customer;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Customer
 */
class CustomerTest extends EntityTestCase
{
    /**
     * Test creating a new customer
     *
     * @return void
     */
    public function testCreateCustomer() : void
    {
        // Set up entity and response data
        $entity = new Customer([
            'email' => $this->customerData['email'],
            'external_customer_id' => $this->customerData['external_customer_id'],
            'first_name' => $this->customerData['first_name'],
            'last_name' => $this->customerData['last_name'],
        ]);
        $response = ['customer' => $this->customerData];

        $this->runEntityTests('create', $entity, '0', 'Customer created.', $response);
    }

    /**
     * Test creating a duplicate customer
     *
     * @return void
     */
    public function testCreateDuplicateCustomer() : void
    {
        // Set up entity
        $entity = new Customer([
            'email' => $this->customerData['email'],
            'external_customer_id' => $this->customerData['external_customer_id'],
            'first_name' => $this->customerData['first_name'],
            'last_name' => $this->customerData['last_name'],
        ]);

        $this->runEntityTests('create', $entity, '2001', 'A customer with this ID already exists', [], 409);
    }

    /**
     * Test deleting a customer
     *
     * @return void
     */
    public function testDeleteCustomer() : void
    {
        // Set up entity
        $entity = new Customer(['id' => $this->customerData['id']]);

        $this->runEntityTests('delete', $entity, '0', 'Customer deleted.');
    }

    /**
     * Test deleting an already deleted customer
     *
     * @return void
     */
    public function testDeleteDeletedCustomer() : void
    {
        // Set up entity
        $entity = new Customer(['id' => $this->customerData['id']]);

        $this->runEntityTests('delete', $entity, '2100', 'Customer not found', [], 404);
    }

    /**
     * Test getting a customer by id
     *
     * @return void
     */
    public function testGetCustomerById() : void
    {
        // Set up entity and response data
        $entity = new Customer(['id' => $this->customerData['id']]);
        $response = ['customer' => $this->customerData];

        $this->runEntityTests('get', $entity, '0', 'Customer found.', $response);
    }

    /**
     * Test getting a customer by external id
     *
     * @return void
     */
    public function testGetCustomerByExternalId() : void
    {
        // Set up entity and response data
        $entity = new Customer(['id' => $this->customerData['external_customer_id']]);
        $response = ['customer' => $this->customerData];

        $this->runEntityTests('get', $entity, '0', 'Customer found.', $response);
    }

    /**
     * Test getting a customer which doesn't exist
     *
     * @return void
     */
    public function testGetDeletedCustomer() : void
    {
        // Set up entity
        $entity = new Customer(['id' => $this->customerData['id']]);

        $this->runEntityTests('get', $entity, '2100', 'Customer not found', [], 404);
    }

    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters() : void
    {
        $entity = new Customer;

        $this->assertInstanceOf(Collection::class, $entity->getBankAccounts());
        $this->assertInstanceOf(Collection::class, $entity->getCreditCards());
        $this->assertInstanceOf(Collection::class, $entity->getReferenceNumbers());
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set updated data
        $updated = [
            'email' => 'test@test.com',
            'external_customer_id' => 'ext_0000000001',
            'first_name' => 'Updated',
            'last_name' => 'Name',
        ];

        $this->runMutableAttributeTests(Customer::class, $this->customerData, $updated, 'ewallet_id');
    }

    /**
     * Test updating a customer
     *
     * @return void
     */
    public function testUpdateCustomer() : void
    {
        // Set up entity and response data
        $entity = new Customer(array_merge($this->customerData, ['first_name' => 'Updated']));
        $response = ['customer' => array_merge($this->customerData, $entity->toArray())];

        $this->runEntityTests('update', $entity, '0', 'Customer updated.', $response);
    }
}
