<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\PaymentSource\BankAccount;
use EoneoPaySdk\Entity\PaymentSource\CreditCard;
use EoneoPaySdk\Entity\PreAuth;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\PreAuth
 */
class PreAuthTest extends EntityTestCase
{
    /**
     * Example payment data
     *
     * @var array
     */
    private $data = [
        'amount' => 50,
        'approved' =>  true,
        'authID' => '000001',
        'bank_txn_id' => 1,
        'card_type' => 'Master Card',
        'card_type_id' => 5,
        'created_at' => '2017-01-01 00:00:00',
        'currency' => 'AUD',
        'customer_id' => 'cus_000000000000000000000001',
        'external_txn_id' => '000007',
        'fee_amount' => 10,
        'fee_schedule' => [
            'card_rates' => [
                'MasterCard' => 1.5,
                'AMEX' => 2.9,
                'Visa' => 1.5,
            ],
            'merchant_id' => '*',
            'payment_type' => 'cc_payment',
            'transaction_fee' => '10',
        ],
        'merchant_id' => 'mer_0000000000000001',
        'merchant_transfer_status' => 0,
        'meta' => 'Metadata',
        'reference' => 'Test',
        'responseCode' => '00',
        'responseText' => 'Approved',
        'settlementDate' => '20170101',
        'statement_descriptor' => 'Test',
        'status' => 1,
        'token' => 'src_000000000000000000000001',
        'txnID' => '000009',
        'txnType' => '0',
        'txn_date' => '2017-01-01 00:00:00',
        'txn_id' => 'txn_000000000000000000000001',
        'type' => 'cc_payment',
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Example credit card data
     *
     * @var array
     */
    private $creditCardData = [
        'id' => 'src_000000000000000000000000',
        'token' => 'tok_000000000000000000000000',
    ];

    /**
     * Get credit card data which matches the SDK
     *
     * @return array
     */
    private function getFilteredResponse() : array
    {
        $data = $this->data;

        // Remove stuff which will be removed by the sdk
        unset(
            $data['approved'],
            $data['authID'],
            $data['responseCode'],
            $data['responseText'],
            $data['settlementDate'],
            $data['txnID'],
            $data['txnType']
        );

        return $data;
    }

    /**
     * Test completing a pre-auth
     *
     * @return void
     */
    public function testCompletePreAuth() : void
    {
        $entity = new PreAuth($this->getFilteredResponse());
        $response = [
            'credit_card' => $this->creditCardData,
            'payment' => $this->getFilteredResponse(),
        ];

        $this->runEntityTests('update', $entity, '0', 'Pre-authorisation request completed', $response);
    }

    /**
     * Test creating a pre-auth
     *
     * @return void
     */
    public function testCreatePreAuth() : void
    {
        // Set up credit card, entity and expected response
        $creditCard = new CreditCard($this->creditCardData);
        $entity = new PreAuth([
            'amount' => $this->data['amount'],
            'reference' => $this->data['reference'],
        ]);
        $response = [
            'credit_card' => $this->creditCardData,
            'payment' => $this->getFilteredResponse(),
        ];

        // Make payment from credit card
        $entity->fromCreditCard($creditCard);

        $this->runEntityTests('create', $entity, '0', 'Pre-authorisation complete request completed', $response);
    }

    /**
     * Test attempting to use an invalid credit card
     *
     * @return void
     */
    public function testInvalidCreditCardAttachment() : void
    {
        $creditCard = new CreditCard;
        $entity = new PreAuth;

        $this->expectException(InvalidEntityException::class);
        $entity->fromCreditCard($creditCard);
    }

    /**
     * Test passing payment source via fromPaymentSource()
     *
     * @return void
     */
    public function testFromPaymentSourceRouting() : void
    {
        $creditCard = new CreditCard($this->creditCardData);
        $entity = new PreAuth;
        $this->assertNull($entity->getToken());

        // Set valid credit card
        $entity->fromPaymentSource($creditCard);
        $this->assertSame($creditCard->getId(), $entity->getToken());

        // Use bank account
        $this->expectException(InvalidEntityException::class);
        $bankAccount = new BankAccount;
        $entity->fromPaymentSource($bankAccount);
    }
}
