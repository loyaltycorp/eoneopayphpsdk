<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\Plan;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Plan
 */
class PlanTest extends EntityTestCase
{
    /**
     * Set example plan data
     *
     * @var array
     */
    private $data = [
        'amount' => 100,
        'created_at' => '2017-01-01 00:00:00',
        'currency' => 'AUD',
        'id' => 'plan_000000000000000000000000',
        'interval' => 'day',
        'interval_count' => 1,
        'merchant_id' => 'mer_0000000000000000',
        'name' => 'Plan',
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Test creating a plan
     *
     * @return void
     */
    public function testCreatePlan() : void
    {
        // Create entity and expected response
        $entity = new Plan($this->data);
        $response = ['plan' => $this->data];

        $this->runEntityTests('create', $entity, '0', 'Plan created.', $response);
    }

    /**
     * Test deleting a plan
     *
     * @return void
     */
    public function testDeletePlan() : void
    {
        // Create entity and expected response
        $entity = new Plan($this->data);

        $this->runEntityTests('delete', $entity, '0', 'Plan deleted.');
    }

    /**
     * Test getting a plan by id
     *
     * @return void
     */
    public function testGetPlanById() : void
    {
        // Create entity and expected response
        $entity = new Plan($this->data);
        $response = ['plan' => $this->data];

        $this->runEntityTests('get', $entity, '0', 'Plan found.', $response);
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set updated data
        $updated = [
            'amount' => '123',
            'interval' => Plan::INTERVAL_WEEK,
            'name' => 'Test',
        ];

        $this->runMutableAttributeTests(Plan::class, $this->data, $updated, 'merchant_id');
    }

    /**
     * Test updating a plan
     *
     * @return void
     */
    public function testUpdatePlan() : void
    {
        // Set up entity and response data
        $entity = new Plan(array_merge($this->data, ['name' => 'Updated']));
        $response = ['plan' => array_merge($this->data, $entity->toArray())];

        $this->runEntityTests('update', $entity, '0', 'Plan updated.', $response);
    }
}
