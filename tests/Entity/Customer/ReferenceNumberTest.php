<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity\Customer;

use EoneoPaySdk\Entity\Customer\ReferenceNumber;
use EoneoPaySdk\Entity\Customer\ReferenceNumber\AusPost;
use EoneoPaySdk\Entity\Customer\ReferenceNumber\Bpay;
use EoneoPaySdk\Entity\Customer\ReferenceNumber\Eft;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Customer\ReferenceNumber
 */
class ReferenceNumberTest extends EntityTestCase
{
    /**
     * Test end point changes
     *
     * @return void
     */
    public function testDeleteEndPoints() : void
    {
        // Create each entity and test
        $referenceNumbers = [
            'allocated_eft' => 'eftId',
            'auspost' => 'ausPostCrn',
            'bpay' => 'bpayCrn',
        ];

        // Test delete endpoint for each
        foreach ($referenceNumbers as $type => $endpoint) {
            $referenceNumber = new ReferenceNumber([
                'customer_id' => $this->customerData['id'],
                'reference_number' => '0000000000',
                'type' => $type
            ]);

            $expected = sprintf(
                'customers/%s/%s/%s',
                $this->customerData['id'],
                $endpoint,
                $referenceNumber->getReferenceNumber()
            );
            $this->assertSame($expected, $referenceNumber->getEndpointFromMethod('delete'));
        }
    }

    /**
     * Test end point changes
     *
     * @return void
     */
    public function testGetEndPoints() : void
    {
        // Test auspost
        $ausPost = new ReferenceNumber([
            'amount' => 100,
            'customer_id' => $this->customerData['id'],
            'pip_id' => '000',
            'reference_number' => '0000000000',
            'type' => 'auspost'
        ]);
        $exp = 'customers/cus_000000000000000000000000/ausPostBarcodeForCrn/0000000000/withPIPId/000/forAmount/100';
        $this->assertSame($exp, $ausPost->getEndpointFromMethod('get'));

        // Test BPAY
        $bpay = new ReferenceNumber([
            'biller_code' => '0000000',
            'customer_id' => $this->customerData['id'],
            'reference_number' => '0000000000',
            'type' => 'bpay'
        ]);
        $expected = 'customers/cus_000000000000000000000000/bpayBillAdviceForCrn/0000000000/withBillerCode/0000000';
        $this->assertSame($expected, $bpay->getEndpointFromMethod('get'));
    }

    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters() : void
    {
        $entity = new ReferenceNumber(['type' => 'auspost']);
        $this->assertInstanceOf(AusPost::class, $entity->getPaymentFacility());

        $entity = new ReferenceNumber(['type' => 'bpay']);
        $this->assertInstanceOf(Bpay::class, $entity->getPaymentFacility());

        $entity = new ReferenceNumber(['type' => 'allocated_eft']);
        $this->assertInstanceOf(Eft::class, $entity->getPaymentFacility());

        $this->expectException(InvalidEntityException::class);
        $entity = new ReferenceNumber;
        $entity->getPaymentFacility();
    }

    /**
     * Test getting a reference number with mapped return types
     *
     * @return void
     */
    public function testReferenceNumberInit() : void
    {
        $referenceNumbers = [
            AusPost::class => 'auspost',
            Bpay::class => 'bpay',
            Eft::class => 'allocated_eft',
        ];

        // Test reference numbers instantiate the correct class based on type
        $referenceNumber = new ReferenceNumber;

        foreach ($referenceNumbers as $class => $type) {
            $data = ['type' => $type];
            $this->assertInstanceOf($class, $referenceNumber->init($data));
        }
    }

    /**
     * Test getting a reference number with mapped return types
     *
     * @return void
     */
    public function testReferenceNumberInitWithReturnData() : void
    {
        $data = [
            'payment_facility' => [
                'biller_code' => '0000000',
                'customer_id' => $this->customerData['id'],
                'reference_number' => '0000000000',
                'type' => 'bpay'
            ],
        ];

        // Test reference numbers instantiate the correct class based on type
        $referenceNumber = new ReferenceNumber;
        $referenceNumber->init($data);

        $this->assertInstanceOf(ReferenceNumber::class, $referenceNumber);
    }

    /**
     * Test getting a reference number with an invalid type
     *
     * @return void
     */
    public function testReferenceNumberInitWithInvalidType() : void
    {
        // Set up unknown reference number
        $data = ['type' => 'unknown'];

        // If the payment type is invalid an exception should be thrown
        $this->expectException(InvalidEntityException::class);
        $this->assertInstanceOf(ReferenceNumber::class, (new ReferenceNumber)->init($data));
    }

    /**
     * Test reference number validation for AusPost
     *
     * @return void
     */
    public function testValidationAusPost() : void
    {
        // Test object with no data
        $referenceNumber = new ReferenceNumber(['type' => 'auspost']);
        $this->assertFalse($referenceNumber->validate('get'));
        $this->assertCount(4, $referenceNumber->getValidationErrors());

        // Test with required fields
        $referenceNumber = $referenceNumber->init([
            'amount' => 100,
            'customer_id' => $this->customerData['id'],
            'pip_id' => '000',
            'reference_number' => '0000000000',
            'type' => 'auspost'
        ]);
        $this->assertTrue($referenceNumber->validate('get'));
    }

    /**
     * Test reference number validation for Bpay
     *
     * @return void
     */
    public function testValidationBpay() : void
    {
        // Test object with no data
        $referenceNumber = new ReferenceNumber(['type' => 'bpay']);
        $this->assertFalse($referenceNumber->validate('get'));
        $this->assertCount(3, $referenceNumber->getValidationErrors());

        // Test with required fields
        $referenceNumber = $referenceNumber->init([
            'biller_code' => '0000000',
            'customer_id' => $this->customerData['id'],
            'reference_number' => '0000000000',
            'type' => 'bpay'
        ]);
        $this->assertTrue($referenceNumber->validate('get'));
    }
}
