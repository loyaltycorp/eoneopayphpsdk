<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity\Customer;

use EoneoPaySdk\Entity\Charge;
use EoneoPaySdk\Entity\Customer;
use EoneoPaySdk\Entity\Customer\ReferenceNumber;
use EoneoPaySdk\Entity\Customer\Subscription;
use EoneoPaySdk\Entity\PaymentSource\BankAccount;
use EoneoPaySdk\Entity\Plan;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Customer\Subscription
 * @SuppressWarnings(PHPMD.TooManyPublicMethods) All test methods must be public
 */
class SubscriptionTest extends EntityTestCase
{
    /**
     * Subscription test data
     *
     * @var array
     */
    private $data = [
        'created_at' => '2017-01-01 00:00:00',
        'current_balance' => 100,
        'current_period_end' => '2017-01-01',
        'current_period_start' => '2017-01-01',
        'customer_id' => 'cus_000000000000000000000000',
        'end_date' => '2017-01-01',
        'id' => 'sub_000000000000000000000000',
        'lead_time' => 3,
        'merchant_id' => 'mer_0000000000000000',
        'plan' => [
            'amount' => 100,
            'created_at' => '2017-01-01 00:00:00',
            'currency' => 'AUD',
            'id' => 'plan_000000000000000000000000',
            'interval' => 'week',
            'interval_count' => 1,
            'merchant_id' => 'mer_0000000000000000',
            'name' => 'Plan',
            'updated_at' => '2017-01-01 00:00:00',
        ],
        'plan_id' => 'plan_000000000000000000000000',
        'start_date' => '2017-01-01',
        'statement' => [
            'charges' => [],
            'end_date' => '2017-01-01',
            'outstanding_amount' => 100,
            'payments' => [],
            'start_date' => '2017-01-01',
            'total_outstanding_to_date' => 100,
        ],
        'status' => 'active',
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Test adding a charge to a subscription
     *
     * @return void
     */
    public function testAddSubscriptionCharge() : void
    {
        // Create charge and entity
        $charge = new Charge(['id' => 'chg_000000000000000000000000']);
        $entity = new Subscription;

        $entity->addCharge($charge);

        $this->assertInstanceOf(Collection::class, $entity->getCharges());
        $this->assertCount(1, $entity->getCharges()->toArray());
    }

    /**
     * Test mutators on certain attributes
     *
     * @return void
     */
    public function testAttributeMutators() : void
    {
        // Set original and expected values
        $original = ['start_date' => '1 July 2017', 'end_date' => 'September 2019'];
        $expected = [
            'start_date' => '2017-07-01',
            'end_date' => '2019-09-01',
        ];

        $this->runAttributeMutatorTests(Subscription::class, $original, $expected);
    }

    /**
     * Test creating a subscription
     *
     * @return void
     */
    public function testCreateSubscription() : void
    {
        // Create customer, plan, entity and expected response
        $customer = new Customer($this->customerData);
        $plan = new Plan($this->data['plan']);
        $entity = new Subscription;
        $response = ['subscription' => $this->data];

        // Attach subscription to customer and plan
        $entity->forCustomer($customer)->forPlan($plan);

        $this->runEntityTests('create', $entity, '0', 'Subscription created.', $response);
    }

    /**
     * Test deleting a subscription
     *
     * @return void
     */
    public function testDeleteSubscription() : void
    {
        // Create entity
        $entity = new Subscription($this->data);

        $this->runEntityTests('delete', $entity, '0', 'Subscription cancelled.');
    }

    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters() : void
    {
        $entity = new Subscription;
        $this->assertInstanceOf(Collection::class, $entity->getCharges());
        $this->assertInstanceOf(Repository::class, $entity->getPaymentFacilities());
        $this->assertInstanceOf(Repository::class, $entity->getStatement());
    }

    /**
     * Test getting a subscription by id
     *
     * @return void
     */
    public function testGetSubscriptionById() : void
    {
        // Create entity and expected response
        $entity = new Subscription($this->data);
        $response = [
            'subscription' => $this->data,
        ];

        $this->runEntityTests('get', $entity, '0', 'Subscription found.', $response);
    }

    /**
     * Test attempting to attach an invalid charge
     *
     * @return void
     */
    public function testInvalidChargeAttachment() : void
    {
        // Set up entity and charge
        $entity = new Subscription;
        $charge = new Charge;

        $this->expectException(InvalidEntityException::class);
        $entity->addCharge($charge);
    }

    /**
     * Test attempting to attach an invalid payment source
     *
     * @return void
     */
    public function testInvalidPaymentSourceAttachment() : void
    {
        // Set up entity and payment source
        $entity = new Subscription;
        $paymentSource = new BankAccount;

        $this->expectException(InvalidEntityException::class);
        $entity->fromPaymentSource($paymentSource);
    }

    /**
     * Test attempting to use an invalid plan
     *
     * @return void
     */
    public function testInvalidPlanAttachment() : void
    {
        $plan = new Plan;
        $entity = new Subscription;

        $this->expectException(InvalidEntityException::class);
        $entity->forPlan($plan);
    }

    /**
     * Test attempting to use an invalid reference number
     *
     * @return void
     */
    public function testInvalidReferenceNumberAttachment() : void
    {
        $referenceNumber = new ReferenceNumber;
        $entity = new Subscription;

        $this->expectException(InvalidEntityException::class);
        $entity->fromReferenceNumber($referenceNumber);
    }

    /**
     * Test attempting to use an invalid reference number
     *
     * @return void
     */
    public function testInvalidReferenceNumberTypeAttachment() : void
    {
        $referenceNumber = new ReferenceNumber(['reference_number' => '00000000']);
        $entity = new Subscription;

        $this->expectException(InvalidEntityException::class);
        $entity->fromReferenceNumber($referenceNumber);
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set updated data
        $updated = ['lead_time' => 5];

        $this->runMutableAttributeTests(Subscription::class, $this->data, $updated, 'customer_id');
    }

    /**
     * Test payment source attachment
     *
     * @return void
     */
    public function testPaymentSourceAttachment() : void
    {
        // Set up entity and payment source
        $entity = new Subscription;
        $paymentSource = new BankAccount(['id' => 'src_000000000000000000000000']);

        // Check source attachment
        $entity->fromPaymentSource($paymentSource);

        $this->assertSame($paymentSource->getId(), $entity->getSource());
    }

    /**
     * Test reference number attachments
     *
     * @return void
     */
    public function testReferenceNumberAttachments() : void
    {
        // Set up entity and references
        $referenceNumbers = [
            'allocated_eft' => '0000000000',
            'auspost' => '0000000000',
            'bpay' => '0000000000',
        ];

        foreach ($referenceNumbers as $type => $number) {
            $entity = new Subscription;
            $referenceNumber = new ReferenceNumber(['type' => $type, 'reference_number' => $number]);
            $entity->fromReferenceNumber($referenceNumber);

            $this->assertSame($type === 'allocated_eft' ? $number : null, $entity->getAllocatedEftReferenceNumber());
            $this->assertSame($type === 'auspost' ? $number : null, $entity->getAuspostReferenceNumber());
            $this->assertSame($type === 'bpay' ? $number : null, $entity->getBpayReferenceNumber());
        }
    }

    /**
     * Test updating a subscription
     *
     * @return void
     */
    public function testUpdateSubscription() : void
    {
        // Set up entity and response data
        $entity = new Subscription(array_merge($this->data, ['end_date' => '2019-01-01']));
        $response = ['subscription' => array_merge($this->data, $entity->toArray())];

        $this->runEntityTests('update', $entity, '0', 'Subscription updated.', $response);
    }
}
