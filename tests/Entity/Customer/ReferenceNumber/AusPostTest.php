<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity\Customer\ReferenceNumber;

use EoneoPaySdk\Entity\Customer;
use EoneoPaySdk\Entity\Customer\ReferenceNumber;
use EoneoPaySdk\Entity\Customer\ReferenceNumber\AusPost;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Customer\ReferenceNumber\AusPost
 */
class AusPostTest extends EntityTestCase
{
    private $data = [
        'allocated' => true,
        'allocation_date' => '2017-01-01',
        'customer_id' => 'cus_000000000000000000000000',
        'created_at' => '2017-01-01 00:00:00',
        'payment_facility' => [
            'clearing_account' => 0,
            'default' => true,
            'id' => 1,
            'pip_id' => '000',
            'type' => 'auspost',
        ],
        'reference_number' => '0000000000',
        'type' => 'auspost',
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Test creating an auspost allocation
     *
     * @return void
     */
    public function testCreateAusPostAllocation() : void
    {
        // Create customer, entity and expected response
        $customer = new Customer(['id' => $this->customerData['id']]);
        $entity = new AusPost;
        $response = ['reference_numbers' => $this->data];

        // Attach to customer
        $entity->forCustomer($customer);

        $this->runEntityTests('create', $entity, '0', 'Customer updated.', $response);
    }

    /**
     * Test creating an auspost allocation with pip id
     *
     * @return void
     */
    public function testCreateAusPostAllocationWithPipId() : void
    {
        // Create customer, entity and expected response
        $customer = new Customer(['id' => $this->customerData['id']]);
        $entity = new AusPost;
        $response = ['reference_numbers' => $this->data];

        // Set pip id
        $entity->setPipId($this->data['payment_facility']['pip_id']);

        // Attach to customer
        $entity->forCustomer($customer);

        $this->runEntityTests('create', $entity, '0', 'Customer updated.', $response);
    }

    /**
     * Test generating a barcode
     *
     * @return void
     */
    public function testGenerateBarcode() : void
    {
        // Create entity
        $entity = new ReferenceNumber($this->data);

        // Set amount
        $entity->setAmount(100);

        // Run tests on raw image
        $this->runRawTests('get', $entity, file_get_contents(__DIR__ . '/../../../Assets/auspost-barcode.png'));
    }

    /**
     * Test end point changes
     *
     * @return void
     */
    public function testEndPoints() : void
    {
        // Create customer, entity and test default endpoint
        $customer = new Customer(['id' => $this->customerData['id']]);
        $entity = new AusPost;
        $entity->forCustomer($customer);
        $endpoint = sprintf('customers/%s/ausPostCrn', $this->customerData['id']);
        $this->assertSame($endpoint, $entity->getEndpointFromMethod('post'));

        // Set pip_id and check endpoint changes
        $entity->setPipId('000');
        $endpoint = sprintf('customers/%s/ausPostCrnForPIPId/000', $this->customerData['id']);
        $this->assertSame($endpoint, $entity->getEndpointFromMethod('post'));
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set updated data
        $updated = [
            'pip_id' => '001',
        ];

        $this->runMutableAttributeTests(AusPost::class, $this->data['payment_facility'], $updated, 'default');
    }
}
