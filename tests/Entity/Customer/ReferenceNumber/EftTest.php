<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity\Customer\ReferenceNumber;

use EoneoPaySdk\Entity\Customer;
use EoneoPaySdk\Entity\Customer\ReferenceNumber\Eft;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Customer\ReferenceNumber\Eft
 */
class EftTest extends EntityTestCase
{
    private $data = [
        'allocated' => true,
        'allocation_date' => '2017-01-01',
        'customer_id' => 'cus_000000000000000000000000',
        'created_at' => '2017-01-01 00:00:00',
        'payment_facility' => [
            'bsb' => '000000',
            'clearing_account' => 0,
            'default' => true,
            'id' => 1,
            'prefix' => '000',
            'type' => 'allocated_eft',
        ],
        'reference_number' => '0000000000',
        'type' => 'allocated_eft',
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Test mutators on certain attributes
     *
     * @return void
     */
    public function testAttributeMutators() : void
    {
        // Set original and expected values
        $original = ['bsb' => '000-000'];
        $expected = ['bsb' => '000000'];

        $this->runAttributeMutatorTests(Eft::class, $original, $expected);
    }

    /**
     * Test creating an eftid allocation
     *
     * @return void
     */
    public function testCreateEftIdAllocation() : void
    {
        // Create customer, entity and expected response
        $customer = new Customer(['id' => $this->customerData['id']]);
        $entity = new Eft;
        $response = ['reference_numbers' => $this->data];

        // Attach to customer
        $entity->forCustomer($customer);

        $this->runEntityTests('create', $entity, '0', 'Customer updated.', $response);
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set updated data
        $updated = [
            'bsb' => '000001',
            'prefix' => '001',
        ];

        $this->runMutableAttributeTests(Eft::class, $this->data['payment_facility'], $updated, 'default');
    }

    /**
     * Test end point changes
     *
     * @return void
     */
    public function testPostEndPoints() : void
    {
        // Create customer, entity and test default endpoint
        $customer = new Customer(['id' => $this->customerData['id']]);
        $entity = new Eft;
        $entity->forCustomer($customer);
        $endpoint = sprintf('customers/%s/eftId', $this->customerData['id']);
        $this->assertSame($endpoint, $entity->getEndpointFromMethod('post'));

        // Set bsb and prefix and check endpoint changes
        $entity->setBsb('000000')->setPrefix('000');
        $endpoint = sprintf('customers/%s/eftIdForBsb/000000/withPrefix/000', $this->customerData['id']);
        $this->assertSame($endpoint, $entity->getEndpointFromMethod('post'));
    }

    /**
     * Test validation changes
     *
     * @return void
     */
    public function testValidation() : void
    {
        // Create customer, entity and test default validation
        $customer = new Customer(['id' => $this->customerData['id']]);
        $entity = new Eft;
        $entity->forCustomer($customer);
        $this->assertTrue($entity->validate('post'));

        // Set bsb and check validation changes
        $entity->setBsb('000000');
        $this->assertFalse($entity->validate('post'));

        // Add prefix and check validation again
        $entity->setPrefix('000');
        $this->assertTrue($entity->validate('post'));
    }
}
