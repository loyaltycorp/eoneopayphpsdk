<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity\Customer\ReferenceNumber;

use EoneoPaySdk\Entity\Customer;
use EoneoPaySdk\Entity\Customer\ReferenceNumber;
use EoneoPaySdk\Entity\Customer\ReferenceNumber\Bpay;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Customer\ReferenceNumber\Bpay
 */
class BpayTest extends EntityTestCase
{
    private $data = [
        'allocated' => true,
        'allocation_date' => '2017-01-01',
        'customer_id' => 'cus_000000000000000000000000',
        'created_at' => '2017-01-01 00:00:00',
        'payment_facility' => [
            'biller_code' => '000000',
            'clearing_account' => 0,
            'default' => true,
            'id' => 1,
            'type' => 'bpay',
        ],
        'reference_number' => '0000000000',
        'type' => 'bpay',
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Test creating an bpay allocation
     *
     * @return void
     */
    public function testCreateBpayAllocation() : void
    {
        // Create customer, entity and expected response
        $customer = new Customer(['id' => $this->customerData['id']]);
        $entity = new Bpay;
        $response = ['reference_numbers' => $this->data];

        // Attach to customer
        $entity->forCustomer($customer);

        $this->runEntityTests('create', $entity, '0', 'Customer updated.', $response);
    }

    /**
     * Test generating a barcode
     *
     * @return void
     */
    public function testGenerateBpayAdvice() : void
    {
        // Create entity
        $entity = new ReferenceNumber($this->data);

        // Run tests on raw image
        $this->runRawTests('get', $entity, file_get_contents(__DIR__ . '/../../../Assets/bpay-advice.png'));
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set updated data
        $updated = [
            'biller_code' => '000001',
        ];

        $this->runMutableAttributeTests(Bpay::class, $this->data['payment_facility'], $updated, 'default');
    }

    /**
     * Test end point changes
     *
     * @return void
     */
    public function testPostEndPoints() : void
    {
        // Create customer, entity and test default endpoint
        $customer = new Customer(['id' => $this->customerData['id']]);
        $entity = new Bpay;
        $entity->forCustomer($customer);
        $endpoint = sprintf('customers/%s/bpayCrn', $this->customerData['id']);
        $this->assertSame($endpoint, $entity->getEndpointFromMethod('post'));

        // Set biller_code and pip_id and check endpoint changes
        $entity->setBillerCode('000000')->setPipId('000');
        $endpoint = sprintf('customers/%s/bpayCrnForBillerCode/000000/AndPIPId/000', $this->customerData['id']);
        $this->assertSame($endpoint, $entity->getEndpointFromMethod('post'));
    }

    /**
     * Test validation changes
     *
     * @return void
     */
    public function testValidation() : void
    {
        // Create customer, entity and test default validation
        $customer = new Customer(['id' => $this->customerData['id']]);
        $entity = new Bpay;
        $entity->forCustomer($customer);
        $this->assertTrue($entity->validate('post'));

        // Set biller_code and check validation changes
        $entity->setBillerCode('000000');
        $this->assertFalse($entity->validate('post'));

        // Add pip_id and check validation again
        $entity->setPipId('000');
        $this->assertTrue($entity->validate('post'));
    }
}
