<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity\Customer\Subscription;

use EoneoPaySdk\Entity\Customer\Subscription;
use EoneoPaySdk\Entity\Customer\Subscription\Statement;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Customer\Subscription\Statement
 */
class StatementTest extends EntityTestCase
{
    /**
     * Test getting a statement for an subscription
     *
     * @return void
     */
    public function testGetStatement() : void
    {
        $data = [
            'charges' => [],
            'end_date' => '2017-01-01 00:00:00',
            'outstanding_amount' => 0,
            'payments' => [],
            'start_date' => '2017-01-31 23:59:59',
        ];

        // Set up subscription, entity and attach them
        $subscription = new Subscription([
            'id' => 'sub_000000000000000000000000',
            'customer_id' => $this->customerData['id']
        ]);
        $entity = (new Statement)->forSubscription($subscription);
        $response = ['subscription_statement' => $data];

        // Run assertions
        $this->runEntityTests('get', $entity, '0', 'Subscription statement found.', $response);
    }

    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters() : void
    {
        $entity = new Statement;
        $this->assertInstanceOf(Collection::class, $entity->getCharges());
        $this->assertInstanceOf(Collection::class, $entity->getPayments());
    }

    /**
     * Test attempting to attach an invalid subscription to a statement
     *
     * @return void
     */
    public function testInvalidSubscriptionAttachment() : void
    {
        $this->expectException(InvalidEntityException::class);

        // Set up entity and subscription without a customer
        $subscription = new Subscription(['id' => 'sub_000000000000000000000000']);
        (new Statement)->forSubscription($subscription);
    }

    /**
     * Test specific billing period can be set
     *
     * @return void
     */
    public function testStatementSetDate() : void
    {
        // Instantiate statement and set date then test result
        $entity = (new Statement)->forDate('July 2017');

        // Test conversion worked
        $this->assertSame('2017-07-01', $entity->getDate());
    }
}
