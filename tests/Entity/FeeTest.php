<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\Fee;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Fee
 */
class FeeTest extends EntityTestCase
{
    /**
     * Fee test data
     *
     * @var array
     */
    private $data = [
        'card_rates' => [
            'AMEX' => 2.9,
            'MasterCard' => 1.5,
            'Visa' => 1.5,
        ],
        'merchant_id' => '*',
        'payment_type' => 'cc_payment',
        'transaction_fee' => '20',
    ];

    /**
     * Test deleting a fee
     *
     * @return void
     */
    public function testDeleteFee() : void
    {
        // Create entity
        $entity = new Fee($this->data);

        $this->runEntityTests('delete', $entity, '0', 'Fee deleted.');
    }

    /**
     * Test getting a fee by payment type
     *
     * @return void
     */
    public function testGetFeeByPaymentType() : void
    {
        // Create entity and expected response
        $entity = new Fee(['payment_type' => $this->data['payment_type']]);
        $response = ['fee' => $this->data];

        $this->runEntityTests('get', $entity, '0', 'Fee found.', $response);
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set original and updated data
        $updated = [
            'transaction_fee' => 100,
        ];

        $this->runMutableAttributeTests(Fee::class, $this->data, $updated, 'merchant_id');
    }

    /**
     * Test setting card rates
     *
     * @return void
     */
    public function testSetCardRates() : void
    {
        $entity = new Fee($this->data);
        $this->assertSame($this->data['card_rates'], $entity->getCardRates()->toArray());

        // Update rates and test again
        $updated = [
            'AMEX' => 5.0,
            'MasterCard' => 1.7,
            'Visa' => 2.0,
        ];

        $entity->getCardRates()->setAmex($updated['AMEX']);
        $entity->getCardRates()->setMasterCard($updated['MasterCard']);
        $entity->getCardRates()->setVisa($updated['Visa']);
        $this->assertSame($updated, $entity->getCardRates()->toArray());
    }

    /**
     * Test setting the fee
     *
     * @return void
     */
    public function testSetFee() : void
    {
        // Set up entity and response data
        $entity = new Fee($this->data);
        $response = ['fee' => $this->data];

        $this->runEntityTests('update', $entity, '0', 'Fee updated.', $response);
    }
}
