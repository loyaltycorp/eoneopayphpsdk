<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity\Merchant;

use EoneoPaySdk\Entity\Fee\CardRate;
use EoneoPaySdk\Entity\Merchant;
use EoneoPaySdk\Entity\Merchant\Fee;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Merchant\Fee
 */
class FeeTest extends EntityTestCase
{
    /**
     * Fee test data
     *
     * @var array
     */
    private $data = [
        'card_rates' => [
            'AMEX' => 2.8,
            'MasterCard' => 1.4,
            'Visa' => 1.4,
        ],
        'merchant_id' => 'mer_0000000000000000',
        'payment_type' => 'cc_refund',
        'transaction_fee' => '20',
    ];

    /**
     * Test deleting a merchant fee
     *
     * @return void
     */
    public function testDeleteMerchantFee() : void
    {
        // Create merchant and entity
        $merchant = new Merchant($this->merchantData);
        $entity = new Fee($this->data);

        // Attach to merchant
        $entity->forMerchant($merchant);

        $this->runEntityTests('delete', $entity, '0', 'MerchantFee deleted.');
    }

    /**
     * Test getting a merchant fee by payment type
     *
     * @return void
     */
    public function testGetMerchantFeeByPaymentType() : void
    {
        // Create merchant, entity and expected response
        $merchant = new Merchant($this->merchantData);
        $entity = new Fee(['payment_type' => $this->data['payment_type']]);
        $response = ['fee' => $this->data];

        // Attach to merchant
        $entity->forMerchant($merchant);

        $this->runEntityTests('get', $entity, '0', 'MerchantFee found.', $response);
    }

    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters() : void
    {
        $entity = new Fee;

        $this->assertInstanceOf(CardRate::class, $entity->getCardRates());
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set original and updated data
        $updated = [
            'transaction_fee' => 100,
        ];

        $this->runMutableAttributeTests(Fee::class, $this->data, $updated, 'merchant_id');
    }

    /**
     * Test setting the fee
     *
     * @return void
     */
    public function testSetMerchantFee() : void
    {
        // Set up merchant, entity and response data
        $merchant = new Merchant($this->merchantData);
        $entity = new Fee($this->data);
        $response = ['fee' => $this->data];

        // Attach to merchant
        $entity->forMerchant($merchant);

        $this->runEntityTests('update', $entity, '0', 'MerchantFee updated.', $response);
    }
}
