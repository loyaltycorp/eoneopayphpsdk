<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity\Merchant;

use EoneoPaySdk\Entity\Merchant\Transfer;
use EoneoPaySdk\Entity\Payment;
use EoneoPaySdk\Entity\PaymentSource\BankAccount;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Merchant\Transfer
 */
class TransferTest extends EntityTestCase
{
    private $data = [
        'amount' => 100,
        'created_at' => '2017-01-01 00:00:00',
        'currency' => 'AUD',
        'destination' => 'src_000000000000000000000000',
        'id' => 'tfr_000000000000000000000000',
        'merchant_id' => 'mer_0000000000000000',
        'status' => 0,
        'status_date' => '2017-01-01 00:00:00',
        'status_reason' => '',
        'transaction_list' => [
            'txn_000000000000000000000000',
        ],
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Test creating a transfer
     *
     * @return void
     */
    public function testCreateTransfer() : void
    {
        // Set up entity and response data
        $entity = new Transfer([
            'amount' => $this->data['amount'],
        ]);
        $bankAccount = new BankAccount(['id' => $this->data['destination']]);
        $payment = new Payment(['txn_id' => 'txn_000000000000000000000000']);
        $response = ['transfer' => $this->data];

        // Add destination to transfer
        $entity->toBankAccount($bankAccount);

        // Add payment to transfer
        $entity->addPayment($payment);

        $this->runEntityTests('create', $entity, '0', 'transfer created.', $response);
    }

    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters() : void
    {
        $entity = new Transfer;
        $this->assertInstanceOf(Collection::class, $entity->getPayments());
        $this->assertInstanceOf(Collection::class, $entity->getTransactionList());
    }

    /**
     * Test getting a transfer by id
     *
     * @return void
     */
    public function testGetTransferById() : void
    {
        // Set up entity and response data
        $entity = new Transfer(['id' => $this->data['id']]);
        $response = ['transfer' => $this->data];

        $this->runEntityTests('get', $entity, '0', 'Transfer found.', $response);
    }

    /**
     * Test attempting to use an invalid bank account
     *
     * @return void
     */
    public function testInvalidBankAccountAttachment() : void
    {
        $bankAccount = new BankAccount;
        $entity = new Transfer;

        $this->expectException(InvalidEntityException::class);
        $entity->toBankAccount($bankAccount);
    }

    /**
     * Test attempting to use an invalid payment
     *
     * @return void
     */
    public function testInvalidPaymentAttachment() : void
    {
        $payment = new Payment;
        $entity = new Transfer;

        $this->expectException(InvalidEntityException::class);
        $entity->addPayment($payment);
    }

    /**
     * Test attempting to add a payment to a transfer with an invalid list
     *
     * @return void
     */
    public function testInvalidTransactionListAttachment() : void
    {
        $payment = new Payment(['txn_id' => 'txn_000000000000000000000000']);
        $entity = new Transfer(['transaction_list' => 'test']);

        // This should automatically create a collection
        $entity->addPayment($payment);
        $this->assertInstanceOf(Collection::class, $entity->getTransactionList());
        $this->assertCount(1, $entity->getTransactionList()->toArray());
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set updated data
        $updated = [
            'amount' => 101,
            'description' => 'Test',
        ];

        $this->runMutableAttributeTests(Transfer::class, $this->data, $updated, 'destination');
    }
}
