<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity\Merchant;

use EoneoPaySdk\Entity\Merchant;
use EoneoPaySdk\Entity\Merchant\Balance;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Merchant\Balance
 */
class BalanceTest extends EntityTestCase
{
    /**
     * Test getting balance from the api
     *
     * @return void
     */
    public function testGetBalance() : void
    {
        // Set entity and expected response
        $entity = new Balance;
        $response = [
            'balance' => ['total' => ['AUD' => ['amount' => 10000]], 'available' => ['AUD' => ['amount' => 5000]]]
        ];

        // Run assertions
        $this->runEntityTests('get', $entity, '0', 'Balance found.', $response);
    }

    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters() : void
    {
        $entity = new Balance;

        $this->assertInstanceOf(Repository::class, $entity->getAvailable());
        $this->assertInstanceOf(Repository::class, $entity->getTotal());
    }

    /**
     * Test updating available balance from the API
     *
     * @return void
     */
    public function testUpdateBalance() : void
    {
        // Set entity, merchant and expected response
        $entity = new Balance;
        $merchant = new Merchant($this->merchantData);
        $response = ['merchant' => $this->merchantData];

        // Attach to merchant
        $entity->forMerchant($merchant);

        // Run assertions
        $this->runEntityTests('update', $entity, '0', 'Merchant updated.', $response);
    }
}
