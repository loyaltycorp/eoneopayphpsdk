<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity\Merchant\Balance;

use EoneoPaySdk\Entity\Merchant\Balance\History;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Merchant\Balance\History
 */
class HistoryTest extends EntityTestCase
{
    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters() : void
    {
        $entity = new History;

        $this->assertInstanceOf(Repository::class, $entity->getBalance());
        $this->assertInstanceOf(Collection::class, $entity->getPayments());
        $this->assertInstanceOf(Collection::class, $entity->getTransfers());
    }
}
