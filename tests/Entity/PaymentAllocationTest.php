<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\PaymentAllocation;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\PaymentAllocation
 */
class PaymentAllocationTest extends EntityTestCase
{
    /**
     * Test getting a payment allocation by id
     *
     * @return void
     */
    public function testGetPaymentAllocationById() : void
    {
        $data = [
            'allocation_date' => '2017-01-01',
            'allocation_id' => 'XXX00000',
            'allocation_type' => 'App\\Models\\EWallet',
            'amount' => 100,
            'created_at' => '2017-01-01 00:00:00',
            'customer_id' => 'cus_000000000000000000000000',
            'deallocated' => false,
            'fee_amount' => 10,
            'id' => 'pal_0000000000000001',
            'merchant_id' => 'mer_0000000000000000',
            'txn_id' => 'txn_000000000000000000000000',
            'updated_at' => '2017-01-01 00:00:00',
            'version' => 1,
        ];

        // Set up entity and expected response
        $entity = new PaymentAllocation(['id' => $data['id']]);
        $response = ['payment_allocation' => $data];

        $this->runEntityTests('get', $entity, '0', 'Payment allocation found.', $response);
    }
}
