<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\ApiKey;
use EoneoPaySdk\Entity\Merchant;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\ApiKey
 */
class ApiKeyTest extends EntityTestCase
{
    /**
     * Api key test data
     *
     * @var array
     */
    private $data = [
        'api_key' => 'sk_test_000000000000000000000000',
        'api_permissions' => [
            'balance',
            'charges',
            'customers',
            'merchants/mer_0000000000000000',
            'merchants/self',
            'overdueSubscriptions',
            'payments',
            'plans',
            'search',
            'subscriptions',
            'testBatchPayments',
            'testAccountInformation',
            'tokens',
            'transfers',
            'webhooks',
        ],
        'api_status' => true,
        'created_at' => '2017-01-01 00:00:00',
        'merchant_id' => 'mer_0000000000000000',
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Test revoking an api key
     *
     * @return void
     */
    public function testDeleteApiKey() : void
    {
        // Create entity
        $entity = new ApiKey($this->data);

        $this->runEntityTests('delete', $entity, '0', 'API key revoked.');
    }

    /**
     * Test getting an api key by api key
     *
     * @return void
     */
    public function testGetApiKeyByApiKey() : void
    {
        // Create entity and expected response
        $entity = new ApiKey(['api_key' => $this->data['api_key']]);
        $response = ['api_key' => $this->data];

        $this->runEntityTests('get', $entity, '0', 'API key found.', $response);
    }

    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters() : void
    {
        $entity = new ApiKey;

        $this->assertInstanceOf(Collection::class, $entity->getApiPermissions());
    }

    /**
     * Test adding a merchant to an api key
     *
     * @return void
     */
    public function testMerchantAttachment() : void
    {
        // Create merchant and entity
        $merchant = new Merchant($this->merchantData);
        $entity = new ApiKey;

        // Attach to merchant and check permissions update
        $entity->forMerchant($merchant);
        $this->assertSame(
            ['merchant/' . $merchant->getId(), 'merchant/self'],
            $entity->getApiPermissions()->toArray()
        );
    }

    /**
     * Test adding an invalid merchant
     *
     * @return void
     */
    public function testInvalidMerchantAttachment() : void
    {
        // Create merchant and entity
        $merchant = new Merchant;
        $entity = new ApiKey;

        $this->expectException(InvalidEntityException::class);
        $entity->forMerchant($merchant);
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set original and updated data
        $updated = ['api_status' => false];
        $this->runMutableAttributeTests(ApiKey::class, $this->data, $updated, 'api_key');
    }

    /**
     * Test updating a charge
     *
     * @return void
     */
    public function testUpdateCharge() : void
    {
        // Set up entity and response data
        $entity = new ApiKey($this->data);
        $entity->setApiStatus(0)->getApiPermissions()->deleteNth(1);
        $response = ['api_key' => array_merge($this->data, ['api_status' => false])];
        unset($response['api_key']['api_permissions'][0]);

        $this->runEntityTests('update', $entity, '0', 'API key updated.', $response);
    }
}
