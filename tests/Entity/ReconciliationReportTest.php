<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\ReconciliationReport;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\ReconciliationReport
 */
class ReconciliationReportTest extends EntityTestCase
{
    /**
     * Test getting a reconciliation report by id
     *
     * @return void
     */
    public function testGetReconciliationReportById() : void
    {
        $data = [
            'bank_transaction_id' => '0000000000-1',
            'created_at' => '2017-01-01 00:00:00',
            'externalRecord' => 'eJzNWF1v2jAUfedX+LF9YE0CHaXSJgVKt0orTCFoD6iajON23hIHOc5U9utn8lUSnMRJo1KLh0DuPb4+9/a==',
            'file' => 'receive/acct_info/0000000000',
            'id' => 'NAI-receive/acct_info/0000000000-1',
            'internalRecord' => 'r6mPUX5ofba2AyBne9s=',
            'message' => 'Exception thrown during nai processing.',
            'status' => 0,
            'updated_at' => '2017-01-01 00:00:00',
            'version' => 1,
        ];

        // Set up entity and expected response
        $entity = new ReconciliationReport(['id' => $data['id']]);
        $response = ['reconciliation_report' => $data];

        $this->runEntityTests('get', $entity, '0', 'ReconciliationReport found.', $response);
    }
}
