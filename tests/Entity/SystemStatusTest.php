<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\SystemStatus;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\SystemStatus
 */
class SystemStatusTest extends EntityTestCase
{
    /**
     * Test mutators on certain attributes
     *
     * @return void
     */
    public function testAttributeMutators() : void
    {
        // Set original and expected values
        $original = ['from_date' => '1 July 2017', 'to_date' => 'September 2019'];
        $expected = [
            'from_date' => '2017-07-01',
            'to_date' => '2019-09-01',
        ];

        $this->runAttributeMutatorTests(SystemStatus::class, $original, $expected);
    }

    /**
     * Test get system status
     *
     * @return void
     */
    public function testGetSystemStatus() : void
    {
        $data = [
            'payment_allocations' => [
                'all' => [
                    'count' => 1,
                    'total_amount' => 100,
                ],
                'by_type' => [
                    'App\\Models\\EWallet' => [
                        'count' => 1,
                        'total_amount' => 100,
                    ],
                ],
            ],
            'payments' => [
                'all' => [
                    'count' => 1,
                    'total_amount' => 100,
                    'total_fee_amount' => 10,
                ],
                'by_merchant' => [
                    'mer_0000000000000000' => [
                        'count' => 1,
                        'total_amount' => 100,
                        'total_fee_amount' => 10,
                    ],
                ],
                'by_type' => [
                    'cc_payment' => [
                        'count' => 1,
                        'total_amount' => 100,
                        'total_fee_amount' => 10,
                    ],
                ],
            ],
            'transfers' => [
                'all' => [
                    'count' => 1,
                    'total_amount' => 100,
                ],
                'by_status' => [
                    '5' => [
                        'count' => 1,
                        'total_amount' => 100,
                    ],
                ],
            ],
        ];

        // Create entity and expected response
        $entity = new SystemStatus;
        $response = ['system_status' => $data];

        $this->runEntityTests('get', $entity, '0', 'System status found.', $response);
    }

    /**
     * Test getters
     *
     * @return void
     */
    public function testGetters() : void
    {
        $entity = new SystemStatus;
        $this->assertInstanceOf(Repository::class, $entity->getPaymentAllocations());
        $this->assertInstanceOf(Repository::class, $entity->getPayments());
        $this->assertInstanceOf(Repository::class, $entity->getTransfers());
    }
}
