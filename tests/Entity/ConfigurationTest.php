<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\Configuration;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Configuration
 */
class ConfigurationTest extends EntityTestCase
{
    /**
     * Test getting a payment allocation by id
     *
     * @return void
     */
    public function testGetPaymentAllocationById() : void
    {
        $data = [
            'key' => 'merchant_transfer_day',
            'value' => 1,
        ];

        // Set up entity and expected response
        $entity = new Configuration(['key' => $data['key']]);
        $response = ['configuration' => $data];

        $this->runEntityTests('get', $entity, '0', 'Configuration found.', $response);
    }

    /**
     * Test setting a payment allocation by id
     *
     * @return void
     */
    public function testSetPaymentAllocationById() : void
    {
        $data = [
            'key' => 'merchant_transfer_day',
            'value' => 1,
        ];

        // Set up entity and expected response
        $entity = new Configuration($data);
        $response = ['configuration' => $data];

        $this->runEntityTests('update', $entity, '0', 'Configuration updated.', $response);
    }
}
