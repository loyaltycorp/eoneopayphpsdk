<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\Customer;
use EoneoPaySdk\Entity\Ewallet;
use EoneoPaySdk\Entity\Merchant;
use Tests\EoneoPaySdk\EntityTestCase;

/**
 * @covers \EoneoPaySdk\Entity\Ewallet
 */
class EwalletTest extends EntityTestCase
{
    /**
     * Set example ewallet data
     *
     * @var array
     */
    private $data = [
        'balance' => 0,
        'clearingAccount' => 1,
        'created_at' => '2017-01-01 00:00:00',
        'customer_id' => 'cus_000000000000000000000000',
        'id' => '0XXXX00',
        'locked_until_date' => '2017-01-01 00:00:00',
        'meta' => 'Meta',
        'name' => 'eWallet',
        'source_id' => 'src_000000000000000000000000',
        'updated_at' => '2017-01-01 00:00:00',
    ];

    /**
     * Test creating an ewallet
     *
     * @return void
     */
    public function testCreateEwallet() : void
    {
        // Create entity and expected response
        $customer = new Customer($this->customerData);
        $entity = new Ewallet;
        $response = ['ewallet' => $this->data];

        // Attach wallet to customer
        $entity->forCustomer($customer);

        $this->runEntityTests('create', $entity, '0', 'eWallet created.', $response);
    }

    /**
     * Test deleting an ewallet
     *
     * @return void
     */
    public function testDeleteEwallet() : void
    {
        // Create entity and expected response
        $entity = new Ewallet($this->data);

        $this->runEntityTests('delete', $entity, '0', 'eWallet deleted.');
    }

    /**
     * Test deleting a primary ewallet
     *
     * @return void
     */
    public function testDeletePrimaryEwallet() : void
    {
        // Create entity
        $entity = new Ewallet($this->data);

        $this->runEntityTests(
            'delete',
            $entity,
            '14103',
            'Deleting a primary ewallet of a customer is not allowed',
            [],
            409
        );
    }

    /**
     * Test getting an ewallet for a customer
     *
     * @return void
     */
    public function testGetCustomerEwallet() : void
    {
        // Remove customer id so request fails if not attached
        $data = $this->data;
        unset($data['customer_id']);

        // Create customer, entity and expected response
        $customer = new Customer($this->customerData);
        $entity = new Ewallet($data);
        $response = ['ewallet' => $this->data];

        // Attach wallet to customer
        $entity->forCustomer($customer);

        $this->runEntityTests('get', $entity, '0', 'eWallet found.', $response);
    }

    /**
     * Test getting an ewallet for a merchant
     *
     * @return void
     */
    public function testGetMerchantEwallet() : void
    {
        // Remove customer id to attach to merchant
        $data = $this->data;
        unset($data['customer_id']);

        // Create merchant, entity and expected response
        $merchant = new Merchant($this->merchantData);
        $entity = new Ewallet($data);
        $response = ['ewallet' => $this->data];

        // Attach wallet to merchant
        $entity->forMerchant($merchant);

        $this->runEntityTests('get', $entity, '0', 'eWallet found.', $response);
    }

    /**
     * Test getting the primary ewallet for a user
     *
     * @return void
     */
    public function testGetPrimaryEwallet() : void
    {
        // Remove customer id to attach to merchant
        $data = $this->data;
        unset($data['customer_id']);

        // Create merchant, entity (without an id) and expected response
        $merchant = new Merchant($this->merchantData);
        $entity = new Ewallet;
        $response = ['ewallet' => $this->data];

        // Attach wallet to merchant
        $entity->forMerchant($merchant);

        $this->runEntityTests('get', $entity, '0', 'eWallet found.', $response);
    }

    /**
     * Test the mutable attributes
     *
     * @return void
     */
    public function testMutableAttributes() : void
    {
        // Set updated data
        $updated = [
            'locked_until_date' => '2018-01-01 00:00:00',
            'meta' => [],
            'name' => 'Test',
        ];

        $this->runMutableAttributeTests(Ewallet::class, $this->data, $updated, 'customer_id');
    }

    /**
     * Test updating an ewallet
     *
     * @return void
     */
    public function testUpdateEwallet() : void
    {
        // Set up entity and response data
        $entity = new Ewallet(array_merge($this->data, ['name' => 'Updated']));
        $response = ['ewallet' => array_merge($this->data, $entity->toArray())];

        $this->runEntityTests('update', $entity, '0', 'eWallet updated.', $response);
    }
}
