<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource\Merchant\Balance;

use EoneoPaySdk\Resource\Merchant\Balance\History;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\Merchant\Balance\History
 */
class HistoryTest extends ResourceTestCase
{
    /**
     * Test getting balance history from the api
     *
     * @return void
     */
    public function testGetBalanceHistory() : void
    {
        // Set list for API response
        $list = [
            'balance' => [
                'total' => [
                    'AUD' => ['amount' => 5000],
                ],
                'available' => [
                    'AUD' => ['amount' => 2500]
                ],
            ],
            'payments' => [
                [
                    'amount' => 100,
                    'card_type' => 'Visa',
                    'card_type_id' => 6,
                    'created_at' => '2017-01-01 00:00:00',
                    'currency' => 'AUD',
                    'customer_id' => 'cus_000000000000000000000000',
                    'external_customer_id' => 'cus_000000000000000000000000',
                    'external_txn_id' => 'xxxx0x0XXXX0',
                    'fee_amount' => 20,
                    'fee_schedule' => [
                        'card_rates' => [
                            'AMEX' => 2.9,
                            'MasterCard' => 1.5,
                            'Visa' => 1.5,
                        ],
                        'merchant_id' => '*',
                        'payment_type' => 'cc_payment',
                        'transaction_fee' => '20',
                    ],
                    'merchant_transfer_status' => 1,
                    'reference' => 'Test',
                    'reference_number' => 'XXXX0X0',
                    'statement_descriptor' => '',
                    'status' => 0,
                    'token' => 'src_000000000000000000000000',
                    'txn_date' => '2017-01-01 00:00:00',
                    'txn_id' => 'txn_000000000000000000000001',
                    'type' => 'cc_payment',
                    'updated_at' => '2017-01-01 00:00:00',
                ]
            ],
            'transfers' => [],
        ];

        // Run assertions
        $this->runResourceTests(new History, 'Balance history found.', $list, 'balance_history');
    }
}
