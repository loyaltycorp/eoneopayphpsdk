<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource\Merchant;

use EoneoPaySdk\Resource\Merchant\Transfer;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\Merchant\Transfer
 */
class TransferTest extends ResourceTestCase
{
    /**
     * Test mutators on certain attributes within a filter
     *
     * @return void
     */
    public function testFilterAttributeMutators() : void
    {
        // Set original and expected values
        $original = ['status_date' => '1 July 2017'];
        $expected = ['status_date' => '2017-07-01 00:00:00'];

        $this->runFilterAttributeMutatorTests(Transfer::class, $original, $expected);
    }

    /**
     * Test getting all transfers from the API
     *
     * @return void
     */
    public function testGetAllTransfers() : void
    {
        $data = [
            'amount' => 100,
            'created_at' => '2017-01-01 00:00:00',
            'currency' => 'AUD',
            'destination' => 'src_000000000000000000000000',
            'id' => 'tfr_000000000000000000000000',
            'merchant_id' => 'mer_0000000000000000',
            'payments' => [
                'txn_000000000000000000000000',
            ],
            'status' => 0,
            'status_date' => '2017-01-01 00:00:00',
            'status_reason' => '',
            'transaction_list' => [],
            'updated_at' => '2017-01-01 00:00:00',
        ];

        // Set list for API response
        $list = [
            $data,
            array_merge($data, ['id' => 'tfr_000000000000000000000001'])
        ];

        // Run assertions
        $this->runResourceTests(new Transfer, 'Token found.', $list);
    }
}
