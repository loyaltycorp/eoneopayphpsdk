<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource;

use EoneoPaySdk\Resource\ReconciliationReport;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\ReconciliationReport
 */
class ReconciliationReportTest extends ResourceTestCase
{
    /**
     * Test getting all reports from the API
     *
     * @return void
     */
    public function testGetAllReconciliationReports() : void
    {
        // Set list for API response
        $list = [
            [
                'created_at' => '2017-01-01 00:00:00',
                'externalRecord' => [
                    'Amount' => 100,
                    'Biller Code' => '0000254169',
                    'Customer Reference Number' => '0000000000',
                    'Error Correction Reason' => '000',
                    'Filler' => '',
                    'Original Reference Number' => '',
                    'Payment Date' => '20170101',
                    'Payment DateTime' => '',
                    'Payment Instruction Type' => 'Payment',
                    'Payment Instruction Type Code' => '05',
                    'Payment Time' => '000000',
                    'Record Type' => '50',
                    'Settlement Date' => '20170101',
                    'Settlement DateTime' => '',
                    'Transaction Reference Number' => 'XXX2017010100000000',
                ],
                'file' => 'receive/bpay_brf/000000_000000.BRF',
                'id' => 'bpay-XXX2017010100000000',
                'message' => 'Customer not found',
                'status' => 0,
                'updated_at' => '2017-01-01 00:00:00',
                'version' => 1,
            ],
            [
                'bank_transaction_id' => '0000000000-1',
                'created_at' => '2017-01-01 00:00:00',
                'externalRecord' => 'eJzNWG1v2jAQ/itWP3UfWJNQRqm0SYGyadJapBC0D6WajON23hIbOc409utn8gZNnMRJo1ILoQB3==',
                'file' => 'receive/acct_info/0000000000',
                'id' => 'NAI-receive/acct_info/0000000000-1',
                'internalRecord' => 'eJxTyovRT1QCAAXZAZ9=',
                'message' => 'Exception thrown during nai processing.',
                'status' => 0,
                'updated_at' => '2017-01-01 00:00:00',
                'version' => 1,
            ],
        ];

        // Run assertions
        $this->runResourceTests(new ReconciliationReport, 'ReconciliationReport found.', $list);
    }
}
