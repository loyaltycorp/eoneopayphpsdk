<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource;

use EoneoPaySdk\Resource\Plan;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\Plan
 */
class PlanTest extends ResourceTestCase
{
    /**
     * Test getting all customers from the API
     *
     * @return void
     */
    public function testGetAllCustomers() : void
    {
        // Set example plan data
        $data = [
            'amount' => 101,
            'created_at' => '2017-01-01 00:00:00',
            'currency' => 'USD',
            'id' => 'plan_000000000000000000000001',
            'interval' => 'week',
            'interval_count' => 1,
            'merchant_id' => 'mer_0000000000000001',
            'name' => 'Plan',
            'updated_at' => '2017-01-01 00:00:00',
        ];

        // Set list for API response
        $list = [
            $data,
            array_merge($data, ['id' => 'plan_000000000000000000000001'])
        ];

        // Run assertions
        $this->runResourceTests(new Plan, 'Charges found.', $list);
    }
}
