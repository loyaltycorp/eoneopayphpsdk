<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource;

use EoneoPaySdk\Resource\Webhook;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\Webhook
 */
class WebhookTest extends ResourceTestCase
{
    /**
     * Test getting all webhooks from the API
     *
     * @return void
     */
    public function testGetAllWebhooks() : void
    {
        //Example webhook data
        $data = [
            'active' => true,
            'created_at' => '2017-01-01 00:00:00',
            'event_name' => 'PaymentCreated',
            'id' => 'webhook_000000000000000000000000',
            'merchant_id' => 'mer_0000000000000000',
            'name' => 'Test',
            'target_url' => 'http://localhost',
            'updated_at' => '2017-01-01 00:00:00',
        ];

        // Set list for API response
        $list = [
            $data,
            array_merge($data, ['id' => 'webhook_000000000000000000000001'])
        ];

        // Run assertions
        $this->runResourceTests(new Webhook, 'Webhooks found.', $list);
    }
}
