<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource;

use EoneoPaySdk\Resource\BankTransaction;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\BankTransaction
 */
class BankTransactionTest extends ResourceTestCase
{
    /**
     * Test mutators on certain attributes within a filter
     *
     * @return void
     */
    public function testFilterAttributeMutators() : void
    {
        // Set original and expected values
        $original = ['txn_date' => '1 January 2017'];
        $expected = ['txn_date' => '2017-01-01'];

        $this->runFilterAttributeMutatorTests(BankTransaction::class, $original, $expected);
    }

    /**
     * Test getting all bank transactions from the API
     *
     * @return void
     */
    public function testGetAllBankTransactions() : void
    {
        $data = [
            'amount' => 100000000000,
            'clearing_account' => 1,
            'created_at' => '2017-01-01 00:00:00',
            'currency' => 'AUD',
            'description' => 'Inter-bank credits',
            'funds_type' => '0',
            'id' => '0000000000-1',
            'record_type' => '16',
            'reference_number' => '',
            'side' => 'CR',
            'text' => 'TEST',
            'transaction_code' => '920',
            'txn_date' => '2017-01-01',
            'updated_at' => '2017-01-01 00:00:00',
            'version' => 1,
        ];

        // Set list for API response
        $list = [
            $data,
            array_merge($data, ['id' => '0000000000-2'])
        ];

        // Run assertions
        $this->runResourceTests(new BankTransaction, 'Bank transactions found.', $list);
    }
}
