<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource;

use EoneoPaySdk\Resource\Search;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\Search
 */
class SearchTest extends ResourceTestCase
{
    /**
     * Test methods made available by the transactional trait
     *
     * @return void
     */
    public function testQuery(): void
    {
        // Create resource
        $resource = new Search;

        // Assert defaults
        $this->assertNull($resource->getQuery());
        $this->assertFalse($resource->hasQuery());

        // Add query
        $resource->query('Test');

        // Assert changes
        $this->assertSame('Test', $resource->getQuery());
        $this->assertTrue($resource->hasQuery());
    }

    /**
     * Test methods made available by the transactional trait
     *
     * @return void
     */
    public function testScope(): void
    {
        // Create resource
        $resource = new Search;

        // Assert defaults
        $this->assertNull($resource->getScope());
        $this->assertFalse($resource->hasScope());

        // Add scope
        $resource->scope(Search::SCOPE_MERCHANT);

        // Assert changes
        $this->assertSame(Search::SCOPE_MERCHANT, $resource->getScope());
        $this->assertTrue($resource->hasScope());
    }

    /**
     * Test searching
     *
     * @return void
     */
    public function testSearch() : void
    {
        // Set up resource
        $resource = new Search;
        $resource->query('Test')->scope(Search::SCOPE_CUSTOMER);

        // Run assertions
        $this->runResourceTests($resource, ':type found.', [$this->customerData]);
    }
}
