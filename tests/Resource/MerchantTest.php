<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource;

use EoneoPaySdk\Resource\Merchant;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\Merchant
 */
class MerchantTest extends ResourceTestCase
{
    /**
     * Test getting all merchants from the API
     *
     * @return void
     */
    public function testGetAllMerchants() : void
    {
        // Set list for API response
        $list = [
            $this->merchantData,
            array_merge($this->merchantData, ['id' => 'mer_0000000000000001'])
        ];

        // Run assertions
        $this->runResourceTests(new Merchant, 'Charges found.', $list);
    }
}
