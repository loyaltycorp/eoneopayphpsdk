<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource;

use EoneoPaySdk\Resource\Charge;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\Charge
 */
class ChargeTest extends ResourceTestCase
{
    /**
     * Test getting all charges for a merchant from the API
     *
     * @return void
     */
    public function testGetAllCharges() : void
    {
        $data = [
            'amount' => '200',
            'created_at' => '2017-01-01 00:00:00',
            'id' => 'chg_000000000000000000000000',
            'merchant_id' => 'mer_0000000000000000',
            'name' => 'Test',
            'outstanding_amount' => '200',
            'precendence' => 'last',
            'updated_at' => '2017-01-01 00:00:00',
        ];

        // Set list for API response
        $list = [
            $data,
            array_merge($data, ['id' => 'chg_000000000000000000000001'])
        ];

        // Run assertions
        $this->runResourceTests(new Charge, 'Charges found.', $list);
    }
}
