<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource;

use EoneoPaySdk\Resource\PaymentAllocation;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\PaymentAllocation
 */
class PaymentAllocationTest extends ResourceTestCase
{
    /**
     * Test mutators on certain attributes within a filter
     *
     * @return void
     */
    public function testFilterAttributeMutators() : void
    {
        // Set original and expected values
        $original = ['allocation_date' => '1 January 2017'];
        $expected = ['allocation_date' => '2017-01-01'];

        $this->runFilterAttributeMutatorTests(PaymentAllocation::class, $original, $expected);
    }

    /**
     * Test getting all payment allocations from the API
     *
     * @return void
     */
    public function testGetAllPaymentAllocations() : void
    {
        $data = [
            'allocation_date' => '2017-01-01',
            'allocation_id' => 'sub_0000000000000000',
            'allocation_type' => 'App\\Models\\Subscription',
            'amount' => 100,
            'created_at' => '2017-01-01 00:00:00',
            'customer_id' => 'cus_000000000000000000000000',
            'deallocated' => false,
            'fee_amount' => 10,
            'id' => 'pal_0000000000000000',
            'merchant_id' => 'mer_0000000000000000',
            'txn_id' => 'txn_000000000000000000000000',
            'updated_at' => '2017-01-01 00:00:00',
            'version' => 1,
        ];

        // Set list for API response
        $list = [
            $data,
            array_merge($data, ['txn_id' => 'txn_000000000000000000000001'])
        ];

        // Run assertions
        $this->runResourceTests(new PaymentAllocation, 'Payments found.', $list);
    }
}
