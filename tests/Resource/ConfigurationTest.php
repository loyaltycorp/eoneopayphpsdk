<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource;

use EoneoPaySdk\Resource\Configuration;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\Configuration
 */
class ConfigurationTest extends ResourceTestCase
{
    /**
     * Test getting configurations from the API
     *
     * @return void
     */
    public function testGetBalanceHistory() : void
    {
        // Set list for API response
        $list = [
            ['key' => 'allocate_indirect_payments', 'value' => true],
            ['key' => 'can_delete_default_payment', 'value' => false],
            ['key' => 'merchant_settlement_period', 'value' => 7],
            ['key' => 'merchant_transfer_day', 'value' => 1],
            ['key' => 'merchant_transfer_frequency', 'value' => 'weekly'],
        ];

        // Run assertions
        $this->runResourceTests(new Configuration, 'Configurations found.', $list);
    }
}
