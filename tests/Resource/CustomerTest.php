<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource;

use EoneoPaySdk\Resource\Customer;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\Customer
 */
class CustomerTest extends ResourceTestCase
{
    /**
     * Test getting all customers from the API
     *
     * @return void
     */
    public function testGetAllCustomers() : void
    {
        // Set list for API response
        $list = [
            $this->customerData,
            array_merge($this->customerData, ['id' => 'cus_000000000000000000000001'])
        ];

        // Run assertions
        $this->runResourceTests(new Customer, 'Charges found.', $list);
    }
}
