<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource;

use EoneoPaySdk\Resource\ClearingAccount;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\ClearingAccount
 */
class ClearingAccountTest extends ResourceTestCase
{
    /**
     * Test getting all clearing accounts
     *
     * @return void
     */
    public function testGetClearingAccounts() : void
    {
        // Clearing accoutn data
        $data = [
            'account' => '000000000',
            'bsb' => '000-000',
            'direct_entry_credit_description' => 'TEST',
            'direct_entry_credit_id' => '000001',
            'direct_entry_credit_name' => 'TEST',
            'direct_entry_debit_description' => 'TEST',
            'direct_entry_debit_id' => '000000',
            'direct_entry_debit_name' => 'TEST',
            'id' => 1,
            'merchant_id' => 'XXX0001',
            'merchant_password' => 'XXx0xxX0',
            'name' => 'TEST',
            'trust' => '0',
        ];

        // Set list for API response
        $list = [
            $data,
            array_merge($data, ['id' => 2])
        ];

        // Run assertions
        $this->runResourceTests(new ClearingAccount, 'Clearing accounts found.', $list);
    }
}
