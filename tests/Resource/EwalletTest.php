<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource;

use EoneoPaySdk\Entity\Merchant;
use EoneoPaySdk\Resource\Ewallet;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\Ewallet
 */
class EwalletTest extends ResourceTestCase
{
    /**
     * Test getting all ewallets for a merchant from the API
     *
     * @return void
     */
    public function testGetAllEwallets() : void
    {
        $data = [
            'balance' => 0,
            'clearingAccount' => 1,
            'created_at' => '2017-01-01 00:00:00',
            'id' => '0XXXX00',
            'merchant_id' => 'mer_0000000000000000',
            'name' => 'eWallet',
            'source_id' => 'src_000000000000000000000000',
            'updated_at' => '2017-01-01 00:00:00',
        ];

        // Set up merchant and entity, attach entity to merchant
        $merchant = new Merchant($this->merchantData);
        $entity = (new Ewallet)->forMerchant($merchant);

        // Set list for API response
        $list = [
            $data,
            array_merge($data, ['id' => '0XXXX01'])
        ];

        // Run assertions
        $this->runResourceTests($entity, 'eWallets found.', $list);
    }
}
