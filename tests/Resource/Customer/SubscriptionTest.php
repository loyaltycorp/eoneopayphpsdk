<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource\Customer;

use EoneoPaySdk\Entity\Customer;
use EoneoPaySdk\Resource\Customer\Subscription;
use EoneoPaySdk\Resource\Customer\Subscription\Overdue;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\Customer\Subscription
 */
class SubscriptionTest extends ResourceTestCase
{
    /**
     * Test getting all subscriptions for a customer from the API
     *
     * @return void
     */
    public function testGetAllSubscriptions() : void
    {
        $data = [
            'created_at' => '2017-01-01 00:00:00',
            'current_balance' => 100,
            'current_period_end' => '2017-01-01',
            'current_period_start' => '2017-01-01',
            'customer_id' => 'cus_000000000000000000000000',
            'end_date' => '2017-01-01',
            'id' => 'sub_000000000000000000000000',
            'merchant_id' => 'mer_0000000000000000',
            'plan_id' => 'plan_000000000000000000000000',
            'start_date' => '2017-01-01',
            'status' => 'active',
            'updated_at' => '2017-01-01 00:00:00',
        ];

        // Set up customer and entity, attach entity to customer
        $customer = new Customer($this->customerData);
        $entity = (new Subscription)->forCustomer($customer);

        // Set list for API response
        $list = [
            $data,
            array_merge($data, ['id' => 'sub_000000000000000000000001'])
        ];

        // Run assertions
        $this->runResourceTests($entity, 'Subscriptions found.', $list);
    }

    /**
     * Test getting overdue subscriptions
     *
     * @return void
     */
    public function testGetOverdueSubscriptions() : void
    {
        // Set up entity and list
        $entity = new Overdue;
        $list = [];

        // Run assertions
        $this->runResourceTests($entity, 'Subscriptions found.', $list);
    }

    /**
     * Test the different endpoints
     *
     * @return void
     */
    public function testEndpoints() : void
    {
        // Set up customer and entity, attach entity to customer
        $customer = new Customer($this->customerData);

        // Test endpoints
        $entity = new Subscription;
        $this->assertSame('subscriptions', $entity->getEndpointFromMethod('get'));

        // Attach to customer
        $entity->forCustomer($customer);
        $this->assertSame(
            sprintf('customers/%s/subscriptions', $this->customerData['id']),
            $entity->getEndpointFromMethod('get')
        );
    }
}
