<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk\Resource;

use EoneoPaySdk\Resource\Payment;
use Tests\EoneoPaySdk\ResourceTestCase;

/**
 * @covers \EoneoPaySdk\Resource\Payment
 */
class PaymentTest extends ResourceTestCase
{
    /**
     * Test mutators on certain attributes within a filter
     *
     * @return void
     */
    public function testFilterAttributeMutators() : void
    {
        // Set original and expected values
        $original = ['txn_date' => '1 January 2017'];
        $expected = ['txn_date' => '2017-01-01 00:00:00'];

        $this->runFilterAttributeMutatorTests(Payment::class, $original, $expected);
    }

    /**
     * Test getting all payments from the API
     *
     * @return void
     */
    public function testGetAllPayments() : void
    {
        $data = [
            'amount' => 50,
            'card_type' => 'Visa',
            'card_type_id' => 6,
            'created_at' => '2017-01-01 00:00:00',
            'currency' => 'AUD',
            'customer_id' => 'cus_000000000000000000000001',
            'external_customer_id' => 'cus_000000000000000000000001',
            'external_txn_id' => 'xxxx0x0XXXXX',
            'fee_amount' => 10,
            'fee_schedule' => [
                'card_rates' => [
                    'AMEX' => 2.9,
                    'MasterCard' => 1.5,
                    'Visa' => 1.5,
                ],
                'merchant_id' => '*',
                'payment_type' => 'cc_payment',
                'transaction_fee' => '10',
            ],
            'merchant_transfer_status' => 0,
            'reference' => 'Test',
            'reference_number' => 'XXXX0X1',
            'statement_descriptor' => '',
            'status' => 1,
            'token' => 'src_000000000000000000000001',
            'txn_date' => '2017-01-01 00:00:00',
            'txn_id' => 'txn_000000000000000000000001',
            'type' => 'cc_payment',
            'updated_at' => '2017-01-01 00:00:00',
        ];

        // Set list for API response
        $list = [
            $data,
            array_merge($data, ['txn_id' => 'cus_000000000000000000000002'])
        ];

        // Run assertions
        $this->runResourceTests(new Payment, 'Payments found.', $list);
    }
}
