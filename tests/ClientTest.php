<?php declare(strict_types = 1);

namespace Tests\EoneoPaySdk;

use EoneoPaySdk\Client;
use EoneoPaySdk\Sdk\Exceptions\InvalidApiKeyException;
use EoneoPaySdk\Sdk\Exceptions\InvalidMerchantIdException;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidBaseUrlException;

/**
 * @covers \EoneoPaySdk\Client
 */
class ClientTest extends TestCase
{
    /**
     * Test setting api key
     *
     * @return void
     */
    public function testSetApiKey() : void
    {
        $client = new Client($this->apiKey);

        // Test good api key
        $result = $client->setApiKey('pk_test_0000000000000000');
        $this->assertInstanceOf(Client::class, $result);

        // Test bad api key
        $this->expectException(InvalidApiKeyException::class);
        $client->setApiKey('Bob');
    }

    /**
     * Test setting the base url
     *
     * @return void
     */
    public function testSetBaseUrl() : void
    {
        $client = new Client($this->apiKey);

        // Test good url
        $result = $client->setBaseUrl('http://localhost');
        $this->assertInstanceOf(Client::class, $result);

        // Test bad url
        $this->expectException(InvalidBaseUrlException::class);
        $client->setBaseUrl('localhost');
    }

    /**
     * Test setting merchant id
     *
     * @return void
     */
    public function testSetMerchantId() : void
    {
        $client = new Client($this->apiKey);

        // Test good merchant
        $result = $client->setMerchantId('mer_0000000000000000');
        $this->assertInstanceOf(Client::class, $result);

        // Test bad merchant
        $this->expectException(InvalidMerchantIdException::class);
        $client->setMerchantId('Bob');
    }
}
