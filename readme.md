# Eoneo Pay PHP SDK

PHP SDK for interacting with the Eoneo Pay API.

## Contents

* [Changelog](#markdown-header-changelog)
* [Documentation](#markdown-header-documentation)

## Changelog

### Version 2.0.0

* Merge PHP SDK and Admin PHP SDK into a single SDK
* Change to semantic versioning to allow patches to allow better control over releases
* Completely rewrite code base to make requests and responses more consistent
* Deprecated version 1.x branch
* Change namespace to `EoneoPaySdk` to allow running of v2.x and v1.x concurrently

### Version 1.7

* Credit card payments are processed differently to bank account payments.
* A new CreditCardPayment object has been introduced. Use this object for all credit card payments
* A new BankAccountPayment object has been introduced. Use this object for all bank account payments
* A new PreAuth object has been intoduced for pre-authorised credit card payments.
* The PreAuth object supports a complete operation for completing pre-authorised payments. This operation returns a payment object.

## Documentation

* [Version 2.x.x](docs/v2.md#markdown-header-eoneo-pay-php-sdk)
* [Version 1.x](docs/v1.md#markdown-header-eoneopay-payment-gateway-api-client)
