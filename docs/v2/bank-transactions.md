# Bank transactions

Bank transactions are used to display credits and debits from a bank account.

## Contents

##### [Bank transaction entity](#markdown-header-bank-transaction-entity_1)
* [Allocate bank transaction](#markdown-header-allocate-bank-transaction)

##### [Bank transaction resource](#markdown-header-bank-transaction-resource_1)
* [Get all bank transactions](#markdown-header-get-all-bank-transactions)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* Bank transactions
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payment-allocations.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## Bank transaction entity

The bank transaction entity represents a single bank transaction.

### Class

`EoneoPaySdk\Entity\BankTransaction`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `allocation_id` | `string` | No | The EoneoPay id for the entity the to allocate this payment to |
| `allocation_type` | `string` | No | The allocation type to allocate this payment to |
| `amount` | `int` | No | The transaction amount |
| `clearing_account` | `int` | No | Clearing account id |
| `created_at` | `string` | No | SQL compatible date/time this transaction was created |
| `currency` | `string` | No | Transaction currency code in [ISO 4217 format](https://en.wikipedia.org/wiki/ISO_4217#Active_codes), always 'AUD' |
| `description` | `string` | No | Transaction description |
| `funds_type` | `string` | No | The funds type, always 0 |
| `id` | `string` | No | The EoneoPay id for this entity, based on the NAB transaction id |
| `record_type` | `string` | No | Institution record type, always 16 |
| `reference_number` | `string` | No | Bank reference number |
| `side` | `string` | No | Credit or debit, CR or DR |
| `text` | `string` | No | Transaction text |
| `transaction_code` | `string` | No | Institution transaction code |
| `txn_date` | `string` | No | The date the transaction was made |
| `updated_at` | `string` | No | SQL compatible date/time this transaction was last updated |
| `version` | `int` | No | The version/revision for this entity |

## Allocate bank transaction

A bank transaction can be allocated to an allocatable type ([charge](charges.md#markdown-header-charges), [eWallet](ewallets.md#markdown-header-ewallets), [subscription](subscriptions.md#markdown-header-subscriptions)) via the `allocateTo()` method.

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `id` | Yes | Yes | The NAB transaction id to allocate |

### Response attributes

The response will only contain the [standard response](../v2.md#markdown-header-responses) attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with id
$bankTransaction = new \EoneoPaySdk\Entity\BankTransaction([
	'id' => '0000000000-1',
]);

// Or instantiate and set id
$bankTransaction = (new \EoneoPaySdk\Entity\BankTransaction)
	->setId('0000000000-1');
	
// Allocate to ewallet assuming $ewallet is an existing
// \EoneoPaySdk\Entity\Ewallet instance
$bankTransaction->allocateTo($ewallet);

// Get bank transaction
$response = $client->create($bankTransaction);

// Check response
if ($response->isSuccessful()) {
	// Transaction was successfully allocated
}
```

## Bank transaction resource

The bank transaction resource represents a group of bank transaction entities.

Like most resources, [pagination](../v2.md#markdown-header-resources) can be used to limit results.

### Class

`EoneoPaySdk\Resource\BankTransaction`

### Filters

| Attribute | Description |
|-----------|-------------|
| `amount` | The transaction amount |
| `clearing_account` | Clearing account id |
| `created_at` | SQL compatible date/time this transaction was created |
| `currency` | Transaction currency code in [ISO 4217 format](https://en.wikipedia.org/wiki/ISO_4217#Active_codes), always 'AUD' |
| `description` | Transaction description |
| `funds_type` | The funds type, always 0 |
| `id` | The EoneoPay id for this entity, based on the NAB transaction id |
| `record_type` | Institution record type, always 16 |
| `reference_number` | Bank reference number |
| `side` | Credit or debit, CR or DR |
| `text` | Transaction text |
| `transaction_code` | Institution transaction code |
| `txn_date` | The date the transaction was made |
| `updated_at` | SQL compatible date/time this transaction was last updated |
| `version` | The version/revision for this entity |

## List all bank transactions

Bank transactions can be filtered via the [bank transaction resource](#markdown-header-bank-transaction-resource_1).

You can also search between transaction dates by using the `fromDate()` and `toDate()` methods.

### Response

Bank transactions found will be returned as a [collection](../v2.md#markdown-header-collections-and-repositories) of [bank transaction entities](#markdown-header-bank-transaction-entity_1). 

If no bank transactions are found, e.g. the filter returns no results or the page and limit exceeds available bank transactions, an empty collection will be returned.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `bank_transactions` | `BankTransaction[]` | Collection of [bank transactions entities](#markdown-header-bank-transactions-entity_1) |

If the collection is empty `hasBankTransactions()` will return false.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate bank transaction resource
$bankTransactions = new \EoneoPaySdk\Resource\BankTransaction;

// Optionally add filter results
$bankTransactions->whereAmount(100);

// Optionally set pagination and limit
$bankTransactions->limit(10)->page(1);

// Get bank transaction list
$response = $client->list($bankTransactions);

// Check response
if ($response->isSuccessful() && $response->hasBankTransactions()) {
	// \EoneoPaySdk\Sdk\Collection instance containing 
	// \EoneoPaySdk\Entity\BankTransaction entities 
	$bankTransactions = $response->getBankTransactions();
}
```
