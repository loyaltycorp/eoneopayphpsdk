# Tokenisation

Tokenisation can be used to store a credit card to charge at a later time without storing the actual credit card data.

## Contents

##### [Tokenisation entity](#markdown-header-tokenisation-entity_1)
* [Tokenise credit card](#markdown-header-tokenise-credit-card)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payments.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* Tokenisation
* [Webhooks](webhooks.md#markdown-header-webhooks)

## Tokenisation entity

The tokenisation resource is used to tokenise credit card details for storage.

### Class

`EoneoPaySdk\Entity\TokenisedCard`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `cardDescription` | string | No | The card type as a string, e.g. Visa |
| `cardType` | int | No | The card type as an integer |
| `cvc` | string | Yes | Credit card CVC/CVV |
| `expiryDate` | string | No | The expiry date in mm/yy format |
| `expiry_month` | string | Yes | The expiry month in m or mm format |
| `expiry_year` | string | Yes | The expiry year in yy or yyyy format |
| `id` | string | No | The EoneoPay id for this card |
| `name` | string | Yes | Credit card holder name |
| `number` | string | Yes | Credit card number |
| `pan` | string | No | Masked credit card number |
| `token` | string | No | Internal token for this credit card |

## Tokenise credit card

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `cvc`   | Yes        | Yes       | Credit card CVC/CVV |
| `expiry_month` | Yes | Yes | The expiry month in m or mm format |
| `expiry_year` | Yes | Yes | The expiry year in yy or yyyy format |
| `name` | No | No | Credit card holder name |
| `number` | Yes | Yes | Credit card number |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `tokenised_card` | `TokenisedCard` | [Tokenised card entity](#markdown-header-tokenisation-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with card data
$card = new \EoneoPaySdk\Entity\TokenisedCard([
    'cvc' => '123',
    'expiry_month' => '01',
    'expiry_year' => '2050',
    'name' => 'Test',
    'number' => '4444333322221111',
]);

// Or instantiate and set id
$card = (new \EoneoPaySdk\Entity\TokenisedCard)
	->setCvc('123')
	->setExpiryMonth('01')
	->setExpiryYear('2050')
	->setName('Test')
	->setNumber('4444333322221111');
	
// Tokenise card
$response = $client->create($card);

// Check response
if ($response->isSuccessful() && $response->hasTokenisedCard()) {
	// Tokenised \EoneoPaySdk\Entity\TokenisedCard instance
	$card = $response->getTokenisedCard();
}
```
