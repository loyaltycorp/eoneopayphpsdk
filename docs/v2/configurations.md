# Configurations

Configurations are a store of key/value pairs within EoneoPay.

## Contents

##### [Configuration entity](#markdown-header-configuration-entity_1)
* [Get configuration by key](#markdown-header-get-configuration-by-key)
* [Set configuration](#markdown-header-set-configuration)

##### [Configuration resource](#markdown-header-configuration-resource_1)
* [List all configurations](#markdown-header-list-all-configurations)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* Configuration
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payments.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## Configuration entity

The configuration entity represents a single configuration pair.

### Class

`EoneoPaySdk\Entity\Configuration`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `key` | `string` | No | Unique key within the EoneoPay system |
| `value` | `mixed` | Yes | Value for this configuration |

## Get configuration by key

A configuration can be retrieved via it's unique [key](#markdown-header-attributes).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `key` | Yes | No | Unique key within the EoneoPay system |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `configuration` | `Configuration` | Retrieved [configuration entity](#markdown-header-configuration-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with id
$configuration = new \EoneoPaySdk\Entity\Configuration([
	'key' => 'test_key',
]);

// Or instantiate and set id
$configuration = (new \EoneoPaySdk\Entity\Configuration)
	->setKey('test_key');

// Get configuration
$response = $client->get($configuration);

// Check response
if ($response->isSuccessful() && $response->hasConfiguration()) {
	// Retrieved \EoneoPaySdk\Entity\Configuration instance
	$configuration = $response->getConfiguration();
}
```

## Set configuration

A value can be set on an [existing configuration entity](../v2.md#markdown-header-existing-entities) or a newly created entity.

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `key` | Yes | No | Unique key within the EoneoPay system |
| `value` | Yes | No | Value for this configuration |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `configuration` | `Configuration` | Created/updated [configuration entity](#markdown-header-configuration-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Create a configuration entity, an existing \EoneoPaySdk\Entity\Fee 
// instance can be used as well
$configuration = new \EoneoPaySdk\Entity\Configuration([
	'key' => 'test_key',
	'value' => 'test value',
]);

// Or create and set/update configuration
$configuration = (new \EoneoPaySdk\Entity\Configuration)
	->setKey('test_key')
	->setValue('test value');

// Update configuration
$response = $client->update($configuration);

// Check response
if ($response->isSuccessful() && $response->hasConfiguration()) {
	// Updated \EoneoPaySdk\Entity\Configuration instance
	$configuration = $response->getConfiguration();
}
```

## Configuration resource

The configuration resource represents a group of configuration entities.

### Class

`EoneoPaySdk\Resource\Configuration`

### Filters

There are no filters available when getting configurations.

## List all configurations

There are no filters available when getting configurations.

### Response

Configurations found will be returned as a [collection](../v2.md#markdown-header-collections-and-repositories) of [configuration entities](#markdown-header-configuration-entity_1). 

If no configurations are found, e.g. the filter returns no results an empty collection will be returned.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `configurations` | `Configuration[]` | Collection of [configuration entities](#markdown-header-configuration-entity_1) |

If the collection is empty `hasConfigurations()` will return false.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate configuration resource
$configurations = new \EoneoPaySdk\Resource\Configuration;

// Get configuration list
$response = $client->list($configurations);

// Check response
if ($response->isSuccessful() && $response->hasConfigurations()) {
	// \EoneoPaySdk\Sdk\Collection instance containing 
	// \EoneoPaySdk\Entity\Configuration entities 
	$configurations = $response->getConfigurations();
}
```
