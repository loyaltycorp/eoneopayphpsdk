# Bank accounts

Bank accounts are a payment source which can be used to draw funds for payment. 

Once a bank account has been created it is managed via the [payment source](../payment-sources.md#markdown-header-payment-sources) entity.

## Contents

##### [Bank account entity](#markdown-header-bank-account-entity_1)
* [Create bank account](#markdown-header-create-bank-account)

##### Endpoints
* [Api keys](../api-keys.md#markdown-header-api-keys)
* [Charges](../charges.md#markdown-header-charges)
* [Clearing accounts](../clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](../configurations.md#markdown-header-configurations)
* [Customers](../customers.md#markdown-header-customers)
    - [Reference numbers](../customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](../customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](../customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](../ewallets.md#markdown-header-ewallets)
    - [Statements](../ewallets/statements.md#markdown-header-statements)
* [Fees](../fees.md#markdown-header-fees)
* [Merchants](../merchants.md#markdown-header-merchants)
    - [Balance](../merchants/balance.md#markdown-header-balance)
    - [Fees](../merchants/fees.md#markdown-header-fees)
    - [Transfers](../merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](../payments.md#markdown-header-payment-allocations)
* [Payment sources](../payment-sources.md#markdown-header-payment-sources)
    - Bank accounts
    - [Credit cards](credit-cards.md#markdown-header-credit-cards)
* [Payments](../payments.md#markdown-header-payments)
* [Plans](../plans.md#markdown-header-plans)
* [Reconciliation reports](../reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](../search.md#markdown-header-search)
* [System status](../system-status.md#markdown-header-system-status)
* [Tokenisation](../tokenisation.md#markdown-header-tokenisation)
* [Webhooks](../webhooks.md#markdown-header-webhooks)

## Bank account entity

The bank account entity represents the information for a single bank account.

### Class

`EoneoPaySdk\Entity\PaymentSource\Info\BankAccount`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `address_city` | `string` | Yes | Bank account address city |
| `address_country` | `string` | Yes | Bank account address country |
| `address_line1` | `string` | Yes | Bank account address line 1 |
| `address_line2` | `string` | Yes | Bank account address line 2 |
| `address_postcode` | `string` | Yes | Bank account address postcode |
| `address_state` | `string` | Yes | Bank account address state |
| `bsb` | `string` | Yes | 6 digit Bank-State-Branch number |
| `currency` | `string` | No | Bank account currency code in [ISO 4217 format](https://en.wikipedia.org/wiki/ISO_4217#Active_codes), always 'AUD' |
| `default` | `bool` | Yes | Whether this is the default bank account for the customer |
| `id` | `string` | No | The EoneoPay id for this entity |
| `name` | `string` | Yes | Bank account holder name |
| `number` | `string` | Yes | Account number for this bank account |
| `trust` | `bool` | No | Whether this account is trusted or not |

## Create bank account

Bank accounts are not stand alone entities, they must be attached to an existing [customer](../customers.md#markdown-header-customers) or [merchant](../merchants.md#markdown-header-merchants) entity via the `forCustomer()` or `forMerchant()` methods respectively before being created.

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `address_city` | No | No | Bank account address city |
| `address_country` | No | No | Bank account address country |
| `address_line1` |  No | No | Bank account address line 1 |
| `address_line2` |  No | No | Bank account address line 2 |
| `address_postcode` | No | No | Bank account address postcode |
| `address_state` | No | No | Bank account address state |
| `bsb` | Yes | Yes | 6 digit Bank-State-Branch number |
| `default` | No | No | Whether this is the default bank account for the customer |
| `name` | Yes | Yes | Bank account holder name |
| `number` | Yes | Yes | Account number for this bank account |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `bank_account` | `BankAccount` | Created bank account as a [payment source entity](../payment-sources.md#markdown-header-payment-source-entity_1) |
| `customer` | `Customer` | [Customer entity](../customers.md#markdown-header-customer-entity_1) if bank account was attached to a customer |
| `merchant` | `Merchant` | [Merchant entity](../merchants.md#markdown-header-merchantr-entity_1) if bank account was attached to a merchant |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with bank account details
$bankAccount = new \EoneoPaySdk\Entity\PaymentSource\Info\BankAccount([
	'bsb' => '000-000',
	'name' => 'Test User',
	'number' => '00000000',
]);

// Or instantiate and set bank account details
$bankAccount = (new \EoneoPaySdk\Entity\PaymentSource\Info\BankAccount)
	->setBsb('000-000')
	->setName('Test User')
	->setNumber('00000000');

// Attach bank account to customer assuming $customer is an existing
// \EoneoPaySdk\Entity\Customer instance
$bankAccount->forCustomer($customer);

// Or attach bank account to merchant assuming $merchant is an existing
// \EoneoPaySdk\Entity\Merchant instance
$bankAccount->forMerchant($merchant);

// Create bank account
$response = $client->create($bankAccount);

// Check response
if ($response->isSuccessful() && $response->hasBankAccount()) {
	// Created \EoneoPaySdk\Entity\PaymentSource instance
	$bankAccount = $response->getBankAccount();
}
```
