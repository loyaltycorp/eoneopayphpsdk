# Credit cards

Credit cards are a payment source which can be used to draw funds for payment. 

Once a credit card has been created it is managed via the [payment source](../payment-sources.md#markdown-header-payment-sources) entity.

## Contents

##### [Credit card entity](#markdown-header-credit-card-entity_1)
* [Create credit card](#markdown-header-create-credit-card)

##### Endpoints
* [Api keys](../api-keys.md#markdown-header-api-keys)
* [Charges](../charges.md#markdown-header-charges)
* [Clearing accounts](../clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](../configurations.md#markdown-header-configurations)
* [Customers](../customers.md#markdown-header-customers)
    - [Reference numbers](../customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](../customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](../customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](../ewallets.md#markdown-header-ewallets)
    - [Statements](../ewallets/statements.md#markdown-header-statements)
* [Fees](../fees.md#markdown-header-fees)
* [Merchants](../merchants.md#markdown-header-merchants)
    - [Balance](../merchants/balance.md#markdown-header-balance)
    - [Fees](../merchants/fees.md#markdown-header-fees)
    - [Transfers](../merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](../payments.md#markdown-header-payment-allocations)
* [Payment sources](../payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](bank-accounts.md#markdown-header-bank-accounts)
    - Credit cards
* [Payments](../payments.md#markdown-header-payments)
* [Plans](../plans.md#markdown-header-plans)
* [Reconciliation reports](../reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](../search.md#markdown-header-search)
* [System status](../system-status.md#markdown-header-system-status)
* [Tokenisation](../tokenisation.md#markdown-header-tokenisation)
* [Webhooks](../webhooks.md#markdown-header-webhooks)

## Credit card entity

The credit card entity represents the information for single credit card.

### Class

`EoneoPaySdk\Entity\PaymentSource\Info\CreditCard`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `address_city` | `string` | Yes | Credit card address city |
| `address_country` | `string` | Yes | Credit card address country |
| `address_line1` | `string` | Yes | Credit card address line 1 |
| `address_line2` | `string` | Yes | Credit card address line 2 |
| `address_postcode` | `string` | Yes | Credit card address postcode |
| `address_state` | `string` | Yes | Credit card address state |
| `currency` | `string` | No | Credit card currency code in [ISO 4217 format](https://en.wikipedia.org/wiki/ISO_4217#Active_codes), always 'AUD' |
| `cardDescription` | string | No | The card type as a string, e.g. Visa |
| `cardType` | int | No | The card type as an integer |
| `expiryDate` | string | No | The expiry date in mm/yy format |
| `expiry_month` | string | No | The expiry month in m or mm format |
| `expiry_year` | string | No | The expiry year in yy or yyyy format |
| `id` | `string` | No | The EoneoPay id for this entity |
| `name` | string | No | Credit card holder name |
| `pan` | string | No | Masked credit card number |
| `token` | string | No | Unique token for this credit card |

## Create credit card

Credit cards are not stand alone entities, they must be attached to an existing [customer](../customers.md#markdown-header-customers) or [merchant](../merchants.md#markdown-header-merchants) entity via the `forCustomer()` or `forMerchant()` methods respectively before being created.

Before creating a credit card it must first be [tokenised](../tokenisation.md#markdown-header-tokenisation). Once the card is tokenised it can be added to a customer or a merchant.

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `address_city` | No | No | Credit card address city |
| `address_country` | No | No | Credit card address country |
| `address_line1` |  No | No | Credit card address line 1 |
| `address_line2` |  No | No | Credit card address line 2 |
| `address_postcode` | No | No | Credit card address postcode |
| `address_state` | No | No | Credit card address state |
| `token` | Yes | Yes | The EoneoPay id or internal token from a [tokenised card entity](../tokenisation.md#markdown-header-attributes) |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `credit_card` | `PaymentSource` | Created credit card as a [payment source entity](../payment-sources.md#markdown-header-payment-source-entity_1) |
| `customer` | `Customer` | [Customer entity](../customers.md#markdown-header-customer-entity_1) if credit card was attached to a customer |
| `merchant` | `Merchant` | [Merchant entity](../merchants.md#markdown-header-merchant-entity_1) if credit card was attached to a merchant |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Get internal token from a tokenised card, $tokenisedCard
// must be an existing \EoneoPaySdk\Entity\TokenisedCard instance
$token = $tokenisedCard->getToken();

// Instantiate with token
$creditCard = new \EoneoPaySdk\Entity\PaymentSource\Info\CreditCard([
	'token' => $token,
]);

// Or instantiate and set token
$creditCard = (new \EoneoPaySdk\Entity\PaymentSource\Info\CreditCard)
	->setToken($token);

// Attach credit card to customer assuming $customer is an existing
// \EoneoPaySdk\Entity\Customer instance
$creditCard->forCustomer($customer);

// Or attach credit card to merchant assuming $merchant is an existing
// \EoneoPaySdk\Entity\Merchant instance
$creditCard->forMerchant($merchant);

// Create credit card
$response = $client->create($creditCard);

// Check response
if ($response->isSuccessful() && $response->hasCreditCard()) {
	// Created \EoneoPaySdk\Entity\PaymentSource instance
	$creditCard = $response->getCreditCard();
}
```
