# Reconciliation reports

Reconciliation reports are used to display the result of processed reconciliations.

## Contents

##### [Reconciliation report entity](#markdown-header-reconciliation-report-entity_1)
* [Get reconciliation report by id](#markdown-header-get-reconciliation-report-by-id)

##### [Reconciliation report resource](#markdown-header-reconciliation-report-resource_1)
* [Get all reconciliation reports](#markdown-header-get-all-reconciliation-reports)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payment-allocations.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* Reconciliation reports
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## Reconciliation report entity

The reconciliation report entity represents a single reconciliation report.

### Class

`EoneoPaySdk\Entity\ReconciliationReport`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `bank_transaction_id` | `string` | No | Bank transaction id |
| `created_at` | `string` | No | SQL compatible date/time this transaction was created |
| `externalRecord` | `string|Repository` | No | Base64 encoded gzipped NAB transact data or repository of data |
| `file` | `string` | No | NAB-file filename |
| `id` | `string` | No | The EoneoPay id for this entity |
| `internalRecord` | `string` | No | Base64 encoded gzipped EoneoPay payment model |
| `message` | `string` | No | Reason report was created |
| `status` | `int` | No | Report status |
| `updated_at` | `string` | No | SQL compatible date/time this transaction was last updated |
| `version` | `int` | No | The version/revision for this entity |

## Get reconciliation report by id

A reconciliation report can be retrieved via it's unique [EoneoPay id](#markdown-header-attributes).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `id` | Yes | Yes | The EoneoPay id for this entity |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `reconciliation_report` | `ReconciliationReport` | Retrieved [reconciliation report entity](#markdown-header-reconciliation-report-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with id
$reconciliationReport = new \EoneoPaySdk\Entity\ReconciliationReport([
	'id' => 'bpay-WBC2017010100000000',
]);

// Or instantiate and set id
$reconciliationReport = (new \EoneoPaySdk\Entity\ReconciliationReport)
	->setId('bpay-WBC2017010100000000');

// Get reconciliation report
$response = $client->get($reconciliationReport);

// Check response
if ($response->isSuccessful() && $response->hasReconciliationReport()) {
	// Retrieved \EoneoPaySdk\Entity\ReconciliationReport instance
	$reconciliationReport = $response->getReconciliationReport(); 
}
```

## Reconciliation report resource

The reconciliation report resource represents a group of reconciliation report entities.

Like most resources, [pagination](../v2.md#markdown-header-resources) can be used to limit results.

### Class

`EoneoPaySdk\Resource\ReconciliationReport`

### Filters

| Attribute | Description |
|-----------|-------------|
| `bank_transaction_id` | Bank transaction id |
| `created_at` | SQL compatible date/time this transaction was created |
| `file` | NAB-file filename |
| `id` | The EoneoPay id for this entity |
| `message` | Reason report was created |
| `status` | Report status |
| `updated_at` | SQL compatible date/time this transaction was last updated |
| `version` | The version/revision for this entity |

## List all reconciliation reports

Reconciliation reports can be filtered via the [reconciliation report resource](#markdown-header-reconciliation-report-resource_1).

### Response

Reconciliation reports found will be returned as a [collection](../v2.md#markdown-header-collections-and-repositories) of [reconciliation report entities](#markdown-header-reconciliation-report-entity_1). 

If no reconciliation reports are found, e.g. the filter returns no results or the page and limit exceeds available reconciliation reports, an empty collection will be returned.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `reconciliation_reports` | `ReconciliationReport[]` | Collection of [reconciliation reports entities](#markdown-header-reconciliation-reports-entity_1) |

If the collection is empty `hasReconciliationReports()` will return false.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate reconciliation report resource
$reconciliationReports = new \EoneoPaySdk\Resource\ReconciliationReport;

// Optionally add filter results
	$reconciliationReports->whereStatus(0);

// Optionally set pagination and limit
$reconciliationReports->limit(10)->page(1);

// Get reconciliation report list
$response = $client->list($reconciliationReports);

// Check response
if ($response->isSuccessful() && $response->hasReconciliationReports()) {
	// \EoneoPaySdk\Sdk\Collection instance containing 
	// \EoneoPaySdk\Entity\ReconciliationReport entities 
	$reconciliationReports = $response->getReconciliationReports();
}
```
