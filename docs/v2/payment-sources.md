# Payment sources

Payment sources can be used to draw funds when making a payment. There are two available payment sources: [bank account](payment-sources/bank-accounts.md#markdown-header-bank-accounts) and [credit card](payment-sources/credit-cards.md#markdown-header-credit-cards).

Payment sources are always attached to [a customer](customers.md#markdown-header-customers) or [a merchant](merchants.md#markdown-header-merchants).

## Contents

##### [Payment source entity](#markdown-header-payment-source-entity_1)
* [Delete payment source](#markdown-header-delete-payment-source)
* [Get payment source by id](#markdown-header-get-payment-source-by-id)
* [Update payment source](#markdown-header-update-payment-source)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payments.md#markdown-header-payment-allocations)
* Payment sources
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## Payment source entity

The payment source entity represents a single payment source.

### Class

`EoneoPaySdk\Entity\PaymentSource`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `created_at` | `string` | No | SQL compatible date/time this payment source was created |
| `default` | `bool` | Yes | Whether this is the default payment source or not |
| `holder_id` | `string` | No | The EoneoPay id for the associated [customer](customers.md#markdown-header-customers) or [merchant](merchants.md#markdown-header-merchants) |
| `id` | `string` | No | The EoneoPay id for this entity |
| `info` | `Info` | Yes | The [bank account](payment-sources/bank-accounts.md#attributes) and [credit card](payment-sources/credit-cards.md#attributes) details |
| `internal_token` | `string` | No | Internal EoneoPay token |
| `token` | `string` | No | External EoneoPay token |
| `type`| `string` | No | The payment source type: `bank_account` or `credit_card` |
| `updated_at` | `string` | No | SQL compatible date/time this payment source was last updated |

## Delete payment source

When deleting a bank account you must have an [existing payment source entity](../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when deleting a payment source.

### Response attributes

The response will only contain the [standard response](../v2.md#markdown-header-responses) attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Delete payment source, $paymentSource must be an existing
// \EoneoPaySdk\Entity\PaymentSource instance
$response = $client->delete($paymentSource);

// Check response
if ($response->isSuccessful()) {
	// Payment source has been successfully deleted
}
```

## Get payment source by id

A payment source can be retrieved via it's unique [EoneoPay id](#markdown-header-attributes).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `id` | Yes | Yes | The EoneoPay id for this entity |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `payment_source` | `PaymentSource` | Retrieved [payment source entity](#markdown-header-payment-source-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with id
$paymentSource = new \EoneoPaySdk\Entity\PaymentSource([
	'id' => 'src_000000000000000000000000'
]);

// Or instantiate and set id
$paymentSource = (new \EoneoPaySdk\Entity\PaymentSource)
	->setId('src_000000000000000000000000');

// Get payment source
$response = $client->get($paymentSource);

// Check response
if ($response->isSuccessful() && $response->hasPaymentSource()) {
	// Retrieved \EoneoPaySdk\Entity\PaymentSource instance
	$paymentSource = $response->getPaymentSource(); 
	
	// \EoneoPaySdk\Entity\PaymentSource\Info\BankAccount or 
	// \EoneoPaySdk\Entity\PaymentSource\Info\CreditCard instance
	$info = $paymentSource->getInfo();
}
```

## Update payment source

To update a payment source you must have an [existing payment source entity](../v2.md#markdown-header-existing-entities).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|------|---------|-------------|
| `info` | Yes | Yes | The [bank account](payment-sources/bank-accounts.md#attributes) and [credit card](payment-sources/credit-cards.md#attributes) details |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `payment_source` | `PaymentSource` | Updated [payment source entity](#markdown-header-payment-source-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Update payment source details assuming $paymentSource
// must be an existing \EoneoPaySdk\Entity\PaymentSource instance
$paymentSource->getInfo()->setName('Updated');

// Update payment source
$response = $client->update($paymentSource);

// Check response
if ($response->isSuccessful() && $response->hasPaymentSource()) {
	// Updated \EoneoPaySdk\Entity\PaymentSource instance
	$paymentSource = $response->getPaymentSource();
}
```
