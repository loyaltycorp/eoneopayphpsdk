# Statements

Statement are a record of transactions made against an [eWallet entity](../ewallets.md#markdown-header-ewallets) for a specific billing period.

## Contents

##### [Statement entity](#markdown-header-statement-entity_1)
* [Get statement for eWallet](#markdown-header-get-statement-for-ewallet)

##### Endpoints
* [Api keys](../api-keys.md#markdown-header-api-keys)
* [Charges](../charges.md#markdown-header-charges)
* [Clearing accounts](../clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](../configurations.md#markdown-header-configurations)
* [Customers](../customers.md#markdown-header-customers)
    - [Reference numbers](../customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](../customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](../customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](../ewallets.md#markdown-header-ewallets)
    - Statements
* [Fees](../fees.md#markdown-header-fees)
* [Merchants](../merchants.md#markdown-header-merchants)
    - [Balance](../merchants/balance.md#markdown-header-balance)
    - [Fees](../merchants/fees.md#markdown-header-fees)
    - [Transfers](../merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](../payments.md#markdown-header-payment-allocations)
* [Payment sources](../payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](../payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](../payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](../payments.md#markdown-header-payments)
* [Plans](../plans.md#markdown-header-plans)
* [Reconciliation reports](../reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](../search.md#markdown-header-search)
* [System status](../system-status.md#markdown-header-system-status)
* [Tokenisation](../tokenisation.md#markdown-header-tokenisation)
* [Webhooks](../webhooks.md#markdown-header-webhooks)

## Statement entity

The statement entity represents the transactions for a single billing period within an [eWallet](../ewallets.md#markdown-header-ewallets).

### Class

`EoneoPaySdk\Entity\Ewallet\Statement`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `balance` | `int` | No | The current [eWallet](../ewallets.md#markdown-header-ewallets) balance in cents |
| `date` | `string` | Yes* | SQL compatible date/time to use as the billing period |
| `deposits` | `Collection` | No | A collection of deposits made during the billing period |
| `endDate` | `string` | No | SQL compatible date/time this billing period ends |
| `startDate` | `string` | No | SQL compatible date/time this billing period starts |
| `withdrawals` | `Collection` | No | A collection of withdrawals made during the billing period |

\* The billing period for the statement can be changed via the `forMonth()` method. This method accepts any [valid date/time format](http://php.net/manual/en/datetime.formats.php).

## Get statement for eWallet

A statement can be retrieved via an [existing eWallet entity](../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when retrieving a statement.

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `ewallet_statement` | `Statement` | Retrieved [statement entity](#markdown-header-statement-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate statement instance
$statement = new \EoneoPaySdk\Entity\Ewallet\Statement;

// Set ewallet to retrieve the statement for, $wallet must be
// an existing \EoneoPaySdk\Entity\Ewallet instance
$statement->forEwallet($ewallet);

// Optionally set the month/billing period to retrieve
$statement->forMonth('July 2017');

// Get statement
$response = $client->get($statement);

// Check response
if ($response->isSuccessful() && $response->hasEwalletStatement()) {
	// Retrieved \EoneoPaySdk\Entity\Ewallet\Statement instance
	$statement = $response->getEwalletStatement();
}
```
