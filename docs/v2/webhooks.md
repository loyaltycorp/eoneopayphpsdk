# Webhooks

Webhooks are a way to push notification to your application when certain events occur.

## Contents

##### [Webhook entity](#markdown-header-webhook-entity_1)
* [Create webhook](#markdown-header-create-webhook)
* [Delete webhook](#markdown-header-delete-webhook)
* [Get webhook by id](#markdown-header-get-webhook-by-id)
* [Update webhook](#markdown-header-update-webhook)

##### [Webhook resource](#markdown-header-webhook-resource_1)
* [List all webhooks](#markdown-header-list-all-webhooks)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payments.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* Webhooks

## Webhook entity

The webhook entity represents a single webhook.

### Class

`EoneoPaySdk\Entity\Webhook`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `active` | `bool` | Yes | Whether the webhook is currently active, defaults to true |
| `created_at` | `string` | No | SQL compatible date/time this payment was created |
| `event_name` | `string` | Yes | The [event name](#markdown-header-event-names) which triggers the webhook |
| `id` | `string` | No | The EoneoPay id for this webhook |
| `merchant_id` | `string` | No | The EoneoPay id for the associated [merchant](merchants.md#markdown-header-merchants) |
| `name` | `string` | Yes | The name/description for this webhook |
| `target_url` | `string` | Yes | The url endpoint to send payload to |
| `updated_at` | `string` | No | SQL compatible date/time this payment was created |

### Constants

#### Event names

| Value | Constant | Description |
|----------|-------|-------------|
| `PaymentCreated` | `EVENT_PAYMENT_CREATED` | Webhook fires when a [payment](payments.md#markdown-header-payments) is created regardless of status |
| `PaymentFailed` | `EVENT_PAYMENT_FAILED` | Webhook fires when a payment is created and fails |
| `PaymentSucceeded` | `EVENT_PAYMENT_SUCCEEDED` | Webhook fires when a payment is created and succeeds |
| `PaymentMethodCreated` | `EVENT_PAYMENT_METHOD_CREATED` | Webhook fires when a [payment source](payment-sources.md#markdown-header-payment-sources) is added to a customer |
| `PaymentMethodDeleted` | `EVENT_PAYMENT_METHOD_DELETED` | Webhook fires when a payment source is removed from a customer |
| `RefundCreated` | `EVENT_REFUND_CREATED` | Webhook fires when a payment is refunded regardless of status |
| `RefundFailed` | `EVENT_REFUND_FAILED` | Webhook fires when a payment is refunded and fails |
| `RefundSucceeded` | `EVENT_REFUND_SUCCEEDED` | Webhook fires when a payment is refunded and succeeds |
| `ReversalCreated` | `EVENT_REVERSAL_CREATED` | Webhook fires when a reversal is created |

## Create webhook

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `active` | Yes | Yes | Whether the webhook is currently active, defaults to true |
| `event_name` | Yes | Yes | The event which triggers the webhook |
| `name` | Yes | Yes | The name/description for this webhook |
| `target_url` | Yes | Yes | The url endpoint to send payload to |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `webhook` | `Webhook` | Created [webhook entity](#markdown-header-webhook-entity_1) |

> **Note:** The reponse will not contain a statement attribute.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with webhook details
$webhook = new \EoneoPaySdk\Entity\Webhook([
	'event_name' => \EoneoPaySdk\Entity\Webhook::EVENT_PAYMENT_CREATED,
	'name' => 'Test',
	'target_url' => 'https://localhost',
]);

// Or instantiate and set webhook details
$webhook = (new \EoneoPaySdk\Entity\Webhook)
	->setEventName(\EoneoPaySdk\Entity\Webhook::EVENT_PAYMENT_CREATED)
	->setName('Test')
	->setTargetUrl('https://localhost');

// Create webhook
$response = $client->create($webhook);

// Check response
if ($response->isSuccessful() && $response->hasWebhook()) {
	// Created \EoneoPaySdk\Entity\Webhook instance
	$webhook = $response->getWebhook();
}
```

## Delete webhook

When deleting a webhook you must have an [existing webhook entity](../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when deleting a webhook.

### Response attributes

The response will only contain the [standard response](../v2.md#markdown-header-responses) attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Delete webhook, $webhook must be an existing
// \EoneoPaySdk\Entity\Webhook instance
$response = $client->delete($webhook);

// Check response
if ($response->isSuccessful()) {
	// Webhook has been successfully deleted
}
```

## Get webhook by id

A webhook can be retrieved via it's unique [EoneoPay id](#markdown-header-attributes).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `id` | Yes | No | The EoneoPay id for this entity |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `webhook` | `Webhook` | Retrieved [webhook entity](#markdown-header-webhook-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with id
$webhook = new \EoneoPaySdk\Entity\Webhook([
	'id' => 'webhook_000000000000000000000000'
]);

// Or instantiate and set id
$webhook = (new \EoneoPaySdk\Entity\Webhook)
	->setId('webhook_000000000000000000000000');

// Get webhook
$response = $client->get($webhook);

// Check response
if ($response->isSuccessful() && $response->hasWebhook()) {
	// Retrieved \EoneoPaySdk\Entity\Webhook instance
	$webhook = $response->getWebhook();
}
```

## Update webhook

When updating a webhook you must have an [existing webhook entity](../v2.md#markdown-header-existing-entities).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `active` | Yes | Yes | Whether the webhook is currently active, defaults to true |
| `event_name` | Yes | Yes | The event which triggers the webhook |
| `name` | Yes | Yes | The name/description for this webhook |
| `target_url` | Yes | Yes | The url endpoint to send payload to |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `webhook` | `Webhook` | Updated [webhook entity](#markdown-header-webhook-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Update some details, $webhook must be an existing 
// \EoneoPaySdk\Entity\Webhook instance
$webhook->setName('Updated');

// Update webhook
$response = $client->update($webhook);

// Check response
if ($response->isSuccessful() && $response->hasWebhook()) {
	// Updated \EoneoPaySdk\Entity\Webhook instance
	$webhook = $response->getWebhook();
}
```

## Webhook resource

The webhook resource represents a group of webhooks.

Like most resources, [pagination](../v2.md#markdown-header-resources) can be used to limit results.

### Class

`EoneoPaySdk\Resource\Webhook`

### Filters

| Attribute | Description |
|-----------|-------------|
| `active` | Whether the webhook is currently active, defaults to true |
| `created_at` | SQL compatible date/time this payment was created |
| `event_name` | The event which triggers the webhook |
| `id` | The EoneoPay id for this webhook |
| `name` | The name/description for this webhook |
| `target_url` | The url endpoint to send payload to |
| `updated_at` | SQL compatible date/time this payment was created |

## List all webhooks

Webhooks can be filtered via the [webhook resource](#markdown-header-webhook-resource_1).

### Response

Webhooks found will be returned as a [collection](../v2.md#markdown-header-collections-and-repositories) of [webhook entities](#markdown-header-webhook-entity_1). 

If no webhooks are found, e.g. the filter returns no results or the page and limit exceeds available webhooks, an empty collection will be returned.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `webhooks` | `Webhook[]` | Collection of [webhook entities](#markdown-header-webhook-entity_1) |

If the collection is empty `hasWebhooks()` will return false.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate webhook resource
$webhooks = new \EoneoPaySdk\Resource\Webhook;

// Optionally add filter results
$webhooks->whereEventName($webhook::EVENT_PAYMENT_CREATED)
	->whereName('Test');

// Optionally set pagination and limit
$webhooks->limit(10)->page(1);

// Get webhook list
$response = $client->list($webhooks);

// Check response
if ($response->isSuccessful() && $response->hasWebhooks()) {
	// \EoneoPaySdk\Sdk\Collection instance containing 
	// \EoneoPaySdk\Entity\Webhook entities 
	$webhooks = $response->getWebhooks();
}
```
