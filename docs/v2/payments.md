# Payments

Payments are used to create a payment from a [payment source](payment-sources.md#markdown-header-payment-sources).

## Contents

##### [Payment entity](#markdown-header-payment-entity_1)
* [Create payment](#markdown-header-create-payment)
* [Get payment by id](#markdown-header-get-payment-by-id)
* [Refund payment](#markdown-header-delete-payment)
* [Update payment](#markdown-header-update-payment)

##### [Pre-authorisation entity](#markdown-header-pre-authorisation-entity_1)
* [Pre-authorise payment](#markdown-header-pre-authorise-payment)
* [Complete pre-authorisation](#markdown-header-complete-pre-authorisation)

##### [Payment resource](#markdown-header-payment-resource_1)
* [List all payments](#markdown-header-list-all-payments)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payments.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* Payments
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## Payment entity

The payment entity represents a single payment.

### Class

`EoneoPaySdk\Entity\Payment`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `allocation_plan` | `Repository` | Yes | How this payment will be allocated |
| `amount` | `int` | Yes | Payment amount, in cents |
| `bank_txn_id` | `string` | No | The transaction id from the bank |
| `created_at` | `string` | No | SQL compatible date/time this payment was created |
| `card_type` | `string` | No | The card type, e.g. Visa |
| `card_type_id` | `int` | No | The card type as an integer |
| `clearing_account` | `int` | No | Clearing account id |
| `currency` | `string` | No | Payment currency code in [ISO 4217 format](https://en.wikipedia.org/wiki/ISO_4217#Active_codes), always 'AUD' |
| `customer_id` | `string` | Yes | The EoneoPay id for the [customer](customers.md#markdown-header-customers) this payment should be allocated to |
| `external_customer_id` | `string` | No | Internal reference for this [customer](customers.md#markdown-header-customers) within your system this payment should be allocated to |
| `external_txn_id` | `string` | No | Internal reference within your system for this payment |
| `fee_amount` | `int` | No | The fee amount, in cents |
| `fee_schedule` | `Repository` | No | Break down of the fee allocations |
| `merchant_id` | `string` | No | The EoneoPay id for the associated [merchant](merchants.md#markdown-header-merchants) |
| `merchant_transfer_status` | `int` | No | The [transfer status](#markdown-header-transfer-status) for this payment |
| `meta` | `mixed` | Yes | Custom meta data for this payment |
| `original_txn_id` | `string` | No | The original transaction id if payment is refunded |
| `reference` | `string` | Yes | Reference for this payment, such as order id within your system |
| `reference_number` | `string` | Yes | Customer reference number for this payment |
| `statement_descriptor` | `string` | No | [Merchant](merchants.md#markdown-header-merchants) statement descriptor |
| `status` | `int` | No | [Payment status](#markdown-header-payment-status) |
| `test_validate_amount` | `bool` | Yes | If testing, whether the amount should be validated or not, this can be used to simulate successful payments with any amount |
| `token` | `string` | Yes | The payment source token this payment was made from |
| `type` | `string` | No | The type of payment made |
| `txn_date` | `string` | No | SQL compatible date/time this transaction was made |
| `txn_id` | `string` | No | The EoneoPay id for this transaction |
| `updated_at` | `string` | No | SQL compatible date/time this payment was last updated |

#### *allocation_plan* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `xxx` | `Repository` | No | For each allocation where `xxx` is the allocation reference or description |

#### *allocation_plan -> xxx* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `amount` | `int` | No | The amount allocated, in cents |
| `type` | `string` | No | The type of allocation |

#### *fee_schedule* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `card_rates` | `Repository` | No | Credit card only, fees for each card type |
| `merchant_id` | `string` | No | The EoneoPay id for the [merchant](merchants.md#markdown-header-merchants) this fee applies to |
| `payment_type` | `string` | No | The type of payment made |
| `transaction_fee` | `string` | No | The fee charged for this payment |

#### *fee\_schedule -> card\_rates* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `AMEX` | `float` | No | Fee applied to American Express transactions as a percentage |
| `MasterCard` | `float` | No | Fee applied to MasterCard transactions as a percentage |
| `Visa` | `float` | No | Fee applied to Visa transactions as a percentage |

### Constants

#### Payment status

| Value | Constant | Description |
|-------|----------|-----|
| `-1` | `STATUS_FAILED` | Processing failed/payment declined |
| `0` | `STATUS_CREATED` | Newly created |
| `1` | `STATUS_PROCESSED` | Processed successfully |
| `10` | `STATUS_PENDING` | Pending processing |

#### Transfer status

| Value | Constant | Description |
|-------|----------|-------------|
| `0` | `TRANSFER_STATUS_PENDING` | Pending clearance |
| `1` | `TRANSFER_STATUS_AVAILABLE` | Processed, available for transfer |
| `2` | `TRANSFER_STATUS_TRANSFERRED` | Balance transferred |

## Create payment

Payments must be attached to an existing [payment source entity](payment-sources.md#markdown-header-payment-sources) via the `fromPaymentSource()` method.

Payments can also be allocated on creation by adding allocations via the `addAllocation()` method.

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `amount` | Yes | Yes | Payment amount, in cents |
| `customer_id` | No | Yes | The EoneoPay id for the [customer](customers.md#markdown-header-customers) this payment should be allocated to |
| `meta` | No | No | Custom meta data for this payment |
| `reference` | Yes | Yes | Reference for this payment, such as order id within your system |
| `reference_number` | No | No | Customer reference number for this payment |
| `test_validate_amount` | `bool` | No | If testing, whether the amount should be validated or not, this can be used to simulate successful payments with any amount |

### Response attributes

| Attribute | Type | Description |
|-----------|------|-------------|
| `payment` | `Payment` | Created [payment entity](../#markdown-header-payment-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with payment details
$payment = new \EoneoPaySdk\Entity\Payment([
	'amount' => 100,
	'reference' => 'Order #10',
]);

// Or instantiate and set payment details
$payment = (new \EoneoPaySdk\Entity\Payment)
   ->setAmount(100)
   ->setReference('Order #10');
   
// Add payment source, assuming $bankAccount is an existing
// \EoneoPaySdk\Entity\PaymentSource\BankAccount instance
$payment->fromPaymentSource($bankAccount);

// Or use credit card, assuming $creditCard is an existing
// \EoneoPaySdk\Entity\PaymentSource\CreditCard instance
$payment->fromPaymentSource($creditCard);

// Create an allocation plan, allocate 50% to an eWallet
// and the rest as a sundry payment
// This assumes $wallet is an existing \EoneoPaySdk\Entity\Ewallet instance
$payment->addAllocation($wallet, 40)->addAllocation('Sundry test', 40);

// Create Payment
$response = $client->create($payment);

// Check response
if ($response->isSuccessful() && $response->hasPayment()) {
	// Created \EoneoPaySdk\Entity\Payment instance
	$bankAccount = $response->getPayment();
}
```

## Delete payment

When deleting a payment you must have an [existing payment entity](../v2.md#markdown-header-existing-entities). Deleting a payment will reverse/refund it within EoneoPay.

### Request attributes

There are no settable attributes when deleting a payment.

### Response attributes

| Attribute | Type | Description |
|-----------|------|-------------|
| `payment` | `Payment` | Refunded [payment entity](../#markdown-header-payment-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Delete payment, $payment must be an existing
// \EoneoPaySdk\Entity\Payment instance
$response = $client->delete($payment);

// Check response
if ($response->isSuccessful() && $response->hasPayment()) {
	// Created \EoneoPaySdk\Entity\Payment instance
	$refund = $response->getPayment();
}
```

## Get payment by id

A payment can be retrieved via it's unique [EoneoPay id](#markdown-header-attributes).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `id` | Yes | Yes | The EoneoPay id for this entity |

### Response attributes

| Attribute | Type | Description |
|-----------|------|-------------|
| `payment` | `Payment` | Retrieved [payment entity](#markdown-header-payment-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with id
$payment = new \EoneoPaySdk\Entity\Payment([
    'id' => 'payment_000000000000000000000000'
]);

// Or instantiate and set id
$payment = (new \EoneoPaySdk\Entity\Payment)
    ->setId('payment_000000000000000000000000');

// Get payment
$response = $client->get($payment);

// Check response
if ($response->isSuccessful() && $response->hasPayment()) {
    // Retrieved \EoneoPaySdk\Entity\Payment instance
    $payment = $response->getPayment();
}
```

## Update payment

When updating a payment you must have an [existing payment entity](../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when updating a payment.

Once created a payment can not be changed however if it wasn't created with an allocation plan an allocation plan can be added via the `addAllocation()` method. Unlike adding an allocation plan on creation the full amount doesn't need to be allocated however you can only allocate a payment once, additional updates will receive an error adivising the payment has already been allocated.

### Response attributes

| Attribute | Type | Description |
|-----------|------|-------------|
| `payment` | `Payment` | Updated [payment entity](#markdown-header-payment-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Add allocation plan, $payment must be an existing
// \EoneoPaySdk\Entity\Payment instance
$payment->addAllocation('Sundry test', 100);

// Update payment
$response = $client->update($payment);

// Check response
if ($response->isSuccessful() && $response->hasPayment()) {
    // Updated \EoneoPaySdk\Entity\Payment instance
    $payment = $response->getPayment();
}
```

## Pre-authorisation entity

The pre-authoriation entity is used to pre-authorise a charge on a credit card.

### Class

`EoneoPaySdk\Entity\PreAuth`

### Attributes

The attributes for the pre-authorisation entity are identical to the [payment entity](#markdown-header-attributes)

## Pre-authorise payment

Pre-authorisations must be attached to an existing credit card [payment source entity](payment-sources.md#markdown-header-payment-sources) via the `fromCreditCard()` method.

Like payments, pre-authorisations can also be allocated on creation by adding allocations via the `addAllocation()` method.

### Request attributes

The request attributes for a pre-authorisation are identical to [creating a payment](#markdown-header-create-payment)

### Response attributes

| Attribute | Type | Description |
|-----------|------|-------------|
| `preauth` | `PreAuth` | Created [pre-authorisation entity](../#markdown-header-payment-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with payment details
$preauth = new \EoneoPaySdk\Entity\PreAuth([
	'amount' => 100,
	'reference' => 'Order #10',
]);

// Or instantiate and set payment details
$preauth = (new \EoneoPaySdk\Entity\PreAuth)
   ->setAmount(100)
   ->setReference('Order #10');

// Add credit card, assuming $creditCard is an existing
// \EoneoPaySdk\Entity\PaymentSource\CreditCard instance
$preauth->fromCreditCard($creditCard);

// Pre-authorise
$response = $client->create($preauth);

// Check response
if ($response->isSuccessful() && $response->hasPreauth()) {
	// Created \EoneoPaySdk\Entity\PreAuth instance
	$preauth = $response->getPreAuth();
}
```

## Complete pre-authorisation

When completing a pre-authorisation you must have an [existing pre-authorisation entity](../v2.md#markdown-header-existing-entities).

### Request attributes

The payment details can be updated before completing the pre-authorisation, this allows allocations and other payment details to be modified to match the completion amount.

All attributes available when [pre-authorising a payment](#markdown-header-pre-authorise-payment) can be modified.

> **Note:** The completion amount must be less than or equal to the pre-authorised amount or the completion will fail. It's not advisable to update the amount, reference or credit card from the original pre-authorisation entity.

### Response attributes

| Attribute | Type | Description |
|-----------|------|-------------|
| `payment` | `Payment` | Completed [payment entity](#markdown-header-payment-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Update pre-authorisation, $preauth must be an
// existing \EoneoPaySdk\Entity\PreAuth instance
$preauth->setAmount(50);

// Complete pre-authorisation
$response = $client->update($preauth);

// Check response
if ($response->isSuccessful() && $response->hasPayment()) {
    // Updated \EoneoPaySdk\Entity\Payment instance
    $payment = $response->getPayment();
}
```

## Payment resource

The payment resource represents a group of payment entities.

Like most resources, [pagination](../v2.md#markdown-header-resources) can be used to limit results.

### Class

`EoneoPaySdk\Resource\Payment`

### Filters

| Attribute | Description |
|-----------|-------------|
| `amount` | Payment amount, in cents |
| `bank_txn_id` | The transaction id from the bank |
| `created_at` | SQL compatible date/time this payment was created |
| `card_type` | The card type, e.g. Visa |
| `card_type_id` | The card type as an integer |
| `clearing_account` | Clearing account id |
| `currency` | Payment currency code in [ISO 4217 format](https://en.wikipedia.org/wiki/ISO_4217#Active_codes), always 'AUD' |
| `customer_id` | The EoneoPay id for the [customer](customers.md#markdown-header-customers) this payment should be allocated to |
| `external_customer_id` | Internal reference for this [customer](customers.md#markdown-header-customers) within your system this payment should be allocated to |
| `external_txn_id` | Internal reference within your system for this payment |
| `fee_amount` | The fee amount, in cents |
| `merchant_transfer_status` | The transfer status for this payment |
| `meta` | Custom meta data for this payment |
| `original_txn_id` | The original transaction id if payment is refunded |
| `reference` | Reference for this payment, such as order id within your system |
| `reference_number` | Customer reference number for this payment |
| `statement_descriptor` | [Merchant](merchants.md#markdown-header-merchants) statement descriptor |
| `status` | Payment status: 0 = failed, 1 = success |
| `token` | The payment source token this payment was made from |
| `type` | The type of payment made |
| `txn_date` | SQL compatible date/time this transaction was made |
| `txn_id` | The EoneoPay id for this transaction |
| `updated_at` |  SQL compatible date/time this payment was last updated |


## List all payments

Payments can be filtered via the [payment resource](#markdown-header-payment-resource_1).

### Response

Payments found will be returned as a [collection](../v2.md#markdown-header-collections-and-repositories) of [payment entities](#markdown-header-payment-entity_1).

If no payments are found, e.g. the filter returns no results or the page and limit exceeds available payments, an empty collection will be returned.

| Attribute | Type | Description |
|-----------|------|-------------|
| `payments` | `Payment[]` | Collection of [payment entities](#markdown-header-payment-entity_1) |

If the collection is empty `hasPayments()` will return false.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate payment resource
$payments = new \EoneoPaySdk\Resource\Payment;

// Optionally add filter results
$payments->whereName('Test')->whereAmount(100);

// Optionally set pagination and limit
$payments->limit(10)->page(1);

// Get payment list
$response = $client->list($payments);

// Check response
if ($response->isSuccessful() && $response->hasPayments()) {
    // \EoneoPaySdk\Sdk\Collection instance containing
    // \EoneoPaySdk\Entity\Payment entities
    $payments = $response->getPayments();
}
```
