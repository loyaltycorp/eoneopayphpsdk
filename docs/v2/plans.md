# Plans

Plans are used to payment schedules within subscriptions.

## Contents

##### [Plan entity](#markdown-header-plan-entity_1)
* [Create plan](#markdown-header-create-plan)
* [Delete plan](#markdown-header-delete-plan)
* [Get plan by id](#markdown-header-get-plan-by-id)
* [Update plan](#markdown-header-update-plan)

##### [Plan resource](#markdown-header-plan-resource_1)
* [List all plans](#markdown-header-list-all-plans)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payments.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* Plans
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## Plan entity

The plan entity represents a single subscription plan.

### Class

`EoneoPaySdk\Entity\Plan`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `amount` | `int` | Yes | Plan amount, in cents |
| `created_at` | `string` | No | SQL compatible date/time this plan was created |
| `currency` | `string` | No | Plan currency code in [ISO 4217 format](https://en.wikipedia.org/wiki/ISO_4217#Active_codes), always 'AUD' |
| `id` | `string` | No | The EoneoPay id for this entity |
| `interval` | `string` | Yes | Plan interval: day, week, month or year |
| `interval_count` | `int` | No | How many intervals have been processed |
| `merchant_id` | `string` | No | The EoneoPay id for the associated [merchant](merchants.md#markdown-header-merchants) |
| `name` | `string` | Yes | The name for this plan |
| `updated_at` | `string` | No | SQL compatible date/time this plan was last updated |

### Constants

| Constant | Usage with | Description |
|----------|-------|-------------|
| `INTERVAL_DAY` | `setInterval()` | Plan charges daily |
| `INTERVAL_WEEK` | `setInterval()` | Plan charges weekly |
| `INTERVAL_MONTH` | `setInterval()` | Plan charges monthly |
| `INTERVAL_YEAR` | `setInterval()` | Plan charges yearly |

## Create plan

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `amount` | Yes | Yes | Plan amount, in cents |
| `interval` | Yes | Yes | Plan interval: day, week, month or year |
| `name` | Yes | Yes | The name for this plan |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `plan` | `Plan` | Created [plan entity](../#markdown-header-plan-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with plan details
$plan = new \EoneoPaySdk\Entity\Plan([
	'amount' => 100,
	'interval' => \EoneoPaySdk\Entity\Ewallet::INTERVAL_WEEK,
	'name' => 'Test Plan',
]);

// Or instantiate and set plan details
$plan = (new \EoneoPaySdk\Entity\Plan)
   ->setAmount(100)
   ->setInterval(\EoneoPaySdk\Entity\Ewallet::INTERVAL_WEEK)
	->setName('Test Plan');
	
// Create Plan
$response = $client->create($plan);

// Check response
if ($response->isSuccessful() && $response->hasPlan()) {
	// Created \EoneoPaySdk\Entity\Plan instance
	$bankAccount = $response->getPlan();
}
```

## Delete plan

When deleting a plan you must have an [existing plan entity](../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when deleting a plan.

### Response attributes

The response will only contain the [standard response](../v2.md#markdown-header-responses) attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Delete plan, $plan must be an existing
// \EoneoPaySdk\Entity\Plan instance
$response = $client->delete($plan);

// Check response
if ($response->isSuccessful()) {
    // Plan has been successfully deleted
}
```

## Get plan by id

A plan can be retrieved via it's unique [EoneoPay id](#markdown-header-attributes).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `id` | Yes | Yes | The EoneoPay id for this entity |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `plan` | `Plan` | Retrieved [plan entity](#markdown-header-plan-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with id
$plan = new \EoneoPaySdk\Entity\Plan([
    'id' => 'plan_000000000000000000000000'
]);

// Or instantiate and set id
$plan = (new \EoneoPaySdk\Entity\Plan)
    ->setId('plan_000000000000000000000000');

// Get plan
$response = $client->get($plan);

// Check response
if ($response->isSuccessful() && $response->hasPlan()) {
    // Retrieved \EoneoPaySdk\Entity\Plan instance
    $plan = $response->getPlan();
}
```

## Update plan

When updating a plan you must have an [existing plan entity](../v2.md#markdown-header-existing-entities).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `amount` | Yes | Yes | Plan amount, in cents |
| `interval` | Yes | Yes | Plan interval: day, week, month or year |
| `name` | Yes | Yes | The name for this plan |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `plan` | `Plan` | Updated [plan entity](#markdown-header-plan-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Update some details, $plan must be an existing 
// \EoneoPaySdk\Entity\Plan instance
$plan->setAmount(500)
    ->setName('Updated Plan');

// Update plan
$response = $client->update($plan);

// Check response
if ($response->isSuccessful() && $response->hasPlan()) {
    // Updated \EoneoPaySdk\Entity\Plan instance
    $plan = $response->getPlan();
}
```

## Plan resource

The plan resource represents a group of plan entities.

Like most resources, [pagination](../v2.md#markdown-header-resources) can be used to limit results.

### Class

`EoneoPaySdk\Resource\Plan`

### Filters

| Attribute | Description |
|-----------|-------------|
| `amount` | Plan amount, in cents |
| `created_at` | SQL compatible date/time the plan was created |
| `currency` | Plan currency code in [ISO 4217 format](https://en.wikipedia.org/wiki/ISO_4217#Active_codes) |
| `id` | The EoneoPay id for the plan entity |
| `interval` | Plan interval: day, week, month or year |
| `interval_count` | How many intervals have been processed |
| `name` | The name for the plan |
| `updated_at` | SQL compatible date/time the plan was last updated |

## List all plans

Plans can be filtered via the [plan resource](#markdown-header-plan-resource_1).

### Response

Plans found will be returned as a [collection](../v2.md#markdown-header-collections-and-repositories) of [plan entities](#markdown-header-plan-entity_1). 

If no plans are found, e.g. the filter returns no results or the page and limit exceeds available plans, an empty collection will be returned.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `plans` | `Plan[]` | Collection of [plan entities](#markdown-header-plan-entity_1) |

If the collection is empty `hasPlans()` will return false.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate plan resource
$plans = new \EoneoPaySdk\Resource\Plan;

// Optionally add filter results
$plans->whereName('Test')->whereAmount(100);

// Optionally set pagination and limit
$plans->limit(10)->page(1);

// Get plan list
$response = $client->list($plans);

// Check response
if ($response->isSuccessful() && $response->hasPlans()) {
    // \EoneoPaySdk\Sdk\Collection instance containing 
    // \EoneoPaySdk\Entity\Plan entities 
    $plans = $response->getPlans();
}
```
