# Clearing accounts

Clearing accounts represent a bank account entity which is used to clear funds for a merchant.

## Contents

##### [Clearing account entity](#markdown-header-clearing-account-entity_1)
* [Get clearing account by id](#markdown-header-get-clearing-account-by-id)

##### [Clearing account resource](#markdown-header-clearing-account-resource_1)
* [List all clearing accounts](#markdown-header-list-all-clearing-accounts)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* [Charges](charges.md#markdown-headers-charges)
* Clearing accounts
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payments.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## Clearing account entity

A clearing account entity represents a single clearing account.

### Class

`EoneoPaySdk\Entity\ClearingAccount`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `account` | `string` | No | SWIFT id in [ISO 9362 format](https://en.wikipedia.org/wiki/ISO_9362) |
| `bsb` | `string` | No | 6 digit Bank-State-Branch number |
| `direct_entry_credit_description` | `string` | No | Description for bank account credits |
| `direct_entry_credit_id` | `string` | No | User id for bank account credits |
| `direct_entry_credit_name` | `string` | No | Name for bank account credits |
| `direct_entry_debit_description` | `string` | No | Description for bank account debits |
| `direct_entry_debit_id` | `string` | No | User id for bank account debits |
| `direct_entry_debit_name` | `string` | No | Name for bank account debits |
| `id` | `string` | No | The EoneoPay clearing account id |
| `merchant_id` | `string` | No | Payment processor merchant id |
| `merchant_password` | `string` | No | Payment processor merchant password |
| `name` | `string` | No | Clearing account name |
| `trust` | `string` | No | Whether clearing account can be trusted or not |

## Get clearing account by id

A clearing account can be retrieved via it's unique [EoneoPay id](#markdown-header-attributes).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `id` | Yes | No | The EoneoPay id for this entity |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `clearing_account` | `ClearingAccount` | Retrieved [clearing account entity](#markdown-header-clearing-account-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with id
$clearingAccount = new \EoneoPaySdk\Entity\ClearingAccount([
	'id' => 1
]);

// Or instantiate and set id
$clearingAccount = (new \EoneoPaySdk\Entity\ClearingAccount)
	->setId(1);

// Get clearing account
$response = $client->get($clearingAccount);

// Check response
if ($response->isSuccessful() && $response->hasClearingAccount()) {
	// Retrieved \EoneoPaySdk\Entity\ClearingAccount instance
	$clearingAccount = $response->getClearingAccount();
}
```

## Clearing account resource

The clearing account resource represents a group of clearing accounts.

Like most resources, [pagination](../v2.md#markdown-header-resources) can be used to limit results.

### Class

`EoneoPaySdk\Resource\ClearingAccount`

### Filters

| Attribute | Description |
|-----------|-------------|
| `account` | SWIFT id in [ISO 9362 format](https://en.wikipedia.org/wiki/ISO_9362) |
| `bsb` | 6 digit Bank-State-Branch number |
| `direct_entry_credit_description` | Description for bank account credits |
| `direct_entry_credit_id` | User id for bank account credits |
| `direct_entry_credit_name` | Name for bank account credits |
| `direct_entry_debit_description` | Description for bank account debits |
| `direct_entry_debit_id` | User id for bank account debits |
| `direct_entry_debit_name` | Name for bank account debits |
| `id` | The EoneoPay clearing account id |
| `merchant_id` | Payment processor merchant id |
| `merchant_password` | Payment processor merchant password |
| `name` | Clearing account name |
| `trust` | Whether clearing account can be trusted or not |

## List all clearing accounts

Clearing accounts can be filtered via the [clearing account resource](#markdown-header-clearing-account-resource_1).

### Response

Clearing accounts found will be returned as a [collection](../v2.md#markdown-header-collections-and-repositories) of [clearing account entities](#markdown-header-clearing-account-entity_1). 

If no clearing accounts are found, e.g. the filter returns no results or the page and limit exceeds available clearing accounts, an empty collection will be returned.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `clearing_accounts` | `ClearingAccount[]` | Collection of [clearing account entities](#markdown-header-clearing-account-entity_1) |

If the collection is empty `hasClearingAccounts()` will return false.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate clearing account resource
$clearingAccounts = new \EoneoPaySdk\Resource\ClearingAccount;

// Optionally add filter results
$clearingAccounts->whereName('Test');

// Optionally set pagination and limit
$clearingAccounts->limit(10)->page(1);

// Get clearing account list
$response = $client->list($clearingAccounts);

// Check response
if ($response->isSuccessful() && $response->hasClearingAccounts()) {
	// \EoneoPaySdk\Sdk\Collection instance containing 
	// \EoneoPaySdk\Entity\ClearingAccount entities 
	$clearingAccounts = $response->getClearingAccounts();
}
```
