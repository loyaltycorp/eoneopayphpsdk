# Fees

Fees are additional charges based on a payment method.

## Contents

##### [Fee entity](#markdown-header-fee-entity_1)
* [Delete fee](#markdown-header-delete-fee)
* [Get fee by type](#markdown-header-get-fee-by-type)
* [Set fee](#markdown-header-set-fee)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* Fees
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payments.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## Fee entity

The fee entity represents a fee for a single payment type.

### Class

`EoneoPaySdk\Entity\Fee`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `card_rates` | `CardRate` | Yes | [Card rates](#markdown-header-card_rates-attributes) as a percentage |
| `merchant_id` | `string` | No | EoneoPay id of the [merchant](merchants.md#markdown-header-merchants) responsible for this fee |
| `payment_type` | `string` | Yes | The [payment type](#markdown-header-payment-types) for this fee |
| `transaction_fee` | `int` | Yes | Fixed fee per transaction, in cents |

#### *card_rates* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `AMEX` | `float` | Yes | The fee percentage for AMEX transactions |
| `MasterCard` | `float` | Yes | The fee percentage for MasterCard transactions |
| `Visa` | `float` | Yes | The fee percentage for Visa transactions |

### Constants

#### Credit card types

| Value | Constant | Description |
|-------|----------|-------------|
| `AMEX` | `CARD_AMEX` | American express |
| `MasterCard` | `CARD_MASTERCARD` | MasterCard |
| `Visa` | `CARD_VISA` | Visa |

#### Payment types

| Value | Constant | Description |
|-------|----------|-------------|
| `auspost` | `TYPE_AUSPOST` | Australia Post payment |
| `auspost_adjustment` | `TYPE_AUSPOST_ADJUSTMENT` | Adjustment to Australia Post payment |
| `auspost_dishonoured` | `TYPE_AUSPOST_DISHONOURED` | Dishonoured Australia Post payment |
| `bpay` | `TYPE_BPAY` | BPAY payment |
| `bpay_correction` | `TYPE_BPAY_CORRECTION` | Correction to BPAY payment |
| `bpay_reversal` | `TYPE_AUSPOST_REVERSAL` | Reversed BPAY payment |
| `cc_payment` | `TYPE_CREDIT_CARD_PAYMENT` | Credit card payment |
| `cc_refund` | `TYPE_CREDIT_CARD_REFUND` | Credit card refund |
| `cheque` | `TYPE_CHEQUE` | Cheque payment |
| `direct_debit` | `TYPE_DIRECT_DEBIT` | Direct debit payment |
| `direct_deposit` | `TYPE_DIRECT_DEPOSIT` | Direct deposit payment |
| `eft` | `TYPE_EFT` | Electronic funds transfer |
| `eft_dishonoured` | `TYPE_EFT_DISHONORED` | Dishonoured electronic funds transfer |
| `ewallet` | `TYPE_EWALLET` | eWallet payment |

## Delete fee

When deleting a fee you must have an [existing fee entity](../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when deleting a fee.

### Response attributes

The response will only contain the [standard response](../v2.md#markdown-header-responses) attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Delete fee, $fee must be an existing
// \EoneoPaySdk\Entity\Fee instance
$response = $client->delete($fee);

// Check response
if ($response->isSuccessful()) {
	// Fee has been successfully deleted
}
```

## Get fee by type

A fee can be retrieved via it's [payment type](#markdown-header-attributes).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `payment_type` | Yes | Yes | The [payment type](#markdown-header-payment-types) for this entity |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `fee` | `Fee` | Retrieved [fee entity](#markdown-header-fee-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with payment type
$fee = new \EoneoPaySdk\Entity\Fee([
	'payment_type' => \EoneoPaySdk\Entity\Fee::TYPE_BPAY,
]);

// Or instantiate and set payment type
$fee = (new \EoneoPaySdk\Entity\Fee)
	->setPaymentType(\EoneoPaySdk\Entity\Fee::TYPE_BPAY);

// Get fee
$response = $client->get($fee);

// Check response
if ($response->isSuccessful() && $response->hasFee()) {
	// Retrieved \EoneoPaySdk\Entity\Fee instance
	$fee = $response->getFee();
}
```

## Set fee

A fee can be set on an [existing fee entity](../v2.md#markdown-header-existing-entities) or a newly created entity.

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `card_rates` | No | Yes | [Card rates](#markdown-header-card_rates-attributes) as a percentage |
| `payment_type` | Yes | Yes | The [payment type](#markdown-header-payment-types) for this fee |
| `transaction_fee` | Yes | Yes | Fixed fee per transaction, in cents |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `fee` | `Fee` | Updated [fee entity](#markdown-header-fee-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Create a fee entity, an existing \EoneoPaySdk\Entity\Fee 
// instance can be used as well
$fee = new \EoneoPaySdk\Entity\Fee([
	'card_rates' => [
		\EoneoPaySdk\Entity\Fee::CARD_AMEX => 5.5,
		\EoneoPaySdk\Entity\Fee::CARD_VISA => 1.5,
	],
	'payment_type' => \EoneoPaySdk\Entity\Fee::TYPE_BPAY,
	'transaction_fee' => 20,
]);

// Or create and set/update fee
$fee = (new \EoneoPaySdk\Entity\Fee)
	->setPaymentType(\EoneoPaySdk\Entity\Fee::TYPE_BPAY)
	->setTransactionFee(20);

// Set card fees	
$fee->getCardRates()->setAmex(5.5)->setVisa(1.5);

// Create/update fee
$response = $client->update($fee);

// Check response
if ($response->isSuccessful() && $response->hasFee()) {
	// Updated \EoneoPaySdk\Entity\Fee instance
	$fee = $response->getFee();
}
```
