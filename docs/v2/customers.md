# Customers

Customers can be used to create, delete, find, get and update customer records.

## Contents

##### [Customer entity](#markdown-header-customer-entity_1)
* [Create customer](#markdown-header-create-customer)
* [Delete customer](#markdown-header-delete-customer)
* [Get customer by id](#markdown-header-get-customer-by-id)
* [Update customer](#markdown-header-update-customer)

##### [Customer resource](#markdown-header-customer-resource_1)
* [List all customers](#markdown-header-list-all-customers)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* Customers
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payments.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## Customer entity

The customer entity represents a single customer record.

### Class

`EoneoPaySdk\Entity\Customer`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `bank_accounts` | `BankAccount[]` | No | A collection of bank account [payment source entities](payment-sources.md#markdown-header-payment-source-entity_1) |
| `created_at` | `string` | No | SQL compatible date/time this customer was created |
| `credit_cards` | `CreditCard[]` | No | A collection of credit card [payment source entities](payment-sources.md#markdown-header-payment-source-entity_1) |
| `email` | `string` | Yes | Customer email address |
| `external_customer_id` | `string` | Yes | Internal reference for this customer within your system |
| `ewallet_id` | `string` | No | The primary [eWallet](ewallets.md#markdown-header-ewallets) EoneoPay id for this customer |
| `first_name`| `string` | Yes | Customer first name |
| `id` | `string` | No | The EoneoPay customer id |
| `last_name` | `string` | Yes | Customer last name |
| `merchant_id` | `string` | No | EoneoPay id of the [merchant](merchants.md#markdown-header-merchants) responsible for this customer |
| `reference_numbers` | `Collection` | No | |
| `updated_at` | `string` | No | SQL compatible date/time this customer was last updated |

## Create customer

A customer is a single user within your application.

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `email`   | No        | Yes       | Email address |
| `external_customer_id` | No | No | Reference within your system, can be used to find customer at a later time |
| `first_name` | Yes | No | First name |
| `last_name` | Yes | No | Last name |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `customer` | `Customer` | Created [customer entity](#markdown-header-customer-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with customer details
$customer = new \EoneoPaySdk\Entity\Customer([
	'external_customer_id' => '1234567890',
	'first_name' => 'Test', 
	'last_name' => 'User'
]);

// Or instantiate and set customer details
$customer = (new \EoneoPaySdk\Entity\Customer)
	->setExternalCustomerId('1234567890')
	->setFirstName('Test')
	->setLastName('User');

// Create customer
$response = $client->create($customer);

// Check response
if ($response->isSuccessful() && $response->hasCustomer()) {
	// Created \EoneoPaySdk\Entity\Customer instance
	$customer = $response->getCustomer();
}
```

## Delete customer

When deleting a customer you must have an [existing customer entity](../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when deleting a customer.

### Response attributes

The response will only contain the [standard response](../v2.md#markdown-header-responses) attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Delete customer, $customer must be an existing
// \EoneoPaySdk\Entity\Customer instance
$response = $client->delete($customer);

// Check response
if ($response->isSuccessful()) {
	// Customer has been successfully deleted
}
```

## Get customer by id

A customer can be retrieved via it's unique [EoneoPay id](#markdown-header-attributes) or your internal customer reference if it has been previously set as the external customer id on the entity.

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `id` | Yes | No | The EoneoPay id or external customer id for this entity |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `customer` | `Customer` | Retrieved [customer entity](#markdown-header-customer-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with id
$customer = new \EoneoPaySdk\Entity\Customer([
	'id' => 'cus_000000000000000000000000'
]);

// Or instantiate and set id
$customer = (new \EoneoPaySdk\Entity\Customer)
	->setId('cus_000000000000000000000000');
	
// Or use your own internal reference which has been 
// previously been set as external_customer_id
$customer = (new \EoneoPaySdk\Entity\Customer)
	->setId('1234567890');

// Get customer
$response = $client->get($customer);

// Check response
if ($response->isSuccessful() && $response->hasCustomer()) {
	// Retrieved \EoneoPaySdk\Entity\Customer instance
	$customer = $response->getCustomer();
}
```

## Update customer

When updating a customer you must have an [existing customer entity](../v2.md#markdown-header-existing-entities).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `email`   | No | Yes | Email address |
| `external_customer_id` | No | No | Reference within your system |
| `first_name` | Yes | No | First name |
| `last_name` | Yes | No | Last name |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `customer` | `Customer` | Updated [customer entity](#markdown-header-customer-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Update some details, $customer must be an existing 
// \EoneoPaySdk\Entity\Customer instance
$customer->setEmail('updated@email.com')
	->setExternalCustomerId('1234567891');

// Update customer
$response = $client->update($customer);

// Check response
if ($response->isSuccessful() && $response->hasCustomer()) {
	// Updated \EoneoPaySdk\Entity\Customer instance
	$customer = $response->getCustomer();
}
```

## Customer resource

The customer resource represents a group of customer entities.

Like most resources, [pagination](../v2.md#markdown-header-resources) can be used to limit results.

### Class

`EoneoPaySdk\Resource\Customer`

### Filters

| Attribute | Description |
|-----------|-------------|
| `created_at` | SQL compatible date/time the customer was created |
| `email` | Customer email address |
| `external_customer_id` | Internal reference for the customer within your system |
| `ewallet_id` | The primary [eWallet](ewallets.md#markdown-header-ewallets) EoneoPay id for the customer |
| `first_name`| Customer first name |
| `id` | The EoneoPay customer id |
| `last_name` | Customer last name |
| `updated_at` | SQL compatible date/time the customer was last updated |

## List all customers

Customers can be filtered via the [customer resource](#markdown-header-customer-resource_1).

### Response

Customers found will be returned as a [collection](../v2.md#markdown-header-collections-and-repositories) of [customer entities](#markdown-header-customer-entity_1). 

If no customers are found, e.g. the filter returns no results or the page and limit exceeds available customers, an empty collection will be returned.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `customers` | `Customer[]` | Collection of [customer entities](#markdown-header-customer-entity_1) |

If the collection is empty `hasCustomers()` will return false.

> **Note:** Although a collection of customer entities will be returned, associated entities such as payment sources will only contain the EoneoPay id. To get the associated entity it will either need to be retrieved directly or you will need to [get the customer entity by id](#markdown-header-get-customer-by-id).

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate customer resource
$customers = new \EoneoPaySdk\Resource\Customer;

// Optionally add filter results
$customers->whereFirstName('Test')->whereLastName('User');

// Optionally set pagination and limit
$customers->limit(10)->page(1);

// Get customer list
$response = $client->list($customers);

// Check response
if ($response->isSuccessful() && $response->hasCustomers()) {
	// \EoneoPaySdk\Sdk\Collection instance containing 
	// \EoneoPaySdk\Entity\Customer entities 
	$customers = $response->getCustomers();
}
```
