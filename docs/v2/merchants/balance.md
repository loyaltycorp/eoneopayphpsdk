# Balance

Balance is used to determine the pending and available balance of the merchant account in EoneoPay.

## Contents

##### [Balance entity](#markdown-header-balance-entity_1)
* [Get balance](#markdown-header-get-balance)
* [Update available balance](#markdown-header-update-available-balance)

##### [Balance history resource](#markdown-header-balance-history-resource_1)
* [Get balance history](#markdown-header-get-balance-history)

##### Endpoints
* [Api keys](../api-keys.md#markdown-header-api-keys)
* [Charges](../charges.md#markdown-header-charges)
* [Clearing accounts](../clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](../configurations.md#markdown-header-configurations)
* [Customers](../customers.md#markdown-header-customers)
    - [Reference numbers](../customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](../customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](../customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](../ewallets.md#markdown-header-ewallets)
    - [Statements](../ewallets/statements.md#markdown-header-statements)
* [Fees](../fees.md#markdown-header-fees)
* [Merchants](../merchants.md#markdown-header-merchants)
    - Balance
    - [Fees](transfers.md#markdown-header-fees)
    - [Transfers](transfers.md#markdown-header-transfers)
* [Payment allocations](../payments.md#markdown-header-payment-allocations)
* [Payment sources](../payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](../payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](../payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](../payments.md#markdown-header-payments)
* [Plans](../plans.md#markdown-header-plans)
* [Reconciliation reports](../reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](../search.md#markdown-header-search)
* [System status](../system-status.md#markdown-header-system-status)
* [Tokenisation](../tokenisation.md#markdown-header-tokenisation)
* [Webhooks](../webhooks.md#markdown-header-webhooks)

## Balance entity

The balance entity represents the current merchant balance within EoneoPay.

### Class

`EoneoPaySdk\Entity\Merchant\Balance`

#### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `available` | `Repository|null` | No | Available funds separated by currency code in [ISO 4217 format](https://en.wikipedia.org/wiki/ISO_4217#Active_codes) |
| `total` | `Repository|null` | No | Total funds separated by currency code in [ISO 4217 format](https://en.wikipedia.org/wiki/ISO_4217#Active_codes) |

If there are no available or total funds the attributes will be null.

#### *balance -> available* and *balance -> total* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `xxx` | `Repository` | No | For each currency where `xxx` represents a currency code in [ISO 4217 format](https://en.wikipedia.org/wiki/ISO_4217#Active_codes) |

#### *balance -> available -> xxx* and *balance -> total -> xxx* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `amount` | `int` | No | The funds, in cents |

## Get balance

### Request attributes

There are no settable attributes when retrieving merchant balance.

### Response

| Attribute | Type | Description |
|-----------|------|-------------|
| `balance` | `Balance` | Retrieved [balance entity](#markdown-header-balance-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate balance entity
$balance = new \EoneoPaySdk\Entity\Merchant\Balance;

// Get balance
$response = $client->get($balance);

// Check response
if ($response->isSuccessful() && $response->hasBalance()) {
	// \EoneoPaySdk\Entity\Merchant\Balance instance
	$balance = $response->getBalance();
}
```

## Update available balance

There may be situations where the available balance isn't up to date, an update can be forced by updating the balance entity.

To update the available balance you must have an [existing merchant entity](../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when updating a balance.

### Response

| Attribute | Type | Description |
|-----------|------|-------------|
| `merchant` | `Merchant` | Updated [merchant entity](../merchants.md#markdown-header-merchant-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate balance entity, an existing entity
// can also be used
$balance = new \EoneoPaySdk\Entity\Merchant\Balance;

// Attach to merchant, assuming $merchant is an existing
// \EoneoPaySdk\Entity\Merchant instance
$balance->forMerchant($merchant);

// Update balance
$response = $client->update($balance);

// Check response
if ($response->isSuccessful() && $response->hasMerchant()) {
	// Updated \EoneoPaySdk\Entity\Merchant instance
	$merchant = $response->getMerchant();
}
```

## Balance history resource

As well as the overall balance a break down can be retrieved showing where the funds came from.

Balance history does not have any filters or pagination.

### Class

`EoneoPaySdk\Resource\Merchant\Balance\History`

### Filters

There are no filters available when getting the balance history.

## Get balance history

There are no filters available when getting the balance history.

### Response

Balances found will be returned as a [repository](../v2.md#markdown-header-collections-and-repositories) seperated by currency code in [ISO 4217 format](https://en.wikipedia.org/wiki/ISO_4217#Active_codes).

If no available or total balance is found, e.g. there are no cleared or pending funds, [the attributes](#markdown-header-balance-resource_1) will be null.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `balance_history` | `Repository` | Balance history separated into payments, transfers and balance summary  |

#### *balance_history* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `balance` | `Repository` | No | Repository containing [available and total balance](#markdown-header-get-balance) |
| `payments` | `Payment[]` | No | A collection of [payment entities](payments.md#markdown-header-payments) which have been allocated to the balance |
| `transfers` | `Transfer[]` | No | A collection of [transfer entities](transfers.md#markdown-header-transfers) which have been allocated to the balance |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate balance history resource
$history = new \EoneoPaySdk\Resource\Merchant\Balance\History;

// Get balance history
$response = $client->list($history);

// Check response
if ($response->isSuccessful() && $response->hasBalanceHistory()) {
	// \EoneoPaySdk\Sdk\Repository of balance,
	// payment and history 
	$history = $response->getBalanceHistory();
}
```
