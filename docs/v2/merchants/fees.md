# Fees

Merchant fees are [fee entities](../fees.md#markdown-header-fees) which can be attached to a specific merchant.

## Contents

##### [Fee entity](#markdown-header-fee-entity_1)

##### Endpoints
* [Api keys](../api-keys.md#markdown-header-api-keys)
* [Charges](../charges.md#markdown-header-charges)
* [Clearing accounts](../clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](../configurations.md#markdown-header-configurations)
* [Customers](../customers.md#markdown-header-customers)
    - [Reference numbers](../customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](../customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](../customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](../ewallets.md#markdown-header-ewallets)
    - [Statements](../ewallets/statements.md#markdown-header-statements)
* [Fees](../fees.md#markdown-header-fees)
* [Merchants](../merchants.md#markdown-header-merchants)
    - [Balance](balance.md#markdown-header-balance)
    - Fees
    - [Transfers](transfers.md#markdown-header-transfers)
* [Payment allocations](../payments.md#markdown-header-payment-allocations)
* [Payment sources](../payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](../payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](../payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](../payments.md#markdown-header-payments)
* [Plans](../plans.md#markdown-header-plans)
* [Reconciliation reports](../reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](../search.md#markdown-header-search)
* [System status](../system-status.md#markdown-header-system-status)
* [Tokenisation](../tokenisation.md#markdown-header-tokenisation)
* [Webhooks](../webhooks.md#markdown-header-webhooks)

## Fee entity

The fee entity and functionality is identical to the [base fee entity](../fees.md#markdown-header-fees) which also allows attaching an [existing merchant entity](../v2.md#markdown-header-existing-entities) via the `forMerchant()` method.
