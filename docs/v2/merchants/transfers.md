# Transfers

Transfers keep track of movement between total and available funds and allow a merchant to transfer their available funds to a merchant [payment source](../payment-sources.md#markdown-header-payment-sources).

## Contents

##### [Transfer entity](#markdown-header-transfer-entity_1)
* [Create transfer](#markdown-header-create-transfer)
* [Get transfer by id](#markdown-header-get-transfer-by-id)

##### [Transfer resource](#markdown-header-transfer-resource_1)
* [List all transfers](#markdown-header-list-all-transfers)

##### Endpoints
* [Api keys](../api-keys.md#markdown-header-api-keys)
* [Charges](../charges.md#markdown-header-charges)
* [Clearing accounts](../clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](../configurations.md#markdown-header-configurations)
* [Customers](../customers.md#markdown-header-customers)
    - [Reference numbers](../customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](../customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](../customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](../ewallets.md#markdown-header-ewallets)
    - [Statements](../ewallets/statements.md#markdown-header-statements)
* [Fees](../fees.md#markdown-header-fees)
* [Merchants](../merchants.md#markdown-header-merchants)
    - [Balance](balance.md#markdown-header-balance)
    - [Fees](balance.md#markdown-header-fees)
    - Transfers
* [Payment allocations](../payments.md#markdown-header-payment-allocations)
* [Payment sources](../payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](../payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](../payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](../payments.md#markdown-header-payments)
* [Plans](../plans.md#markdown-header-plans)
* [Reconciliation reports](../reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](../search.md#markdown-header-search)
* [System status](../system-status.md#markdown-header-system-status)
* [Tokenisation](../tokenisation.md#markdown-header-tokenisation)
* [Webhooks](../webhooks.md#markdown-header-webhooks)

## Transfer entity

The transfer entity represents a single transfer.

### Class

`EoneoPaySdk\Entity\Merchant\Transfer`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `amount` | `int` | Yes | The amount to transfer |
| `created_at` | `string` | No | SQL compatible date/time this payment was created |
| `currency` | `string` | No | The currency to transfer, always 'AUD' |
| `description` | `string` | Yes | Description of the transfer |
| `destination` | `string` | Yes | The [payment source](../payment-sources.md#markdown-header-payment-sources) destination for this transfer |
| `id` | `string` | No | The EoneoPay id for this transfer |
| `merchant_id` | `string` | No | The EoneoPay id for the associated [merchant](merchants.md#markdown-header-merchants) |
| `payments` | `Payment[]` | No | A collection of [payment entity](payments.md#markdown-header-payment-entity_1) transaction ids successfully reconciled for this transfer |
| `status` | `int` | No | [Transfer status](#markdown-header-transfer-statuses) |
| `status_date` | `string` | No | SQL compatible date/time the status was last updated |
| `status_reason` | `string` | No | More information about the status if applicable |
| `transaction_list` | `Payment[]` | Yes | A collection of [payment entity](payments.md#markdown-header-payment-entity_1) transaction ids to reconcile into this transfer |
| `updated_at` | `string` | No | SQL compatible date/time this payment was created |

#### Transfer statuses

The transfer status will be represented by a number.

| Value | Constant | Status |
|-------|----------|--------|
| `0` | `STATUS_CREATED` | Created/pending |
| `1` | `STATUS_QUEUED` | Queued for transfer |
| `2` | `STATUS_PENDING` | Sent to bank for processing |
| `3` | `STATUS_FAILED` | Failed, reason will be in `status_reason` |
| `4` | `STATUS_PROCESSING` | Currently being processed |
| `5` | `STATUS_DISBURSED` | Disbursed by bank |
| `6` | `STATUS_RETURNED` | Funds returned by bank |
| `7` | `STATUS_TRUST_DISBURSED` | Disbursed to internal trust account |

## Create transfer

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `amount` | Yes | Yes | The amount to transfer |
| `description` | No | No | Description of the transfer |
| `destination` | Yes | Yes | The [payment source](../payment-sources.md#markdown-header-payment-sources) destination for this transfer |
| `transaction_list` | Yes | Yes | A collection of [payment entity](payments.md#markdown-header-payment-entity_1) transaction ids to reconcile into this transfer |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `transfer` | `Transfer` | Created [transfer entity](#markdown-header-transfer-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with transfer details
$transfer = new \EoneoPaySdk\Entity\Transfer([
	'amount' => 100,
]);

// Or instantiate and set transfer details
$transfer = (new \EoneoPaySdk\Entity\Transfer)
	->setAmount(100);
	
// Set destination, assuming $bankAccount is an existing
// \EoneoPaySdk\Entity\PaymentSource\BankAccount instance
$transfer->toBankAccount($bankAccount);

// Add transactions, assuming $payments is a collection of
// \EoneoPaySdk\Entity\Payment instances
foreach ($payments as $payment) {
	$transfer->addPayment($payment);
}

// Create transfer
$response = $client->create($transfer);

// Check response
if ($response->isSuccessful() && $response->hasTransfer()) {
	// Created \EoneoPaySdk\Entity\Transfer instance
	$transfer = $response->getTransfer();
}
```

#### Example using payment resource

The [payment resource](../payments.md#markdown-header-payment-resource_1) with a filter on `merchant_tranfer_status` is a simple way to get all payments available for transfer.

```php
// Get payments which are available for transfer, $payment
// must be a valid \EoneoPaySdk\Resource\Payment instance
$payment->whereMerchantTransferStatus($payment::TRANSFER_STATUS_AVAILABLE);
$response = $client->get($payment);

if ($response->isSuccessful() && $response->hasPayments()) {
	// Keep track of payment balance
	$balance = 0;
	
	foreach ($response->getPayments() as $payment) {
		$transfer->addPayment($payment);
		$balance += $payment->getAmount() - $payment->getFeeAmount();
	}
	
	// Set amount on transfer
	$transfer->setAmount($balance);
}
```

## Get transfer by id

A transfer can be retrieved via it's unique [EoneoPay id](#markdown-header-attributes).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `id` | Yes | No | The EoneoPay id for this entity |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `transfer` | `Transfer` | Retrieved [transfer entity](#markdown-header-transfer-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with id
$transfer = new \EoneoPaySdk\Entity\Transfer([
	'id' => 'tfr_000000000000000000000000'
]);

// Or instantiate and set id
$transfer = (new \EoneoPaySdk\Entity\Transfer)
	->setId('tfr_000000000000000000000000');

// Get transfer
$response = $client->get($transfer);

// Check response
if ($response->isSuccessful() && $response->hasTransfer()) {
	// Retrieved \EoneoPaySdk\Entity\Transfer instance
	$transfer = $response->getTransfer();
}
```

## Transfer resource

The transfer resource represents a group of transfers.

Like most resources, [pagination](../v2.md#markdown-header-resources) can be used to limit results.

### Class

`EoneoPaySdk\Resource\Merchant\Transfer`

### Filters

| Attribute | Description |
|-----------|-------------|
| `active` | Whether the transfer is currently active, defaults to true |
| `created_at` | SQL compatible date/time this payment was created |
| `event_name` | The event which triggers the transfer |
| `id` | The EoneoPay id for this transfer |
| `name` | The name/description for this transfer |
| `target_url` | The url endpoint to send payload to |
| `updated_at` | SQL compatible date/time this payment was created |

## List all transfers

Transfers can be filtered via the [transfer resource](#markdown-header-transfer-resource_1).

### Response

Transfers found will be returned as a [collection](../v2.md#markdown-header-collections-and-repositories) of [transfer entities](#markdown-header-transfer-entity_1). 

If no transfers are found, e.g. the filter returns no results or the page and limit exceeds available transfers, an empty collection will be returned.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `transfers` | `Transfer[]` | Collection of [transfer entities](#markdown-header-transfer-entity_1) |

If the collection is empty `hasTransfers()` will return false.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate transfer resource
$transfers = new \EoneoPaySdk\Resource\Transfer;

// Optionally add filter results
$transfers->whereAmount(100)
	->whereStatus($transfers::STATUS_DISBURSED);

// Optionally set pagination and limit
$transfers->limit(10)->page(1);

// Get transfer list
$response = $client->list($transfers);

// Check response
if ($response->isSuccessful() && $response->hasTransfers()) {
	// \EoneoPaySdk\Sdk\Collection instance containing 
	// \EoneoPaySdk\Entity\Transfer entities 
	$transfers = $response->getTransfers();
}
```
