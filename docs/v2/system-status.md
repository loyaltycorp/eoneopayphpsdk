# System status

The system status displays a list of payments, transfers and allocations within EoneoPay within a specific timeframe.

## Contents

##### [System status entity](#markdown-header-system-status-entity_1)
* [Get system status](#markdown-header-get-system-status)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payments.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* System status
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## System status entity

### Class

`EoneoPaySdk\Entity\SystemStatus`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `from_date` | `string` | No | SQL compatible date/time to get status from |
| `merchant_id` | `string` | No | The merchant id to filter the status by |
| `payment_allocations` | `Repository` | No | See [payment allocations attributes](#markdown-header-payment_allocations-attributes) |
| `payments` | `Repository` | No | See [payments attributes](#markdown-header-payments-attributes) |
| `to_date` | `string` | No | SQL compatible date/time to get status to |
| `transfers` | `Repository` | No | See [trasfers attributes](#markdown-header-payments-attributes) |

#### *payment_allocations* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `all` | `Repository` | No | All statuses, see [status attributes](#markdown-header-status-attributes) |
| `by_type` | `Repository` | No | Statuses grouped by type, see [payment allocations by type attributes](#markdown-payment-allocations-by-type-attributes) |

#### *payment_allocations -> by\_type* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `xxx` | `Repository` | No | Statuses grouped by type where `xxx` represents the namespaced class within EoneoPay, see [status attributes](#markdown-header-status-attributes) |

#### *payments* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `all` | `Repository` | No | All statuses, see [status attributes](#markdown-header-status-attributes) |
| `by_merchant` | `Repository` | No | Statuses grouped by merchant, see [payments by merchant attributes](#markdown-header-payments-by-merchant-attributes) |
| `by_type` | `Repository` | No | Statuses grouped by type, see [payments by type attributes](#markdown-payments-by-type-attributes) |

#### *payments -> by_merchant* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `xxx` | `Repository` | No | Statuses grouped by merchant where `xxx` represents the [EoneoPay id for the merchant](merchants.md#markdown-header-attributes), see [status attributes](#markdown-header-status-attributes) |

#### *payments -> by_type* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `xxx` | `Repository` | No | Statuses grouped by payment type where `xxx` represents the [payment type](fees.md#markdown-header-payment-types), see [status attributes](#markdown-header-status-attributes) |

#### *transfers* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `all` | `Repository` | No | All statuses, see [status attributes](#markdown-header-status-attributes) |
| `by_status` | `Repository` | No | Statuses grouped by status, see [transfers by status attributes](#markdown-header-payments-by-merchant-attributes) |

#### *transfers -> by_status* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `xxx` | `Repository` | No | Statuses grouped by transfer status where `xxx` represents the [transfer status id](merchants/transfers.md#markdown-header-transfer-statuses), see [status attributes](#markdown-header-status-attributes) |

#### *status* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `count` | `int` | No | The number of transactions for this status |
| `total_amount` | `int` | No | The transaction totals, in cents |
| `total_fee_amount` | `int` | No | The transaction fee totals, in cents |

## Get system status

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `from_date` | No | No | SQL compatible date/time to get status from |
| `to_date` | No | No | SQL compatible date/time to get status to |

The status can also be filtered to a single merchant by attaching an [existing merchant entity](../v2.md#markdown-header-existing-entities) via the `forMerchant()` method.

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `system_status` | `SystemStatus` | Retrieved [system status entity](#markdown-header-system-status-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with dates
$status = new \EoneoPaySdk\Entity\SystemStatus([
	'from_date' => '1 January',
	'to_date' => '31 December',
]);

// Or instantiate and set dates
$status = (new \EoneoPaySdk\Entity\SystemStatus)
	->setFromDate('1 January')
	->setToDate('31 December');

// Optionally attach to a merchant assuming $merchant is
// an existing \EoneoPaySdk\Entity\Merchant instance
$status->forMerchant($merchant);

// Get status
$response = $client->get($status);

// Check response
if ($response->isSuccessful() && $response->hasSystemStatus()) {
	// Retrieved \EoneoPaySdk\Entity\SystemStatus instance
	$status = $response->getSystemStatus();
}
```
