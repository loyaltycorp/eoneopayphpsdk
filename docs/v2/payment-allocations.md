# Payment allocations

Payment allocations can be used to show where transactions have been allocated.

## Contents

##### [Payment allocation entity](#markdown-header-payment-allocation-entity_1)
* [Get payment allocation by id](#markdown-header-get-payment-allocation-by-id)

##### [Payment allocation resource](#markdown-header-payment-allocation-resource_1)
* [Get all payment allocations](#markdown-header-get-all-payment-allocations)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* Payment allocations
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## Payment allocation entity

The payment allocation entity represents a single payment allocation.

### Class

`EoneoPaySdk\Entity\PaymentAllocation`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `allocation_date` | `string` | No | SQL compatible date this transaction was allocated |
| `allocation_id` | `string` | No | The EoneoPay id for the entity the transaction was allocated to |
| `allocation_type` | `string` | No | The namespaced class within EoneoPay this transaction was allocated to |
| `amount` | `int` | No | The transaction amount |
| `created_at` | `string` | No | SQL compatible date/time this transaction was created |
| `customer_id` | `string` | No | The EoneoPay id for the associated [customer](customers.md#markdown-header-customers) |
| `deallocated` | `bool` | No | Whether this transaction is deallocated or not |
| `fee_amount` | `int` | No | The fee amount for this transaction |
| `id` | `string` | No | The EoneoPay id for this entity |
| `merchant_id` | `string` | No | The EoneoPay id for the associated [merchant](merchants.md#markdown-header-merchants) |
| `txn_id` | `string` | No | The EoneoPay id for the associated [transaction](payments.md#markdown-header-payments) |
| `updated_at` | `string` | No | SQL compatible date/time this transaction was last updated |
| `version` | `int` | No | The version/revision for this entity |

## Get payment allocation by id

A payment allocation can be retrieved via it's unique [EoneoPay id](#markdown-header-attributes).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `id` | Yes | Yes | The EoneoPay id for this entity |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `payment_allocation` | `PaymentAllocation` | Retrieved [payment allocation entity](#markdown-header-payment-allocation-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with id
$paymentAllocation = new \EoneoPaySdk\Entity\PaymentAllocation([
	'id' => 'pal_000000000000000000000000'
]);

// Or instantiate and set id
$paymentAllocation = (new \EoneoPaySdk\Entity\PaymentAllocation)
	->setId('pal_000000000000000000000000');

// Get payment allocation
$response = $client->get($paymentAllocation);

// Check response
if ($response->isSuccessful() && $response->hasPaymentAllocation()) {
	// Retrieved \EoneoPaySdk\Entity\PaymentAllocation instance
	$paymentAllocation = $response->getPaymentAllocation(); 
}
```

## Payment allocation resource

The payment allocation resource represents a group of payment allocation entities.

Like most resources, [pagination](../v2.md#markdown-header-resources) can be used to limit results.

### Class

`EoneoPaySdk\Resource\PaymentAllocation`

### Filters

| Attribute | Description |
|-----------|-------------|
| `allocation_date` | SQL compatible date this transaction was allocated |
| `allocation_id` | The EoneoPay id for the entity the transaction was allocated to |
| `allocation_type` | The namespaced class within EoneoPay this transaction was allocated to |
| `amount` | The transaction amount |
| `created_at` | SQL compatible date/time this transaction was created |
| `customer_id` | The EoneoPay id for the associated [customer](customers.md#markdown-header-customers) |
| `deallocated` | Whether this transaction is deallocated or not |
| `fee_amount` | The fee amount for this transaction |
| `id` | The EoneoPay id for this entity |
| `merchant_id` | The EoneoPay id for the associated [merchant](merchants.md#markdown-header-merchants) |
| `txn_id` | The EoneoPay id for the associated [transaction](payments.md#markdown-header-payments) |
| `updated_at` | SQL compatible date/time this transaction was last updated |
| `version` | The version/revision for this entity |

## List all payment allocations

Payment allocations can be filtered via the [payment allocation resource](#markdown-header-payment-allocation-resource_1).

You can also search between allocation dates by using the `fromDate()` and `toDate()` methods.

### Response

Payment allocations found will be returned as a [collection](../v2.md#markdown-header-collections-and-repositories) of [payment allocation entities](#markdown-header-payment-allocation-entity_1). 

If no payment allocations are found, e.g. the filter returns no results or the page and limit exceeds available payment allocations, an empty collection will be returned.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `payment_allocations` | `PaymentAllocation[]` | Collection of [payment allocations entities](#markdown-header-payment-allocations-entity_1) |

If the collection is empty `hasPaymentAllocations()` will return false.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate payment allocation resource
$paymentAllocations = new \EoneoPaySdk\Resource\PaymentAllocation;

// Optionally add filter results
$paymentAllocations->whereAmount(100);

// Optionally set pagination and limit
$paymentAllocations->limit(10)->page(1);

// Get payment allocation list
$response = $client->list($paymentAllocations);

// Check response
if ($response->isSuccessful() && $response->hasPaymentAllocations()) {
	// \EoneoPaySdk\Sdk\Collection instance containing 
	// \EoneoPaySdk\Entity\PaymentAllocation entities 
	$paymentAllocations = $response->getPaymentAllocations();
}
```
