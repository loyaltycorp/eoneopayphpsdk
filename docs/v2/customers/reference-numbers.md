# Reference numbers

Reference numbers are used with to allocate funds for a [payment](../payments.md#markdown-header-payments) and generate images to accept payments.

## Contents

##### [Reference number entity](#markdown-header-reference-number-entity_1)
* [Delete reference number](#markdown-header-delete-reference-number)

##### [Australia Post reference number entity](#markdown-header-australia-post-reference-number-entity_1)
* [Create Australia Post reference number](#markdown-header-create-create-australia-post-reference-number)
* [Generate Australia Post barcode](#markdown-header-get-australia-post-barcode)

##### [BPAY reference number entity](#markdown-header-bpay-reference-number-entity_1) 
* [Create BPAY reference number](#markdown-header-create-bpay-reference-number)
* [Generate BPAY bill advice](#markdown-header-get-australia-post-barcode)

##### [Electronic funds transfer reference number entity](#markdown-header-electronic-funds-transfer-reference-number-entity_1)
* [Create electronic funds transfer reference number](#markdown-header-create-electronic-funds-transfer-reference-number)

##### Endpoints
* [Api keys](../api-keys.md#markdown-header-api-keys)
* [Charges](../charges.md#markdown-header-charges)
* [Clearing accounts](../clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](../configurations.md#markdown-header-configurations)
* [Customers](../customers.md#markdown-header-customers)
    - Reference numbers
    - [Subscriptions](subscriptions.md#markdown-header-subscriptions)
        - [Statements](subscriptions/statements.md#markdown-header-statements)
* [eWallets](../ewallets.md#markdown-header-ewallets)
    - [Statements](../ewallets/statements.md#markdown-header-statements)
* [Fees](../fees.md#markdown-header-fees)
* [Merchants](../merchants.md#markdown-header-merchants)
    - [Balance](../merchants/balance.md#markdown-header-balance)
    - [Fees](../merchants/fees.md#markdown-header-fees)
    - [Transfers](../merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](../payments.md#markdown-header-payment-allocations)
* [Payment sources](../payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](../payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](../payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](../payments.md#markdown-header-payments)
* [Plans](../plans.md#markdown-header-plans)
* [Reconciliation reports](../reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](../search.md#markdown-header-search)
* [System status](../system-status.md#markdown-header-system-status)
* [Tokenisation](../tokenisation.md#markdown-header-tokenisation)
* [Webhooks](../webhooks.md#markdown-header-webhooks)

## Reference number entity

The reference number entity represents a single customer reference number.

### Class

`EoneoPaySdk\Entity\Customer\ReferenceNumber`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `allocated` | `bool` | No | Whether the reference number has been allocated or not |
| `allocation_date` | `string` | No | SQL compatible date the reference number was allocated |
| `amount` | `string` | Yes | Amount in cents, only used to [generate an Australia Post barcode](#markdown-header-get-australia-post-barcode)
| `created_at` | `string` | No | SQL compatible date/time this reference number was created |
| `customer_id` | `string` | No | The EoneoPay id for the associated [customer](customers.md#markdown-header-customers) |
| `payment_facility` | `AusPost|Bpay|Eft` | No | Payment facility: [AusPost](#markdown-header-australia-post-reference-number_entity_1), [BPAY](#markdown-header-bpay-reference-number_entity_1) or [EFT](#markdown-header-electronic-funds-transfer-reference-number_entity_1) |
| `reference_number` | `string` | No | The reference number itself |
| `type` | `string` | No | The type of reference number |
| `updated_at` | `string` | No | SQL compatible date/time this reference number was last updated |

## Delete reference number

When deleting a payment reference you must have an [existing reference number entity](../../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when deleting a payment reference.

### Response attributes

The response will only contain the [standard response](../../v2.md#markdown-header-responses) attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Delete reference number, $referenceNumber must be an existing
// \EoneoPaySdk\Entity\Customer\ReferenceNumber instance
$response = $client->delete($referenceNumber);

// Check response
if ($response->isSuccessful()) {
	// Reference number has been successfully deleted
}
```

## Australia Post reference number entity

### Class

`EoneoPaySdk\Entity\Customer\ReferenceNumber\AusPost`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `clearing_account` | `int` | No | Clearing account id |
| `default` | `bool` | No | Whether this is the default reference number for it's type |
| `pip_id` | `string` | Yes | The Australia Post PIP id |
| `type` | `string` | No | The reference number type |

## Create Australia Post reference number

Reference numbers are not stand alone entities, they must be attached to an existing [customer](../customers.md#markdown-header-customers) via the `forCustomer()` method before being created.

### Request attributes

There are no settable attributes when creating an Australia Post reference number.

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `customer` | `Customer` | Updated [customer entity](../customers.md#markdown-header-customer-entity_1) with created reference number |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate
$referenceNumber = new \EoneoPaySdk\Entity\Customer\ReferenceNumber\AusPost;

// Optionally use existing PIP id
$referenceNumber->setPipId('000');
	
// Attach reference number to a customer assuming $customer 
// is an existing \EoneoPaySdk\Entity\Customer instance
$referenceNumber->forCustomer($customer);

// Create reference number
$response = $client->create($referenceNumber);

// Check response
if ($response->isSuccessful() && $response->hasCustomer()) {
	// Updated \EoneoPaySdk\Entity\Customer instance
	$customer = $response->getCustomer();
	
	if ($customer->hasReferenceNumbers()) {
		// \EoneoPaySdk\Sdk\Collection of reference numbers containing the newly
		// created \EoneoPaySdk\Entity\Customer\ReferenceNumber\Auspost instance
		$referenceNumbers = $customer->getReferenceNumbers();
	}
}
```

## Generate Australia Post barcode

To generate an Australia Post barcode you must have an [existing Australia Post reference number entity](../../v2.md#markdown-header-existing-entities).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `amount`   | Yes | Yes | The amount to create a barcode for, in cents |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `raw` | `mixed` | Raw data returned containing the generated image |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Set amount, $reference must be an existing AusPost
// \EoneoPaySdk\Entity\ReferenceNumber instance
$reference->setAmount(100);

// Get barcode
$response = $client->get($reference);

// Check response
if ($response->isSuccessful() && $response->hasRaw()) {
	// Save image
	file_put_contents('barcode.png', $response->getRaw());
}
```

## BPAY reference number entity

### Class

`EoneoPaySdk\Entity\Customer\ReferenceNumber\Bpay`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `biller_code` | `string` | Yes | The BPAY biller code |
| `clearing_account` | `int` | No | Clearing account id |
| `default` | `bool` | No | Whether this is the default reference number for it's type |
| `pip_id` | `string` | Yes | The Australia Post PIP id, only used on creation |
| `type` | `string` | No | The reference number type |

## Create BPAY reference number

Reference numbers are not stand alone entities, they must be attached to an existing [customer](../customers.md#markdown-header-customers) via the `forCustomer()` method before being created.

### Request attributes

There are no settable attributes when creating an BPAY reference number.

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `customer` | `Customer` | Updated [customer entity](../customers.md#markdown-header-customer-entity_1) with created reference number |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate
$referenceNumber = new \EoneoPaySdk\Entity\Customer\ReferenceNumber\Bpay;

// Optionally use existing biller code and PIP id, you must set both or none
$referenceNumber->setBillerCode('000000')->setPipId('000');
	
// Attach reference number to a customer assuming $customer 
// is an existing \EoneoPaySdk\Entity\Customer instance
$referenceNumber->forCustomer($customer);

// Create reference number
$response = $client->create($referenceNumber);

// Check response
if ($response->isSuccessful() && $response->hasCustomer()) {
	// Updated \EoneoPaySdk\Entity\Customer instance
	$customer = $response->getCustomer();
	
	if ($customer->hasReferenceNumbers()) {
		// \EoneoPaySdk\Sdk\Collection of reference numbers containing the newly
		// created \EoneoPaySdk\Entity\Customer\ReferenceNumber\Bpay instance
		$referenceNumbers = $customer->getReferenceNumbers();
	}
}
```

## Generate BPAY bill advice

To generate a BPAY bill advice image you must have an [existing BPAY reference number entity](../../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when generating a BPAY bill advice image.

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `raw` | `mixed` | Raw data returned containing the generated image |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Get barcode, $reference must be an existing BPAY
// \EoneoPaySdk\Entity\ReferenceNumber instance
$response = $client->get($reference);

// Check response
if ($response->isSuccessful() && $response->hasRaw()) {
	// Save image
	file_put_contents('advice.png', $response->getRaw());
}
```

## Electronic funds transfer reference number entity

### Class

`EoneoPaySdk\Entity\Customer\ReferenceNumber\Eft`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `bsb` | `string` | Yes | 6 digit Bank-State-Branch number |
| `clearing_account` | `int` | No | Clearing account id |
| `default` | `bool` | No | Whether this is the default reference number for it's type |
| `prefix` | `string` | Yes | The reference number prefix |
| `type` | `string` | No | The reference number type |

## Create electronic funds transfer reference number

Reference numbers are not stand alone entities, they must be attached to an existing [customer](../customers.md#markdown-header-customers) via the `forCustomer()` method before being created.

### Request attributes

There are no settable attributes when creating an electronic funds transfer reference number.

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `customer` | `Customer` | Updated [customer entity](../customers.md#markdown-header-customer-entity_1) with created reference number |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate
$referenceNumber = new \EoneoPaySdk\Entity\Customer\ReferenceNumber\Eft;

// Optionally use existing bsb and prefix, you must set both or none
$referenceNumber->setBsb('000000')->setPrefix('000');
	
// Attach reference number to a customer assuming $customer 
// is an existing \EoneoPaySdk\Entity\Customer instance
$referenceNumber->forCustomer($customer);

// Create reference number
$response = $client->create($referenceNumber);

// Check response
if ($response->isSuccessful() && $response->hasCustomer()) {
	// Updated \EoneoPaySdk\Entity\Customer instance
	$customer = $response->getCustomer();
	
	if ($customer->hasReferenceNumbers()) {
		// \EoneoPaySdk\Sdk\Collection of reference numbers containing the newly
		// created \EoneoPaySdk\Entity\Customer\ReferenceNumber\Eft instance
		$referenceNumbers = $customer->getReferenceNumbers();
	}
}
```
