# Subscriptions

Subscriptions are used to manage recurring payments for a [customer](../customers.md#markdown-header-customers) via a [plan](../plans.md#markdown-header-plans).

## Contents

##### [Subscription entity](#markdown-header-subscription-entity_1)
* [Create subscription](#markdown-header-create-subscription)
* [Delete subscription](#markdown-header-delete-subscription)
* [Get subscription by id](#markdown-header-get-subscription-by-id)
* [Update subscription](#markdown-header-update-subscription)

##### [Subscription resource](#markdown-header-subscription-resource_1)
* [List all subscriptions](#markdown-header-list-all-subscriptions)

##### [Overdue subscription resource](#markdown-header-overdue-subscription-resource_1)
* [List overdue subscriptions](#markdown-header-list-overdue-subscriptions)

##### Endpoints
* [Api keys](../api-keys.md#markdown-header-api-keys)
* [Charges](../charges.md#markdown-header-charges)
* [Clearing accounts](../clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](../configurations.md#markdown-header-configurations)
* [Customers](../customers.md#markdown-header-customers)
    - [Reference numbers](reference-numbers.md#markdown-header-reference-numbers)
    - Subscriptions
        - [Statements](subscriptions/statements.md#markdown-header-statements)
* [eWallets](../ewallets.md#markdown-header-ewallets)
    - [Statements](../ewallets/statements.md#markdown-header-statements)
* [Fees](../fees.md#markdown-header-fees)
* [Merchants](../merchants.md#markdown-header-merchants)
    - [Balance](../merchants/balance.md#markdown-header-balance)
    - [Fees](../merchants/fees.md#markdown-header-fees)
    - [Transfers](../merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](../payments.md#markdown-header-payment-allocations)
* [Payment sources](../payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](../payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](../payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](../payments.md#markdown-header-payments)
* [Plans](../plans.md#markdown-header-plans)
* [Reconciliation reports](../reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](../search.md#markdown-header-search)
* [System status](../system-status.md#markdown-header-system-status)
* [Tokenisation](../tokenisation.md#markdown-header-tokenisation)
* [Webhooks](../webhooks.md#markdown-header-webhooks)

## Subscription entity

The subscription entity represents a single subscription for a [customer](../customers.md#markdown-header-customers).

### Class

`EoneoPaySdk\Entity\Customer\Subscription`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `auspost_reference_number` | `string` | Yes | The AusPost reference number for this subscription |
| `allocated_eft_reference_number` | `string` | Yes | The eft reference number for this subscription |
| `bpay_reference_number` | `string` | Yes | The bpay reference number for this subscription |
| `charges` | `Charge[]` | No | Collection of [charge entities](../../charges.md#markdown-header-charges) associated with this subscription |
| `created_at` | `string` | No | SQL compatible date/time this subscription was created |
| `current_balance` | `int` | No | Current subscription balance |
| `current_period_end` | `string` | No | Current subscription period end date |
| `current_period_start` | `string` | No | Current subscription period start date |
| `customer_id` | `string` | No | The EoneoPay id for the associated [customer](customers.md#markdown-header-customers) |
| `end_date` | `string` | Yes | SQL compatible date/time this subscription will end |
| `id` | `string` | No | The EoneoPay subscription id |
| `lead_time` | `int` | Yes | The number of days to process the subscription before it's due |
| `merchant_id` | `string` | No | EoneoPay id of the [merchant](../merchants.md#markdown-header-merchants) responsible for this subscription |
| `payment_facilities` | ? | No | |
| `plan` | `string|Plan` | Yes* | The plan id or associated [plan entity](../plans.md#markdown-header-plans) |
| `plan_id` | `string` | No | The EoneoPay of the associated [plan entity](../plans.md#markdown-header-plans) |
| `source` | `string` | Yes | The payment source for this subscription |
| `start_date` | `string` | Yes | SQL compatible date/time this subscription will start |
| `statement` | `Repository` | No | See [statement attributes](#markdown-header-statement-attributes) |
| `status` | `string` | No | Subscription status |
| `updated_at` | `string` | No | SQL compatible date/time this subscription was last updated |

\* Before a subscription is created the plan attribute will contain the associated plan id.

#### *payment_facilities* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `xxx` | `Repository` | No | For each payment facility where `xxx` represents the facility type, see [payment facilities xxx attributes](#markdown-payment-payment_facilities-xxx-attributes) |

#### *payment_facilities -> xxx* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `biller_code` | `string` | No | Bpay biller code |
| `bsb` | `string` | No | Bank account bsb |
| `clearing_account` | `string` | No | Clearing account id |
| `default` | `bool` | No | Whether this is the default payment facility of this type |
| `id` | `string` | No | Id for this payment facility |
| `prefix` | `string` | No | Bank account prefix | 
| `type` | `string` | No | the payment facility type |

#### *statement* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `charges` | `Charge[]` | No | Collection of charges allocated to this subscription |
| `end_date` | `string` | No | SQL compatible date/time this statement ends |
| `outstanding_amount` | `int` | No | The total balance outstanding |
| `payments` | `Payment[]` | No | Collection of payments allocated to this subscription |
| `start_date` | `string` | No | SQL compatible date/time this statement starts |
| `total_outstanding_to_date` | `int` | No | The balance outstanding to the current date |

## Create subscription

Subscriptions are not stand alone entities, they must be attached to an existing [customer](../customers.md#markdown-header-customers) and [plan](../plans.md#markdown-header-plans) entities via the `forCustomer()` and `forPlan()` methods respecitvely before being created.

Optionally `*_reference_number` fields can by passing a [reference number entity](reference-numbers.md#markdown-header-reference-number-entity_1) to `fromReferenceNumber()` and `source` can be set by passing a [payment source entity](../payment-sources.md#markdown-header-payment-sources-entity_1) to `fromPaymentSource()`.

[Charges](../charges.md#markdown-header-charges) can also be set on creation by passing one or more [charge entities](../charges.md#markdown-header-charge-entity_1) to the `addCharge()` method.

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `end_date` | No | Yes | SQL compatible date/time this subscription will end |
| `lead_time` | No | Yes | The number of days to process the subscription before it's due |
| `start_date` | No | Yes | SQL compatible date/time this subscription will start |

Reference numbers can't be set directly but can be set by passing an existing [reference number entity](reference-numbers.md#markdown-header-reference-number-entity_1) to `fromReferenceNumber()`.

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `subscription` | `Subscription` | Created [subscription entity](#markdown-header-subscription-entity_1) |

> **Note:** The response will not contain payment_facilities or statement attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with subscription details
$subscription = new \EoneoPaySdk\Entity\Customer\Subscription([
	'start_date' => '2018-01-01',
]);

// Or instantiate and set subscription details
$subscription = (new \EoneoPaySdk\Entity\Customer\Subscription)
	->setStartDate('2018-01-01');
	
// Attach subscription to a customer assuming $customer 
// is an existing \EoneoPaySdk\Entity\Customer instance
$subscription->forCustomer($customer);

// Associated plan with subscription assuming $plan
// is an existing \EoneoPaySdk\Entity\Plan instance
$subscription->forPlan($plan);

// Optionally add a reference number assuming $referenceNumber
// is an existing \EoneoPaySdk\Entity\Customer\ReferenceNumber instance
$subscription->fromReferenceNumber($referenceNumber);

// Create subscription
$response = $client->create($subscription);

// Check response
if ($response->isSuccessful() && $response->hasSubscription()) {
	// Created \EoneoPaySdk\Entity\Customer\Subscription instance
	$subscription = $response->getSubscription();
}
```

## Delete subscription

When deleting a subscription you must have an [existing subscription entity](../../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when deleting a subscription.

### Response attributes

The response will only contain the [standard response](../../v2.md#markdown-header-responses) attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Delete subscription, $subscription must be an existing
// \EoneoPaySdk\Entity\Customer\Subscription instance
$response = $client->delete($subscription);

// Check response
if ($response->isSuccessful()) {
	// Subscription has been successfully deleted
}
```

## Get subscription by id

An eWallet can be retrieved via it's unique [EoneoPay id](#markdown-header-attributes). The subscription must also be attached to the owning [customer entity](../customers.md#markdown-header-customers) via the `forCustomer()` method.

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `id` | Yes | No | The EoneoPay id for this entity |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `subscription` | `Subscription` | Retrieved [subscription entity](#markdown-header-subscription-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with id
$subscription = new \EoneoPaySdk\Entity\Customer\Subscription([
	'id' => 'sub_000000000000000000000000'
]);

// Or instantiate and set id
$subscription = (new \EoneoPaySdk\Entity\Customer\Subscription)
	->setId('sub_000000000000000000000000');

// Attach subscription to owning customer assuming $customer 
// is an existing \EoneoPaySdk\Entity\Customer instance
$subscription->forCustomer($customer);

// Get subscription
$response = $client->get($subscription);

// Check response
if ($response->isSuccessful() && $response->hasSubscription()) {
	// Retrieved \EoneoPaySdk\Entity\Customer\Subscription instance
	$subscription = $response->getSubscription();
}
```

## Update subscription

When updating a subscription you must have an [existing subscription entity](../../v2.md#markdown-header-existing-entities).

Similar to [creating a subscription](#markdown-header-create-subscription), reference numbers, payment source and charges can be updated via their respective methods.

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `end_date` | `string` | Yes | SQL compatible date/time this subscription will end |
| `lead_time` | `int` | Yes | The number of days to process the subscription before it's due |
| `start_date` | `string` | Yes | SQL compatible date/time this subscription will start |

Reference numbers can't be set directly but can be set by passing an existing [reference number entity](reference-numbers.md#markdown-header-reference-number-entity_1) to `fromReferenceNumber()`.

The associated [plan entity](../plans.md#markdown-header-plans) can also be changed via the `forPlan()` method.

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `subscription` | `Subscription` | Updated [subscription entity](#markdown-header-subscription-entity_1) |

> **Note:** The response will not contain associated plan, payment_facilities or statement attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Update some details, $subscription must be an existing 
// \EoneoPaySdk\Entity\Customer\Subscription instance
$subscription->setEndDate('August 2019');

// Update subscription
$response = $client->update($subscription);

// Check response
if ($response->isSuccessful() && $response->hasSubscription()) {
	// Updated \EoneoPaySdk\Entity\Customer\Subscription instance
	$subscription = $response->getSubscription();
}
```

## Subscription resource

The subscription resource represents a group of subscription entities.

### Class

`EoneoPaySdk\Resource\Customer\Subscription`

### Filters

There are no filters available when getting the subscriptions.

## List all subscriptions

There are no filters available when getting the balance history.

Subscriptions for a single [customer](../customers.md#markdown-header-customers) can be fetched by passing an [existing customer entity](../../v2.md#markdown-header-existing-entities) to the `forCustomer()` method.

### Response

Subscriptions found will be returned as a [collection](../../v2.md#markdown-header-collections-and-repositories) of [subscription entities](#markdown-header-subscription-entity_1). 

If no subscriptions are found, e.g. the filter returns no results or the page and limit exceeds available subscriptions, an empty collection will be returned.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `subscriptions` | `Subscription[]` | Collection of [subscription entities](#markdown-header-subscription-entity_1) |

If the collection is empty `hasSubscriptions()` will return false.

> **Note:** Subscriptions will be returned without associated plan, payment_facilities or statement attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate subscription and optionally attach to customer assuming 
// $customer is an existing \EoneoPaySdk\Entity\Customer instance
$subscription = (new \EoneoPaySdk\Resource\Customer\Subscription)
	->forCustomer($customer);

// Get subscription list
$response = $client->list($subscriptions);

// Check response
if ($response->isSuccessful() && $response->hasSubscriptions()) {
	// \EoneoPaySdk\Sdk\Collection instance containing 
	// \EoneoPaySdk\Entity\Customer\Subscription entities 
	$subscriptions = $response->getSubscriptions();
}
```

## Overdue subscription resource

The overdue subscription resource represents a group of subscription entities which are overdue.

### Class

`EoneoPaySdk\Resource\Customer\Subscription\Overdue`

### Filters

There are no filters available when getting overdue subscriptions.

## List overdue subscriptions

There are no filters available when getting overdue subscriptions.

### Response

Subscriptions found will be returned as a [collection](../../v2.md#markdown-header-collections-and-repositories) of [subscription entities](#markdown-header-subscription-entity_1). 

If no subscriptions are found, e.g. the filter returns no results or the page and limit exceeds available subscriptions, an empty collection will be returned.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `subscriptions` | `Subscription[]` | Collection of [subscription entities](#markdown-header-subscription-entity_1) |

If the collection is empty `hasSubscriptions()` will return false.

> **Note:** Subscriptions will be returned without associated plan, payment_facilities or statement attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate overdue subscription
$overdue = new \EoneoPaySdk\Resource\Customer\Subscription\Overdue;

// Get overdue subscription list
$response = $client->list($overdue);

// Check response
if ($response->isSuccessful() && $response->hasSubscriptions()) {
	// \EoneoPaySdk\Sdk\Collection instance containing 
	// \EoneoPaySdk\Entity\Customer\Subscription entities 
	$subscriptions = $response->getSubscriptions();
}
```
