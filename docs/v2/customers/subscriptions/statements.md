# Statements

Statement are a record of transactions made against a [subscription entity](../subscriptions.md#markdown-header-ewallets) for a specific billing period.

## Contents

##### [Statement entity](#markdown-header-statement-entity_1)
* [Get statement for subscription](#markdown-header-get-statement-for-subscription)

##### Endpoints
* [Api keys](../../api-keys.md#markdown-header-api-keys)
* [Charges](../../charges.md#markdown-header-charges)
* [Clearing accounts](../../clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](../../configurations.md#markdown-header-configurations)
* [Customers](../../customers.md#markdown-header-customers)
    - [Reference numbers](../reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](../subscriptions.md#markdown-header-subscriptions)
        - Statements
* [eWallets](../../ewallets.md#markdown-header-ewallets)
    - [Statements](../../ewallets/statements.md#markdown-header-statements)
* [Fees](../fees.md#markdown-header-fees)
* [Merchants](../../merchants.md#markdown-header-merchants)
    - [Balance](../../merchants/balance.md#markdown-header-balance)
    - [Fees](../../merchants/fees.md#markdown-header-fees)
    - [Transfers](../../merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](../../payments.md#markdown-header-payment-allocations)
* [Payment sources](../../payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](../../payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](../../payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](../../payments.md#markdown-header-payments)
* [Plans](../../plans.md#markdown-header-plans)
* [Reconciliation reports](../../reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](../../search.md#markdown-header-search) 
* [System status](../../system-status.md#markdown-header-system-status)
* [Tokenisation](../../tokenisation.md#markdown-header-tokenisation)
* [Webhooks](../../webhooks.md#markdown-header-webhooks)

## Statement entity

The statement entity represents the transactions for a single billing period within a [subscription](../subscriptions.md#markdown-header-subscriptions).

### Class

`EoneoPaySdk\Entity\Customer\Subscription\Statement`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `charges` | `Charge[]` | No | A collection of [charge entities](../../charges.md#markdown-header-charges) allocated for the statement period |
| `date` | `string` | Yes* | SQL compatible date/time to use as the statement period |
| `end_date` | `string` | No | SQL compatible date/time this statement period ends |
| `outstanding_balance` | `int` | No | The outstanding balance in cents |
| `payments` | `Payment[]` | No | A collection of [payment entities](../../payments.md#markdown-header-payments) allocated for the statement period || `start_date` | `string` | No | SQL compatible date/time this statement period starts |

\* The statement period for the statement can be changed via the `forDate()` method. This method accepts any [valid date/time format](http://php.net/manual/en/datetime.formats.php).

## Get statement for subscription

A statement can be retrieved via an [existing subscription entity](../../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when retrieving a statement.

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `subscription_statement` | `Statement` | Retrieved [statement entity](#markdown-header-statement-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate statement instance
$statement = new \EoneoPaySdk\Entity\Customer\Subscription\Statement;

// Set subscription to retrieve the statement for, $subscription must
// be an existing \EoneoPaySdk\Entity\Customer\Subscription instance
$statement->forSubscription($subscription);

// Optionally set the statement period to retrieve
$statement->forDate('July 2017');

// Get statement
$response = $client->get($statement);

// Check response
if ($response->isSuccessful() && $response->hasSubscriptionStatement()) {
	// Retrieved \EoneoPaySdk\Entity\Customer\Subscription\Statement instance
	$statement = $response->getSubscriptionStatement();
}
```
