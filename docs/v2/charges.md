# Charges

Charges are a way to create a charge for fulfilment from a [payment](payments.md#markdown-header-payments) or subscription.

## Contents

##### [Charge entity](#markdown-header-charge-entity_1)
* [Create charge](#markdown-header-create-charge)
* [Delete charge](#markdown-header-delete-charge)
* [Get charge by id](#markdown-header-get-charge-by-id)
* [Update charge](#markdown-header-update-charge)

##### [Charge resource](#markdown-header-charge-resource_1)
* [List all charges](#markdown-header-list-all-charges)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* Charges
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payments.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## Charge entity

The charge entity represents a single charge.

### Class

`EoneoPaySdk\Entity\Charge`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `amount` | `string` | Yes | Amount of charge, in cents |
| `created_at` | `string` | No | SQL compatible date/time this charge was created |
| `id` | `string` | No | The EoneoPay charge id |
| `merchant_id` | `string` | No | EoneoPay id of the [merchant](merchants.md#markdown-header-merchants) responsible for this charge |
| `name` | `string` | Yes | Charge name/description |
| `outstanding_amount` | `string` | No | The amount oustanding for the charge, in cents |
| `precedence` | `string` | Yes | The charge precedence |
| `statement` | `Repository` | No | Payments and allocations made towards this charge |
| `updated_at` | `string` | No | SQL compatible date/time this charge was last updated |

#### *statement* attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `payments` | `Payment[]` | No | Collection of payments allocated to this charge |

## Create charge

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `amount` | Yes | Yes | Amount of charge, in cents |
| `name` | Yes | Yes | Charge name/description |
| `precedence` | No | Yes | The charge precedence |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `charge` | `Charge` | Created [charge entity](#markdown-header-charge-entity_1) |

> **Note:** The reponse will not contain a statement attribute.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with charge details
$charge = new \EoneoPaySdk\Entity\Charge([
	'amount' => 100,
	'name' => 'Test charge',
]);

// Or instantiate and set charge details
$charge = (new \EoneoPaySdk\Entity\Charge)
	->setAmount(100)
	->setName('Test charge');

// Create charge
$response = $client->create($charge);

// Check response
if ($response->isSuccessful() && $response->hasCharge()) {
	// Created \EoneoPaySdk\Entity\Charge instance
	$charge = $response->getCharge();
}
```

## Delete charge

When deleting a charge you must have an [existing charge entity](../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when deleting a charge.

### Response attributes

The response will only contain the [standard response](../v2.md#markdown-header-responses) attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Delete charge, $charge must be an existing
// \EoneoPaySdk\Entity\Charge instance
$response = $client->delete($charge);

// Check response
if ($response->isSuccessful()) {
	// Charge has been successfully deleted
}
```

## Get charge by id

A charge can be retrieved via it's unique [EoneoPay id](#markdown-header-attributes).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `id` | Yes | No | The EoneoPay id for this entity |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `charge` | `Charge` | Retrieved [charge entity](#markdown-header-charge-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with id
$charge = new \EoneoPaySdk\Entity\Charge([
	'id' => 'chg_000000000000000000000000'
]);

// Or instantiate and set id
$charge = (new \EoneoPaySdk\Entity\Charge)
	->setId('chg_000000000000000000000000');

// Get charge
$response = $client->get($charge);

// Check response
if ($response->isSuccessful() && $response->hasCharge()) {
	// Retrieved \EoneoPaySdk\Entity\Charge instance
	$charge = $response->getCharge();
}
```

## Update charge

When updating a charge you must have an [existing charge entity](../v2.md#markdown-header-existing-entities).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `amount` | Yes | Yes | Amount of charge, in cents |
| `name` | Yes | Yes | Charge name/description |
| `precedence` | No | Yes | The charge precedence |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `charge` | `Charge` | Updated [charge entity](#markdown-header-charge-entity_1) |

> **Note:** The reponse will not contain a statement attribute.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Update some details, $charge must be an existing 
// \EoneoPaySdk\Entity\Charge instance
$charge->setName('Updated');

// Update charge
$response = $client->update($charge);

// Check response
if ($response->isSuccessful() && $response->hasCharge()) {
	// Updated \EoneoPaySdk\Entity\Charge instance
	$charge = $response->getCharge();
}
```

## Charge resource

The charge resource represents a group of charges.

Like most resources, [pagination](../v2.md#markdown-header-resources) can be used to limit results.

### Class

`EoneoPaySdk\Resource\Charge`

### Filters

| Attribute | Description |
|-----------|-------------|
| `amount` | Amount of charge, in cents |
| `created_at` | SQL compatible date/time this charge was created |
| `id` | The EoneoPay charge id |
| `name` | Charge name/description |
| `outstanding_amount` | The amount oustanding for the charge, in cents |
| `precedence` | The charge precedence |
| `updated_at` | SQL compatible date/time this charge was last updated |

## List all charges

Charges can be filtered via the [charge resource](#markdown-header-charge-resource_1).

### Response

Charges found will be returned as a [collection](../v2.md#markdown-header-collections-and-repositories) of [charge entities](#markdown-header-charge-entity_1). 

If no charges are found, e.g. the filter returns no results or the page and limit exceeds available charges, an empty collection will be returned.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `charges` | `Charge[]` | Collection of [charge entities](#markdown-header-charge-entity_1) |

If the collection is empty `hasCharges()` will return false.

> **Note:** Although a collection of charge entities will be returned they will not contain a statement attribute.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate charge resource
$charges = new \EoneoPaySdk\Resource\Charge;

// Optionally add filter results
$charges->whereName('Test');

// Optionally set pagination and limit
$charges->limit(10)->page(1);

// Get charge list
$response = $client->list($charges);

// Check response
if ($response->isSuccessful() && $response->hasCharges()) {
	// \EoneoPaySdk\Sdk\Collection instance containing 
	// \EoneoPaySdk\Entity\Charge entities 
	$charges = $response->getCharges();
}
```
