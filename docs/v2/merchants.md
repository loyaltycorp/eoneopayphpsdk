# Merchants

A merchant is a representation of your application within the EoneoPay system.

## Contents

##### [Merchant entity](#markdown-header-merchant-entity_1)
* [Get current merchant](#markdown-header-get-current-merchant)
* [Get merchant by id](#markdown-header-get-merchant-by-id)
* [Update merchant](#markdown-header-update-merchant)

##### [Merchant resource](#markdown-header-merchant-resource_1)
* [List all merchants](#markdown-header-list-all-merchants)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* Merchants
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payments.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## Merchant entity

The merchant entity represents a single merchant record.

### Class

`EoneoPaySdk\Entity\Merchant`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `abn` | `string` | Yes | 11 digit Australian Business Number |
| `address_city` | `string` | Yes | Merchant primary address city |
| `address_line_1` | `string` | Yes | Merchant primary address line 1 |
| `address_line_2` | `string` | Yes | Merchant primary address line 2 |
| `address_postal_code` | `string` | Yes | Merchant primary address post code |
| `address_state` | `string` | Yes | Merchant primary address state |
| `automatic_transfer` | `bool` | Yes | Whether the available balance is automatically transferred to the nominated bank account |
| `available_balance` | `Collection` | No | A collection of available balances in cents, grouped by currency |
| `balance` | `Collection` | No | A collection of available balances in cents, grouped by currency |
| `bank_accounts` | `BankAccount[]` | No | A collection of bank account [payment source entities](payment-sources.md#markdown-header-payment-source-entity_1) |
| `business_name` | `string` | Yes | Business name |
| `business_phone` | `string` | Yes | Business phone number |
| `business_website` | `string` | Yes | Business website |
| `clearing_account` | `int` | No | Clearing account id |
| `created_at` | `string` | No | SQL compatible date/time this merchant was created |
| `credit_cards` | `CreditCard[]` | No | A collection of credit card [payment source entities](payment-sources.md#markdown-header-payment-source-entity_1) |
| `email` | `string` | Yes | Primary contact email address |
| `first_name`| `string` | Yes | Primary contact first name |
| `id` | `string` | No | The EoneoPay merchant id |
| `last_name` | `string` | Yes | Primary contact last name |
| `statement_descriptor` | `string` | Yes | Default description to use on plans, payments and statements for this merchant |
| `title` | `string` | Yes | Primary contact title |
| `transfer_day` | `int` | No | The day of the month to transfer funds if frequency is monthly, or [day of the week](#markdown-header-transfer-day) if frequency is weekly |
| `transfer_frequency` | `string` | Yes | How often to transfer available funds: daily, weekly or monthly |
| `trust_clearing_account` | `bool` | Yes | Whether the associated clearing account can be trusted or not |
| `updated_at` | `string` | No | SQL compatible date/time this merchant was last updated |
| `version` | `int` | No | The version/revision for this entity |

### Constants

#### Transfer day

| Value | Constant | Description |
|-------|----------|-------------|
| `1` | `DAY_MONDAY` | Monday |
| `2` | `DAY_TUESDAY` | Tuesday |
| `3` | `DAY_WEDNESDAY` | Wednesday |
| `4` | `DAY_THURSDAY` | Thursday |
| `5` | `DAY_FRIDAY` | Friday |
| `6` | `DAY_SATURDAY` | Saturday |
| `7` | `DAY_SUNDAY` | Sunday |

#### Transfer frequency

| Value | Constant | Description |
|-------|----------|-------------|
| `daily` | `FREQUENCY_DAILY` | Transfer available balance daily |
| `weekly` | `FREQUENCY_WEEKLY` | Transfer available balance weekly on `transfer_day` |
| `monthly` | `FREQUENCY_MONTHLY` | Transfer available balance monthly on `transfer_day` |

## Create merchant

A merchant represents your application within the EoneoPay system.

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `abn` | Yes | No | 11 digit Australian Business Number |
| `address_city` | No | No | Merchant primary address city |
| `address_line_1` | No | No | Merchant primary address line 1 |
| `address_line_2` | No | No | Merchant primary address line 2 |
| `address_postal_code` | No | No | Merchant primary address post code |
| `address_state` | No | No | Merchant primary address state |
| `automatic_transfer` | No | No | Whether the available balance is automatically transferred to the nominated bank account |
| `business_name` | Yes | No | Business name |
| `business_phone` | Yes | No | Business phone number |
| `business_website` | Yes | No | Business website |
| `email` | Yes | Yes | Primary contact email address |
| `first_name`| Yes | No | Primary contact first name |
| `last_name` | Yes | No | Primary contact last name |
| `statement_descriptor` | No | No | Default description to use on plans, payments and statements for this merchant |
| `title` | Yes | No | Primary contact title |
| `transfer_day` | No | Yes | The day of the month to transfer funds if frequency is monthly, or [day of the week](#markdown-header-transfer-day) if frequency is weekly |
| `transfer_frequency` | No | Yes | How often to transfer available funds: daily, weekly or monthly |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `merchant` | `Merchant` | Created [merchant entity](#markdown-header-merchant-entity_1) |

### Example

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with merchant details
$merchant = new \EoneoPaySdk\Entity\Merchant([
	'abn' => '00000000000',
	'business_name' => 'Merchant Pty Ltd',
	'business_phone' => '0300000000',
	'business_website' => 'http://merchant.com',
	'email' => 'info@merchant.com',
	'first_name' => 'Merchant',
	'last_name' => 'User',
	'title' => 'Mr',
]);

// Or instantiate and set merchant details
$merchant = (new \EoneoPaySdk\Entity\Merchant)
	->setAbn('00000000000')
	->setBusinessName('Merchant Pty Ltd')
	->businessPhone('0300000000')
	->businessWebsite('http://merchant.com')
	->setEmail('info@merchant.com')
	->setFirstName('Merchant')
	->setLastName('User')
	->setTitle('Mr');

// Create merchant
$response = $client->create($merchant);

// Check response
if ($response->isSuccessful() && $response->hasMerchant()) {
	// Created \EoneoPaySdk\Entity\Merchant instance
	$merchant = $response->getMerchant();
}
```

## Delete merchant

When deleting a merchant you must have an [existing merchant entity](../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when deleting a merchant.

### Response attributes

The response will only contain the [standard response](../v2.md#markdown-header-responses) attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Delete merchant, $merchant must be an existing
// \EoneoPaySdk\Entity\Merchant instance
$response = $client->delete($merchant);

// Check response
if ($response->isSuccessful()) {
	// Merchant has been successfully deleted
}
```

## Get current merchant

### Request attributes

There are no settable attributes when getting the current merchant.

### Response attributes

| Attribute | Type | Description |
|-----------|------|-------------|
| `merchant` | `Merchant` | Retrieved [merchant entity](#markdown-header-merchant-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate
$merchant = new \EoneoPaySdk\Entity\Merchant;

// Get merchant
$response = $client->get($merchant);

// Check response
if ($response->isSuccessful() && $response->hasMerchant()) {
	// Retrieved \EoneoPaySdk\Entity\Merchant instance
	$merchant = $response->getMerchant();
}
```

## Get merchant by id

A merchant can be retrieved via it's unique [EoneoPay id](#markdown-header-attributes).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `id` | Yes | Yes | The EoneoPay id for this entity |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `merchant` | `Merchant` | Retrieved [merchant entity](#markdown-header-merchant-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with id
$merchant = new \EoneoPaySdk\Entity\Merchant([
	'id' => 'mer_0000000000000000'
]);

// Or instantiate and set id
$merchant = (new \EoneoPaySdk\Entity\Merchant)
	->setId('mer_0000000000000000');

// Get merchant
$response = $client->get($merchant);

// Check response
if ($response->isSuccessful() && $response->hasMerchant()) {
	// Retrieved \EoneoPaySdk\Entity\Merchant instance
	$merchant = $response->getMerchant();
}
```

## Update merchant

When updating a merchant you must have an [existing merchant entity](../v2.md#markdown-header-existing-entities).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `abn` | Yes | No | 11 digit Australian Business Number |
| `address_city` | No | No | Merchant primary address city |
| `address_line_1` | No | No | Merchant primary address line 1 |
| `address_line_2` | No | No | Merchant primary address line 2 |
| `address_postal_code` | No | No | Merchant primary address post code |
| `address_state` | No | No | Merchant primary address state |
| `automatic_transfer` | No | No | Whether the available balance is automatically transferred to the nominated bank account |
| `business_name` | Yes | No | Business name |
| `business_phone` | Yes | No | Business phone number |
| `business_website` | Yes | No | Business website |
| `email` | Yes | Yes | Primary contact email address |
| `first_name`| Yes | No | Primary contact first name |
| `last_name` | Yes | No | Primary contact last name |
| `statement_descriptor` | No | No | Default description to use on plans, payments and statements for this merchant |
| `title` | Yes | No | Primary contact title |
| `transfer_day` | No | Yes | The day of the month to transfer funds if frequency is monthly, or [day of the week](#markdown-header-transfer-day) if frequency is weekly |
| `transfer_frequency` | No | Yes | How often to transfer available funds: daily, weekly or monthly |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `merchant` | `Merchant` | Updated [merchant entity](#markdown-header-merchant-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Update some details, $merchant must be an existing 
// \EoneoPaySdk\Entity\Merchant instance
$merchant->setAbn('12345678901')
	->setEmail('updated@email.com');

// Update merchant
$response = $client->update($merchant);

// Check response
if ($response->isSuccessful() && $response->hasMerchant()) {
	// Updated \EoneoPaySdk\Entity\Merchant instance
	$merchant = $response->getMerchant();
}
```

## Merchant resource

The merchant resource represents a group of merchant entities.

Like most resources, [pagination](../v2.md#markdown-header-resources) can be used to limit results.

### Class

`EoneoPaySdk\Resource\Merchant`

### Filters

| Attribute | Description |
|-----------|-------------|
| `abn` | 11 digit Australian Business Number |
| `address_city` | Merchant primary address city |
| `address_line_1` | Merchant primary address line 1 |
| `address_line_2` | Merchant primary address line 2 |
| `address_postal_code` | Merchant primary address post code |
| `address_state` | Merchant primary address state |
| `automatic_transfer` | Whether the available balance is automatically transferred to the nominated bank account | 
| `business_name` | Business name |
| `business_phone` | Business phone number |
| `business_website` | Business website |
| `clearing_account` | Clearing account id |
| `created_at` | SQL compatible date/time the merchant was created |
| `email` | Primary contact email address |
| `first_name`| Primary contact first name |
| `id` | The EoneoPay merchant id |
| `last_name` | Primary contact last name |
| `statement_descriptor` | Default description to use on plans, payments and statements for this merchant |
| `title` | Primary contact title |
| `transfer_day` | The day of the week to transfer available funds: 1 = Monday, 7 = Sunday for weekly frequency or day of the month for montly frequency |
| `transfer_frequency` | How often to transfer available funds: daily, weekly or monthly |
| `trust_clearing_account` | Whether the associated clearing account can be trusted or not |
| `updated_at` | SQL compatible date/time the merchant was last updated |

## List all merchants

Merchants can be filtered via the [merchant resource](#markdown-header-merchant-resource_1).

### Response

Merchants found will be returned as a [collection](../v2.md#markdown-header-collections-and-repositories) of [merchant entities](#markdown-header-merchant-entity_1). 

If no merchants are found, e.g. the filter returns no results or the page and limit exceeds available merchants, an empty collection will be returned.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `merchants` | `Merchant[]` | Collection of [merchant entities](#markdown-header-merchant-entity_1) |

If the collection is empty `hasMerchants()` will return false.

> **Note:** Although a collection of merchant entities will be returned, associated entities such as payment sources will only contain the EoneoPay id. To get the associated entity it will either need to be retrieved directly or you will need to [get the merchant entity by id](#markdown-header-get-merchant-by-id).

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate merchant resource
$merchants = new \EoneoPaySdk\Resource\Merchant;

// Optionally add filter results
$merchants->whereFirstName('Test')->whereLastName('User');

// Optionally set pagination and limit
$merchants->limit(10)->page(1);

// Get merchant list
$response = $client->list($merchants);

// Check response
if ($response->isSuccessful() && $response->hasMerchants()) {
	// \EoneoPaySdk\Sdk\Collection instance containing 
	// \EoneoPaySdk\Entity\Merchant entities 
	$merchants = $response->getMerchants();
}
```
