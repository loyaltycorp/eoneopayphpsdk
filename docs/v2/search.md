# Search

The search resource allows for full text searching across [customers](customers.md#markdown-header-customers), [merchants](merchants.md#markdown-header-merchants), [payments](payments.md#markdown-header-payments) and [plans](plans.md#markdown-header-plans).

## Contents

##### [Search resource](#markdown-header-search-resource_1)
* [Perform search](#markdown-header-perform-search)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* [Bank transactions](bank-transactions.md#markdown-header-bank-transactions)
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payment-allocations.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* Search
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## Search resource

The search resource allows searching across [customers](customers.md#markdown-header-customers), [merchants](merchants.md#markdown-header-merchants), [payments](payments.md#markdown-header-payments) and [plans](plans.md#markdown-header-plans).

Like most resources, [pagination](../v2.md#markdown-header-resources) can be used to limit results.

### Class

`EoneoPaySdk\Resource\Search`

### Filters

There are no filters available when searching.

### Constants

#### Scopes

| Value | Constant | Description |
|-------|----------|-------------|
| `customers` | `SCOPE_CUSTOMER` | [Customer](customers.md#markdown-header-customers) endpoint |
| `merchants` | `SCOPE_MERCHANT` | [Merchant](merchants.md#markdown-header-merchants) endpoint |
| `payments` | `SCOPE_PAYMENT` | [Payment](payments.md#markdown-header-payments) endpoint |
| `plans` | `SCOPE_PLAN` | [Plan](plans.md#markdown-header-plans) endpoint |

## Perform search

There are no filters available when searching.

A query string can be set via the `query()` method and a scope can be added to restrict results via the `scope()` method.

#### Query

The query will be matched against the following attributes:

| Endpoint | Attribute | Description | 
|----------|-----------|-------------|
| [Customers](customers.md#markdown-header-customers) | `first_name` | Customer first name |
| [Customers](customers.md#markdown-header-customers) | `last_name` | Customer last name |
| [Merchants](merchants.md#markdown-header-merchants) | `business_name` | Merchant business name | 
| [Payments](payments.md#markdown-header-payments) | `reference` | Payment reference |
| [Plans](plans.md#markdown-header-plans) | `name` | Plan name |

> **Note:** The search term is case sensitive, but allows partial matches, e.g. 'bob' will not find customers named 'Bob' but 'Bob', 'ob' and 'Bo' will.

#### Scope

The scope is a set of endpoints to search against, one or more scopes can be passed as a comma separated string:

| Endpoint | Value | 
|----------|-------|
| [Customers](customers.md#markdown-header-customers) | `customers` |
| [Merchants](merchants.md#markdown-header-merchants) | `merchants` |
| [Payments](payments.md#markdown-header-payments) | `payments` |
| [Plans](plans.md#markdown-header-plans) | `plans` |

> **Note:** It is always faster to search with a scope than without as by default all endpoints will be searched. If you know what scope you're attempting to search on you should send it with your request.

### Response

Search results will be returned as a repository with each endpoint as an attribute.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `results` | `Repository` | A repository of search results |

#### *results* attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `customers` | `Customer[]` | Collection of [customer entities](customers.md#markdown-header-customer-entity_1) |
| `merchants` | `Merchant[]` | Collection of [merchant entities](customers.md#markdown-header-merchant-entity_1) |
| `payments` | `Payment[]` | Collection of [payment entities](customers.md#markdown-header-payment-entity_1) |
| `plans` | `Plan[]` | Collection of [plan entities](customers.md#markdown-header-plan-entity_1) |

If there are no results for an endpoint the attribute will be omitted.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate search resource
$search = new \EoneoPaySdk\Resource\Search;

// Set query and scope
$search->query('Bob')->scope(\EoneoPaySdk\Resource\Search::SCOPE_CUSTOMERS);

// Optionally set pagination and limit
$search->limit(10)->page(1);

// Get search results
$response = $client->list($search);

// Check response
if ($response->isSuccessful() && $response->hasResults()) {
	// \EoneoPaySdk\Sdk\Respository instance containing 
	// \EoneoPaySdk\Sdk\Collection instances of entities
	$results = $response->getResults();
}
```
