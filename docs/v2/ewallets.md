# eWallets

eWallets are a funding source which can be used to hold funds for future use.

## Contents

##### [eWallet entity](#markdown-header-ewallet-entity_1)
* [Create eWallet](#markdown-header-create-ewallet)
* [Delete eWallet](#markdown-header-delete-ewallet)
* [Get eWallet by id](#markdown-header-get-ewallet-by-id)
* [Get primary eWallet](#markdown-header-get-primary-ewallet)
* [Update eWallet](#markdown-header-update-ewallet)

##### [eWallet resource](#markdown-header-ewallet-resource_1)
* [List all ewallets](#markdown-header-list-all-ewallets)

##### Endpoints
* [Api keys](api-keys.md#markdown-header-api-keys)
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* eWallets
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payments.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## eWallet entity

The eWallet entity represents a single EoneoPay wallet.

### Class

`EoneoPaySdk\Entity\Ewallet`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `balance` | `int` | No | Balance of the wallet in cents |
| `clearingAccount` | `int` | No | The clearing account the wallet is linked to |
| `created_at` | `string` | No | SQL compatible date/time this wallet was created |
| `customer_id` | `string` | No | The EoneoPay id for the associated [customer](customers.md#markdown-header-customers) |
| `id` | `string` | No | The EoneoPay id for this entity |
| `locked_until_date` | `string` | Yes | SQL compatible date/time the funds in this wallet are locked until |
| `meta` | `mixed` | Yes | Custom meta data for this wallet |
| `merchant_id` | `string` | No | The EoneoPay id for the associated [merchant](merchants.md#markdown-header-merchants) |
| `name` | `string` | Yes | The name for this wallet |
| `source_id` | `string` | No | The EoneoPay source id for this wallet |
| `updated_at` | `string` | No | SQL compatible date/time this wallet was last updated |

## Create eWallet

eWallets are not stand alone entities, they must be attached to an existing [customer](../customers.md#markdown-header-customers) or [merchant](../merchants.md#markdown-header-merchants) entity via the `forCustomer()` or `forMerchant()` methods respecitvely before being created.

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `locked_until_date` | No | No | Date the funds in this wallet are locked until, accepts any [valid date/time format](http://php.net/manual/en/datetime.formats.php) |
| `meta` | No | No | Custom meta data for this wallet |
| `name` | No | No | The name for this wallet |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `ewallet` | `Ewallet` | Created [eWallet entity](../#markdown-header-ewallet-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with eWallet details
$wallet = new \EoneoPaySdk\Entity\Ewallet([
	'name' => 'Test Wallet',
]);

// Or instantiate and set eWallet details
$wallet = (new \EoneoPaySdk\Entity\Ewallet)
	->setName('Test Wallet');

// Attach eWallet to a customer assuming $customer 
// is an existing \EoneoPaySdk\Entity\Customer instance
$wallet->forCustomer($customer);

// Or attach eWallet to a merchant assuming $merchant
// is an existing \EoneoPaySdk\Entity\Merchant instance
$wallet->forMerchant($merchant);

// Create eWallet
$response = $client->create($wallet);

// Check response
if ($response->isSuccessful() && $response->hasEwallet()) {
	// Created \EoneoPaySdk\Entity\Ewallet instance
	$bankAccount = $response->getEwallet();
}
```

## Delete eWallet

When deleting an eWallet you must have an [existing eWallet entity](../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when deleting an eWallet.

### Response attributes

The response will only contain the [standard response](../v2.md#markdown-header-responses) attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Delete eWallet, $wallet must be an existing
// \EoneoPaySdk\Entity\Ewallet instance
$response = $client->delete($wallet);

// Check response
if ($response->isSuccessful()) {
	// eWallet has been successfully deleted
}
```

## Get eWallet by id

An eWallet can be retrieved via it's unique [EoneoPay id](#markdown-header-attributes). The eWallet must also be attached to the owning [customer](../customers.md#markdown-header-customers) or [merchant](../merchants.md#markdown-header-merchants) entity via the `forCustomer()` or `forMerchant()` methods respectively.

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `id` | Yes | No | The EoneoPay id for this entity |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `ewallet` | `Ewallet` | Retrieved [eWallet entity](#markdown-header-ewallet-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with id
$wallet = new \EoneoPaySdk\Entity\Ewallet([
	'id' => '0XXXX00'
]);

// Or instantiate and set id
$wallet = (new \EoneoPaySdk\Entity\eWallet)
	->setId('0XXXX00');
	
// Attach eWallet to owning customer assuming $customer 
// is an existing \EoneoPaySdk\Entity\Customer instance
$wallet->forCustomer($customer);

// Or attach eWallet to owning merchant assuming $merchant
// is an existing \EoneoPaySdk\Entity\Merchant instance
$wallet->forMerchant($merchant);

// Get eWallet
$response = $client->get($wallet);

// Check response
if ($response->isSuccessful() && $response->hasEwallet()) {
	// Retrieved \EoneoPaySdk\Entity\Ewallet instance
	$wallet = $response->getEwallet();
}
```

## Get primary eWallet

The primary eWallet can be retrieved for a [customer](../customers.md#markdown-header-customers) or [merchant](../merchants.md#markdown-header-merchants) can be retrieved in the same way as [getting an eWallet by id](#markdown-header-get-ewallet-by-id) without providing the eWallet EoneoPay id.

### Request attributes

There are no settable attributes when getting a primary eWallet.

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `ewallet` | `Ewallet` | Retrieved [eWallet entity](#markdown-header-ewallet-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate eWallet and attach to customer assuming $customer is 
// an existing \EoneoPaySdk\Entity\Customer instance. forMerchant() can 
// also be used with an existing \EoneoPaySdk\Entity\Merchant instance
$wallet = (new \EoneoPaySdk\Entity\Ewallet)->forCustomer($customer);

// Get eWallet
$response = $client->get($wallet);

// Check response
if ($response->isSuccessful() && $response->hasEwallet()) {
	// Retrieved \EoneoPaySdk\Entity\Ewallet instance
	$wallet = $response->getEwallet();
}
```

## Update eWallet

When updating an eWallet you must have an [existing eWallet entity](../v2.md#markdown-header-existing-entities).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `locked_until_date` | No | No | Date the funds in this wallet are locked until, accepts any [valid date/time format](http://php.net/manual/en/datetime.formats.php) |
| `meta` | No | No | Custom meta data for this wallet |
| `name` | No | No | The name for this wallet |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `ewallet` | `Ewallet` | Updated [eWallet entity](#markdown-header-ewallet-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Update some details, $wallet must be an existing 
// \EoneoPaySdk\Entity\Ewallet instance
$customer->setLockedUntilDate('2050-01-01');

// Update eWallet
$response = $client->update($wallet);

// Check response
if ($response->isSuccessful() && $response->hasEwallet()) {
	// Updated \EoneoPaySdk\Entity\Ewallet instance
	$wallet = $response->getEwallet();
}
```

## eWallet resource

The eWallet resource represents a group of eWallet entities for a specific [customer](../customers.md#markdown-header-customers) or [merchant](../merchants.md#markdown-header-merchants).

Like most resources, [pagination](../v2.md#markdown-header-resources) can be used to limit results.

### Class

`EoneoPaySdk\Resource\Ewallet`

### Filters

There are no filters available for the eWallet resource.

## List all customers

### Response

eWallets found will be returned as a [collection](../v2.md#markdown-header-collections-and-repositories) of [eWallet entities](#markdown-header-ewallet-entity_1). 

If no eWallets are found, e.g. the page and limit exceeds available eWallets, an empty collection will be returned.

| Attribute | Type | Description | 
|-----------|------|-------------|
| `ewallets` | `Ewallet[]` | Collection of [ewallet entities](#markdown-header-customer-entity_1) |

If the collection is empty `hasEwallets()` will return false.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate eWallet and attach to customer assuming $customer is 
// an existing \EoneoPaySdk\Entity\Customer instance. forMerchant() can 
// also be used with an existing \EoneoPaySdk\Entity\Merchant instance
$wallets = (new \EoneoPaySdk\Resource\Ewallet)->forCustomer($customer);

// Optionally set pagination and limit
$wallets->limit(10)->page(1);

// Get ewallet list
$response = $client->list($wallets);

// Check response
if ($response->isSuccessful() && $response->hasEwallets()) {
	// \EoneoPaySdk\Sdk\Collection instance containing 
	// \EoneoPaySdk\Entity\Ewallet entities 
	$wallets = $response->getEwallets();
}
```
