# Api keys

Api keys are used to allow or restrict access to endpoints within EoneoPay.

## Contents

##### [Api key entity](#markdown-header-api-key-entity_1)
* [Delete api key](#markdown-header-delete-api-key)
* [Get api key by api key](#markdown-header-get-api-key-by-api-key)
* [Update api key](#markdown-header-update-api-key)

##### Endpoints
* Api keys
* [Charges](charges.md#markdown-header-charges)
* [Clearing accounts](clearing-accounts.md#markdown-header-clearing-accounts)
* [Configurations](configurations.md#markdown-header-configurations)
* [Customers](customers.md#markdown-header-customers)
    - [Reference numbers](customers/reference-numbers.md#markdown-header-reference-numbers)
    - [Subscriptions](customers/subscriptions.md#markdown-header-subscriptions)
        - [Statements](customers/subscriptions/statements.md#markdown-header-statements)
* [eWallets](ewallets.md#markdown-header-ewallets)
    - [Statements](ewallets/statements.md#markdown-header-statements)
* [Fees](fees.md#markdown-header-fees)
* [Merchants](merchants.md#markdown-header-merchants)
    - [Balance](merchants/balance.md#markdown-header-balance)
    - [Fees](merchants/fees.md#markdown-header-fees)
    - [Transfers](merchants/transfers.md#markdown-header-transfers)
* [Payment allocations](payments.md#markdown-header-payment-allocations)
* [Payment sources](payment-sources.md#markdown-header-payment-sources)
    - [Bank accounts](payment-sources/bank-accounts.md#markdown-header-bank-accounts)
    - [Credit cards](payment-sources/credit-cards.md#markdown-header-credit-cards)
* [Payments](payments.md#markdown-header-payments)
* [Plans](plans.md#markdown-header-plans)
* [Reconciliation reports](reconciliation-reports.md#markdown-header-reconciliation-reports)
* [Search](search.md#markdown-header-search)
* [System status](system-status.md#markdown-header-system-status)
* [Tokenisation](tokenisation.md#markdown-header-tokenisation)
* [Webhooks](webhooks.md#markdown-header-webhooks)

## Api key entity

The api key entity represents a single api key.

### Class

`EoneoPaySdk\Entity\ApiKey`

### Attributes

| Attribute | Type | Mutable | Description |
|-----------|------|---------|-------------|
| `api_key` | `string` | No | The actual api key |
| `api_permissions` | `Collection` | Yes | Permissions available to the api key |
| `api_status` | `string` | Yes | The status of the api key |
| `created_at` | `string` | No | SQL compatible date/time this api key was created |
| `merchant_id` | `string` | No | EoneoPay id of the [merchant](merchants.md#markdown-header-merchants) this api key is assigned to |
| `updated_at` | `string` | No | SQL compatible date/time this api key was last updated |

### Constants

#### Api permissions

> **Note:** To add the permssion which allows a merchant to access their own profile the api key needs to be attached to an [existing merchant entity](../v2.md#markdown-header-existing-entities) via the `forMerchant()` method
 
Permissions can be managed directly on the `api_permissions` collection or via the `addApiPermission()` method.

| Value | Constant | Description |
|-------|----------|-------------|
| `*` | `PERMISSION_ADMIN` | Full access to everything |
| `balance` | `PERMISSION_MERCHANT_BALANCE` | [Merchant balance](merchant/balance.md#markdown-header-balance) access |
| `charges` | `PERMISSION_CHARGE` | [Charge](charges.md#markdown-header-charges) access |
| `customers` | `PERMISSION_CUSTOMER` | [Customer](customers.md#markdown-header-customers) access |
| `merchants` | `PERMISSION_MERCHANT` | [Merchant](merchants.md#markdown-header-merchants) access |
| `overdueSubscriptions` | `PERMISSION_CUSTOMER_OVERDUE_SUBSCRIPTION` | [Customer overdue subscription](customers/subscriptions.md#markdown-header-subscriptions) access |
| `payments` | `PERMISSION_PAYMENT` | [Payment](payments.md#markdown-header-payments) access |
| `plans` | `PERMISSION_PLAN` | [Plan](plans.md#markdown-header-planss) access |
| `search` | `PERMISSION_SEARCH` | Search access |
| `subscriptions` | `PERMISSION_CUSTOMER_SUBSCRIPTION` | [Customer subscription](customers/subscriptions.md#markdown-header-subscriptions) access |
| `tokens` | `PERMISSION_TOKENISE` | [Tokenisation](tokenisation.md#markdown-header-tokenisation) access |
| `transfers` | `PERMISSION_MERCHANT_TRANSFER` | [Merchant transfer](merchant/transfers.md#markdown-header-transfers) access |
| `webhooks` | `PERMISSION_WEBHOOK` | [Webhook](webhooks.md#markdown-header-webhooks) access |

#### Api status

| Value | Constant | Description |
|-------|----------|-------------|
| `0` | `STATUS_DISABLED` | Key is disabled/inactive |
| `1` | `STATUS_ENABLED` | Key is enabled/active |

## Delete api key

When deleting a api key you must have an [existing api key entity](../v2.md#markdown-header-existing-entities).

### Request attributes

There are no settable attributes when deleting a api key.

### Response attributes

The response will only contain the [standard response](../v2.md#markdown-header-responses) attributes.

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Delete api key, $apiKey must be an existing
// \EoneoPaySdk\Entity\ApiKey instance
$response = $client->delete($apiKey);

// Check response
if ($response->isSuccessful()) {
	// Api key has been successfully deleted/revoked
}
```

## Get api key by api key

Details about an api key can be retrieved via it's [api key](#markdown-header-attributes).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `api_key` | Yes | Yes | The actual api key for this entity |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `api_key` | `ApiKey` | Retrieved [api key entity](#markdown-header-api-key-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Instantiate with api key
$apiKey = new \EoneoPaySdk\Entity\ApiKey([
	'api_key' => 'sk_test_someapikey'
]);

// Or instantiate and set api key
$apiKey = (new \EoneoPaySdk\Entity\ApiKey)
	->setApiKey('sk_test_someapikey');

// Get api key
$response = $client->get($apiKey);

// Check response
if ($response->isSuccessful() && $response->hasApiKey()) {
	// Retrieved \EoneoPaySdk\Entity\ApiKey instance
	$apiKey = $response->getApiKey();
}
```

## Update api key

When updating a api key you must have an [existing api key entity](../v2.md#markdown-header-existing-entities).

### Request attributes

| Attribute | Mandatory | Validated | Description |
|-----------|-----------|-----------|-------------|
| `api_permissions` | No | No | Permissions available to the api key |
| `api_status` | No | No | The status of the api key |

### Response attributes

| Attribute | Type | Description | 
|-----------|------|-------------|
| `api_key` | `ApiKey` | Updated [api key entity](#markdown-header-api-key-entity_1) |

### Example

```php
$client = new \EoneoPaySdk\Client('sk_test_someapikey');

// Update some details, $apiKey must be an existing 
// \EoneoPaySdk\Entity\ApiKey instance
$apiKey->setStatus(false);
$apiKey->getApiPermissions()
	->add(\EoneoPaySdk\Entity\ApiKey::PERMISSION_SEARCH);

// Update api key
$response = $client->update($apiKey);

// Check response
if ($response->isSuccessful() && $response->hasApiKey()) {
	// Updated \EoneoPaySdk\Entity\ApiKey instance
	$apiKey = $response->getApiKey();
}
```
