<?php declare(strict_types = 1);

namespace EoneoPaySdk;

use EoneoPaySdk\Sdk\RequestParser;
use EoneoPaySdk\Sdk\Response;
use GuzzleHttp\Client as GuzzleClient;
use LoyaltyCorp\SdkBlueprint\Client as BaseClient;

class Client extends BaseClient
{
    /**
     * Create a new EoneoPay SDK client
     *
     * @param string $apiKey The EoneoPay SDK API key
     * @param \GuzzleHttp\Client $client The guzzle HTTP client to use for requests
     */
    public function __construct(string $apiKey, GuzzleClient $client = null)
    {
        parent::__construct(new Response, (new RequestParser)->setApiKey($apiKey), null, $client);
    }

    /**
     * Set api key
     *
     * @param string $apiKey The api key to use with each request
     *
     * @return \EoneoPaySdk\Client This instance, chainable
     */
    public function setApiKey(string $apiKey) : self
    {
        /** @var \EoneoPaySdk\Sdk\RequestParser $requestParser */
        $requestParser = $this->getRequestParser();
        $requestParser->setApiKey($apiKey);

        // Make chainable
        return $this;
    }

    /**
     * Set base url
     *
     * @param string $baseUrl The base url to use for requests
     *
     * @return \EoneoPaySdk\Client This instance, chainable
     */
    public function setBaseUrl(string $baseUrl) : self
    {
        /** @var \EoneoPaySdk\Sdk\RequestParser $requestParser */
        $requestParser = $this->getRequestParser();
        $requestParser->setBaseUrl($baseUrl);

        // Make chainable
        return $this;
    }

    /**
     * Set merchant masquerade id
     *
     * @param string $merchantId The merchant id to masquerade as
     *
     * @return \EoneoPaySdk\Client This instance, chainable
     */
    public function setMerchantId(string $merchantId) : self
    {
        /** @var \EoneoPaySdk\Sdk\RequestParser $requestParser */
        $requestParser = $this->getRequestParser();
        $requestParser->setMerchantId($merchantId);

        // Make chainable
        return $this;
    }
}
