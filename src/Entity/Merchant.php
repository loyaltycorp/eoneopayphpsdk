<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\PaymentSource\BankAccount;
use EoneoPaySdk\Entity\PaymentSource\CreditCard;
use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Interfaces\Entities\Constants\MerchantConstants;
use EoneoPaySdk\Sdk\Interfaces\Entities\UserInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getAbn() Alias of $this->get('abn')
 * @method null|string getAddressCity() Alias of $this->get('address_city')
 * @method null|string getAddressLine1() Alias of $this->get('address_line_2')
 * @method null|string getAddressLine2() Alias of $this->get('address_line_1')
 * @method null|string getAddressPostalCode() Alias of $this->get('address_postal_code')
 * @method null|string getAddressState() Alias of $this->get('address_state')
 * @method null|string getBusinessName() Alias of $this->get('business_name')
 * @method null|string getBusinessPhone() Alias of $this->get('business_phone')
 * @method null|string getBusinessWebsite() Alias of $this->get('business_website')
 * @method null|int getClearingAccount() Alias of $this->get('clearing_account')
 * @method null|string getEmail() Alias of $this->get('email')
 * @method null|string getFirstName() Alias of $this->get('first_name')
 * @method null|string getLastName() Alias of $this->get('last_name')
 * @method null|string getStatementDescriptor() Alias of $this->get('statement_descriptor')
 * @method null|string getTitle() Alias of $this->get('title')
 * @method null|int getTransferDay() Alias of $this->get('transfer_day')
 * @method null|string getTransferFrequency() Alias of $this->get('transfer_frequency')
 * @method null|int getVersion() Alias of $this->get('version')
 * @method bool hasAbn() Alias of $this->has('abn')
 * @method bool hasAddressCity() Alias of $this->has('address_city')
 * @method bool hasAddressLine1() Alias of $this->has('address_line_2')
 * @method bool hasAddressLine2() Alias of $this->has('address_line_1')
 * @method bool hasAddressPostalCode() Alias of $this->has('address_postal_code')
 * @method bool hasAddressState() Alias of $this->has('address_state')
 * @method bool hasAvailableBalance() Alias of $this->has('available_balance')
 * @method bool hasAutomaticTransfer() Alias of $this->has('automatic_transfer')
 * @method bool hasBalance() Alias of $this->has('balance')
 * @method bool hasBankAccounts() Alias of $this->has('bank_accounts')
 * @method bool hasBusinessName() Alias of $this->has('business_name')
 * @method bool hasBusinessPhone() Alias of $this->has('business_phone')
 * @method bool hasBusinessWebsite() Alias of $this->has('business_website')
 * @method bool hasClearingAccount() Alias of $this->has('clearing_account')
 * @method bool hasCreditCards() Alias of $this->has('credit_cards')
 * @method bool hasEmail() Alias of $this->has('email')
 * @method bool hasFirstName() Alias of $this->has('first_name')
 * @method bool hasLastName() Alias of $this->has('last_name')
 * @method bool hasStatementDescriptor() Alias of $this->has('statement_descriptor')
 * @method bool hasTitle() Alias of $this->has('title')
 * @method bool hasTransferDay() Alias of $this->has('transfer_day')
 * @method bool hasTransferFrequency() Alias of $this->has('transfer_frequency')
 * @method bool hasTrustClearingAccount() Alias of $this->has('trust_clearing_account')
 * @method bool hasVersion() Alias of $this->has('version')
 * @method null|bool isAutomaticTransfer() Alias of $this->is('automatic_transfer')
 * @method null|bool isTrustClearingAccount() Alias of $this->is('trust_clearing_account')
 * @method $this setAbn(mixed $value) Alias of $this->set('abn', $value)
 * @method $this setAddressCity(mixed $value) Alias of $this->set('address_city', $value)
 * @method $this setAddressLine1(mixed $value) Alias of $this->set('address_line_2', $value)
 * @method $this setAddressLine2(mixed $value) Alias of $this->set('address_line_1', $value)
 * @method $this setAddressPostalCode(mixed $value) Alias of $this->set('address_postal_code', $value)
 * @method $this setAddressState(mixed $value) Alias of $this->set('address_state', $value)
 * @method $this setBusinessName(mixed $value) Alias of $this->set('business_name', $value)
 * @method $this setBusinessPhone(mixed $value) Alias of $this->set('business_phone', $value)
 * @method $this setBusinessWebsite(mixed $value) Alias of $this->set('business_website', $value)
 * @method $this setEmail(mixed $value) Alias of $this->set('email', $value)
 * @method $this setFirstName(mixed $value) Alias of $this->set('first_name', $value)
 * @method $this setLastName(mixed $value) Alias of $this->set('last_name', $value)
 * @method $this setStatementDescriptor(mixed $value) Alias of $this->set('statement_descriptor', $value)
 * @method $this setTitle(mixed $value) Alias of $this->set('title', $value)
 * @method $this setTransferDay(mixed $value) Alias of $this->set('transfer_day', $value)
 * @method $this setTransferFrequency(mixed $value) Alias of $this->set('transfer_frequency', $value)
 */
class Merchant extends Entity implements MerchantConstants, UserInterface
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'abn',
        'address_city',
        'address_line_1',
        'address_line_2',
        'address_postal_code',
        'address_state',
        'automatic_transfer',
        'available_balance',
        'balance',
        'business_name',
        'business_phone',
        'business_website',
        'clearing_account',
        'email',
        'first_name',
        'last_name',
        'statement_descriptor',
        'title',
        'transfer_day',
        'transfer_frequency',
        'trust_clearing_account',
        'version',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'delete' => 'merchants/:id',
        'get' => 'merchants/:id',
        'post' => 'merchants',
        'put' => 'merchants/:id',
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'abn',
        'address_city',
        'address_line_1',
        'address_line_2',
        'address_postal_code',
        'address_state',
        'automatic_transfer',
        'business_name',
        'business_phone',
        'business_website',
        'email',
        'first_name',
        'last_name',
        'statement_descriptor',
        'title',
        'transfer_day',
        'transfer_frequency',
    ];

    /**
     * Repository classes which can be used as part of a collection
     *
     * @var array
     */
    protected $repositories = [
        'bank_accounts' => BankAccount::class,
        'credit_cards' => CreditCard::class,
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'delete,put' => [
            'id' => 'required|regex:/^mer_[\da-zA-Z]+$/',
        ],
        'post,put' => [
            'abn' => 'required',
            'business_name' => 'required',
            'business_phone' => 'required',
            'business_website' => 'required',
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
            'title' => 'required',
            'transfer_day' => 'integer|greaterThanOrEqualTo:1|lessThanOrEqualTo:31',
            'transfer_frequency' => 'enum:daily,weekly,monthly',
        ],
    ];

    /**
     * Get available balance
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository A repository of available balances
     */
    public function getAvailableBalance() : Repository
    {
        return $this->enforceRepository('available_balance', Repository::class);
    }

    /**
     * Get total balance
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository A repository of total balances
     */
    public function getBalance() : Repository
    {
        return $this->enforceRepository('balance', Repository::class);
    }

    /**
     * Get bank accounts
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection A collection of bank accounts
     */
    public function getBankAccounts() : Collection
    {
        return $this->enforceCollection('bank_accounts');
    }

    /**
     * Get credit cards
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection A collection of credit cards
     */
    public function getCreditCards() : Collection
    {
        return $this->enforceCollection('credit_cards');
    }

    /**
     * Get the endpoint for resource actions
     *
     * @param string $method The method being requested
     *
     * @return string The endpoint for the request
     */
    public function getEndpointFromMethod(string $method) : string
    {
        // If no id is provided get the current merchant profile
        if (mb_strtolower($method) === 'get') {
            // Set endpoint based on whether an id has been provided or not
            $this->endpoints['get'] = $this->hasId() ?
                'merchants/:id' :
                'merchants/self';
        }

        // Pass through
        return parent::getEndpointFromMethod($method);
    }

    /**
     * Set whether automatic transfer is enabled or not
     *
     * @param mixed $automaticTransfer Whether automatic transfer is enabled or not
     *
     * @return \EoneoPaySdk\Entity\Merchant This resource, chainable
     */
    public function setAutomaticTransfer($automaticTransfer) : self
    {
        return $this->set('automatic_transfer', (bool)$automaticTransfer);
    }
}
