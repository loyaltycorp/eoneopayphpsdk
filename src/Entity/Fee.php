<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\Fee\CardRate;
use EoneoPaySdk\Sdk\Entity;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getMerchantId() Alias of $this->get('merchant_id')
 * @method null|string getPaymentType() Alias of $this->get('payment_type')
 * @method null|string getTransactionFee() Alias of $this->get('transaction_fee')
 * @method bool hasCardRates() Alias of $this->has('card_rates')
 * @method bool hasMerchantId() Alias of $this->has('merchant_id')
 * @method bool hasPaymentType() Alias of $this->has('payment_type')
 * @method bool hasTransactionFee() Alias of $this->has('transaction_fee')
 * @method $this setPaymentType(mixed $value) Alias of $this->set('payment_type', $value)
 * @method $this setTransactionFee(mixed $value) Alias of $this->set('transaction_fee', $value)
 */
class Fee extends Entity
{
    /**
     * Credit card types
     */
    const CARD_AMEX = 'AMEX';
    const CARD_MASTERCARD = 'MasterCard';
    const CARD_VISA = 'Visa';

    /**
     * Fee types
     */
    const TYPE_AUSPOST = 'auspost';
    const TYPE_AUSPOST_ADJUSTMENT = 'auspost_adjustment';
    const TYPE_AUSPOST_DISHONOURED = 'auspost_dishonoured';
    const TYPE_BPAY = 'bpay';
    const TYPE_BPAY_CORRECTION = 'bpay_correction';
    const TYPE_BPAY_REVERSAL = 'bpay_reversal';
    const TYPE_CHEQUE = 'cheque';
    const TYPE_CREDIT_CARD_PAYMENT = 'cc_payment';
    const TYPE_CREDIT_CARD_REFUND = 'cc_refund';
    const TYPE_DIRECT_DEBIT = 'direct_debit';
    const TYPE_DIRECT_DEPOSIT = 'direct_deposit';
    const TYPE_EFT = 'eft';
    const TYPE_EFT_DISHONOURED = 'eft_dishonoured';
    const TYPE_EWALLET = 'ewallet';

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'card_rates',
        'merchant_id',
        'payment_type',
        'transaction_fee',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'delete' => 'fees/:payment_type',
        'get' => 'fees/:payment_type',
        'put' => 'fees/:payment_type',
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'transaction_fee',
    ];

    /**
     * The primary key for this repository
     *
     * @var string
     */
    protected $primaryKey = 'payment_type';

    /**
     * Repository classes which can be used as part of a collection
     *
     * @var array
     */
    protected $repositories = [
        'card_rates' => CardRate::class,
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'delete,get,put' => [
            'payment_type' => [
                'required',
                'enum' => [
                    'auspost',
                    'auspost_adjustment',
                    'auspost_dishonoured',
                    'bpay',
                    'bpay_correction',
                    'bpay_reversal',
                    'cheque',
                    'cc_payment',
                    'cc_refund',
                    'direct_debit',
                    'direct_deposit',
                    'eft',
                    'eft_dishonoured',
                    'ewallet',
                ],
            ],
        ],
        'put' => [
            'transaction_fee' => 'required|integer|greaterThanOrEqualTo:0',
        ],
    ];

    /**
     * Get card rates
     *
     * @return \EoneoPaySdk\Entity\Fee\CardRate The card rates entity
     */
    public function getCardRates() : CardRate
    {
        return $this->enforceRepository('card_rates', CardRate::class);
    }
}
