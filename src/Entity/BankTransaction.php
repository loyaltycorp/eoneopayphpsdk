<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Interfaces\Entities\AllocationInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getAllocationId() Alias of $this->get('allocation_id')
 * @method null|string getAllocationType() Alias of $this->get('allocation_type')
 * @method null|int getAmount() Alias of $this->get('amount')
 * @method null|int getClearingAccount() Alias of $this->get('clearing_account')
 * @method null|string getCurrency() Alias of $this->get('currency')
 * @method null|string getDescription() Alias of $this->get('description')
 * @method null|string getFundsType() Alias of $this->get('funds_type')
 * @method null|string getRecordType() Alias of $this->get('record_type')
 * @method null|string getReferenceNumber() Alias of $this->get('reference_number')
 * @method null|string getSide() Alias of $this->get('side')
 * @method null|string getText() Alias of $this->get('text')
 * @method null|string getTransactionCode() Alias of $this->get('transaction_code')
 * @method null|string getTxnDate() Alias of $this->get('txn_date')
 * @method null|int getVersion() Alias of $this->get('version')
 * @method bool hasAllocationId() Alias of $this->has('allocation_id')
 * @method bool hasAllocationType() Alias of $this->has('allocation_type')
 * @method bool hasAmount() Alias of $this->has('amount')
 * @method bool hasClearingAccount() Alias of $this->has('clearing_account')
 * @method bool hasCurrency() Alias of $this->has('currency')
 * @method bool hasDescription() Alias of $this->has('description')
 * @method bool hasFundsType() Alias of $this->has('funds_type')
 * @method bool hasRecordType() Alias of $this->has('record_type')
 * @method bool hasReferenceNumber() Alias of $this->has('reference_number')
 * @method bool hasSide() Alias of $this->has('side')
 * @method bool hasText() Alias of $this->has('text')
 * @method bool hasTransactionCode() Alias of $this->has('transaction_code')
 * @method bool hasTxnDate() Alias of $this->has('txn_date')
 * @method bool hasVersion() Alias of $this->has('version')
 */
class BankTransaction extends Entity
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'allocation_id',
        'allocation_type',
        'amount',
        'clearing_account',
        'currency',
        'description',
        'funds_type',
        'record_type',
        'reference_number',
        'side',
        'text',
        'transaction_code',
        'txn_date',
        'version',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'post' => 'bankTransactions/:id/allocate',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'post' => [
            'allocation_id' => 'required',
            'allocation_type' => 'required',
            'id' => 'required',
        ],
    ];

    /**
     * Add allocation destination
     *
     * @param \EoneoPaySdk\Sdk\Interfaces\Entities\AllocationInterface $allocation The entity to allocate
     *
     * @return \EoneoPaySdk\Entity\BankTransaction This instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the allocation is invalid
     */
    public function allocateTo(AllocationInterface $allocation) : self
    {
        // Validate id
        if ($allocation->getId() === null) {
            throw new InvalidEntityException('Unable to attach entity: not a valid allocation');
        }

        // Set allocations
        $this->set('allocation_id', $allocation->getId());
        $this->set('allocation_type', $allocation->getType());

        // Make chainable with this entity
        return $this;
    }
}
