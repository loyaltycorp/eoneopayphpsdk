<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Interfaces\Entities\AllocationInterface;
use EoneoPaySdk\Sdk\Traits\CustomerAttachable;
use EoneoPaySdk\Sdk\Traits\Entities\Allocatable;
use EoneoPaySdk\Sdk\Traits\MerchantAttachable;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|int getBalance() Alias of $this->get('balance')
 * @method null|int getClearingAccount() Alias of $this->get('clearingAccount')
 * @method null|string getCustomerId() Alias of $this->get('customer_id')
 * @method null|string getLockedUntilDate() Alias of $this->get('locked_until_date')
 * @method null|string getMerchantId() Alias of $this->get('merchant_id')
 * @method null|mixed getMeta() Alias of $this->get('meta')
 * @method null|string getName() Alias of $this->get('name')
 * @method null|string getSourceId() Alias of $this->get('source_id')
 * @method bool hasBalance() Alias of $this->has('balance')
 * @method bool hasClearingAccount() Alias of $this->has('clearingAccount')
 * @method bool hasCustomerId() Alias of $this->has('customer_id')
 * @method bool hasLockedUntilDate() Alias of $this->has('locked_until_date')
 * @method bool hasMerchantId() Alias of $this->has('merchant_id')
 * @method bool hasMeta() Alias of $this->has('meta')
 * @method bool hasName() Alias of $this->has('id')
 * @method bool hasSourceId() Alias of $this->has('source_id')
 * @method $this setMeta(mixed $value) Alias of $this->set('meta' $value)
 * @method $this setName(mixed $value) Alias of $this->set('name', $value)
 */
class Ewallet extends Entity implements AllocationInterface
{
    use Allocatable, CustomerAttachable, MerchantAttachable;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'balance',
        'clearingAccount',
        'customer_id',
        'merchant_id',
        'meta',
        'locked_until_date',
        'name',
        'source_id',
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'meta',
        'name'
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'delete' => ':resource/:resource_id/ewallets/:id',
        'get' => '', // This is set at run time via getEndpointFromMethod()
        'post' => ':resource/:resource_id/ewallets',
        'put' => ':resource/:resource_id/ewallets/:id',
    ];

    /**
     * Create a new eWallet instance
     *
     * @param array $data The data to populate the repository with
     */
    public function __construct(array $data = null)
    {
        // Set resource and resource id from data
        $this->setEndpointVariablesFromData($data, ['customer_id', 'merchant_id']);

        // Pass through to entity
        parent::__construct($data);
    }

    /**
     * Get the endpoint for resource actions
     *
     * @param string $method The method being requested
     *
     * @return string The endpoint for the request
     */
    public function getEndpointFromMethod(string $method) : string
    {
        // Ewallet 'get' has special ability to get the primary ewallet or get by id
        if (mb_strtolower($method) === 'get') {
            // Set endpoint based on whether an id has been provided or not
            $this->endpoints['get'] = $this->hasId() ?
                ':resource/:resource_id/ewallets/:id' :
                ':resource/:resource_id/ewallet';
        }

        // Pass through
        return parent::getEndpointFromMethod($method);
    }

    /**
     * Set locked until date, this method formats the date correctly
     *
     * @param string $date The date to set
     *
     * @return \EoneoPaySdk\Entity\Ewallet This instance
     */
    public function setLockedUntilDate(string $date) : self
    {
        return $this->set('locked_until_date', $this->formatDate($date, 'Y-m-d H:i:s'));
    }
}
