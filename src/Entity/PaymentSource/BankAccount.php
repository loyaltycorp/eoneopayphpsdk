<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\PaymentSource;

use EoneoPaySdk\Entity\PaymentSource;
use EoneoPaySdk\Entity\PaymentSource\Info\BankAccount as BankAccountInfo;

class BankAccount extends PaymentSource
{
    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'delete' => ':resource/:holder_id/bankAccounts/:id',
        'get' => ':resource/:holder_id/bankAccounts/:id',
        'put' => ':resource/:holder_id/bankAccounts/:id',
    ];

    /**
     * Repository classes which can be used as part of a collection
     *
     * @var array
     */
    protected $repositories = [
        'info' => BankAccountInfo::class,
    ];
}
