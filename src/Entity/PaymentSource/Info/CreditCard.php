<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\PaymentSource\Info;

use DateTime;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getCardDescription() Alias of $this->get('cardDescription')
 * @method null|int getCardType() Alias of $this->get('cardType')
 * @method null|string getExpiryDate() Alias of $this->get('expiryDate')
 * @method null|string getExpiryMonth() Alias of $this->get('expiry_month')
 * @method null|string getExpiryYear() Alias of $this->get('expiry_year')
 * @method null|string getName() Alias of $this->get('name')
 * @method null|string getPan() Alias of $this->get('pan')
 * @method null|string getToken() Alias of $this->get('token')
 * @method bool hasCardDescription() Alias of $this->has('cardDescription')
 * @method bool hasCardType() Alias of $this->has('cardType')
 * @method bool hasDefault() Alias of $this->is('default')
 * @method bool hasExpiryDate() Alias of $this->has('expiryDate')
 * @method bool hasExpiryMonth() Alias of $this->has('expiry_month')
 * @method bool hasExpiryYear() Alias of $this->has('expiry_year')
 * @method bool hasName() Alias of $this->has('name')
 * @method bool hasPan() Alias of $this->has('pan')
 * @method bool hasToken() Alias of $this->has('token')
 * @method null|bool isDefault() Alias of $this->is('default')
 * @method $this setToken(mixed $value) Alias of $this->set('token', $value)
 */
class CreditCard extends Info
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'cardDescription',
        'cardType',
        'expiryDate',
        'expiry_month',
        'expiry_year',
        'name',
        'pan',
        'token',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'post' => ':resource/:resource_id/cards',
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'default',
        'token',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'post,put' => [
            'token' => 'required|regex:/^tok_[\da-zA-Z]+$/',
        ],
    ];

    /**
     * Create a new credit card
     *
     * @param array $data The data to populate the repository with
     */
    public function __construct(array $data = null)
    {
        // Grab the resource from data
        $this->setEndpointVariablesFromData($data, ['holder_id']);

        if (is_array($data)) {
            // Re-map number to pan
            if (array_key_exists('number', $data)) {
                $data['pan'] = $data['number'];
                unset($data['number']);
            }

            // Set expiry date
            if (isset($data['expiry_month'], $data['expiry_year'])) {
                // Correctly format month and year
                $month = (int)preg_replace('/\D/', '', $data['expiry_month']);
                $year = (int)preg_replace('/\D/', '', $data['expiry_year']);
                $year = $year < 2000 ? 2000 + $year : $year;

                $expiry = (new DateTime)->setDate($year, $month, 1);
                $data['expiryDate'] = $expiry->format('m/y');
            }
        }

        // Create repository
        parent::__construct($data);
    }
}
