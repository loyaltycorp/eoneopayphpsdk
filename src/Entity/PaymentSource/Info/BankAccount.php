<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\PaymentSource\Info;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getBsb() Alias of $this->get('bsb')
 * @method null|string getName() Alias of $this->get('name')
 * @method null|string getNumber() Alias of $this->get('number')
 * @method bool hasBsb() Alias of $this->has('bsb')
 * @method bool hasDefault() Alias of $this->has('default)
 * @method bool hasName() Alias of $this->has('name')
 * @method bool hasNumber() Alias of $this->has('number')
 * @method bool hasTrust() Alias of $this->is('trust')
 * @method null|bool isDefault() Alias of $this->is('default')
 * @method null|bool isTrust() Alias of $this->is('trust')
 * @method $this setName(mixed $value) Alias of $this->set('name', $value)
 * @method $this setNumber(mixed $value) Alias of $this->set('number', $value)
 */
class BankAccount extends Info
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'bsb',
        'default',
        'name',
        'number',
        'trust'
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'post' => ':resource/:resource_id/bankAccounts'
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'bsb',
        'default',
        'name',
        'number',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'post,put' => [
            'bsb' => 'required|regex:/^[\d]{3}-[\d]{3}$/',
            'name' => 'required',
            'number' => 'required',
        ],
    ];

    /**
     * Format bsb correctly
     *
     * @param string $bsb The bsb to format
     *
     * @return \EoneoPaySdk\Entity\PaymentSource\Info\BankAccount This resource, chainable
     */
    public function setBsb(string $bsb) : self
    {
        // Reformat bsb as XXX-XXX
        return $this->set('bsb', preg_replace('/([\d]{3})([\d]{3})/', '$1-$2', preg_replace('/\D/', '', $bsb)));
    }

    /**
     * Set bank account as the default payment source, this method forces a boolean value
     *
     * @param mixed $default Whether this bank account is the default source or not
     *
     * @return \EoneoPaySdk\Entity\PaymentSource\Info\BankAccount This resource, chainable
     */
    public function setDefault($default) : self
    {
        return $this->set('default', (bool)$default);
    }
}
