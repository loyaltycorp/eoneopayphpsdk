<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\PaymentSource\Info;

use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Traits\CustomerAttachable;
use EoneoPaySdk\Sdk\Traits\MerchantAttachable;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getAddressCity() Alias of $this->get('address_city')
 * @method null|string getAddressCountry() Alias of $this->get('address_country')
 * @method null|string getAddressLine1() Alias of $this->get('address_line1')
 * @method null|string getAddressLine2() Alias of $this->get('address_line2')
 * @method null|string getAddressPostcode() Alias of $this->get('address_postcode')
 * @method null|string getAddressState() Alias of $this->get('address_state')
 * @method null|string getCurrency() Alias of $this->get('currency')
 * @method bool hasAddressCity() Alias of $this->has('address_city')
 * @method bool hasAddressCountry() Alias of $this->has('address_country')
 * @method bool hasAddressLine1() Alias of $this->has('address_line1')
 * @method bool hasAddressLine2() Alias of $this->has('address_line2')
 * @method bool hasAddressPostcode() Alias of $this->has('address_postcode')
 * @method bool hasAddressState() Alias of $this->has('address_state')
 * @method bool hasCurrency() Alias of $this->has('currency')
 * @method $this setAddressCity(mixed $value) Alias of $this->set('address_city', $value)
 * @method $this setAddressCountry(mixed $value) Alias of $this->set('address_country', $value)
 * @method $this setAddressLine1(mixed $value) Alias of $this->set('address_line1', $value)
 * @method $this setAddressLine2(mixed $value) Alias of $this->set('address_line2', $value)
 * @method $this setAddressPostcode(mixed $value) Alias of $this->set('address_postcode', $value)
 * @method $this setAddressState(mixed $value) Alias of $this->set('address_state', $value)
 */
class Info extends Entity
{
    use CustomerAttachable, MerchantAttachable;

    /**
     * Create a new payment source
     *
     * @param array $data The data to pre-load the repository with
     */
    public function __construct(array $data = null)
    {
        // Add shared payment source info fields to attributes and mutable arrays
        $this->addAttributes([
            'address_city',
            'address_country',
            'address_line1',
            'address_line2',
            'address_postcode',
            'address_state',
            'currency',
        ]);

        $this->addMutable([
            'address_city',
            'address_country',
            'address_line1',
            'address_line2',
            'address_postcode',
            'address_state',
        ]);

        parent::__construct($data);
    }
}
