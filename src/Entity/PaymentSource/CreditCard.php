<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\PaymentSource;

use EoneoPaySdk\Entity\PaymentSource;
use EoneoPaySdk\Entity\PaymentSource\Info\CreditCard as CreditCardInfo;

class CreditCard extends PaymentSource
{
    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'delete' => ':resource/:holder_id/cards/:id',
        'get' => ':resource/:holder_id/cards/:id',
        'put' => ':resource/:holder_id/cards/:id',
    ];

    /**
     * Repository classes which can be used as part of a collection
     *
     * @var array
     */
    protected $repositories = [
        'info' => CreditCardInfo::class,
    ];
}
