<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Interfaces\Entities\Constants\WebhookConstants;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getActive() Alias of $this->get('active')
 * @method null|string getEventName() Alias of $this->get('event_name')
 * @method null|string getMerchantId() Alias of $this->get('merchant_id')
 * @method null|string getName() Alias of $this->get('name')
 * @method null|string getTargetUrl() Alias of $this->get('target_url')
 * @method bool hasActive() Alias of $this->has('active')
 * @method bool hasEventName() Alias of $this->has('event_name')
 * @method bool hasMerchantId() Alias of $this->has('merchant_id')
 * @method bool hasName() Alias of $this->has('name')
 * @method bool hasTargetUrl() Alias of $this->has('target_url')
 * @method $this setEventName(mixed $value) Alias of $this->set('event_name', $value)
 * @method $this setName(mixed $value) Alias of $this->set('name', $value)
 * @method $this setTargetUrl(mixed $value) Alias of $this->set('target_url', $value)
 */
class Webhook extends Entity implements WebhookConstants
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'active',
        'event_name',
        'merchant_id',
        'name',
        'target_url',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'delete' => 'webhooks/:id',
        'get' => 'webhooks/:id',
        'post' => 'webhooks',
        'put' => 'webhooks/:id',
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'active',
        'event_name',
        'name',
        'target_url',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'delete,get,put' => [
            'id' => 'required|regex:/^webhook_[\da-zA-Z]+$/'
        ],
        'post,put' => [
            'active' => 'required',
            'event_name' => [
                'required',
                'enum' => [
                    'PaymentCreated',
                    'PaymentFailed',
                    'PaymentMethodCreated',
                    'PaymentMethodDeleted',
                    'PaymentSucceeded',
                    'RefundCreated',
                    'RefundFailed',
                    'RefundSucceeded',
                    'ReversalCreated',
                ],
            ],
            'name' => 'required',
            'target_url' => 'required|url',
        ],
    ];

    /**
     * Create new webhook
     *
     * @param array $data The data to pre-load the repository with
     */
    public function __construct(array $data = null)
    {
        // Make webhook active by default
        $this->setActive(true);

        parent::__construct($data);
    }

    /**
     * Set whether this webhook is active or not
     *
     * @param mixed $status Whether the webhook is active or not
     *
     * @return \EoneoPaySdk\Entity\Webhook This entity, chainable
     */
    public function setActive($status) : self
    {
        return $this->set('active', (bool)$status);
    }
}
