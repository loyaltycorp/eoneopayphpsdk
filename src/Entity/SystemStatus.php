<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Traits\MerchantAttachable;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getFromDate() Alias of $this->get('from_date')
 * @method null|string getMerchantId() Alias of $this->get('merchant_id')
 * @method null|string getToDate() Alias of $this->get('from_date')
 * @method bool hasFromDate() Alias of $this->has('from_date')
 * @method bool hasMerchantId() Alias of $this->has('merchant_id')
 * @method bool hasPaymentAllocations() Alias of $this->has('payment_allocations')
 * @method bool hasPayments() Alias of $this->has('payments')
 * @method bool hasToDate() Alias of $this->has('to_date')
 * @method bool hasTransfers() Alias of $this->has('transfers')
 */
class SystemStatus extends Entity
{
    use MerchantAttachable;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'from_date',
        'merchant_id',
        'payment_allocations',
        'payments',
        'to_date',
        'transfers',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'systemStatus',
    ];

    /**
     * Get payment allocations
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository A repository with payment allocations
     */
    public function getPaymentAllocations() : Repository
    {
        return $this->enforceRepository('payment_allocations', Repository::class);
    }

    /**
     * Get payments
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository A repository with payments
     */
    public function getPayments() : Repository
    {
        return $this->enforceRepository('payments', Repository::class);
    }

    /**
     * Get transfers
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository A repository with transfers
     */
    public function getTransfers() : Repository
    {
        return $this->enforceRepository('transfers', Repository::class);
    }

    /**
     * Set the date to get the status from
     *
     * @param string $date The date to set
     *
     * @return \EoneoPaySdk\Entity\SystemStatus This instance
     */
    public function setFromDate(string $date) : self
    {
        return $this->set('from_date', $this->formatDate($date, 'Y-m-d'));
    }

    /**
     * Set the date to get the status to
     *
     * @param string $date The date to set
     *
     * @return \EoneoPaySdk\Entity\SystemStatus This instance
     */
    public function setToDate(string $date) : self
    {
        return $this->set('to_date', $this->formatDate($date, 'Y-m-d'));
    }
}
