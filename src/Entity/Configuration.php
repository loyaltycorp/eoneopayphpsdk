<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Sdk\Entity;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getKey() Alias of $this->get('key')
 * @method null|mixed getValue() Alias of $this->get('value')
 * @method bool hasKey() Alias of $this->has('key')
 * @method bool hasValue() Alias of $this->has('value')
 * @method $this setKey(mixed $value) Alias of $this->set('key', $value)
 * @method $this setValue(mixed $value) Alias of $this->set('value', $value)
 */
class Configuration extends Entity
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'key',
        'value',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'configuration/:key',
        'put' => 'configuration/:key',
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'value',
    ];

    /**
     * The primary key for this repository
     *
     * @var string
     */
    protected $primaryKey = 'key';

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'get,put' => [
            'key' => 'required',
        ],
        'put' => [
            'value' => 'required',
        ],
    ];
}
