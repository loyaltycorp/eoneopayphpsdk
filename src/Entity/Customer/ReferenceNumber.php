<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\Customer;

use EoneoPaySdk\Entity\Customer\ReferenceNumber\AusPost;
use EoneoPaySdk\Entity\Customer\ReferenceNumber\Bpay;
use EoneoPaySdk\Entity\Customer\ReferenceNumber\Eft;
use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Interfaces\Entities\ReferenceNumberInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getAllocationDate() Alias of $this->get('allocation_date')
 * @method null|string getAmount() Alias of $this->get('amount')
 * @method null|string getCustomerId() Alias of $this->get('customer_id')
 * @method null|string getReferenceNumber() Alias of $this->get('reference_number')
 * @method null|string getType() Alias of $this->get('type')
 * @method bool hasAllocated() Alias of $this->has('allocated')
 * @method bool hasAllocationDate() Alias of $this->has('allocation_date')
 * @method bool hasAmount() Alias of $this->has('amount')
 * @method bool hasCustomerId() Alias of $this->has('customer_id')
 * @method bool hasPaymentFacility() Alias of $this->has('payment_facility')
 * @method bool hasReferenceNumber() Alias of $this->has('reference_number')
 * @method bool hasType() Alias of $this->has('type')
 * @method null|bool isAllocated() Alias of $this->get('allocated')
 * @method $this setAmount(mixed $value) Alias of $this->set('amount', $value)
 * @method $this setReferenceNumber(mixed $value) Alias of $this->set('reference_number', $value)
 */
class ReferenceNumber extends Entity implements ReferenceNumberInterface
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'allocated',
        'allocation_date',
        'amount',
        'biller_code',
        'amount',
        'customer_id',
        'pip_id',
        'reference_number',
        'type',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => '', // This is set at run time via getEndpointFromMethod()
        'delete' => '', // This is set at run time via getEndpointFromMethod()
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'amount',
    ];

    /**
     * The primary key for this repository
     *
     * @var string
     */
    protected $primaryKey = 'reference_number';

    /**
     * Repository classes which can be used as part of a collection
     *
     * @var array
     */
    protected $repositories = [
        'payment_facility' => ReferenceNumber::class,
    ];

    /**
     * Get the endpoint for resource actions
     *
     * @param string $method The method being requested
     *
     * @return string The endpoint for the request
     */
    public function getEndpointFromMethod(string $method) : string
    {
        // Lowercase the method
        $method = mb_strtolower($method);

        // Remove endpoint so call fails if unrecognized type
        unset($this->endpoints[$method]);

        // End points are based on type
        switch ($method) {
            case 'delete':
                switch ($this->getType()) {
                    case 'allocated_eft':
                        $this->endpoints['delete'] = 'customers/:customer_id/eftId/:reference_number';
                        break;

                    case 'auspost':
                        $this->endpoints['delete'] = 'customers/:customer_id/ausPostCrn/:reference_number';
                        break;

                    case 'bpay':
                        $this->endpoints['delete'] = 'customers/:customer_id/bpayCrn/:reference_number';
                        break;
                }
                break;

            case 'get':
                switch ($this->getType()) {
                    case 'auspost':
                        $this->endpoints['get'] = sprintf(
                            'customers/%s/ausPostBarcodeForCrn/%s/withPIPId/%s/forAmount/%s',
                            ':customer_id',
                            ':reference_number',
                            ':pip_id',
                            ':amount'
                        );
                        break;

                    case 'bpay':
                        $this->endpoints['get'] = sprintf(
                            'customers/%s/bpayBillAdviceForCrn/%s/withBillerCode/%s',
                            ':customer_id',
                            ':reference_number',
                            ':biller_code'
                        );
                        break;
                }
                break;
        }

        // Pass through
        return parent::getEndpointFromMethod($method);
    }

    /**
     * Get payment facility
     *
     * @return mixed The payment facility
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the payment type isn't set
     */
    public function getPaymentFacility() : ReferenceNumberInterface
    {
        // Get class based on type
        switch ($this->getType()) {
            case 'allocated_eft':
                $class = Eft::class;
                break;

            case 'auspost':
                $class = AusPost::class;
                break;

            case 'bpay':
                $class = Bpay::class;
                break;

            default:
                // Payment facility isn't valid
                throw new InvalidEntityException('Unable to get payment facility: invalid reference number type');
        }

        return $this->enforceRepository('payment_facility', $class);
    }

    /**
     * Init a reference number object
     *
     * @param array $data Array of data to populate the reference number with
     *
     * @return \EoneoPaySdk\Sdk\Interfaces\Entities\ReferenceNumberInterface The instantiated reference number entity
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the reference number type is invalid
     */
    public function init(array $data = null) : ReferenceNumberInterface
    {
        // If this hasn't been instantiated directly, or data isn't an array with type or
        // data contains payment_facility, return original reference number instance
        if (!is_array($data) ||
            !array_key_exists('type', $data) ||
            array_key_exists('payment_facility', $data) ||
            get_class($this) !== __CLASS__
        ) {
            return new $this($data);
        }

        // Create resource based on type
        switch ($data['type']) {
            case 'allocated_eft':
                return new Eft($data);

            case 'auspost':
                return new AusPost($data);

            case 'bpay':
                return new Bpay($data);

            default:
                // Reference number isn't valid
                throw new InvalidEntityException('Unable to create entity: invalid reference number type');
        }
    }

    /**
     * Validate request against a ruleset
     *
     * @param string $method The method to validate agains
     *
     * @return bool The validation status
     */
    public function validate(string $method = null) : bool
    {
        // Check variables are set when performing a get
        if (mb_strtolower($method) === 'get') {
            // Remove rules by default
            $this->rules['get'] = [];

            // Build rules based on type
            switch ($this->getType()) {
                case 'auspost':
                    // Get pip from payment facility, use null if payment facility is invalid so validation fails
                    $this->set(
                        'pip_id',
                        $this->getPaymentFacility() instanceof AusPost ? $this->getPaymentFacility()->getPipId() : null
                    );

                    $this->rules['get'] = [
                        'amount' => 'required|greaterThan:0',
                        'customer_id' => 'required:|regex:/^cus_[\da-zA-Z]+|mer_[\da-zA-Z]+$/',
                        'pip_id' => 'required',
                        'reference_number' => 'required',
                    ];
                    break;

                case 'bpay':
                    // Get biller code from payment facility, use null if payment facility is invalid
                    $this->set(
                        'biller_code',
                        $this->getPaymentFacility() instanceof Bpay ?
                            $this->getPaymentFacility()->getBillerCode() :
                            null
                    );

                    $this->rules['get'] = [
                        'biller_code' => 'required',
                        'customer_id' => 'required:|regex:/^cus_[\da-zA-Z]+|mer_[\da-zA-Z]+$/',
                        'reference_number' => 'required',
                    ];
                    break;
            }
        }

        // Pass through
        return parent::validate($method);
    }
}
