<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\Customer;

use EoneoPaySdk\Entity\Charge;
use EoneoPaySdk\Entity\PaymentSource;
use EoneoPaySdk\Entity\Plan;
use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Interfaces\Entities\AllocationInterface;
use EoneoPaySdk\Sdk\Traits\CustomerAttachable;
use EoneoPaySdk\Sdk\Traits\Entities\Allocatable;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getAllocatedEftReferenceNumber() Alias of $this->get('allocated_eft_reference_number')
 * @method null|string getAuspostReferenceNumber() Alias of $this->get('auspost_reference_number')
 * @method null|string getBpayReferenceNumber() Alias of $this->get('bpay_reference_number')
 * @method null|string getCustomerId() Alias of $this->get('customer_id')
 * @method null|int getCurrentBalance() Alias of $this->get('current_balance')
 * @method null|string getCurrentPeriodEnd() Alias of $this->get('current_period_end')
 * @method null|string getCurrentPeriodStart() Alias of $this->get('current_period_start')
 * @method null|string getEndDate() Alias of $this->get('end_date')
 * @method null|int getLeadTime() Alias of $this->get('lead_time')
 * @method null|string getMerchantId() Alias of $this->get('merchant_id')
 * @method null|string|Plan getPlan() Alias of $this->get('plan')
 * @method null|string getPlanId() Alias of $this->get('plan_id')
 * @method null|string getSource() Alias of $this->get('source')
 * @method null|string getStartDate() Alias of $this->get('start_date')
 * @method null|string getStatus() Alias of $this->get('status')
 * @method bool hasAllocatedEftReferenceNumber() Alias of $this->has('allocated_eft_reference_number')
 * @method bool hasAuspostReferenceNumber() Alias of $this->has('auspost_reference_number')
 * @method bool hasBpayReferenceNumber() Alias of $this->has('bpay_reference_number')
 * @method bool hasCharges() Alias of $this->has('charges')
 * @method bool hasCustomerId() Alias of $this->has('customer_id')
 * @method bool hasCurrentBalance() Alias of $this->has('current_balance')
 * @method bool hasCurrentPeriodEnd() Alias of $this->has('current_period_end')
 * @method bool hasCurrentPeriodStart() Alias of $this->has('current_period_start')
 * @method bool hasEndDate() Alias of $this->has('end_date')
 * @method bool hasLeadTime() Alias of $this->has('lead_time')
 * @method bool hasMerchantId() Alias of $this->has('merchant_id')
 * @method bool hasPaymentFacilities() Alias of $this->has('payment_facilities')
 * @method bool hasPlan() Alias of $this->has('plan')
 * @method bool hasPlanId() Alias of $this->has('plan_id')
 * @method bool hasSource() Alias of $this->has('source')
 * @method bool hasStartDate() Alias of $this->has('start_date')
 * @method bool hasStatement() Alias of $this->has('statement')
 * @method bool hasStatus() Alias of $this->has('status')
 * @method $this setLeadTime(mixed $value) Alias of $this->set('lead_time', $value)
 */
class Subscription extends Entity implements AllocationInterface
{
    use Allocatable, CustomerAttachable;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'allocated_eft_reference_number',
        'auspost_reference_number',
        'bpay_reference_number',
        'customer_id',
        'current_balance',
        'current_period_end',
        'current_period_start',
        'end_date',
        'lead_time',
        'merchant_id',
        'payment_facilities',
        'plan_id',
        'source',
        'start_date',
        'statement',
        'status',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'delete' => 'customers/:resource_id/subscriptions/:id',
        'get' => 'customers/:resource_id/subscriptions/:id',
        'post' => 'customers/:resource_id/subscriptions',
        'put' => 'customers/:resource_id/subscriptions/:id',
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'end_date',
        'lead_time',
        'start_date',
    ];

    /**
     * Repository classes which can be used as part of a collection
     *
     * @var array
     */
    protected $repositories = [
        'charges' => Charge::class,
        'plan' => Plan::class,
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'delete,get,put' => [
            'id' => 'required|regex:/^sub_[\da-zA-Z]+$/',
        ],
        'post' => [
            'plan' => 'required|regex:/^plan_[\da-zA-Z]+$/',
        ],
        'post,put' => [
            'lead_time' => 'integer',
            'source' => 'regex:/^src_[\da-zA-Z]+$/',
        ],
        'put' => [
            'plan' => 'entity:' . Plan::class,
        ],
    ];

    /**
     * Create a new statement instance
     *
     * @param array $data The data to populate the repository with
     */
    public function __construct(array $data = null)
    {
        // Set resource and resource id from data
        $this->setEndpointVariablesFromData($data, ['customer_id']);

        // Pass through to entity
        parent::__construct($data);
    }

    /**
     * Add a charge to a plan
     *
     * @param \EoneoPaySdk\Entity\Charge $charge The charge to add to this subscription
     *
     * @return \EoneoPaySdk\Entity\Customer\Subscription This instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the charge is invalid
     */
    public function addCharge(Charge $charge) : self
    {
        // Validate payment source
        if (!preg_match('/^chg_[\da-zA-Z]+$/', (string)$charge->getId())) {
            throw new InvalidEntityException('Unable to attach entity: not a valid charge');
        }

        // Add charge to subscription
        $this->getCharges()->add($charge);

        // Make chainable
        return $this;
    }

    /**
     * Set the plan this subscription is for
     *
     * @param \EoneoPaySdk\Entity\Plan $plan The plan to add to this subscription
     *
     * @return \EoneoPaySdk\Entity\Customer\Subscription This instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the plan is invalid
     */
    public function forPlan(Plan $plan) : self
    {
        // Validate payment source
        if (!preg_match('/^plan_[\da-zA-Z]+$/', (string)$plan->getId())) {
            throw new InvalidEntityException('Unable to attach entity: not a valid plan');
        }

        // Set plan and make chainable
        return $this->set($this->getId() ? 'plan_id' : 'plan', $plan->getId());
    }

    /**
     * Set the reference this subscription is for
     *
     * @param \EoneoPaySdk\Entity\Customer\ReferenceNumber $referenceNumber The reference number
     *
     * @return \EoneoPaySdk\Entity\Customer\Subscription This instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the reference is invalid
     */
    public function fromReferenceNumber(ReferenceNumber $referenceNumber) : self
    {
        // Validate reference number
        if (!preg_match('/^[\d]{8,}$/', (string)$referenceNumber->getReferenceNumber())) {
            throw new InvalidEntityException('Unable to attach entity: not a valid reference number');
        }

        // Set reference number and make chainable
        switch ($referenceNumber->getType()) {
            case 'allocated_eft':
                return $this->set('allocated_eft_reference_number', $referenceNumber->getReferenceNumber());

            case 'auspost':
                return $this->set('auspost_reference_number', $referenceNumber->getReferenceNumber());

            case 'bpay':
                return $this->set('bpay_reference_number', $referenceNumber->getReferenceNumber());

            default:
                // Reference is invalid
                throw new InvalidEntityException('Unable to attach entity: not a valid reference number');
        }
    }

    /**
     * Set the payment source this subscription is for
     *
     * @param \EoneoPaySdk\Entity\PaymentSource $paymentSource The payment source to set
     *
     * @return \EoneoPaySdk\Entity\Customer\Subscription This instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the payment source
     */
    public function fromPaymentSource(PaymentSource $paymentSource) : self
    {
        // Validate payment source
        if (!preg_match('/^src_[\da-zA-Z]+$/', (string)$paymentSource->getId())) {
            throw new InvalidEntityException('Unable to attach entity: not a valid payment source');
        }

        // Set source
        return $this->set('source', $paymentSource->getId());
    }

    /**
     * Get charges
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection A collection of charges
     */
    public function getCharges() : Collection
    {
        return $this->enforceCollection('charges');
    }

    /**
     * Get payment facilities
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository A repository of payment facilities
     */
    public function getPaymentFacilities() : Repository
    {
        return $this->enforceRepository('payment_facilities', Repository::class);
    }

    /**
     * Get statement
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository A repository for a statement
     */
    public function getStatement() : Repository
    {
        return $this->enforceRepository('statement', Repository::class);
    }

    /**
     * Set start date, this method formats the date correctly
     *
     * @param string $date The date to set
     *
     * @return \EoneoPaySdk\Entity\Customer\Subscription This instance
     */
    public function setStartDate(string $date) : self
    {
        return $this->set('start_date', $this->formatDate($date, 'Y-m-d'));
    }

    /**
     * Set end date, this method formats the date correctly
     *
     * @param string $date The date to set
     *
     * @return \EoneoPaySdk\Entity\Customer\Subscription This instance
     */
    public function setEndDate(string $date) : self
    {
        return $this->set('end_date', $this->formatDate($date, 'Y-m-d'));
    }
}
