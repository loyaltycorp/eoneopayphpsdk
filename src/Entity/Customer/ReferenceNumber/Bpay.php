<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\Customer\ReferenceNumber;

use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Interfaces\Entities\ReferenceNumberInterface;
use EoneoPaySdk\Sdk\Traits\CustomerAttachable;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getBillerCode() Alias of $this->get('biller_code')
 * @method null|int getClearingAccount() Alias of $this->get('clearing_account')
 * @method null|string getPipId() Alias of $this->get('pip_id')
 * @method null|string getType() Alias of $this->get('type')
 * @method bool hasBillerCode() Alias of $this->has('biller_code')
 * @method bool hasClearingAccount() Alias of $this->has('clearing_account')
 * @method bool hasDefault() Alias of $this->has('default')
 * @method bool hasPipId() Alias of $this->has('pip_id')
 * @method bool hasType() Alias of $this->has('type')
 * @method null|bool isDefault() Alias of $this->is('default')
 * @method $this setBillerCode(mixed $value) Alias of $this->set('biller_code', $value)
 * @method $this setPipId(mixed $value) Alias of $this->set('pip_id', $value)
 */
class Bpay extends Entity implements ReferenceNumberInterface
{
    use CustomerAttachable;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'biller_code',
        'clearing_account',
        'default',
        'pip_id',
        'type',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'post' => '', // This is set at run time via getEndpointFromMethod()
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'biller_code',
        'pip_id',
    ];

    /**
     * Get the endpoint for resource actions
     *
     * @param string $method The method being requested
     *
     * @return string The endpoint for the request
     */
    public function getEndpointFromMethod(string $method) : string
    {
        // Reference number can be created with or without a biller_code and pip_id
        if (mb_strtolower($method) === 'post') {
            // Set endpoint based on whether a biller_code or pip_id has been provided or not
            $this->endpoints['post'] = $this->hasBillerCode() || $this->hasPipId() ?
                'customers/:resource_id/bpayCrnForBillerCode/:biller_code/AndPIPId/:pip_id' :
                'customers/:resource_id/bpayCrn';
        }

        // Pass through
        return parent::getEndpointFromMethod($method);
    }

    /**
     * Validate request against a ruleset
     *
     * @param string $method The method to validate agains
     *
     * @return bool The validation status
     */
    public function validate(string $method = null) : bool
    {
        // Reference number may contain a biller_code and pip_id, change rules based on this
        if (mb_strtolower($method) === 'post') {
            // Set endpoint based on whether a pip_id has been provided or not
            $this->rules['post'] = $this->hasBillerCode() || $this->hasPipId() ?
                ['biller_code' => 'required', 'pip_id' => 'required'] :
                [];
        }

        // Pass through
        return parent::validate($method);
    }
}
