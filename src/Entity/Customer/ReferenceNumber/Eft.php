<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\Customer\ReferenceNumber;

use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Interfaces\Entities\ReferenceNumberInterface;
use EoneoPaySdk\Sdk\Traits\CustomerAttachable;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getBsb() Alias of $this->get('bsb')
 * @method null|int getClearingAccount() Alias of $this->get('clearing_account')
 * @method null|string getPrefix() Alias of $this->get('prefix')
 * @method null|string getType() Alias of $this->get('type')
 * @method null|bool isDefault() Alias of $this->is('default')
 * @method bool hasBsb() Alias of $this->has('bsb')
 * @method bool hasClearingAccount() Alias of $this->has('clearing_account')
 * @method bool hasDefault() Alias of $this->has('default')
 * @method bool hasPrefix() Alias of $this->has('prefix')
 * @method bool hasType() Alias of $this->has('type')
 * @method $this setPrefix(mixed $value) Alias of $this->set('prefix', $value)
 */
class Eft extends Entity implements ReferenceNumberInterface
{
    use CustomerAttachable;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'bsb',
        'clearing_account',
        'default',
        'prefix',
        'type',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'post' => 'customers/:resource_id/eftId',
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'bsb',
        'prefix',
    ];

    /**
     * Get the endpoint for resource actions
     *
     * @param string $method The method being requested
     *
     * @return string The endpoint for the request
     */
    public function getEndpointFromMethod(string $method) : string
    {
        // Reference number can be created with or without a bsb or prefix
        if (mb_strtolower($method) === 'post') {
            // Set endpoint based on whether a bsb or prefix has been provided or not
            $this->endpoints['post'] = $this->hasBsb() || $this->hasPrefix() ?
                'customers/:resource_id/eftIdForBsb/:bsb/withPrefix/:prefix' :
                'customers/:resource_id/eftId';
        }

        // Pass through
        return parent::getEndpointFromMethod($method);
    }

    /**
     * Format bsb correctly
     *
     * @param string $bsb The bsb to format
     *
     * @return \EoneoPaySdk\Entity\Customer\ReferenceNumber\Eft This entity, chainable
     */
    public function setBsb(string $bsb) : self
    {
        // Extract just the numbers from the bsb
        return $this->set('bsb', preg_replace('/\D/', '', $bsb));
    }

    /**
     * Validate request against a ruleset
     *
     * @param string $method The method to validate agains
     *
     * @return bool The validation status
     */
    public function validate(string $method = null) : bool
    {
        // Reference number may contain a bsb and prefix, change rules based on this
        if (mb_strtolower($method) === 'post') {
            // Set endpoint based on whether a bsb or prefix has been provided or not
            $this->rules['post'] = $this->hasBsb() || $this->hasPrefix() ?
                ['bsb' => 'required', 'prefix' => 'required'] :
                [];
        }

        // Pass through
        return parent::validate($method);
    }
}
