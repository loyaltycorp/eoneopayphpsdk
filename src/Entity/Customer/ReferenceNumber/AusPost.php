<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\Customer\ReferenceNumber;

use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Interfaces\Entities\ReferenceNumberInterface;
use EoneoPaySdk\Sdk\Traits\CustomerAttachable;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|int getClearingAccount() Alias of $this->get('clearing_account')
 * @method null|string getPipId() Alias of $this->get('pip_id')
 * @method null|string getType() Alias of $this->get('type')
 * @method null|bool isDefault() Alias of $this->is('default')
 * @method bool hasClearingAccount() Alias of $this->has('clearing_account')
 * @method bool hasDefault() Alias of $this->has('default')
 * @method bool hasPipId() Alias of $this->has('pip_id')
 * @method bool hasType() Alias of $this->has('type')
 * @method $this setPipId(mixed $value) Alias of $this->set('pip_id', $value)
 */
class AusPost extends Entity implements ReferenceNumberInterface
{
    use CustomerAttachable;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'clearing_account',
        'default',
        'pip_id',
        'type',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'post' => '', // This is set at run time via getEndpointFromMethod()
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'pip_id',
    ];

    /**
     * Get the endpoint for resource actions
     *
     * @param string $method The method being requested
     *
     * @return string The endpoint for the request
     */
    public function getEndpointFromMethod(string $method) : string
    {
        // Reference number can be created with or without a pip_id
        if (mb_strtolower($method) === 'post') {
            // Set endpoint based on whether a pip_id has been provided or not
            $this->endpoints['post'] = $this->hasPipId() ?
                'customers/:resource_id/ausPostCrnForPIPId/:pip_id' :
                'customers/:resource_id/ausPostCrn';
        }

        // Pass through
        return parent::getEndpointFromMethod($method);
    }

    /**
     * Validate request against a ruleset
     *
     * @param string $method The method to validate agains
     *
     * @return bool The validation status
     */
    public function validate(string $method = null) : bool
    {
        // Reference number may contain a biller_code and pip_id, change rules based on this
        if (mb_strtolower($method) === 'post') {
            // Set rules based on whether a pip_id has been provided or not
            $this->rules['post'] = $this->hasPipId() ?
                ['pip_id' => 'required'] :
                [];
        }

        // Pass through
        return parent::validate($method);
    }
}
