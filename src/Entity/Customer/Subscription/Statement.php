<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\Customer\Subscription;

use EoneoPaySdk\Entity\Customer\Subscription;
use EoneoPaySdk\Resource\Charge;
use EoneoPaySdk\Resource\Payment;
use EoneoPaySdk\Sdk\Entity;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getDate() Alias of $this->get('date')
 * @method null|string getEndDate() Alias of $this->get('end_date')
 * @method null|int getOutstandingAmount Alias of $this->get('outstanding_amount')
 * @method null|string getStartDate() Alias of $this->get('start_date')
 * @method bool hasCharges() Alias of $this->has('charges')
 * @method bool hasDate() Alias of $this->has('date')
 * @method bool hasEndDate() Alias of $this->has('end_date')
 * @method bool hasOutstandingAmount Alias of $this->has('outstanding_amount')
 * @method bool hasPayments() Alias of $this->has('payments')
 * @method bool hasStartDate() Alias of $this->has('start_date')
 */
class Statement extends Entity
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'date',
        'end_date',
        'outstanding_amount',
        'start_date',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'customers/:resource_id/subscriptions/:parent_id/statement',
    ];

    /**
     * Repository classes which can be used as part of a collection
     *
     * @var array
     */
    protected $repositories = [
        'charges' => Charge::class,
        'payments' => Payment::class,
    ];

    /**
     * Set date, this method formats the date correctly
     *
     * @param string $date The date to set
     *
     * @return \EoneoPaySdk\Entity\Customer\Subscription\Statement This instance
     */
    public function forDate(string $date) : self
    {
        return $this->set('date', $this->formatDate($date, 'Y-m-d'));
    }

    /**
     * Set the subscription to get a statement from
     *
     * @param \EoneoPaySdk\Entity\Customer\Subscription $subscription The subscription this statement is for
     *
     * @return \EoneoPaySdk\Entity\Customer\Subscription\Statement This instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the ewallet is invalid
     */
    public function forSubscription(Subscription $subscription) : self
    {
        // Determine subscription customer and id
        $resourceId = (string)$subscription->getCustomerId();
        $parentId = (string)$subscription->getId();

        // Validate ids
        if (!preg_match('/^cus_[\da-zA-Z]+$/', $resourceId) ||
            !preg_match('/^sub_[\da-zA-Z]+$/', $parentId)) {
            throw new InvalidEntityException('Unable to attach entity: not a subscription');
        }

        // Set parent id and resource id
        $this->parentId = $parentId;
        $this->resourceId = $resourceId;

        // Make chainable
        return $this;
    }

    /**
     * Get charges
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection A collection of charges
     */
    public function getCharges() : Collection
    {
        return $this->enforceCollection('charges');
    }

    /**
     * Get payments
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection A collection of payments
     */
    public function getPayments() : Collection
    {
        return $this->enforceCollection('payments');
    }
}
