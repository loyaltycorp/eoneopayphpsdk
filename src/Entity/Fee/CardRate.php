<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\Fee;

use EoneoPaySdk\Sdk\Entity;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|float getAmex() Alias of $this->get('AMEX')
 * @method null|float getMasterCard() Alias of $this->get('MasterCard')
 * @method null|float getVisa() Alias of $this->get('Visa')
 * @method bool hasAmex() Alias of $this->has('AMEX')
 * @method bool hasMasterCard() Alias of $this->has('MasterCard')
 * @method bool hasVisa() Alias of $this->has('Visa')
 * @method $this setAmex(mixed $value) Alias of $this->set('AMEX', $value)
 * @method $this setMasterCard(mixed $value) Alias of $this->set('MasterCard', $value)
 * @method $this setVisa(mixed $value) Alias of $this->set('Visa', $value)
 */
class CardRate extends Entity
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'AMEX',
        'MasterCard',
        'Visa',
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = ['*'];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'put' => [
            'AMEX' => 'numeric|greaterThanOrEqualTo:0',
            'MasterCard' => 'numeric|greaterThanOrEqualTo:0',
            'Visa' => 'numeric|greaterThanOrEqualTo:0',
        ],
    ];
}
