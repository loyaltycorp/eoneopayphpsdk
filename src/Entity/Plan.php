<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Sdk\Entity;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|int getAmount() Alias of $this->get('amount')
 * @method null|string getCurrency() Alias of $this->get('currency')
 * @method null|string getInterval() Alias of $this->get('interval')
 * @method null|int getIntervalCount() Alias of $this->get('interval_count')
 * @method null|string getMerchantId() Alias of $this->get('merchant_id')
 * @method null|string getName() Alias of $this->get('name')
 * @method bool hasAmount() Alias of $this->has('amount')
 * @method bool hasCurrency() Alias of $this->has('currency')
 * @method bool hasInterval() Alias of $this->has('interval')
 * @method bool hasIntervalCount() Alias of $this->has('interval_count')
 * @method bool hasMerchantId() Alias of $this->has('merchant_id')
 * @method bool hasName() Alias of $this->has('name')
 * @method $this setAmount(mixed $value) Alias of $this->set('amount', $value)
 * @method $this setInterval(mixed $value) Alias of $this->set('interval', $value)
 * @method $this setName(mixed $value) Alias of $this->set('name', $value)
 */
class Plan extends Entity
{
    /**
     * Constants for setting the interval
     *
     * @var string
     */
    const INTERVAL_DAY = 'day';
    const INTERVAL_WEEK = 'week';
    const INTERVAL_MONTH = 'month';
    const INTERVAL_YEAR = 'year';

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'amount',
        'currency',
        'interval',
        'interval_count',
        'merchant_id',
        'name',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'delete' => 'plans/:id',
        'get' => 'plans/:id',
        'post' => 'plans',
        'put' => 'plans/:id',
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'amount',
        'interval',
        'name',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'delete,get,put' => [
            'id' => 'required|regex:/^plan_[\da-zA-Z]+$/',
        ],
        'post,put' => [
            'amount' => 'required|integer|greaterThanOrEqualTo:0',
            'interval' => 'required|enum:day,week,month,year',
            'name' => 'required',
        ],
    ];
}
