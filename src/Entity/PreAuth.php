<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\PaymentSource\CreditCard;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;

class PreAuth extends Payment
{
    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'post' => 'creditcard/pre-auth',
        'put' => 'creditcard/pre-auth/:txn_id',
    ];

    /**
     * Define the versions for each endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpointVersions = [
        'post' => 2,
        'put' => 2,
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'post' => [
            'payment' => 'preauth',
        ],
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'post,put' => [
            'amount' => 'required|integer|greaterThanOrEqualTo:0',
            'reference' => 'required',
            'token' => 'required|regex:/^(src|tok)_[\da-zA-Z]+$/',
        ],
        'put' => [
            'txn_id' => 'required|regex:/^txn_[\da-zA-Z]+$/',
        ],
    ];

    /**
     * Set the payment source this payment will be taken from
     *
     * @param \EoneoPaySdk\Entity\PaymentSource $paymentSource The payment source to pay from
     *
     * @return \EoneoPaySdk\Entity\Payment This instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the payment source is invalid
     */
    public function fromPaymentSource(PaymentSource $paymentSource) : Payment
    {
        // Only credit cards are allowed
        if (!$paymentSource instanceof CreditCard) {
            throw new InvalidEntityException('Unable to attach entity: not a valid credit card');
        }

        // Remap to fromCreditCard
        return $this->fromCreditCard($paymentSource);
    }

    /**
     * Set the credit card to be pre-authorised
     *
     * @param \EoneoPaySdk\Entity\PaymentSource\CreditCard $creditCard The credit card to use for the pre-auth
     *
     * @return \EoneoPaySdk\Entity\PreAuth This instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the credit card is invalid
     */
    public function fromCreditCard(CreditCard $creditCard) : self
    {
        // Validate credit card info
        if (!preg_match('/^src_[\da-zA-Z]+$/', (string)$creditCard->getId())) {
            throw new InvalidEntityException('Unable to attach entity: not a valid credit card');
        }

        // Set token and make chainable
        return $this->set('token', $creditCard->getId());
    }

    /**
     * Set endpoints based on payment source
     *
     * @return void
     */
    protected function setEndpoints() : void
    {
        // Don't change endpoints
    }
}
