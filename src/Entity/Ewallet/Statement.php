<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\Ewallet;

use EoneoPaySdk\Entity\Ewallet;
use EoneoPaySdk\Sdk\Entity;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|int getBalance() Alias of $this->get('balance')
 * @method null|string getDate() Alias of $this->get('date')
 * @method null|string getEndDate() Alias of $this->get('endDate')
 * @method null|string getStartDate() Alias of $this->get('startDate')
 * @method bool hasBalance() Alias of $this->has('balance')
 * @method bool hasDate() Alias of $this->has('date')
 * @method bool hasDeposits() Alias of $this->has('deposits')
 * @method bool hasEndDate() Alias of $this->has('endDate')
 * @method bool hasWithdrawals() Alias of $this->has('withdrawals')
 * @method bool hasStartDate() Alias of $this->has('startDate')
 */
class Statement extends Entity
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'balance',
        'date',
        'deposits',
        'endDate',
        'startDate',
        'withdrawals',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => ':resource/:resource_id/ewallets/:parent_id/statement',
    ];

    /**
     * Set the date to get a statement for
     *
     * @param string $date The date to set
     *
     * @return \EoneoPaySdk\Entity\Ewallet\Statement This instance
     */
    public function forMonth(string $date) : self
    {
        return $this->set('date', $this->formatDate($date, 'Y-m-d'));
    }

    /**
     * Set the ewallet to get a statement from
     *
     * @param \EoneoPaySdk\Entity\Ewallet $ewallet The ewallet this statement is for
     *
     * @return \EoneoPaySdk\Entity\Ewallet\Statement This instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the ewallet is invalid
     */
    public function forEwallet(Ewallet $ewallet) : self
    {
        // Determine eWallet owner and id
        $resourceId = (string)($ewallet->getCustomerId() ?? $ewallet->getMerchantId());
        $parentId = (string)$ewallet->getId();

        // Validate ids
        if (!preg_match('/^cus_[\da-zA-Z]+|mer_[\da-zA-Z]+$/', $resourceId) ||
            !preg_match('/^[\da-zA-Z]+$/', $parentId)) {
            throw new InvalidEntityException('Unable to attach entity: not an ewallet');
        }

        // Set parent id, resource id and resource
        $this->parentId = $parentId;
        $this->resourceId = $resourceId;
        $this->getResourceEndpointFromEntityId($this->resourceId);

        // Make chainable
        return $this;
    }

    /**
     * Get deposits
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection A collection of deposits
     */
    public function getDeposits() : Collection
    {
        return $this->enforceCollection('deposits');
    }

    /**
     * Get withdrawals
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection A collection of withdrawals
     */
    public function getWithdrawals() : Collection
    {
        return $this->enforceCollection('withdrawals');
    }
}
