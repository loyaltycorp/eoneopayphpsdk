<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\PaymentSource\CreditCard;
use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Interfaces\Entities\AllocationInterface;
use EoneoPaySdk\Sdk\Interfaces\Entities\Constants\PaymentConstants;
use EoneoPaySdk\Sdk\Traits\CustomerAttachable;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|int getAmount() Alias of $this->get('amount')
 * @method null|string getBankTxnId() Alias of $this->get('bank_txn_id')
 * @method null|string getCardType() Alias of $this->get('card_type')
 * @method null|int getCardTypeId() Alias of $this->get('card_type_id')
 * @method null|int getClearingAccount() Alias of $this->get('clearing_account')
 * @method null|string getCurrency() Alias of $this->get('currency')
 * @method null|string getCustomerId() Alias of $this->get('customer_id')
 * @method null|string getExternalCustomerId() Alias of $this->get('external_customer_id')
 * @method null|string getExternalTxnId() Alias of $this->get('external_txn_id')
 * @method null|int getFeeAmount() Alias of $this->get('fee_amount')
 * @method null|string getMerchantId() Alias of $this->get('merchant_id')
 * @method null|int getMerchantTransferStatus() Alias of $this->get('merchant_transfer_status')
 * @method null|mixed getMeta() Alias of $this->get('meta')
 * @method null|string getOriginalTxnId() Alias of $this->get('original_txn_id')
 * @method null|string getReference() Alias of $this->get('reference')
 * @method null|string getReferenceNumber() Alias of $this->get('reference_number')
 * @method null|string getStatementDescriptor() Alias of $this->get('statement_descriptor')
 * @method null|int getStatus() Alias of $this->get('status')
 * @method null|string getTestValidateAmount() Alias of $this->get('test_validate_amount')
 * @method null|string getToken() Alias of $this->get('token')
 * @method null|string getType() Alias of $this->get('type')
 * @method null|string getTxnDate() Alias of $this->get('txn_date')
 * @method null|string getTxnId() Alias of $this->get('txn_id')
 * @method bool hasAllocationPlan() Alias of $this->has('allocation_plan')
 * @method bool hasAmount() Alias of $this->has('amount')
 * @method bool hasBankTxnId() Alias of $this->has('bank_txn_id')
 * @method bool hasCardType() Alias of $this->has('card_type')
 * @method bool hasCardTypeId() Alias of $this->has('card_type_id')
 * @method bool hasClearingAccount() Alias of $this->has('clearing_account')
 * @method bool hasCurrency() Alias of $this->has('currency')
 * @method bool hasCustomerId() Alias of $this->has('customer_id')
 * @method bool hasExternalCustomerId() Alias of $this->has('external_customer_id')
 * @method bool hasExternalTxnId() Alias of $this->has('external_txn_id')
 * @method bool hasFeeAmount() Alias of $this->has('fee_amount')
 * @method bool hasFeeSchedule() Alias of $this->has('fee_schedule')
 * @method bool hasMerchantId() Alias of $this->has('merchant_id')
 * @method bool hasMerchantTransferStatus() Alias of $this->has('merchant_transfer_status')
 * @method bool hasMeta() Alias of $this->has('meta')
 * @method bool hasOriginalTxnId() Alias of $this->has('original_txn_id')
 * @method bool hasReference() Alias of $this->has('reference')
 * @method bool hasReferenceNumber() Alias of $this->has('reference_number')
 * @method bool hasStatementDescriptor() Alias of $this->has('statement_descriptor')
 * @method bool hasStatus() Alias of $this->has('status')
 * @method bool hasTestValidateAmount() Alias of $this->has('test_validate_amount')
 * @method bool hasToken() Alias of $this->has('token')
 * @method bool hasType() Alias of $this->has('type')
 * @method bool hasTxnDate() Alias of $this->has('txn_date')
 * @method bool hasTxnId() Alias of $this->has('txn_id')
 * @method $this setAmount(mixed $value) Alias of $this->has('amount', $value)
 * @method $this setClearingAccount(mixed $value) Alias of $this->set('clearing_account', $value)
 * @method $this setMeta(mixed $value) Alias of $this->has('meta', $value)
 * @method $this setReference(mixed $value) Alias of $this->has('reference', $value)
 * @method $this setTxnId(mixed $value) Alias of $this->has('txn_id', $value)
 */
class Payment extends Entity implements PaymentConstants
{
    use CustomerAttachable;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'allocation_plan',
        'amount',
        'bank_txn_id',
        'card_type',
        'card_type_id',
        'clearing_account',
        'currency',
        'customer_id',
        'external_customer_id',
        'external_txn_id',
        'fee_amount',
        'fee_schedule',
        'merchant_id',
        'merchant_transfer_status',
        'meta',
        'original_txn_id',
        'reference',
        'reference_number',
        'statement_descriptor',
        'status',
        'test_validate_amount',
        'token',
        'type',
        'txn_date',
        'txn_id',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'delete' => '', // This is set at run time via getEndpointFromMethod()
        'get' => 'payments/:txn_id',
        'post' => '', // This is set at run time via getEndpointFromMethod()
        'put' => 'payments/:txn_id',
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'refund' => 'payment',
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'amount',
        'clearing_account',
        'meta',
        'reference',
        'reference_number',
    ];

    /**
     * The primary key for this repository
     *
     * @var string
     */
    protected $primaryKey = 'txn_id';

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'delete,get,put' => [
            'txn_id' => 'required|regex:/^txn_[\da-zA-Z]+$/',
        ],
        'post' => [
            'amount' => 'required|integer|greaterThanOrEqualTo:0',
            'reference' => 'required',
            'token' => 'required|regex:/^(src|tok)_[\da-zA-Z]+$/',
        ],
        'put' => [
            'allocation_plan' => 'required',
        ],
    ];

    /**
     * Create a new payment instance
     *
     * @param array $data The data to populate the repository with
     */
    public function __construct(array $data = null)
    {
        // Pass through to entity
        parent::__construct($data);

        // Set endpoints
        $this->setEndpoints();
    }

    /**
     * Add an item to the allocation plan
     *
     * @param string|\EoneoPaySdk\Sdk\Interfaces\Entities\AllocationInterface $reference The allocation reference
     * @param int $amount The amount to allocate
     *
     * @return \EoneoPaySdk\Entity\Payment This instance
     */
    public function addAllocation($reference, int $amount) : self
    {
        // Add allocation to plan
        $method = sprintf(
            'set%s',
            $reference instanceof AllocationInterface ? $reference->getId() : $reference
        );
        $this->getAllocationPlan()->$method([
            'amount' => $amount,
            'type' => $reference instanceof AllocationInterface ? $reference->getType() : 'sundry',
        ]);

        // Make chainable with this entity
        return $this;
    }

    /**
     * Set the payment source this payment will be taken from
     *
     * @param \EoneoPaySdk\Entity\PaymentSource $paymentSource The payment source to pay from
     *
     * @return \EoneoPaySdk\Entity\Payment This instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the payment source is invalid
     */
    public function fromPaymentSource(PaymentSource $paymentSource) : self
    {
        // Validate payment source
        if (!preg_match('/^src_[\da-zA-Z]+$/', (string)$paymentSource->getId())) {
            throw new InvalidEntityException('Unable to attach entity: not a valid payment source');
        }

        // Set type for credit card and endpoints
        $this->set('type', $paymentSource instanceof CreditCard ? 'cc_payment' : '');
        $this->setEndpoints();

        // Set token and make chainable
        return $this->set('token', $paymentSource->getId());
    }

    /**
     * Get allocation plan
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository A repository of allocations
     */
    public function getAllocationPlan() : Repository
    {
        return $this->enforceRepository('allocation_plan', Repository::class);
    }

    /**
     * Get fee schedule
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository A repository of fees
     */
    public function getFeeSchedule() : Repository
    {
        return $this->enforceRepository('fee_schedule', Repository::class);
    }

    /**
     * Set endpoints based on payment source
     *
     * @return void
     */
    protected function setEndpoints() : void
    {
        switch ($this->getType()) {
            case 'cc_payment':
                $this->endpoints['delete'] = 'creditcard/charge/:txn_id';
                $this->endpoints['post'] = 'creditcard/charge';
                $this->endpointVersions['delete'] = 2;
                $this->endpointVersions['post'] = 2;
                break;

            default:
                $this->endpoints['delete'] = 'payments/:txn_id';
                $this->endpoints['post'] = 'payments';
                $this->endpointVersions['delete'] = 1;
                $this->endpointVersions['post'] = 1;
                break;
        }
    }

    /**
     * Set a string value for test validate payment to correctly interact with the black box
     *
     * @param mixed $value Whether the transactions should be validated or not
     *
     * @return \EoneoPaySdk\Entity\Payment This instance
     */
    public function setTestValidateAmount($value) : self
    {
        // If value is falsey or string false set to false
        if ((bool)$value === false || mb_strtolower((string)$value) === 'false') {
            return $this->set('test_validate_amount', 'false');
        }

        // Remove current value so validation happens
        return $this->set('test_validate_amount', null);
    }
}
