<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Sdk\Entity;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getApiKey() Alias of $this->get('api_key')
 * @method null|int getApiStatus() Alias of $this->get('api_status')
 * @method null|string getMerchantId() Alias of $this->get('merchant_id')
 * @method bool hasApiKey() Alias of $this->has('api_key')
 * @method bool hasApiStatus() Alias of $this->has('api_status')
 * @method bool hasApiPermissions() Alias of $this->has('api_permissions')
 * @method bool hasMerchantId() Alias of $this->has('merchant_id')
 * @method $this setApiKey(mixed $value) Alias of $this->set('api_key', $value)
 */
class ApiKey extends Entity
{
    /**
     * Constants for permissions
     */
    const PERMISSION_ADMIN = '*';
    const PERMISSION_CHARGE = 'charges';
    const PERMISSION_CUSTOMER = 'customers';
    const PERMISSION_CUSTOMER_OVERDUE_SUBSCRIPTION = 'overdueSubscriptions';
    const PERMISSION_CUSTOMER_SUBSCRIPTION = 'subscriptions';
    const PERMISSION_MERCHANT = 'merchants';
    const PERMISSION_MERCHANT_BALANCE = 'balance';
    const PERMISSION_MERCHANT_TRANSFER = 'transfers';
    const PERMISSION_PAYMENT = 'payments';
    const PERMISSION_PLAN = 'plans';
    const PERMISSION_SEARCH = 'search';
    const PERMISSION_TOKENISE = 'tokens';
    const PERMISSION_WEBHOOK = 'webhooks';

    /**
     * Constants for status
     */
    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'api_key',
        'api_status',
        'merchant_id',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'delete' => 'apikeys/:api_key',
        'get' => 'apikeys/:api_key',
        'put' => 'apikeys/:api_key',
    ];

    /**
     * The primary key for this repository
     *
     * @var string
     */
    protected $primaryKey = 'api_key';

    /**
     * Repository classes which can be used as part of a collection
     *
     * @var array
     */
    protected $repositories = [
        'api_permissions' => Repository::class,
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'delete,get,put' => [
            'api_key' => 'required|regex:/^(sk|pk)_test_[\da-zA-Z]+$/',
        ],
    ];

    /**
     * Add a permission to the api key
     *
     * @param string $permission The permission to add
     *
     * @return \EoneoPaySdk\Entity\ApiKey This instance, chainable
     */
    public function addApiPermission(string $permission) : self
    {
        // Add to collection
        $this->getApiPermissions()->add($permission);

        // Make chainable
        return $this;
    }

    /**
     * Get api permissions
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection A collection of permissions
     */
    public function getApiPermissions() : Collection
    {
        return $this->enforceCollection('api_permissions');
    }

    /**
     * Attach this entity to a merchant
     *
     * @param \EoneoPaySdk\Entity\Merchant $merchant The merchant to attach this entity to
     *
     * @return \EoneoPaySdk\Entity\ApiKey This instance, chainable
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the merchant is invalid
     */
    public function forMerchant(Merchant $merchant) : self
    {
        // Validate id
        if (!preg_match('/^mer_[\da-zA-Z]+$/', (string)$merchant->getId())) {
            throw new InvalidEntityException('Unable to attach entity: not a valid merchant');
        }

        // Add permissions
        $this->addApiPermission('merchant/' . $merchant->getId());
        $this->addApiPermission('merchant/self');

        // Make chainable
        return $this;
    }

    /**
     * Set whether the api key is enabled or not
     *
     * @param mixed $status Whether this api key is enabled or not
     *
     * @return \EoneoPaySdk\Entity\ApiKey This resource, chainable
     */
    public function setApiStatus($status) : self
    {
        return $this->set('api_status', (bool)$status);
    }
}
