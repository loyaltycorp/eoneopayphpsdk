<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Interfaces\Entities\AllocationInterface;
use EoneoPaySdk\Sdk\Traits\Entities\Allocatable;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getAmount() Alias of $this->get('amount')
 * @method null|string getMerchantId() Alias of $this->get('merchant_id')
 * @method null|string getName() Alias of $this->get('name')
 * @method null|string getOutstandingAmount() Alias of $this->get('outstanding_amount')
 * @method null|string getPrecedence() Alias of $this->get('precedence')
 * @method bool hasAmount() Alias of $this->has('amount')
 * @method bool hasMerchantId() Alias of $this->has('merchant_id')
 * @method bool hasName() Alias of $this->has('name')
 * @method bool hasOutstandingAmount() Alias of $this->has('outstanding_amount')
 * @method bool hasPrecedence() Alias of $this->has('precedence')
 * @method bool hasStatement() Alias of $this->has('statement')
 * @method $this setAmount(mixed $value) Alias of $this->set('amount' $value)
 * @method $this setName(mixed $value) Alias of $this->set('name', $value)
 * @method $this setPrecendence(mixed $value) Alias of $this->set('precedence', $value)
 */
class Charge extends Entity implements AllocationInterface
{
    use Allocatable;

    /**
     * Constants for setting the charge precedence
     *
     * @var string
     */
    const PRECEDENCE_FIRST = 'first';
    const PRECEDENCE_SHARED = 'shared';
    const PRECEDENCE_LAST = 'last';

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'amount',
        'merchant_id',
        'name',
        'outstanding_amount',
        'precendence',
        'statement',
        'subscription',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'delete' => 'charges/:id',
        'get' => 'charges/:id',
        'post' => 'charges',
        'put' => 'charges/:id',
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'amount',
        'name',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'delete,get,put' => [
            'id' => 'required|regex:/^chg_[\da-zA-Z]+$/',
        ],
        'post,put' => [
            'amount' => 'required',
            'name' => 'required',
            'precendence' => 'enum:first,shared,last',
        ],
    ];

    /**
     * Get statement
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository A repository for the statement
     */
    public function getStatement() : Repository
    {
        return $this->enforceRepository('statement', Repository::class);
    }
}
