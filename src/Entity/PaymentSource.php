<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\PaymentSource\BankAccount;
use EoneoPaySdk\Entity\PaymentSource\CreditCard;
use EoneoPaySdk\Entity\PaymentSource\Info\Info;
use EoneoPaySdk\Entity\PaymentSource\Info\BankAccount as BankAccountInfo;
use EoneoPaySdk\Entity\PaymentSource\Info\CreditCard as CreditCardInfo;
use EoneoPaySdk\Sdk\Entity;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getHolderId() Alias of $this->get('holder_id')
 * @method null|string getInternalToken() Alias of $this->get('internal_token')
 * @method null|string getToken() Alias of $this->get('token')
 * @method null|string getType() Alias of $this->get('type')
 * @method bool hasHolderId() Alias of $this->has('holder_id')
 * @method bool hasInfo() Alias of $this->has('info')
 * @method bool hasInternalToken() Alias of $this->has('internal_token')
 * @method bool hasToken() Alias of $this->has('token')
 * @method bool hasType() Alias of $this->has('type')
 */
class PaymentSource extends Entity
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'holder_id',
        'info',
        'internal_token',
        'token',
        'type'
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'merchants/self/paymentSources/:id'
    ];

    /**
     * Define the intial resource for this entity, used where the same data is used
     * for multiple endpoints, e.g. the customer/merchant crossover
     *
     * @var string
     */
    protected $resource;

    /**
     * Validation rules
     *
     * Delete can't be called directly on a PaymentSource but can on an extended class
     *
     * @var array
     */
    public $rules = [
        'delete' => [
            'holder_id' => 'required|regex:/^cus_[\da-zA-Z]+|mer_[\da-zA-Z]+$/'
        ],
        'delete,get' => [
            'id' => 'required|regex:/^src_[\da-zA-Z]+$/'
        ],
    ];

    /**
     * Create a new payment source
     *
     * @param array $data The data to populate the repository with
     */
    public function __construct(array $data = null)
    {
        // Grab the resource from data
        $this->setEndpointVariablesFromData($data, ['holder_id']);

        // If info doesn't exist but it looks like it's meant to be
        // an Info instance - remap
        if (is_array($data) && !array_key_exists('info', $data) && array_key_exists('number', $data)) {
            $data = ['info' => $data];
        }

        // Create repository
        parent::__construct($data);
    }

    /**
     * Get info
     *
     * @return \EoneoPaySdk\Entity\PaymentSource\Info\Info A repository of payment source data
     */
    public function getInfo() : Info
    {
        // Determine class from type
        switch ($this->getType()) {
            case 'bank_account':
                $class = BankAccountInfo::class;
                break;

            case 'credit_card':
                $class = CreditCardInfo::class;
                break;

            default:
                $class = Info::class;
                break;
        }

        return $this->enforceRepository('info', $class);
    }


    /**
     * Init a payment method object
     *
     * @param array $data Array of data to populate the payment source with
     *
     * @return \EoneoPaySdk\Entity\PaymentSource The created payment source
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the payment source is invalid
     */
    public function init(array $data = null) : PaymentSource
    {
        // If this hasn't been instantiated directly, or data isn't an array with type, return original payment source
        if (!is_array($data) || !array_key_exists('type', $data) || get_class($this) !== __CLASS__) {
            return new $this($data);
        }

        // Create resource based on type
        switch ($data['type']) {
            case 'bank_account':
                // Return bank account
                return new BankAccount($data);

            case 'credit_card':
                // Return credit card
                return new CreditCard($data);

            default:
                // Payment source isn't valid
                throw new InvalidEntityException('Unable to create entity: invalid payment source');
        }
    }
}
