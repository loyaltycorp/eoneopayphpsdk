<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\Merchant;

use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Traits\MerchantAttachable;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

/**
 * This class uses magic methods to access attributes
 *
 * @method bool hasAvailable() Alias of $this->has('available')
 * @method bool hasTotal() Alias of $this->has('total')
 */
class Balance extends Entity
{
    use MerchantAttachable;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'available',
        'total',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'balance',
        'put' => 'merchants/:resource_id/availableBalance',
    ];

    /**
     * Get available balances
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository A repository of available balances
     */
    public function getAvailable() : Repository
    {
        return $this->enforceRepository('available', Repository::class);
    }

    /**
     * Get total balances
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository A repository of total balances
     */
    public function getTotal() : Repository
    {
        return $this->enforceRepository('total', Repository::class);
    }
}
