<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\Merchant;

use EoneoPaySdk\Entity\Fee as BaseFee;
use EoneoPaySdk\Sdk\Traits\MerchantAttachable;

class Fee extends BaseFee
{
    use MerchantAttachable;

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'delete' => 'merchantFees/:payment_type',
        'get' => 'merchantFees/:payment_type',
        'put' => 'merchantFees/:payment_type',
    ];
}
