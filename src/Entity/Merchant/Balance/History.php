<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\Merchant\Balance;

use EoneoPaySdk\Entity\Payment;
use EoneoPaySdk\Sdk\Entity;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;

/**
 * This class uses magic methods to access attributes
 *
 * @method bool hasBalance() Alias of $this->has('balance')
 * @method bool hasPayments() Alias of $this->has('payments')
 * @method bool hasTransfers() Alias of $this->has('transfers')
 */
class History extends Entity
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'balance',
        'transfers',
    ];

    /**
     * Repository classes which can be used as part of a collection
     *
     * @var array
     */
    protected $repositories = [
        'payments' => Payment::class,
    ];

    /**
     * Get balance
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Repository A repository of balances
     */
    public function getBalance() : Repository
    {
        return $this->enforceRepository('balance', Repository::class);
    }

    /**
     * Get payments
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection A collection of payments
     */
    public function getPayments() : Collection
    {
        return $this->enforceCollection('payments');
    }

    /**
     * Get transfers
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection A collection of transfers
     */
    public function getTransfers() : Collection
    {
        return $this->enforceCollection('transfers');
    }
}
