<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity\Merchant;

use EoneoPaySdk\Entity\Payment;
use EoneoPaySdk\Entity\PaymentSource\BankAccount;
use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Interfaces\Entities\Constants\TransferConstants;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|int getAmount() Alias of $this->get('amount')
 * @method null|string getCurrency() Alias of $this->get('currency')
 * @method null|string getDescription() Alias of $this->get('description')
 * @method null|string getDestination() Alias of $this->get('destination')
 * @method null|string getMerchantId() Alias of $this->get('merchant_id')
 * @method null|int getStatus() Alias of $this->get('status')
 * @method null|string getStatusDate() Alias of $this->get('status_date')
 * @method null|string getStatusReason() Alias of $this->get('status_reason')
 * @method bool hasAmount() Alias of $this->has('amount')
 * @method bool hasCurrency() Alias of $this->has('currency')
 * @method bool hasDescription() Alias of $this->has('description')
 * @method bool hasDestination() Alias of $this->has('destination')
 * @method bool hasMerchantId() Alias of $this->has('merchant_id')
 * @method bool hasPayments() Alias of $this->has('payments')
 * @method bool hasStatus() Alias of $this->has('status')
 * @method bool hasStatusDate() Alias of $this->has('status_date')
 * @method bool hasStatusReason() Alias of $this->has('status_reason')
 * @method bool hasTransactionList() Alias of $this->has('transaction_list')
 * @method $this setAmount(mixed $value) Alias of $this->set('amount', $value)
 * @method $this setDestination(mixed $value) Alias of $this->set('destination', $value)
 */
class Transfer extends Entity implements TransferConstants
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'amount',
        'currency',
        'description',
        'destination',
        'merchant_id',
        'payments',
        'status',
        'status_date',
        'status_reason',
        'transaction_list',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'post' => 'transfers',
        'get' => 'transfers/:id',
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'amount',
        'description',
    ];

    /**
     * Repository classes which can be used as part of a collection
     *
     * @var array
     */
    protected $repositories = [
        'payments' => Payment::class,
        'transaction_list' => Payment::class,
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'get' => [
            'id' => 'required|regex:/^tfr_[\da-zA-Z]+$/',
        ],
        'post' => [
            'amount' => 'required',
            'destination' => 'required|regex:/^src_[\da-zA-Z]+$/',
            'transaction_list' => 'required|notEmpty',
        ],
    ];

    /**
     * Create new transfer
     *
     * @param array $data The data to pre-load the repository with
     */
    public function __construct(array $data = null)
    {
        // Set currency to aud
        $this->set('currency', 'AUD');

        parent::__construct($data);
    }

    /**
     * Add a transaction to the transaction list
     *
     * @param \EoneoPaySdk\Entity\Payment $payment The payment to add to the transaction list
     *
     * @return \EoneoPaySdk\Entity\Merchant\Transfer This instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the payment isn't valid
     */
    public function addPayment(Payment $payment) : self
    {
        // Validate payment info
        if (!preg_match('/^txn_[\da-zA-Z]+$/', (string)$payment->getId())) {
            throw new InvalidEntityException('Unable to attach entity: not a valid payment');
        }

        // Add payment to transaction list
        $this->getTransactionList()->add($payment->getId());

        // Make chainable
        return $this;
    }

    /**
     * Get payments
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection A collection of payments
     */
    public function getPayments() : Collection
    {
        return $this->enforceCollection('payments');
    }

    /**
     * Get transaction list
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection A collection of transactions
     */
    public function getTransactionList() : Collection
    {
        return $this->enforceCollection('transaction_list');
    }

    /**
     * Add a destination bank account
     *
     * @param \EoneoPaySdk\Entity\PaymentSource\BankAccount The account to transfer funds to
     *
     * @return \EoneoPaySdk\Entity\Merchant\Transfer This instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the bank account isn't valid
     */
    public function toBankAccount(BankAccount $account) : self
    {
        // Validate payment info
        if (!preg_match('/^src_[\da-zA-Z]+$/', (string)$account->getId())) {
            throw new InvalidEntityException('Unable to attach entity: not a valid payment');
        }

        return $this->set('destination', $account->getId());
    }
}
