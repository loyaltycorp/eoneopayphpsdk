<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use DateTime;
use EoneoPaySdk\Sdk\Entity;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getCardDescription() Alias of $this->get('cardDescription')
 * @method null|int getCardType() Alias of $this->get('cardType')
 * @method null|string getCvc() Alias of $this->get('cvc')
 * @method null|string getExpiryDate() Alias of $this->get('expiryDate')
 * @method null|string getExpiryMonth() Alias of $this->get('expiry_month')
 * @method null|string getExpiryYear() Alias of $this->get('expiry_year')
 * @method null|string getName() Alias of $this->get('name')
 * @method null|string getNumber() Alias of $this->get('number')
 * @method null|string getPan() Alias of $this->get('pan')
 * @method null|string getToken() Alias of $this->get('token')
 * @method bool hasCardDescription() Alias of $this->has('cardDescription')
 * @method bool hasCardType() Alias of $this->has('cardType')
 * @method bool hasCvc() Alias of $this->has('cvc')
 * @method bool hasExpiryDate() Alias of $this->has('expiryDate')
 * @method bool hasExpiryMonth() Alias of $this->has('expiry_month')
 * @method bool hasExpiryYear() Alias of $this->has('expiry_year')
 * @method bool hasName() Alias of $this->has('name')
 * @method bool hasNumber() Alias of $this->has('number')
 * @method bool hasPan() Alias of $this->has('pan')
 * @method bool hasToken() Alias of $this->has('token')
 * @method $this setCvc(mixed $value) Alias of $this->set('cvc', $value)
 * @method $this setName(mixed $value) Alias of $this->set('name', $value)
 */
class TokenisedCard extends Entity
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'cardDescription',
        'cardType',
        'cvc',
        'expiryDate',
        'expiry_month',
        'expiry_year',
        'name',
        'number',
        'pan',
        'token',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'post' => 'creditcard/tokenise'
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'credit_card' => 'tokenised_card'
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'cvc',
        'expiry_month',
        'expiry_year',
        'name',
        'number'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'post' => [
            'cvc' => 'required|regex:/^[\d]{3,4}$/',
            'expiry_month' => 'required|regex:/^[\d]{2}+$/',
            'expiry_year' => 'required|regex:/^[\d]{4}+$/',
            'number' => 'required|regex:/^[\d]{13,19}$/',
        ],
    ];

    /**
     * Format expiry month for validation
     *
     * @param string $month The month to format
     *
     * @return \EoneoPaySdk\Entity\TokenisedCard This entity, chainable
     */
    public function setExpiryMonth(string $month) : self
    {
        // Convert month to 2 digits, remove all non-numeric characters
        $expiryMonth = (int)preg_replace('/\D/', '', $month);
        $expiry = (new DateTime)->setDate(2000, $expiryMonth, 1);

        return $this->set('expiry_month', $expiry->format('m'));
    }

    /**
     * Format expiry year for validation
     *
     * @param string $year The year to format
     *
     * @return \EoneoPaySdk\Entity\TokenisedCard This entity, chainable
     */
    public function setExpiryYear(string $year) : self
    {
        // Convert year to 4 digits, remove all non-numeric characters
        $expiryYear = preg_replace('/\D/', '', $year);
        $expiryYear = (int)$expiryYear < 2000 ? 2000 + (int)$expiryYear : (int)$expiryYear;
        $expiry = (new DateTime)->setDate($expiryYear, 1, 1);

        return $this->set('expiry_year', $expiry->format('Y'));
    }

    /**
     * Format number for validation
     *
     * @param string $number The number to format
     *
     * @return \EoneoPaySdk\Entity\TokenisedCard This entity, chainable
     */
    public function setNumber(string $number) : self
    {
        // Reformat number as 16 digits without any non-number characters
        return $this->set('number', preg_replace('/\D/', '', $number));
    }
}
