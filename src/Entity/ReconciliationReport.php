<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Sdk\Entity;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getBankTransactionId() Alias of $this->get('bank_transaction_id')
 * @method mixed getExternalRecord() Alias of $this->get('externalRecord')
 * @method null|string getFile() Alias of $this->get('file')
 * @method mixed getInternalRecord() Alias of $this->get('internalRecord')
 * @method null|string getMessage() Alias of $this->get('message')
 * @method null|int getStatus() Alias of $this->get('status')
 * @method null|int getVersion() Alias of $this->get('version')
 * @method bool hasBankTransactionId() Alias of $this->has('bank_transaction_id')
 * @method bool hasExternalRecord() Alias of $this->has('externalRecord')
 * @method bool hasFile() Alias of $this->has('file')
 * @method bool hasInternalRecord() Alias of $this->has('internalRecord')
 * @method bool hasMessage() Alias of $this->has('message')
 * @method bool hasStatus() Alias of $this->has('status')
 * @method bool hasVersion() Alias of $this->has('version')
 */
class ReconciliationReport extends Entity
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'bank_transaction_id',
        'externalRecord',
        'file',
        'internalRecord',
        'message',
        'status',
        'version',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'reconciliationReports/:id',
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'reconciliationreport' => 'reconciliation_report'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'get' => [
            'id' => 'required',
        ],
    ];
}
