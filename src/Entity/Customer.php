<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Entity\Customer\ReferenceNumber;
use EoneoPaySdk\Entity\PaymentSource\BankAccount;
use EoneoPaySdk\Entity\PaymentSource\CreditCard;
use EoneoPaySdk\Sdk\Entity;
use EoneoPaySdk\Sdk\Interfaces\Entities\UserInterface;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getEmail() Alias of $this->get('email')
 * @method null|string getExternalCustomerId() Alias of $this->get('external_customer_id')
 * @method null|string getEwalletId() Alias of $this->get('ewallet_id')
 * @method null|string getFirstName() Alias of $this->get('first_name')
 * @method null|string getLastName() Alias of $this->get('last_name')
 * @method null|string getMerchantId() Alias of $this->get('merchant_id')
 * @method bool hasBankAccounts() Alias of $this->has('bank_accounts')
 * @method bool hasCreditCards() Alias of $this->has('credit_cards')
 * @method bool hasEmail() Alias of $this->has('email')
 * @method bool hasExternalCustomerId() Alias of $this->has('external_customer_id')
 * @method bool hasEwalletId() Alias of $this->has('ewallet_id')
 * @method bool hasFirstName() Alias of $this->has('first_name')
 * @method bool hasLastName() Alias of $this->has('last_name')
 * @method bool hasMerchantId() Alias of $this->has('merchant_id')
 * @method bool hasReferenceNumbers() Alias of $this->has('reference_numbers')
 * @method $this setEmail(mixed $value) Alias of $this->set('email', $value)
 * @method $this setExternalCustomerId(mixed $value) Alias of $this->set('external_customer_id', $value)
 * @method $this setFirstName(mixed $value) Alias of $this->set('first_name', $value)
 * @method $this setLastName(mixed $value) Alias of $this->set('last_name', $value)
 */
class Customer extends Entity implements UserInterface
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'email',
        'external_customer_id',
        'ewallet_id',
        'first_name',
        'last_name',
        'merchant_id',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'delete' => 'customers/:id',
        'get' => 'customers/:id',
        'post' => 'customers',
        'put' => 'customers/:id',
    ];

    /**
     * The attributes which can be directly filled or set
     *
     * @var array
     */
    protected $mutable = [
        'email',
        'external_customer_id',
        'first_name',
        'last_name',
    ];

    /**
     * Repository classes which can be used as part of a collection
     *
     * @var array
     */
    protected $repositories = [
        'bank_accounts' => BankAccount::class,
        'credit_cards' => CreditCard::class,
        'reference_numbers' => ReferenceNumber::class,
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'delete,get,put' => [
            'id' => 'required', // No regex match since external id can be used
        ],
        'post,put' => [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'email',
        ],
    ];

    /**
     * Get bank accounts
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection A collection of bank accounts
     */
    public function getBankAccounts() : Collection
    {
        return $this->enforceCollection('bank_accounts');
    }

    /**
     * Get credit cards
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection A collection of credit cards
     */
    public function getCreditCards() : Collection
    {
        return $this->enforceCollection('credit_cards');
    }

    /**
     * Get reference numbers
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection A collection of credit cards
     */
    public function getReferenceNumbers() : Collection
    {
        return $this->enforceCollection('reference_numbers');
    }
}
