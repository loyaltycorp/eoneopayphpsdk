<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Sdk\Entity;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getAccount() Alias of $this->get('account')
 * @method null|string getBsb() Alias of $this->get('bsb')
 * @method null|string getDirectEntryCreditDescription() Alias of $this->get('direct_entry_credit_description')
 * @method null|string getDirectEntryCreditId() Alias of $this->get('direct_entry_credit_id')
 * @method null|string getDirectEntryCreditName() Alias of $this->get('direct_entry_credit_name')
 * @method null|string getDirectEntryDebitDescription() Alias of $this->get('direct_entry_debit_description')
 * @method null|string getDirectEntryDebitId() Alias of $this->get('direct_entry_debit_id')
 * @method null|string getDirectEntryDebitName() Alias of $this->get('direct_entry_debit_name')
 * @method null|string getMerchantId() Alias of $this->get('merchant_id')
 * @method null|string getMerchantPassword() Alias of $this->get('merchant_password')
 * @method null|string getName() Alias of $this->get('name')
 * @method null|string getTrust() Alias of $this->get('trust')
 * @method bool hasAccount() Alias of $this->has('account')
 * @method bool hasBsb() Alias of $this->has('bsb')
 * @method bool hasDirectEntryCreditDescription() Alias of $this->has('direct_entry_credit_description')
 * @method bool hasDirectEntryCreditId() Alias of $this->has('direct_entry_credit_id')
 * @method bool hasDirectEntryCreditName() Alias of $this->has('direct_entry_credit_name')
 * @method bool hasDirectEntryDebitDescription() Alias of $this->has('direct_entry_debit_description')
 * @method bool hasDirectEntryDebitId() Alias of $this->has('direct_entry_debit_id')
 * @method bool hasDirectEntryDebitName() Alias of $this->has('direct_entry_debit_name')
 * @method bool hasMerchantId() Alias of $this->has('merchant_id')
 * @method bool hasMerchantPassword() Alias of $this->has('merchant_password')
 * @method bool hasName() Alias of $this->has('name')
 * @method bool hasTrust() Alias of $this->has('trust')
 */
class ClearingAccount extends Entity
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'account',
        'bsb',
        'direct_entry_credit_description',
        'direct_entry_credit_id',
        'direct_entry_credit_name',
        'direct_entry_debit_description',
        'direct_entry_debit_id',
        'direct_entry_debit_name',
        'merchant_id',
        'merchant_password',
        'name',
        'trust',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'clearingAccounts/:id',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'get' => [
            'id' => 'required|integer|greaterThan:0',
        ],
    ];
}
