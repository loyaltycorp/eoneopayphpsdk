<?php declare(strict_types = 1);

namespace EoneoPaySdk\Entity;

use EoneoPaySdk\Sdk\Entity;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|string getAllocationDate() Alias of $this->get('allocation_date')
 * @method null|string getAllocationId() Alias of $this->get('allocation_id')
 * @method null|string getAllocationType() Alias of $this->get('allocation_type')
 * @method null|int getAmount() Alias of $this->get('amount')
 * @method null|string getCustomerId() Alias of $this->get('customer_id')
 * @method null|int getFeeAmount() Alias of $this->get('fee_amount')
 * @method null|string getMerchantId() Alias of $this->get('merchant_id')
 * @method null|string getTxnId() Alias of $this->get('txn_id')
 * @method null|int getVersion() Alias of $this->get('version')
 * @method bool hasAllocationDate() Alias of $this->has('allocation_date')
 * @method bool hasAllocationId() Alias of $this->has('allocation_id')
 * @method bool hasAllocationType() Alias of $this->has('allocation_type')
 * @method bool hasAmount() Alias of $this->has('amount')
 * @method bool hasCustomerId() Alias of $this->has('customer_id')
 * @method bool hasFeeAmount() Alias of $this->has('fee_amount')
 * @method bool hasMerchantId() Alias of $this->has('merchant_id')
 * @method bool hasTxnId() Alias of $this->has('txn_id')
 * @method bool hasVersion() Alias of $this->has('version')
 * @method bool hasDeallocated() Alias of $this->has('deallocated')
 * @method null|bool isDeallocated() Alias of $this->is('deallocated')
 */
class PaymentAllocation extends Entity
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'allocation_date',
        'allocation_id',
        'allocation_type',
        'amount',
        'customer_id',
        'deallocated',
        'fee_amount',
        'merchant_id',
        'txn_id',
        'version',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'paymentAllocations/:id',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'get' => [
            'id' => 'required|regex:/^pal_[\da-zA-Z]+$/',
        ],
    ];
}
