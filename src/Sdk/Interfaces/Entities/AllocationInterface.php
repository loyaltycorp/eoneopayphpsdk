<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk\Interfaces\Entities;

interface AllocationInterface
{
    /**
     * Get the id for the allocation
     *
     * @return mixed
     */
    public function getId();

    /**
     * Get the allocation type
     *
     * @return mixed
     */
    public function getType();
}
