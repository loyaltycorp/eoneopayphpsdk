<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk\Interfaces\Entities;

interface ReferenceNumberInterface
{
    /**
     * Get the endpoint for resource actions
     *
     * @param string $method The method being requested
     *
     * @return string The endpoint for the request
     */
    public function getEndpointFromMethod(string $method) : string;

    /**
     * Validate request against a ruleset
     *
     * @param string $method The method to validate agains
     *
     * @return bool The validation status
     */
    public function validate(string $method = null) : bool;
}
