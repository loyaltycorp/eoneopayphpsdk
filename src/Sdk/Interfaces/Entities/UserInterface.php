<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk\Interfaces\Entities;

interface UserInterface
{
    /**
     * Get the id for the user
     *
     * @return mixed
     */
    public function getId();
}
