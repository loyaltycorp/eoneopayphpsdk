<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk\Interfaces\Entities\Constants;

interface WebhookConstants
{
    /**
     * Constants for setting the event name
     */
    const EVENT_PAYMENT_CREATED = 'PaymentCreated';
    const EVENT_PAYMENT_FAILED = 'PaymentFailed';
    const EVENT_PAYMENT_SUCCEEDED = 'PaymentSucceeded';
    const EVENT_PAYMENT_METHOD_CREATED = 'PaymentMethodCreated';
    const EVENT_PAYMENT_METHOD_DELETED = 'PaymentMethodDeleted';
    const EVENT_REFUND_CREATED = 'RefundCreated';
    const EVENT_REFUND_FAILED = 'RefundFailed';
    const EVENT_REFUND_SUCCEEDED = 'RefundSucceeded';
    const EVENT_REVERSAL_CREATED = 'ReversalCreated';
}
