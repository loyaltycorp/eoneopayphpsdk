<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk\Interfaces\Entities\Constants;

interface PaymentConstants
{
    /**
     * Constants for status
     */
    const STATUS_CREATED = 0;
    const STATUS_FAILED = -1;
    const STATUS_PENDING = 10;
    const STATUS_PROCESSED = 1;

    /**
     * Constants for merchant_transfer_status
     */
    const TRANSFER_STATUS_AVAILABLE = 1;
    const TRANSFER_STATUS_PENDING = 0;
    const TRANSFER_STATUS_TRANSFERRED = 2;
}
