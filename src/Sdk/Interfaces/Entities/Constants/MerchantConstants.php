<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk\Interfaces\Entities\Constants;

interface MerchantConstants
{
    /**
     * Constants for setting the transfer day for weekly transfers
     */
    const DAY_MONDAY = 1;
    const DAY_TUESDAY = 2;
    const DAY_WEDNESDAY = 3;
    const DAY_THURSDAY = 4;
    const DAY_FRIDAY = 5;
    const DAY_SATURDAY = 6;
    const DAY_SUNDAY = 7;

    /**
     * Constants for setting the transfer frequency
     */
    const FREQUENCY_DAILY = 'daily';
    const FREQUENCY_WEEKLY = 'weekly';
    const FREQUENCY_MONTHLY = 'monthly';
}
