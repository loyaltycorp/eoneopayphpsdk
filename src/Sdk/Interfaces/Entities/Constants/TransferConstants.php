<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk\Interfaces\Entities\Constants;

interface TransferConstants
{
    /**
     * Constants for status
     */
    const STATUS_CREATED = 0;
    const STATUS_DISBURSED = 5;
    const STATUS_FAILED = 3;
    const STATUS_PENDING = 2;
    const STATUS_PROCESSING = 4;
    const STATUS_RETURNED = 6;
    const STATUS_QUEUED = 1;
    const STATUS_TRUST_DISBURSED = 7;
}
