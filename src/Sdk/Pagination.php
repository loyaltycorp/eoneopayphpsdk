<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk;

use LoyaltyCorp\SdkBlueprint\Sdk\Resources\Pagination as BasePagination;

/**
 * @method int getLimit()
 * @method string getOrder()
 * @property array mutable Mutable attributes from \LoyaltyCorp\SdkBlueprint\Sdk\Repository
 */
class Pagination extends BasePagination
{
    /**
     * Create a new collection
     *
     * @param array $data This will be ignored as data can't be set to a collection
     */
    public function __construct(array $data = null)
    {
        // Force attributes needed for resource
        $this->addAttributes(['limit', 'offset', 'order']);

        // Force non-mutable
        $this->mutable = [];

        // Create repository with default API values ignoring passed data
        parent::__construct(['limit' => 100, 'offset' => 0, 'order' => 'asc']);
    }

    /**
     * Return the current page number selected
     *
     * @return int
     */
    public function getPage() : int
    {
        return (int)$this->get('offset') + 1;
    }

    /**
     * Set the limit for results returned
     *
     * @param int $limit The limit to return, defaults to 100
     *
     * @return \EoneoPaySdk\Sdk\Pagination This instance
     */
    public function setLimit(int $limit) : self
    {
        $limit = $limit < 1 ? 100 : $limit;
        return $this->set('limit', $limit);
    }

    /**
     * Set order for results reutrned
     *
     * @param string $order The order to set, should be asc or desc
     *
     * @return \EoneoPaySdk\Sdk\Pagination This instance
     */
    public function setOrder(string $order) : self
    {
        $order = mb_strtolower($order) === 'desc' ? 'desc' : 'asc';
        return $this->set('order', $order);
    }

    /**
     * Set the page for the results
     *
     * @param int $page The page to use, defaults to 1
     *
     * @return \EoneoPaySdk\Sdk\Pagination This instance
     */
    public function setPage(int $page) : self
    {
        // Set offset to 1 less than page so the API returns the right result
        $page = $page < 0 ? 0 : $page - 1;
        return $this->set('offset', $page);
    }
}
