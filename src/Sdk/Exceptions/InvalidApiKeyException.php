<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk\Exceptions;

use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\SdkBlueprintException;

class InvalidApiKeyException extends SdkBlueprintException
{
    //
}
