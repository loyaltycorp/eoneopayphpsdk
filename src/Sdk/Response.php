<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk;

use EoneoPaySdk\Entity\ApiKey;
use EoneoPaySdk\Entity\BankTransaction;
use EoneoPaySdk\Entity\Charge;
use EoneoPaySdk\Entity\ClearingAccount;
use EoneoPaySdk\Entity\Configuration;
use EoneoPaySdk\Entity\Customer;
use EoneoPaySdk\Entity\Customer\ReferenceNumber;
use EoneoPaySdk\Entity\Customer\Subscription;
use EoneoPaySdk\Entity\Customer\Subscription\Statement as SubscriptionStatement;
use EoneoPaySdk\Entity\Ewallet;
use EoneoPaySdk\Entity\Ewallet\Statement as EwalletStatement;
use EoneoPaySdk\Entity\Fee;
use EoneoPaySdk\Entity\Merchant;
use EoneoPaySdk\Entity\Merchant\Balance;
use EoneoPaySdk\Entity\Merchant\Balance\History;
use EoneoPaySdk\Entity\Merchant\Transfer;
use EoneoPaySdk\Entity\Payment;
use EoneoPaySdk\Entity\PaymentAllocation;
use EoneoPaySdk\Entity\PaymentSource;
use EoneoPaySdk\Entity\PaymentSource\BankAccount;
use EoneoPaySdk\Entity\PaymentSource\CreditCard;
use EoneoPaySdk\Entity\Plan;
use EoneoPaySdk\Entity\PreAuth;
use EoneoPaySdk\Entity\ReconciliationReport;
use EoneoPaySdk\Entity\SystemStatus;
use EoneoPaySdk\Entity\TokenisedCard;
use EoneoPaySdk\Entity\Webhook;
use LoyaltyCorp\SdkBlueprint\Sdk\Response as BaseResponse;
use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Repository;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * This class uses magic methods to access attributes
 *
 * @method null|ApiKey getApiKey() Alias of $this->get('api_key')
 * @method null|Balance getBalance() Alias of $this->get('balance')
 * @method null|History getBalanceHistory() Alias of $this->get('balance_history')
 * @method null|BankAccount getBankAccount() Alias of $this->get('bank_account')
 * @method null|BankTransaction getBankTransaction() Alias of $this->get('bank_transaction')
 * @method null|Collection getBankTransactions() Alias of $this->get('bank_transactions')
 * @method null|Charge getCharge() Alias of $this->get('charge')
 * @method null|Collection getCharges() Alias of $this->get('charges')
 * @method null|ClearingAccount getClearingAccount() Alias of $this->get('clearing_account')
 * @method null|Collection getClearingAccounts() Alias of $this->get('clearing_accounts')
 * @method null|string getCode() Alias of $this->get('code')
 * @method null|Configuration getConfiguration() Alias of $this->get('configuration')
 * @method null|Collection getConfigurations() Alias of $this->get('configurations')
 * @method null|CreditCard getCreditCard() Alias of $this->get('credit_card')
 * @method null|Customer getCustomer() Alias of $this->get('customer')
 * @method null|Collection getCustomers() Alias of $this->get('customers')
 * @method null|Ewallet getEwallet() Alias of $this->get('ewallet')
 * @method null|Collection getEwallets() Alias of $this->get('ewallets')
 * @method null|EwalletStatement getEwalletStatement() Alias of $this->get('ewallet_statement')
 * @method null|Fee getFee() Alias of $this->get('fee')
 * @method null|string getMessage() Alias of $this->get('message')
 * @method null|Merchant getMerchant() Alias of $this->get('merchant')
 * @method null|Collection getMerchants() Alias of $this->get('merchants')
 * @method null|Payment getPayment() Alias of $this->get('payment')
 * @method null|PaymentAllocation getPaymentAllocation() Alias of $this->get('payment_allocation')
 * @method null|Collection getPaymentAllocations() Alias of $this->get('payment_allocations')
 * @method null|Collection getPayments() Alias of $this->get('payments')
 * @method null|PaymentSource getPaymentSource() Alias of $this->get('payment_source')
 * @method null|Plan getPlan() Alias of $this->get('plan')
 * @method null|Collection getPlans() Alias of $this->get('plans')
 * @method null|PreAuth getPreauth() Alias of $this->get('preauth')
 * @method null|mixed getRaw() Alias of $this->get('raw')
 * @method null|ReconciliationReport getReconciliationReport() Alias of $this->get('reconciliation_report')
 * @method null|Collection getReconciliationReports() Alias of $this->get('reconciliation_reports')
 * @method null|Payment getReferenceNumbers() Alias of $this->get('reference_numbers')
 * @method null|RequestInterface getRequest() Alias of $this->get('request')
 * @method null|ResponseInterface getResponse() Alias of $this->get('response')
 * @method null|Repository getResults() Alias of $this->get('results')
 * @method null|int getStatusCode() Alias of $this->get('status_code')
 * @method null|Subscription getSubscription() Alias of $this->get('subscription')
 * @method null|SubscriptionStatement getSubscriptionStatement() Alias of $this->get('subscription_statement')
 * @method null|Collection getSubscriptions() Alias of $this->get('subscriptions')
 * @method null|SystemStatus getSystemStatus() Alias of $this->get('system_status')
 * @method null|Transfer getTransfer() Alias of $this->get('transfer')
 * @method null|Collection getTransfers() Alias of $this->get('transfers')
 * @method null|TokenisedCard getTokenisedCard() Alias of $this->get('tokenised_card')
 * @method null|Webhook getWebhook() Alias of $this->get('webhook')
 * @method null|Collection getWebhooks() Alias of $this->get('webhooks')
 * @method bool hasApiKey() Alias of $this->has('api_key')
 * @method bool hasBalance() Alias of $this->has('balance')
 * @method bool hasBalanceHistory() Alias of $this->has('balance_history')
 * @method bool hasBankAccount() Alias of $this->has('bank_account')
 * @method bool hasBankTransaction() Alias of $this->has('bank_transaction')
 * @method bool hasBankTransactions() Alias of $this->has('bank_transactions')
 * @method bool hasCharge() Alias of $this->has('charge')
 * @method bool hasCharges() Alias of $this->has('charges')
 * @method bool hasClearingAccount() Alias of $this->has('clearing_account')
 * @method bool hasClearingAccounts() Alias of $this->has('clearing_accounts')
 * @method bool hasCode() Alias of $this->has('code')
 * @method bool hasConfiguration() Alias of $this->has('configuration')
 * @method bool hasConfigurations() Alias of $this->has('configurations')
 * @method bool hasCreditCard() Alias of $this->has('credit_card')
 * @method bool hasCustomer() Alias of $this->has('customer')
 * @method bool hasCustomers() Alias of $this->has('customers')
 * @method bool hasEwallet() Alias of $this->has('ewallet')
 * @method bool hasEwallets() Alias of $this->has('ewallets')
 * @method bool hasEwalletStatement() Alias of $this->has('ewallet_statement')
 * @method bool hasFee() Alias of $this->has('fee')
 * @method bool hasMerchant() Alias of $this->has('merchant')
 * @method bool hasMerchants() Alias of $this->has('merchants')
 * @method bool hasPayment() Alias of $this->has('payment')
 * @method bool hasPaymentAllocation() Alias of $this->has('payment_allocation')
 * @method bool hasPaymentAllocations() Alias of $this->has('payment_allocations')
 * @method bool hasPayments() Alias of $this->has('payments')
 * @method bool hasPaymentSource() Alias of $this->has('payment_source')
 * @method bool hasPlan() Alias of $this->has('plan')
 * @method bool hasPlans() Alias of $this->has('plans')
 * @method bool hasPreauth() Alias of $this->has('preauth')
 * @method bool hasRaw() Alias of $this->has('raw')
 * @method bool hasReconciliationReport() Alias of $this->has('reconciliation_report')
 * @method bool hasReconciliationReports() Alias of $this->has('reconciliation_reports')
 * @method bool hasReferenceNumbers() Alias of $this->has('reference_numbers')
 * @method bool hasRequest() Alias of $this->has('request')
 * @method bool hasResponse() Alias of $this->has('request')
 * @method bool hasResults() Alias of $this->has('results')
 * @method bool hasStatusCode() Alias of $this->has('status_code')
 * @method bool hasSubscription() Alias of $this->has('subscription')
 * @method bool hasSubscriptionStatement() Alias of $this->has('subscription_statement')
 * @method bool hasSubscriptions() Alias of $this->has('subscriptions')
 * @method bool hasSystemStatus() Alias of $this->has('system_status')
 * @method bool hasTransfer() Alias of $this->has('transfer')
 * @method bool hasTransfers() Alias of $this->has('transfers')
 * @method bool hasTokenisedCard() Alias of $this->has('tokenised_card')
 * @method bool hasWebhook() Alias of $this->has('webhook')
 * @method bool hasWebhooks() Alias of $this->has('webhooks')
 * @method bool isSuccessful() Alias of $this->get('successful')
 */
class Response extends BaseResponse
{
    /**
     * Repository classes which can be used as part of a collection
     *
     * @var array
     */
    protected $repositories = [
        'api_key' => ApiKey::class,
        'balance' => Balance::class,
        'balance_history' => History::class,
        'bank_account' => BankAccount::class,
        'bank_transaction' => BankTransaction::class,
        'bank_transactions' => BankTransaction::class,
        'charge' => Charge::class,
        'charges' => Charge::class,
        'clearing_account' => ClearingAccount::class,
        'clearing_accounts' => ClearingAccount::class,
        'configuration' => Configuration::class,
        'configurations' => Configuration::class,
        'credit_card' => CreditCard::class,
        'customer' => Customer::class,
        'customers' => Customer::class,
        'ewallet' => Ewallet::class,
        'ewallet_statement' => EwalletStatement::class,
        'ewallets' => Ewallet::class,
        'fee' => Fee::class,
        'merchant' => Merchant::class,
        'merchants' => Merchant::class,
        'payment' => Payment::class,
        'payments' => Payment::class,
        'payment_allocation' => PaymentAllocation::class,
        'payment_allocations' => PaymentAllocation::class,
        'payment_source' => PaymentSource::class,
        'preauth' => PreAuth::class,
        'plan' => Plan::class,
        'plans' => Plan::class,
        'raw' => Repository::class,
        'reconciliation_report' => ReconciliationReport::class,
        'reconciliation_reports' => ReconciliationReport::class,
        'reference_numbers' => ReferenceNumber::class,
        'results' => Repository::class,
        'subscription' => Subscription::class,
        'subscription_statement' => SubscriptionStatement::class,
        'subscriptions' => Subscription::class,
        'system_status' => SystemStatus::class,
        'transfer' => Transfer::class,
        'transfers' => Transfer::class,
        'tokenised_card' => TokenisedCard::class,
        'webhook' => Webhook::class,
        'webhooks' => Webhook::class,
    ];
}
