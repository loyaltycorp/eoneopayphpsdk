<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk;

use EoneoPaySdk\Sdk\Exceptions\InvalidApiKeyException;
use EoneoPaySdk\Sdk\Exceptions\InvalidMerchantIdException;
use LoyaltyCorp\SdkBlueprint\Sdk\Parsers\JsonRequestParser;

class RequestParser extends JsonRequestParser
{
    /**
     * The SDK API key to access EoneoPay
     *
     * @var string
     */
    private $apiKey;

    /**
     * The url to use for API requests
     *
     * @var string
     */
    protected $baseUrl = 'https://api.eoneopay.com';

    /**
     * Merchant masquerade identity
     *
     * @var string
     */
    private $merchantId = '';

    /**
     * Get request parameters for the HTTP client.
     *
     * @return array
     */
    public function getParameters() : array
    {
        $parameters = ['auth' => [$this->apiKey, $this->merchantId]];

        // Set body if not using delete/get
        if (!in_array(mb_strtolower($this->getMethod()), ['delete', 'get'], true)) {
            $parameters['json'] = $this->getEndpoint() === null ? [] : $this->getEndpoint()->toArray();
        }

        return $parameters;
    }

    /**
     * Set api key
     *
     * @param string $apiKey The api key to use with each request
     *
     * @return \EoneoPaySdk\Sdk\RequestParser This instance, chainable
     *
     * @throws \EoneoPaySdk\Sdk\Exceptions\InvalidApiKeyException If the api key is invalid
     */
    public function setApiKey(string $apiKey) : self
    {
        // If api key is invalid throw exception, should be [pk|sk]_[live|test]_################
        if (!preg_match('/^(pk|sk)_(live|test)_[\da-zA-Z]+$/', $apiKey)) {
            throw new InvalidApiKeyException("Unable to set api key to '$apiKey' as it's invalid");
        }

        $this->apiKey = $apiKey;

        // Make chainable
        return $this;
    }

    /**
     * Set merchant masquerade id
     *
     * @param string $merchantId The merchant id to masquerade as
     *
     * @return \EoneoPaySdk\Sdk\RequestParser This instance, chainable
     *
     * @throws \EoneoPaySdk\Sdk\Exceptions\InvalidMerchantIdException If the merchantId is invalid
     */
    public function setMerchantId(string $merchantId) : self
    {
        // If merchant id is invalid throw exception, should be mer_################
        if (!preg_match('/^mer_[\da-zA-Z]+$/', $merchantId)) {
            throw new InvalidMerchantIdException("Unable to set merchant id to '$merchantId' as it's invalid");
        }

        $this->merchantId = $merchantId;

        // Make chainable
        return $this;
    }
}
