<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk;

use LoyaltyCorp\SdkBlueprint\Sdk\Entity as BaseEntity;

/**
 * @method string|null getCreatedAt()
 * @method string|null getId()
 * @method string|null getUpdatedAt()
 */
class Entity extends BaseEntity
{
    /**
     * Create a new entity
     *
     * @param array $data The data to pre-load the repository with
     */
    public function __construct(array $data = null)
    {
        // Add standard fields to attributes array
        $this->addAttributes(['created_at', 'id', 'updated_at']);

        parent::__construct($data);
    }
}
