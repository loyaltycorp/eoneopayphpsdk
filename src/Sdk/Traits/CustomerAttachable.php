<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk\Traits;

use EoneoPaySdk\Entity\Customer;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;

trait CustomerAttachable
{
    /**
     * Attach this entity to a customer
     *
     * @param \EoneoPaySdk\Entity\Customer $customer The customer to attach this entity to
     *
     * @return \EoneoPaySdk\Sdk\Traits\CustomerAttachable This instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the merchant is invalid
     */
    public function forCustomer(Customer $customer) : self
    {
        // Validate id
        if (!preg_match('/^cus_[\da-zA-Z]+$/', (string)$customer->getId())) {
            throw new InvalidEntityException('Unable to attach entity: not a valid customer');
        }

        // Add customer id attribute if available
        if ($this->hasAttribute('customer_id')) {
            $this->set('customer_id', $customer->getId());
        }

        // Set resource id and resource
        $this->resourceId = $customer->getId();
        $this->getResourceEndpointFromEntityId($this->resourceId);

        // Make chainable
        return $this;
    }
}
