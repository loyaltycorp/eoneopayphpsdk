<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk\Traits\Resources;

trait Transactional
{
    /**
     * Set start date, this method formats the date correctly
     *
     * @param string $date The date to set
     *
     * @return \EoneoPaySdk\Sdk\Traits\Resources\Transactional This instance
     */
    public function fromDate(string $date) : self
    {
        return $this->set('startDate', $this->formatDate($date, 'Y-m-d'));
    }

    /**
     * Get start date
     *
     * @return null|string The start date
     */
    public function getFromDate() : ?string
    {
        return $this->get('startDate');
    }

    /**
     * Get end date
     *
     * @return null|string The end date
     */
    public function getToDate() : ?string
    {
        return $this->get('endDate');
    }

    /**
     * Determine if start date exists
     *
     * @return bool Whether start date is set or not
     */
    public function hasFromDate() : bool
    {
        return $this->has('startDate');
    }

    /**
     * Determine if end date exists
     *
     * @return bool Whether end date is set or not
     */
    public function hasToDate() : bool
    {
        return $this->has('endDate');
    }

    /**
     * Set end date, this method formats the date correctly
     *
     * @param string $date The date to set
     *
     * @return \EoneoPaySdk\Sdk\Traits\Resources\Transactional This instance
     */
    public function toDate(string $date) : self
    {
        return $this->set('endDate', $this->formatDate($date, 'Y-m-d'));
    }
}
