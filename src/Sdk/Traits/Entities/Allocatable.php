<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk\Traits\Entities;

use EoneoPaySdk\Entity\Charge;
use EoneoPaySdk\Entity\Customer\Subscription;
use EoneoPaySdk\Entity\Ewallet;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;

trait Allocatable
{
    /**
     * Get the allocation type
     *
     * @return string
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the entity passed isn't allocatable
     */
    public function getType() : string
    {
        switch (get_class($this)) {
            case Charge::class:
                return 'charge';

            case Ewallet::class:
                return 'ewallet';

            case Subscription::class:
                return 'subscription';
        }

        // If entity is invalid, throw exception
        throw new InvalidEntityException('Unable to get type: not allocatable');
    }
}
