<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk\Traits;

use EoneoPaySdk\Entity\Merchant;
use LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException;

trait MerchantAttachable
{
    /**
     * Attach this entity to a merchant
     *
     * @param \EoneoPaySdk\Entity\Merchant $merchant The merchant to attach this entity to
     *
     * @return \EoneoPaySdk\Sdk\Traits\MerchantAttachable This instance
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidEntityException If the merchant is invalid
     */
    public function forMerchant(Merchant $merchant) : self
    {
        // Validate id
        if (!preg_match('/^mer_[\da-zA-Z]+$/', (string)$merchant->getId())) {
            throw new InvalidEntityException('Unable to attach entity: not a valid merchant');
        }

        // Add customer id attribute if available
        if ($this->hasAttribute('merchant_id')) {
            $this->set('merchant_id', $merchant->getId());
        }

        // Set resource id and resource
        $this->resourceId = $merchant->getId();
        $this->getResourceEndpointFromEntityId($this->resourceId);

        // Make chainable
        return $this;
    }
}
