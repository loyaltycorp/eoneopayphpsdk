<?php declare(strict_types = 1);

namespace EoneoPaySdk\Sdk;

use LoyaltyCorp\SdkBlueprint\Sdk\Collection;
use LoyaltyCorp\SdkBlueprint\Sdk\Resource as BaseResource;

class Resource extends BaseResource
{
    /**
     * Create a new resource
     */
    public function __construct()
    {
        parent::__construct(null, new Pagination);

        $this->addAttributes(['endDate', 'includes', 'q', 's', 'startDate']);
    }

    /**
     * Get includes
     *
     * @return \LoyaltyCorp\SdkBlueprint\Sdk\Collection Includes collection
     */
    public function getIncludes() : Collection
    {
        return $this->enforceCollection('includes');
    }

    /**
     * Return the limit
     *
     * @return int
     */
    public function getLimit() : int
    {
        /** @var \EoneoPaySdk\Sdk\Pagination $paginator */
        $paginator = $this->getPaginator();
        return $paginator->getLimit();
    }

    /**
     * Return the order
     *
     * @return string
     */
    public function getOrder() : string
    {
        /** @var \EoneoPaySdk\Sdk\Pagination $paginator */
        $paginator = $this->getPaginator();
        return $paginator->getOrder();
    }

    /**
     * Return the current page number selected
     *
     * @return int
     */
    public function getPage() : int
    {
        /** @var \EoneoPaySdk\Sdk\Pagination $paginator */
        $paginator = $this->getPaginator();
        return $paginator->getPage();
    }

    /**
     * Set includes to return with the resource
     *
     * @param array $includes The resources to include
     *
     * @return \EoneoPaySdk\Sdk\Resource This instance
     */
    public function includes(array $includes = null) : self
    {
        // Remove existing includes and reset
        $this->getIncludes()->clear();

        // If includes is null return as this is used to clear the current includes
        if ($includes === null) {
            return $this;
        }

        foreach ($includes as $include) {
            $this->getIncludes()->add($include);
        }

        return $this;
    }

    /**
     * Set the limit for results returned
     *
     * @param int $limit The limit to return, defaults to 100
     *
     * @return \EoneoPaySdk\Sdk\Resource This instance
     */
    public function limit(int $limit) : self
    {
        /** @var \EoneoPaySdk\Sdk\Pagination $paginator */
        $paginator = $this->getPaginator();
        $paginator->setLimit($limit);
        return $this;
    }

    /**
     * Set result order
     *
     * @param string $order The order to set
     *
     * @return \EoneoPaySdk\Sdk\Resource This instance
     */
    public function order(string $order) : self
    {
        /** @var \EoneoPaySdk\Sdk\Pagination $paginator */
        $paginator = $this->getPaginator();
        $paginator->setOrder($order);
        return $this;
    }

    /**
     * Set the page for the results
     *
     * @param int $page The page to use, defaults to 1
     *
     * @return \EoneoPaySdk\Sdk\Resource This instance
     */
    public function page(int $page) : self
    {
        /** @var \EoneoPaySdk\Sdk\Pagination $paginator */
        $paginator = $this->getPaginator();
        $paginator->setPage($page);
        return $this;
    }

    /**
     * Convert resource to array
     *
     * @return array
     */
    public function toArray() : array
    {
        // Convert resource to array
        $array = parent::toArray();

        // Convert includes to comma separated string
        if (isset($array['includes']) && is_array($array['includes'])) {
            $array['includes'] = implode(',', $array['includes']);
        }

        return $array;
    }
}
