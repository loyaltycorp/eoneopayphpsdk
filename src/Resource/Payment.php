<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource;

use EoneoPaySdk\Sdk\Interfaces\Entities\Constants\PaymentConstants;
use EoneoPaySdk\Sdk\Resource;
use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Resources\Timestampable;

/**
 * This class uses magic methods to access attributes
 *
 * @method $this whereAmount(mixed $value) Alias of $this->where('amount', $value)
 * @method $this whereBankTxnId(mixed $value) Alias of $this->where('bank_txn_id', $value)
 * @method $this whereCardType(mixed $value) Alias of $this->where('card_type', $value)
 * @method $this whereCardTypeId(mixed $value) Alias of $this->where('card_type_id', $value)
 * @method $this whereClearingAccount(mixed $value) Alias of $this->where('clearing_account', $value)
 * @method $this whereCurrency(mixed $value) Alias of $this->where('currency', $value)
 * @method $this whereCustomerId(mixed $value) Alias of $this->where('customer_id', $value)
 * @method $this whereExternalCustomerId(mixed $value) Alias of $this->where('external_customer_id', $value)
 * @method $this whereExternalTxnId(mixed $value) Alias of $this->where('external_txn_id', $value)
 * @method $this whereFeeAmount(mixed $value) Alias of $this->where('fee_amount', $value)
 * @method $this whereMerchantTransferStatus(mixed $value) Alias of $this->where('merchant_transfer_status', $value)
 * @method $this whereMeta(mixed $value) Alias of $this->where('meta', $value)
 * @method $this whereOriginalTxnId(mixed $value) Alias of $this->where('original_txn_id', $value)
 * @method $this whereReference(mixed $value) Alias of $this->where('reference', $value)
 * @method $this whereReferenceNumber(mixed $value) Alias of $this->where('reference_number', $value)
 * @method $this whereStatementDescriptor(mixed $value) Alias of $this->where('statement_descriptor', $value)
 * @method $this whereStatus(mixed $value) Alias of $this->where('status', $value)
 * @method $this whereToken(mixed $value) Alias of $this->where('token', $value)
 * @method $this whereType(mixed $value) Alias of $this->where('type', $value)
 * @method $this whereTxnId(mixed $value) Alias of $this->where('txn_id', $value)
 */
class Payment extends Resource implements PaymentConstants
{
    use Timestampable;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'amount',
        'bank_txn_id',
        'card_type',
        'card_type_id',
        'clearing_account',
        'created_at',
        'currency',
        'customer_id',
        'external_customer_id',
        'external_txn_id',
        'fee_amount',
        'merchant_transfer_status',
        'meta',
        'original_txn_id',
        'reference',
        'reference_number',
        'statement_descriptor',
        'status',
        'token',
        'type',
        'txn_date',
        'txn_id',
        'updated_at',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'payments'
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'list' => 'payments'
    ];

    /**
     * Set transaction date date, this method formats the date correctly
     *
     * @param string $date The date to set
     *
     * @return \EoneoPaySdk\Resource\Payment This instance
     */
    public function whereTxnDate(string $date) : self
    {
        return $this->where('txn_date', $this->formatDate($date, 'Y-m-d H:i:s'));
    }
}
