<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource;

use EoneoPaySdk\Sdk\Resource;

/**
 * This class uses magic methods to access attributes
 *
 * @method $this whereAccount(mixed $value) Alias of $this->where('account', $value)
 * @method $this whereBsb(mixed $value) Alias of $this->where('bsb', $value)
 * @method $this whereDirectEntryCreditDescription(mixed $value) Alias of $this->where('d_e_c_d', $value)
 * @method $this whereDirectEntryCreditId(mixed $value) Alias of $this->where('d_e_c_i', $value)
 * @method $this whereDirectEntryCreditName(mixed $value) Alias of $this->where('d_e_c_n', $value)
 * @method $this whereDirectEntryDebitDescription(mixed $value) Alias of $this->where('d_e_d_d', $value)
 * @method $this whereDirectEntryDebitId(mixed $value) Alias of $this->where('d_e_d_i', $value)
 * @method $this whereDirectEntryDebitName(mixed $value) Alias of $this->where('d_e_d_n', $value)
 * @method $this whereMerchantId(mixed $value) Alias of $this->where('merchant_id', $value)
 * @method $this whereMerchantPassword(mixed $value) Alias of $this->where('merchant_password', $value)
 * @method $this whereName(mixed $value) Alias of $this->where('name', $value)
 * @method $this whereTrust(mixed $value) Alias of $this->where('trust', $value)
 */
class ClearingAccount extends Resource
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'account',
        'bsb',
        'direct_entry_credit_description',
        'direct_entry_credit_id',
        'direct_entry_credit_name',
        'direct_entry_debit_description',
        'direct_entry_debit_id',
        'direct_entry_debit_name',
        'merchant_id',
        'merchant_password',
        'name',
        'trust',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'clearingAccounts'
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'list' => 'clearing_accounts'
    ];
}
