<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource;

use EoneoPaySdk\Sdk\Interfaces\Entities\Constants\WebhookConstants;
use EoneoPaySdk\Sdk\Resource;
use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Resources\Timestampable;

/**
 * This class uses magic methods to access attributes
 *
 * @method $this whereActive(mixed $value) Alias of $this->where('active', $value)
 * @method $this whereEventName(mixed $value) Alias of $this->where('event_name', $value)
 * @method $this whereId(mixed $value) Alias of $this->where('id', $value)
 * @method $this whereName(mixed $value) Alias of $this->where('name', $value)
 * @method $this whereTargetUrl(mixed $value) Alias of $this->where('target_url', $value)
 */
class Webhook extends Resource implements WebhookConstants
{
    use Timestampable;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'active',
        'created_at',
        'event_name',
        'id',
        'name',
        'target_url',
        'updated_at',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'webhooks'
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'list' => 'webhooks'
    ];
}
