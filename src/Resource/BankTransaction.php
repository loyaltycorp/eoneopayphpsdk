<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource;

use EoneoPaySdk\Sdk\Resource;
use EoneoPaySdk\Sdk\Traits\Resources\Transactional;
use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Resources\Timestampable;

/**
 * This class uses magic methods to access attributes
 *
 * @method $this whereAmount(mixed $value) Alias of $this->where('amount', $value)
 * @method $this whereClearingAccount(mixed $value) Alias of $this->where('clearing_account', $value)
 * @method $this whereCurrency(mixed $value) Alias of $this->where('currency', $value)
 * @method $this whereDescription(mixed $value) Alias of $this->where('description', $value)
 * @method $this whereFundsType(mixed $value) Alias of $this->where('funds_type', $value)
 * @method $this whereId(mixed $value) Alias of $this->where('id', $value)
 * @method $this whereRecordType(mixed $value) Alias of $this->where('record_type', $value)
 * @method $this whereReferenceNumber(mixed $value) Alias of $this->where('reference_number', $value)
 * @method $this whereSide(mixed $value) Alias of $this->where('side', $value)
 * @method $this whereText(mixed $value) Alias of $this->where('text', $value)
 * @method $this whereTransactionCode(mixed $value) Alias of $this->where('transaction_code', $value)
 * @method $this whereVersion(mixed $value) Alias of $this->where('version', $value)
 */
class BankTransaction extends Resource
{
    use Timestampable, Transactional;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'amount',
        'clearing_account',
        'created_at',
        'currency',
        'description',
        'endDate',
        'funds_type',
        'id',
        'record_type',
        'reference_number',
        'side',
        'startDate',
        'text',
        'transaction_code',
        'txn_date',
        'updated_at',
        'version',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'bankTransactions'
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'list' => 'bank_transactions'
    ];

    /**
     * Set transaction date, this method formats the date correctly
     *
     * @param string $date The date to set
     *
     * @return \EoneoPaySdk\Resource\BankTransaction This instance
     */
    public function whereTxnDate(string $date) : self
    {
        return $this->where('txn_date', $this->formatDate($date, 'Y-m-d'));
    }
}
