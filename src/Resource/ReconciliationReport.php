<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource;

use EoneoPaySdk\Sdk\Resource;

/**
 * This class uses magic methods to access attributes
 *
 * @method $this whereBankTransactionId(mixed $value) Alias of $this->where('bank_transaction_id', $value)
 * @method $this whereFile(mixed $value) Alias of $this->where('file', $value)
 * @method $this whereMessage(mixed $value) Alias of $this->where('message', $value)
 * @method $this whereStatus(mixed $value) Alias of $this->where('status', $value)
 * @method $this whereVersion(mixed $value) Alias of $this->where('version', $value)
 */
class ReconciliationReport extends Resource
{
    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'bank_transaction_id',
        'file',
        'message',
        'status',
        'version',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'reconciliationReports',
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'list' => 'reconciliation_reports'
    ];
}
