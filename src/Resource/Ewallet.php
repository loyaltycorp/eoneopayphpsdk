<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource;

use EoneoPaySdk\Sdk\Resource;
use EoneoPaySdk\Sdk\Traits\CustomerAttachable;
use EoneoPaySdk\Sdk\Traits\MerchantAttachable;

class Ewallet extends Resource
{
    use CustomerAttachable, MerchantAttachable;

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => ':resource/:resource_id/ewallets'
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'list' => 'ewallets'
    ];
}
