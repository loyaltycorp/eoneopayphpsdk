<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource;

use EoneoPaySdk\Sdk\Resource;
use EoneoPaySdk\Sdk\Traits\Resources\Transactional;
use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Resources\Timestampable;

/**
 * This class uses magic methods to access attributes
 *
 * @method $this whereAllocationId(mixed $value) Alias of $this->where('allocation_id', $value)
 * @method $this whereAllocationType(mixed $value) Alias of $this->where('allocation_type', $value)
 * @method $this whereAmount(mixed $value) Alias of $this->where('amount', $value)
 * @method $this whereCustomerId(mixed $value) Alias of $this->where('customer_id', $value)
 * @method $this whereDeallocated(mixed $value) Alias of $this->where('deallocated', $value)
 * @method $this whereFeeAmount(mixed $value) Alias of $this->where('fee_amount', $value)
 * @method $this whereId(mixed $value) Alias of $this->where('id', $value)
 * @method $this whereMerchantId(mixed $value) Alias of $this->where('merchant_id', $value)
 * @method $this whereTxnId(mixed $value) Alias of $this->where('txn_id', $value)
 * @method $this whereVersion(mixed $value) Alias of $this->where('version', $value)
 */
class PaymentAllocation extends Resource
{
    use Timestampable, Transactional;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'allocation_date',
        'allocation_id',
        'allocation_type',
        'amount',
        'created_at',
        'customer_id',
        'deallocated',
        'endDate',
        'fee_amount',
        'id',
        'merchant_id',
        'startDate',
        'txn_id',
        'updated_at',
        'version',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'paymentAllocations'
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'list' => 'payment_allocations'
    ];

    /**
     * Set allocation date, this method formats the date correctly
     *
     * @param string $date The date to set
     *
     * @return \EoneoPaySdk\Resource\PaymentAllocation This instance
     */
    public function whereAllocationDate(string $date) : self
    {
        return $this->where('allocation_date', $this->formatDate($date, 'Y-m-d'));
    }
}
