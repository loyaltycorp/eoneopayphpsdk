<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource;

use EoneoPaySdk\Sdk\Resource;

class Configuration extends Resource
{
    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'configuration'
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'list' => 'configurations'
    ];
}
