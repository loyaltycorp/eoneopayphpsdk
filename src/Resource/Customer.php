<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource;

use EoneoPaySdk\Sdk\Resource;
use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Resources\Timestampable;

/**
 * This class uses magic methods to access attributes
 *
 * @method $this whereEmail(mixed $value) Alias of $this->where('email', $value)
 * @method $this whereEwalletId(mixed $value) Alias of $this->where('ewallet_id', $value)
 * @method $this whereExternalCustomerId(mixed $value) Alias of $this->where('external_customer_id', $value)
 * @method $this whereFirstName(mixed $value) Alias of $this->where('first_name', $value)
 * @method $this whereId(mixed $value) Alias of $this->where('id', $value)
 * @method $this whereLastName(mixed $value) Alias of $this->where('last_name', $value)
 */
class Customer extends Resource
{
    use Timestampable;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'created_at',
        'email',
        'ewallet_id',
        'external_customer_id',
        'first_name',
        'id',
        'last_name',
        'updated_at',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'customers'
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'list' => 'customers'
    ];
}
