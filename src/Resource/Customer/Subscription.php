<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource\Customer;

use EoneoPaySdk\Sdk\Resource;
use EoneoPaySdk\Sdk\Traits\CustomerAttachable;

class Subscription extends Resource
{
    use CustomerAttachable;

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => '', // This is set at run time via getEndpointFromMethod()
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'list' => 'subscriptions'
    ];

    /**
     * Get the endpoint for resource actions
     *
     * @param string $method The method being requested
     *
     * @return string The endpoint for the request
     */
    public function getEndpointFromMethod(string $method) : string
    {
        // We could be getting subscriptions for a single user or all subscriptions
        if (mb_strtolower($method) === 'get') {
            // Set endpoint based on whether an id has been provided or not
            $this->endpoints['get'] = $this->resourceId ?
                'customers/:resource_id/subscriptions' :
                'subscriptions';
        }

        // Pass through
        return parent::getEndpointFromMethod($method);
    }
}
