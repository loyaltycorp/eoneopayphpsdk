<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource\Customer\Subscription;

use EoneoPaySdk\Sdk\Resource;

class Overdue extends Resource
{
    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'overdueSubscriptions'
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'list' => 'subscriptions'
    ];
}
