<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource\Merchant\Balance;

use EoneoPaySdk\Sdk\Resource;

class History extends Resource
{
    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'balance/history'
    ];
}
