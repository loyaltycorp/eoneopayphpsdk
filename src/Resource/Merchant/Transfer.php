<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource\Merchant;

use EoneoPaySdk\Sdk\Interfaces\Entities\Constants\TransferConstants;
use EoneoPaySdk\Sdk\Resource;
use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Resources\Timestampable;

/**
 * This class uses magic methods to access attributes
 *
 * @method $this whereAmount(mixed $value) Alias of $this->where('amount', $value)
 * @method $this whereCurrency(mixed $value) Alias of $this->where('currency', $value)
 * @method $this whereDescription(mixed $value) Alias of $this->where('description', $value)
 * @method $this whereDestination(mixed $value) Alias of $this->where('destination', $value)
 * @method $this whereId(mixed $value) Alias of $this->where('id', $value)
 * @method $this whereMerchantId(mixed $value) Alias of $this->where('merchant_id', $value)
 * @method $this whereStatus(mixed $value) Alias of $this->where('status', $value)
 * @method $this whereStatusReason(mixed $value) Alias of $this->where('status_reason', $value)
 */
class Transfer extends Resource implements TransferConstants
{
    use Timestampable;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'amount',
        'currency',
        'description',
        'destination',
        'merchant_id',
        'status',
        'status_date',
        'status_reason',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'transfers'
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'list' => 'transfers'
    ];

    /**
     * Set status date, this method formats the date correctly
     *
     * @param string $date The date to set
     *
     * @return \EoneoPaySdk\Resource\Merchant\Transfer This instance
     */
    public function whereStatusDate(string $date) : self
    {
        return $this->where('status_date', $this->formatDate($date, 'Y-m-d H:i:s'));
    }
}
