<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource;

use EoneoPaySdk\Sdk\Resource;

class Search extends Resource
{
    /**
     * Constants for setting the scope
     */
    const SCOPE_CUSTOMER = 'customers';
    const SCOPE_MERCHANT = 'merchants';
    const SCOPE_PAYMENT = 'payments';
    const SCOPE_PLAN = 'plans';

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'search'
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'list' => 'results'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public $rules = [
        'q' => 'required',
    ];

    /**
     * Get query string
     *
     * @return null|string The query string
     */
    public function getQuery() : ?string
    {
        return $this->get('q');
    }

    /**
     * Get scope
     *
     * @return null|string The query scope
     */
    public function getScope() : ?string
    {
        return $this->get('s');
    }

    /**
     * Determine if query string exists
     *
     * @return bool Whether a query string is set or not
     */
    public function hasQuery() : bool
    {
        return $this->has('q');
    }

    /**
     * Get scope
     *
     * @return bool Whether a query scope is set or not
     */
    public function hasScope() : bool
    {
        return $this->has('s');
    }

    /**
     * Set query string
     *
     * @param string $query The query string to set
     *
     * @return \EoneoPaySdk\Resource\Search This instance
     */
    public function query(string $query) : self
    {
        return $this->set('q', $query);
    }

    /**
     * Set query scope
     *
     * @param string $scope The query scope to use
     *
     * @return \EoneoPaySdk\Resource\Search This instance
     */
    public function scope(string $scope) : self
    {
        return $this->set('s', $scope);
    }
}
