<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource;

use EoneoPaySdk\Sdk\Interfaces\Entities\Constants\MerchantConstants;
use EoneoPaySdk\Sdk\Resource;
use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Resources\Timestampable;

/**
 * This class uses magic methods to access attributes
 *
 * @method $this whereAbn(mixed $value) Alias of $this->where('abn', $value)
 * @method $this whereBusinessName(mixed $value) Alias of $this->where('business_name', $value)
 * @method $this whereBusinessPhone(mixed $value) Alias of $this->where('business_phone', $value)
 * @method $this whereBusinessWebsite(mixed $value) Alias of $this->where('business_website', $value)
 * @method $this whereClearingAccount(mixed $value) Alias of $this->where('clearing_account', $value)
 * @method $this whereEmail(mixed $value) Alias of $this->where('email', $value)
 * @method $this whereFirstName(mixed $value) Alias of $this->where('first_name', $value)
 * @method $this whereId(mixed $value) Alias of $this->where('id', $value)
 * @method $this whereLastName(mixed $value) Alias of $this->where('last_name', $value)
 * @method $this whereTitle(mixed $value) Alias of $this->where('title', $value)
 */
class Merchant extends Resource implements MerchantConstants
{
    use Timestampable;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'abn',
        'business_name',
        'business_phone',
        'business_website',
        'clearing_account',
        'created_at',
        'email',
        'first_name',
        'id',
        'last_name',
        'title',
        'updated_at',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'merchants'
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'list' => 'merchants'
    ];
}
