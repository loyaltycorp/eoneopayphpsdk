<?php declare(strict_types = 1);

namespace EoneoPaySdk\Resource;

use EoneoPaySdk\Sdk\Resource;
use LoyaltyCorp\SdkBlueprint\Sdk\Traits\Resources\Timestampable;

/**
 * This class uses magic methods to access attributes
 *
 * @method $this whereAmount(mixed $value) Alias of $this->where('amount', $value)
 * @method $this whereCurrency(mixed $value) Alias of $this->where('currency', $value)
 * @method $this whereId(mixed $value) Alias of $this->where('id', $value)
 * @method $this whereInterval(mixed $value) Alias of $this->where('interval', $value)
 * @method $this whereIntervalCount(mixed $value) Alias of $this->where('interval_count', $value)
 * @method $this whereName(mixed $value) Alias of $this->where('name', $value)
 */
class Plan extends Resource
{
    use Timestampable;

    /**
     * The attributes for this resource
     *
     * @var array
     */
    protected $attributes = [
        'amount',
        'created_at',
        'currency',
        'id',
        'interval',
        'interval_count',
        'name',
        'updated_at',
    ];

    /**
     * Define the endpoints for this resource based on method
     *
     * @var array
     */
    protected $endpoints = [
        'get' => 'plans'
    ];

    /**
     * Mapping for response fields
     *
     * @var array
     */
    public $mappings = [
        'list' => 'plans'
    ];
}
