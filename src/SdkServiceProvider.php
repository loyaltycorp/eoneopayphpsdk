<?php

namespace EoneoPaySdk;

use Illuminate\Support\ServiceProvider;
use \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidConfigurationException;

/**
 * @codeCoverageIgnore - File is only included within Laravel/Lumen
 */
class SdkServiceProvider extends ServiceProvider
{
    /**
     * Bind Client::class to the container
     *
     * @return void
     *
     * @throws \LoyaltyCorp\SdkBlueprint\Sdk\Exceptions\InvalidConfigurationException If the api key isn't set in .env
     */
    public function register() : void
    {
        // Bind tokenize to container so it can be used via DI and $app->make
        $this->app->bind(Client::class, function () {
            // Ensure API key exists
            if (!env('EONEOPAYSDK_API_KEY')) {
                throw new InvalidConfigurationException('EONEOPAYSDK_API_KEY missing or not loaded from .env file');
            }

            // Create client
            $client = new Client(env('EONEOPAYSDK_API_KEY'));

            // Set base url if required
            if (env('EONEOPAYSDK_BASE_URL')) {
                $client->setBaseUrl(env('EONEOPAYSDK_BASE_URL'));
            }

            // Set masquerade merchant id if required
            if (env('EONEOPAYSDK_MERCHANT_ID')) {
                $client->setMerchantId(env('EONEOPAYSDK_MERCHANT_ID'));
            }

            return $client;
        });
    }
}
